package de.qossire.yaams.desktop;

import javax.swing.JOptionPane;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.qossire.yaams.base.Yaams;

/**
 * Launches the desktop (LWJGL) application.
 */
public class DesktopLauncher {
	public static void main(String[] args) {

		try {
			new LwjglApplication(new Yaams(new DesktopPlattform()), getDefaultConfiguration());
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage() + ": " + e.getStackTrace()[0], e.getClass().toString(), JOptionPane.ERROR_MESSAGE);
		}
	}

	private static LwjglApplicationConfiguration getDefaultConfiguration() {
		LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
		configuration.title = "yaaMs";
		configuration.width = 800;
		configuration.height = 600;
		for (int size : new int[] { 512, 256, 128, 48, 32, 16 }) {
			configuration.addIcon("system/logo/logo" + size + ".png", FileType.Internal);
		}
		return configuration;
	}
}