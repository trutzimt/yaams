/**
 * 
 */
package de.qossire.yaams.desktop;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.VisCheckBox;

import de.qossire.yaams.base.IPlattform;
import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class DesktopPlattform implements IPlattform {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#showErrorDialog(java.lang.String)
	 */
	@Override
	public void showErrorDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	@Override
	public void addInputs(final MapScreen mapScreen, InputMultiplexer multiplexer) {
		// TODO Auto-generated method stub
		multiplexer.addProcessor(new InputAdapter() {

			@Override
			public boolean keyDown(int keycode) {

				if (keycode == Input.Keys.W || keycode == Input.Keys.UP) {
					mapScreen.getMap().moveMapView(0, 32);
					return true;
				}

				if (keycode == Input.Keys.S || keycode == Input.Keys.DOWN) {
					mapScreen.getMap().moveMapView(0, -32);
					return true;
				}

				if (keycode == Input.Keys.A || keycode == Input.Keys.LEFT) {
					mapScreen.getMap().moveMapView(-32, 0);
					return true;
				}

				if (keycode == Input.Keys.D || keycode == Input.Keys.RIGHT) {
					mapScreen.getMap().moveMapView(32, 0);
					return true;
				}

				return false;

			}

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {

				if (button == Input.Buttons.RIGHT && mapScreen.getInputs().hasActiveAction()) {
					YSounds.cancel();
					mapScreen.getInputs().setActiveAction(null);
					return true;
				}

				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				// can scroll?
				if (mapScreen.getMapgui().AnyWindowIsOpen()) {
					return false;
				}

				if (amount < 0) {
					mapScreen.getMap().zoomIn();
					return true;
				}
				if (amount > 0) {
					mapScreen.getMap().zoomOut();
					return true;
				}
				return false;
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#formatDate(long)
	 */
	@Override
	public String formatDate(long date) {
		return new SimpleDateFormat().format(new Date(date));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IPlattform#startUpCode()
	 */
	@Override
	public void startUpCode() {
		Gdx.graphics.setTitle("yaaMs V" + YConfig.VERSION);
		Gdx.graphics.setVSync(YSettings.isVSync());

		// change size?
		if (YSettings.isFullscreen()) {
			Gdx.graphics.setUndecorated(true);
			Gdx.graphics.setWindowedMode(Gdx.graphics.getDisplayMode().width, Gdx.graphics.getDisplayMode().height);
		} else {
			Gdx.graphics.setWindowedMode(YSettings.getPref().getInteger("screen.width", 800), YSettings.getPref().getInteger("screen.height", 600));
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IPlattform#addGraphicOptions(de.qossire.yaams.ui.
	 * YTable)
	 */
	@Override
	public void addGraphicOptions(YTable cont) {
		VisCheckBox box;
		// add fullscreen
		box = new VisCheckBox("Fullscreen", YSettings.isFullscreen());
		box.addCaptureListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				YSounds.click();
				YSettings.getPref().putBoolean("fullscreen", ((VisCheckBox) actor).isChecked());

			}
		});
		cont.addL(null, box);

		// add vsync
		box = new VisCheckBox("vSnyc", YSettings.isVSync());
		box.addCaptureListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				YSounds.click();
				YSettings.getPref().putBoolean("vsync", ((VisCheckBox) actor).isChecked());

			}
		});
		cont.addL(null, box);

	}

}
