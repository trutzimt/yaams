Hello! 
Nice to meet you, i'm Sven and develop yaaMs. This small game is under active development. If you have found any mistakes, bugs oder you have questions, just click the feedback button. I'm happy to hear from you. To understand yaaMs, select Scenario and than the tutorial.

Notice: Names, works of art and cities are fictitious. Any similarities with actual, living or deceased persons would be purely coincidental.

What's new in V0.1?
* Art in the Workshop cost money and has a capacity
* New Quest Display
* two new scenario
* one new tutorial and a win status (to see the second tutorial, you need to play the first again)
* new usability rang
* You can rename your art
* The streets are more expense and you can build a metro entrance 
* Support for pause and continue on android
* And lot's more ;)

Fixed
* Crash without music
* The customers rotate to the tasks and the animation works better in fast speed
* Loading, without playing a game before is working and the game save more data.
* Rank joy is working
* Wall build correct
* And lot’s more ;)

You will find the old change logs on yaams.de

Thanks for your interest :)
Greetings Sven