/**
 * 
 */
package de.qossire.yaams.android;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;

import de.qossire.yaams.base.IPlattform;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class AndroidPlattform implements IPlattform {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#showErrorDialog(java.lang.String)
	 */
	@Override
	public void showErrorDialog(String message) {
		Gdx.app.error("Error", message);
	}

	@Override
	public void addInputs(final MapScreen mapScreen, InputMultiplexer multiplexer) {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#formatDate(long)
	 */
	@Override
	public String formatDate(long date) {
		return new SimpleDateFormat().format(new Date(date));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IPlattform#startUpCode()
	 */
	@Override
	public void startUpCode() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IPlattform#addGraphicOptions(de.qossire.yaams.ui.
	 * YTable)
	 */
	@Override
	public void addGraphicOptions(YTable cont) {}

}
