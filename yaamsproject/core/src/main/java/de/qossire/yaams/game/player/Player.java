/**
 * 
 */
package de.qossire.yaams.game.player;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.game.art.ArtworkMgmt;
import de.qossire.yaams.game.museum.Museum;
import de.qossire.yaams.game.museum.Town;
import de.qossire.yaams.game.museum.funding.FundingsMgmt;
import de.qossire.yaams.game.museum.rank.RankMgmt;
import de.qossire.yaams.game.quest.QuestMgmt;
import de.qossire.yaams.game.rooms.RoomMgmt;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTextDialogMgmt.YCharacter;

/**
 * @author sven
 *
 */
public class Player {

	private int money;
	private String name;
	private Town town;
	private MapScreen mapScreen;
	private Museum museum;
	private ArtworkMgmt artwork;
	private QuestMgmt quests;
	private RoomMgmt rooms;
	private RankMgmt rang;
	private FundingsMgmt fundings;
	private Stats stats;

	/**
	 *  
	 */
	public Player(MapScreen mapScreen) {
		name = YSettings.getUserName();
		town = new Town();
		quests = new QuestMgmt();
		rooms = new RoomMgmt();
		artwork = new ArtworkMgmt(mapScreen);
		museum = new Museum(mapScreen, this);
		rang = new RankMgmt();
		stats = new Stats();
		fundings = new FundingsMgmt();
		this.mapScreen = mapScreen;

	}

	/**
	 * @return the money
	 */
	public double getMoney() {
		return (mapScreen.getSettings().isActive(ScenConf.MONEY)) ? money : 999_999;
	}

	/**
	 * @param money
	 *            the money to set
	 */
	public void addMoney(int money) {
		this.money += money;
		mapScreen.getMapgui().refreshMoney();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the town
	 */
	public Town getTown() {
		return town;
	}

	/**
	 * @param town
	 *            the town to set
	 */
	public void setTown(Town town) {
		this.town = town;
	}

	/**
	 * Show a notification for the player
	 * 
	 * @param string
	 */
	@Deprecated
	public void addNotification(String string) {

		// show the message
		// TODO right helper
		mapScreen.getMapgui().getDialogs().add(YCharacter.FINANCE, string);

	}

	/**
	 * @return the artwork
	 */
	public ArtworkMgmt getArtwork() {
		return artwork;
	}

	/**
	 * Load it
	 */
	public void init() {
		artwork.init();

	}

	/**
	 * @return the quests
	 */
	public QuestMgmt getQuests() {
		return quests;
	}

	/**
	 * Update in the threads the player logic
	 * 
	 * @category thread
	 */
	public void updateLogic() {
		quests.updateLogic();
		museum.updateLogic();
		rooms.updateLogic();
	}

	/**
	 * Reset for the next day
	 * 
	 * @category thread
	 */
	public void nextDay() {
		rooms.nextDay();
		artwork.nextDay();
		fundings.nextDay();
	}

	/**
	 * @return the rooms
	 */
	public RoomMgmt getRooms() {
		return rooms;
	}

	/**
	 * @return the museum
	 */
	public Museum getMuseum() {
		return museum;
	}

	/**
	 * @return the rang
	 */
	public RankMgmt getRang() {
		return rang;
	}

	/**
	 * @return the stats
	 */
	public Stats getStats() {
		return stats;
	}

	/**
	 * @return the fundings
	 */
	public FundingsMgmt getFundings() {
		return fundings;
	}

}
