package de.qossire.yaams.screens.game.thread;

import de.qossire.yaams.screens.game.MapScreen;

public interface IGameThread {

	/**
	 * Update in the threads the player logic
	 * 
	 * @category thread
	 */
	public void updateLogic(MapScreen mapScreen);

}
