/**
 * 
 */
package de.qossire.yaams.game.persons.needs;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.game.persons.PersonManagement.PersonAnimationNeeds;
import de.qossire.yaams.game.persons.PersonManagement.PersonDir;
import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.game.rooms.RoomToilet;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YNotification;

/**
 * @author sven
 *
 */
public class NeedToilet extends BaseNeed {

	/**
	 */
	public NeedToilet() {
		super(PersonNeeds.TOILET, 2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#updateLogic(de.qossire.yaams.
	 * game.persons.Customer, de.qossire.yaams.screens.map.MapScreen, int)
	 */
	@Override
	public boolean updateLogic(final Customer customer, final MapScreen map, int deltaTime) {

		customer.addNeed(id, 4);

		// need drink? but it is to late?
		if (customer.getNeed(id) >= MAX) {
			customer.addNeed(PersonNeeds.LEAVE, SET_MAX);
			return false;
		}

		// found a toilet?
		if ((customer.isSex() ? RoomToilet.WOMEN : RoomToilet.MAN) == map.getData().getBuildTile(customer.getGameX(), customer.getGameY())
				&& customer.getWaitMin() == 0) {
			map.getMap().getPersonStage().getMgmt().showNeed(customer, PersonAnimationNeeds.DOTDOT);
			customer.setWaitMin((int) (20 * (100 - map.getPlayer().getRooms().getRoomAt(customer.getGameX(), customer.getGameY()).getBonus()) / 100f),
					PersonDir.NORTH, new Runnable() {

						@Override
						public void run() {
							customer.addNeed(id, SET_MIN);
						}

					});
			return false;
		}

		// Gdx.app.debug(id.toString(),
		// "field: " + map.getMap().getProps(BuildProperties.TICKET,
		// customer.getGameX(), customer.getGameY()));

		// want to buy a ticket
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#tryToFix(de.qossire.yaams.game.
	 * persons.Customer, de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void tryToFix(Customer customer, MapScreen map) {
		LinkedList<YPoint> toilets = map.getData().getAllBuildTilePoints(customer.isSex() ? RoomToilet.WOMEN : RoomToilet.MAN);

		// has no toilets?
		if (toilets == null) {
			map.getMap().getPersonStage().getMgmt().showNeed(customer, PersonAnimationNeeds.DROPLET);
			map.getMapgui().addNotification(
					new YNotification("Leaving", customer.getPersonName() + " found no toilet. " + customer.getPersonNameProp() + " will leave.",
							new Image(YIcons.getBuildIcon(customer.isSex() ? RoomToilet.WOMEN : RoomToilet.MAN, true))));

			// will leave
			customer.addNeed(PersonNeeds.LEAVE, SET_MAX);
			return;
		}

		// find nearst toilet
		if (!map.getMap().getPersonStage().goToNearestTileFrom(customer, toilets)) {
			// so wait max 10x
			customer.addNeed(PersonNeeds.LEAVE, 200);
			Gdx.app.debug(id.toString(), "find no toilet, will wait");

		}

	}

}
