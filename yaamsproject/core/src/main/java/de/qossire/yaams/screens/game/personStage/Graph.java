package de.qossire.yaams.screens.game.personStage;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;

public class Graph implements IndexedGraph<YNode> {
	private Array<YNode> nodes = new Array<YNode>();

	public Graph(Array<YNode> nodes) {
		this.nodes = nodes;
	}

	// public Node getNodeByCoordinates(float x, float y) {
	// return nodes.get((int) x / GameInfo.ONE_TILE + GraphGenerator.mapWidth *
	// (int) y / GameInfo.ONE_TILE);
	// }

	@Override
	public int getIndex(YNode node) {
		return node.getIndex();
	}

	@Override
	public Array<Connection<YNode>> getConnections(YNode fromNode) {
		return fromNode.getConnections();
	}

	@Override
	public int getNodeCount() {
		return nodes.size;
	}
}
