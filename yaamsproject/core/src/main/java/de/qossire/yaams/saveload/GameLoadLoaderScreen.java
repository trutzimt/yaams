package de.qossire.yaams.saveload;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.level.ScenarioManagement;
import de.qossire.yaams.level.endless.EndlessGameScenario;
import de.qossire.yaams.screens.game.GameData;
import de.qossire.yaams.screens.game.GameLoaderScreen;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;

public class GameLoadLoaderScreen extends GameLoaderScreen {

	protected FileHandle saveGame;
	protected MapScreen map;

	/**
	 * Load everything for the game
	 *
	 * @param qossire
	 * @param scenario
	 */
	public GameLoadLoaderScreen(Yaams yaams, FileHandle saveGame) {
		super(yaams, null);

		this.saveGame = saveGame;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.menu.BaseLoaderScreen#nextScreen()
	 */
	@Override
	public void nextScreen() {
		yaams.switchScreen(map);
	}

	@Override
	public void secondCode() {

		Json json = new Json();
		YSaveInfo qi = json.fromJson(YSaveInfo.class, saveGame.read());

		String c = qi.getSettings().get("campaign");
		String s = qi.getSettings().get("scenario");

		// TODO find better way
		BaseScenario scen = null;
		if ("endlessgame".equals(s)) {
			scen = new EndlessGameScenario(qi.getSettings().get("map"), null);
		} else {
			scen = ScenarioManagement.getCampaign(c).getScenario(s);
		}
		map = new MapScreen(yaams, scen);
		map.load(json.fromJson(GameData.class, saveGame.sibling(saveGame.name() + "data").read()));

		// map.getScenario().preLoad(map);
		map.getEvents().perform(MapEventHandlerTyp.FILE_LOAD);

		// delete it?
		// TODO
		if ("temp".equals(saveGame.nameWithoutExtension())) {
			SaveLoadManagement.delete("temp");
		}

	}

}
