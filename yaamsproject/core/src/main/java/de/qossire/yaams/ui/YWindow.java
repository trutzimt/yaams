package de.qossire.yaams.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisWindow;

public class YWindow extends VisWindow {

	public YWindow(String title) {
		super(title);

		addCloseButton();
		closeOnEscape();
		setResizable(true);
		setCenterOnAdd(true);

	}

	/**
	 * Add add the left a icon
	 * 
	 * @param i
	 */
	public void addTitleIcon(Image i) {
		// remove old
		getTitleTable().clearChildren();

		// add icon
		getTitleTable().add(i);
		getTitleTable().add(getTitleLabel()).expandX().fillX().minWidth(0);
		addCloseButton();
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	protected Widget addL(String label, String content) {
		return addL(label, new VisLabel(content));
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	protected Widget addL(String label, Widget content) {
		add(label + ":").align(Align.right);
		add(content).align(Align.left).growX().row();

		return content;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	protected WidgetGroup addL(String label, WidgetGroup content) {
		add(label + ":").align(Align.right);
		add(content).align(Align.left).growX().row();

		return content;
	}
}
