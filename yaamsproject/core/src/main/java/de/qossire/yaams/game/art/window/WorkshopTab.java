package de.qossire.yaams.game.art.window;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.qossire.yaams.game.art.ArtTimeAction;
import de.qossire.yaams.game.art.ArtworkMgmt;
import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.thread.YClock;
import de.qossire.yaams.ui.TextHelper;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YNotification;
import de.qossire.yaams.ui.YProgressBar;
import de.qossire.yaams.ui.YTable;

public class WorkshopTab extends ArtworkTab {

	private VisTextButton workshop, repair, explore;

	public WorkshopTab() {
		super("Workshop", "At the moment you have no art in the workshop. Go in depot and send some.");

		repair = new VisTextButton("");
		repair.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				repairArt();
			}
		});

		buttonBar.addActor(repair);

		explore = new VisTextButton("");
		explore.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				exploreArt();
			}
		});

		buttonBar.addActor(explore);

		workshop = new VisTextButton("Send to depot");
		workshop.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				((BaseArt) active).setStatus(ArtStatus.DEPOT);
				for (int i = 0; i < elements.size(); i++) {
					if (elements.get(i).getUserObject() == active) {
						elements.remove(i);
						break;
					}
				}
				reset();
				YSounds.click();
				rebuild();

			}
		});
		buttonBar.addActor(workshop);

		reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#getDefaultPanel()
	 */
	@Override
	protected Actor getDefaultPanel() {
		ArtworkMgmt p = MapScreen.get().getPlayer().getArtwork();

		int id = p.getArtsCount(ArtStatus.WORKSHOP);

		YTable t = new YTable();
		t.addH("General");
		t.addL("Capacity", new YProgressBar(id, p.getWorkshopCapacity()));
		t.addL("Cost per day", "Total " + TextHelper.getMoneyString(id * 200));

		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		elements.clear();

		if (MapScreen.get().getPlayer().getArtwork().getArtsCount(ArtStatus.WORKSHOP) > 0) {
			addElement("Overview", YIcons.getIconD(YIType.INFOABOUT), null);

			// add all elements
			for (final BaseArt art : MapScreen.get().getPlayer().getArtwork().getArts(ArtStatus.WORKSHOP)) {
				addElement(art.getName(), art.getIcon(true), art);
			}
		}

		super.rebuild();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#reset()
	 */
	@Override
	protected void reset() {
		super.reset();
		resetLight();
	}

	/**
	 * 
	 */
	private void resetLight() {
		workshop.setDisabled(true);
		repair.setDisabled(true);
		repair.setText("Repair 5%");
		explore.setDisabled(true);
		explore.setText("Explore more");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#clickElement(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected void clickElement(Button btn) {

		if (btn.getUserObject() == null) {
			resetLight();
			return;
		}

		BaseArt a = ((BaseArt) btn.getUserObject());

		workshop.setDisabled(a.getTimeAction() != null);
		if (a.getTimeAction() != null) {
			repair.setDisabled(true);
			repair.setText("Please wait. It is working");
		} else if (a.getCondition() >= 100) {
			repair.setDisabled(true);
			repair.setText("Can not repair more");
		} else {
			int m = (int) ((a.getPrice() * 1f) / a.getCondition() * 10);
			repair.setDisabled(MapScreen.get().getPlayer().getMoney() < m);
			repair.setText("Repair 5% for " + TextHelper.getMoneyString(m));
		}

		if (a.getTimeAction() != null) {
			explore.setDisabled(true);
			explore.setText("Please wait.");
		} else if (a.getExplore() >= 100) {
			explore.setDisabled(true);
			explore.setText("Can not explore more");
		} else {
			explore.setDisabled(MapScreen.get().getPlayer().getMoney() < 200);
			explore.setText("Explore for " + TextHelper.getMoneyString(200));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.art.window.ArtworkTab#doubleClickElement(com.badlogic.
	 * gdx.scenes.scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		repairArt();
	}

	/**
	 * Repair it
	 */
	public void repairArt() {
		if (active == null) {
			YSounds.buzzer();
			return;
		}

		final BaseArt a = ((BaseArt) active);

		// remove money
		MapScreen.get().getPlayer().addMoney((int) -((a.getPrice() * 1f) / a.getCondition() * 10));

		// add moving option
		ArtTimeAction t = new ArtTimeAction("Repair it", YClock.addCalcTimestamp(MapScreen.get().getClock().getTimeStamp(), 0, 6, 0)) {

			@Override
			public boolean run() {
				// repair it
				a.setCondition(a.getCondition() + 5);
				if (a.getCondition() > 100) {
					a.setCondition(100);
				}
				a.reCalcDecorPrice();
				a.setTimeAction(null);
				MapScreen.get().getMapgui().addNotification(new YNotification("Workshop", a.getName() + " was repaired.", new Image(a.getIcon(true))));

				return true;
			}
		};

		a.setTimeAction(t);
		MapScreen.get().getClock().addTimeAction(t);

		YSounds.buy();
		rebuild();
		reset();
	}

	/**
	 * Repair it
	 */
	public void exploreArt() {
		final BaseArt a = ((BaseArt) active);

		// remove money
		MapScreen.get().getPlayer().addMoney(-200);

		// add moving option
		ArtTimeAction t = new ArtTimeAction("Explore it",
				YClock.addCalcTimestamp(MapScreen.get().getClock().getTimeStamp(), 0, (10 * a.getExplore()) / 100, 0)) {

			@Override
			public boolean run() {
				// repair it
				a.setExplore(a.getExplore() + NamesGenerator.getIntBetween(2, 6));
				if (a.getExplore() > 100) {
					a.setExplore(100);
				}
				a.reCalcDecorPrice();
				a.setTimeAction(null);
				MapScreen.get().getMapgui().addNotification(new YNotification("Workshop", a.getName() + " was more explored.", new Image(a.getIcon(true))));

				return true;
			}
		};

		a.setTimeAction(t);
		MapScreen.get().getClock().addTimeAction(t);

		YSounds.buy();
		rebuild();
		reset();
	}
}
