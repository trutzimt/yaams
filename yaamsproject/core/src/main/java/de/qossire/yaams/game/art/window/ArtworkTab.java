/**
 * 
 */
package de.qossire.yaams.game.art.window;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.ui.YSplitTab;

/**
 * @author sven
 *
 */
public abstract class ArtworkTab extends YSplitTab {

	/**
	 * @param title
	 * @param error
	 */
	public ArtworkTab(String title, String error) {
		super(title, error);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(Button btn) {
		return ((BaseArt) btn.getUserObject()).getInfoPanel();
	}

}
