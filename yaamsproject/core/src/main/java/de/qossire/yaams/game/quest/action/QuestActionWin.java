/**
 * 
 */
package de.qossire.yaams.game.quest.action;

import com.badlogic.gdx.Gdx;

import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.level.BaseCampaign;
import de.qossire.yaams.music.YMusic;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.GameLoaderScreen;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.menu.MainMenuScreen;
import de.qossire.yaams.ui.YMessageDialog;
import de.qossire.yaams.ui.YTextButton;

/**
 * @author sven
 *
 */
public class QuestActionWin extends BaseQuestAction {

	/**
	 * @param character
	 * @param message
	 */
	public QuestActionWin() {
		super("Win the game");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.action.BaseQuestAction#perform(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public void perform() {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				// save it
				MapScreen.get().getScenario().setWon();
				YMusic.winScenario();

				final Yaams y = MapScreen.get().getYaams();

				final YMessageDialog m = new YMessageDialog(y, "You have won!", "Do you want to continue playing?", null);

				m.addButton(new YTextButton("Continue Playing") {

					@Override
					public void perform() {
						YSounds.click();
						m.close();

					}
				});

				m.addButton(new YTextButton("Main Menu") {

					@Override
					public void perform() {
						YSounds.click();
						y.switchScreen(new MainMenuScreen(y));
						m.close();

					}
				});

				// add next game?
				final BaseCampaign c = MapScreen.get().getScenario().getCamp();
				if (c.isCampaign()) {
					// find pos
					final int p = c.getScenarios().indexOf(MapScreen.get().getScenario());
					if (p != -1 && p < c.getScenarios().size() - 1) {

						m.addButton(new YTextButton("Next Scenario") {

							@Override
							public void perform() {
								YSounds.click();
								y.switchScreen(new GameLoaderScreen(y, c.getScenarios().get(p + 1)));
								m.close();

							}
						});

					}
				}

				m.build(MapScreen.get().getStage());
				m.pack();
			}
		});

	}

}
