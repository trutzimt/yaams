/**
 *
 */
package de.qossire.yaams.screens.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.util.TableUtils;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisWindow;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.music.YMusic;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YRandomField;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class OptionsWindow extends VisWindow {

	/**
	 * @param title
	 */
	public OptionsWindow(Yaams yaams) {
		super("Options");

		addCloseButton();
		closeOnEscape();

		YTable cont = new YTable();
		TableUtils.setSpacingDefaults(cont);

		final YRandomField name;
		name = new YRandomField(YSettings.getUserName()) {

			@Override
			protected void saveText(String text) {
				YSettings.getPref().putString("playername", text);
			}

			@Override
			protected String getRndText() {
				return NamesGenerator.getName(NamesGenerator.r.nextBoolean());
			}
		};
		cont.addL("Player Name", name);

		cont.addH("Audio");

		final VisSlider sound = new VisSlider(0, 1, 0.1f, false);
		sound.setValue(YSettings.getSoundVolume());
		sound.addListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				if (YSettings.getSoundVolume() != sound.getValue()) {
					YSettings.getPref().putFloat("sound", sound.getValue());
					YSounds.click();
				}
			}
		});
		cont.addL("Sound volume", sound);

		// add music?
		if (Gdx.files.internal("music").exists()) {
			final VisSlider music = new VisSlider(0, 1, 0.1f, false);
			music.setValue(YSettings.getMusicVolume());
			music.addListener(new YChangeListener(false) {

				@Override
				public void changedY(Actor actor) {
					if (YSettings.getMusicVolume() != music.getValue()) {
						YSettings.getPref().putFloat("music", music.getValue());
						YMusic.updateVol();
						YMusic.clickForOptions();
					}
				}
			});
			cont.addL("Music volume", music);
			if (YSettings.isDebug()) {
				cont.addL("Playing", YStatic.music.getActSong());
			}
		}

		VisCheckBox box;

		cont.addH("Graphic");

		// add plattform code
		yaams.getPlattform().addGraphicOptions(cont);

		final boolean bS = Gdx.files.internal("64").exists();

		// add gui scale
		if (yaams.getScreen() instanceof MainMenuScreen) {
			box = new VisCheckBox("Bigger Gui" + (bS ? "" : " light"), YSettings.isBiggerGui());
			box.addCaptureListener(new ChangeListener() {

				@Override
				public void changed(ChangeEvent event, Actor actor) {
					YSounds.click();
					YSettings.getPref().putBoolean("biggerGui", ((VisCheckBox) actor).isChecked());
					YSettings.getPref().putInteger("guiScale", bS && ((VisCheckBox) actor).isChecked() ? 64 : 32);

				}
			});
			cont.addL(null, box);
		}

		// add autosave
		box = new VisCheckBox("Autosave", YSettings.isAutosave());
		box.addCaptureListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				YSounds.click();
				YSettings.getPref().putBoolean("autosave", ((VisCheckBox) actor).isChecked());

			}
		});
		cont.addL(null, box);

		// add debug
		if (YSettings.isDebug()) {
			box = new VisCheckBox("Debug Mode", YSettings.isDebug());
			box.addCaptureListener(new ChangeListener() {

				@Override
				public void changed(ChangeEvent event, Actor actor) {
					YSounds.click();
					YSettings.getPref().putBoolean("dev", ((VisCheckBox) actor).isChecked());

				}
			});
			cont.addL("Debug", box);
		}

		cont.addH("For some settings you have to restart it.");

		add(cont).grow();

		pack();
		setCenterOnAdd(true);
	}

	/**
	 * Close it
	 */
	@Override
	public void close() {
		super.close();
		YSettings.save();
	}
}
