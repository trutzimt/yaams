package de.qossire.yaams.screens.game.thread;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YNotification;

public class YClock {

	private int timeSpeed;
	private long timeClock;
	private LinkedList<YTimeAction> actions;

	public YClock() {
		timeSpeed = 0;
		actions = new LinkedList<>();

		// set start time
		addHour(7);

		// save it
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_SAVE, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				MapScreen.get().getData().setP("timestamp", timeClock);
				return false;
			}
		});

		// load it
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_LOAD, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				timeClock = (long) MapScreen.get().getData().getP("timestamp");
				return false;
			}
		});

		// show next day
		MapScreen.get().getEvents().register(MapEventHandlerTyp.NEXTDAY, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				// add blackscreen
				Image black = new Image(YStatic.gameAssets.get("system/person/black.png", Texture.class));
				black.setFillParent(true);
				black.getColor().a = 0;
				black.addAction(Actions.sequence(Actions.fadeIn(1), Actions.fadeOut(1), Actions.run(new Runnable() {

					@Override
					public void run() {
						YSounds.nextDay();
						MapScreen.get().getMapgui()
								.addNotification(new YNotification("New Day", "Day " + getDay() + " will be beginn.", YIcons.getIconI(YIType.NEXTDAY)));

					}
				}), Actions.removeActor()));
				MapScreen.get().getMapgui().getStage().addActor(black);
				return false;
			}
		});
	}

	/**
	 * @return the timeSpeed
	 */
	public int getTimeSpeed() {
		return timeSpeed;
	}

	/**
	 * @return the timeSpeed
	 */
	public int getTimeSpeedSafe() {
		return timeSpeed == 0 ? 1 : timeSpeed;
	}

	/**
	 * Return the time to tick in milli sec, for the thread
	 * 
	 * @return
	 */
	public int getTickTimeMillis() {
		if (timeSpeed == 0)
			return 0;
		return 1000 / timeSpeed;
	}

	/**
	 * Add the time
	 */
	public void tick() {
		timeClock += getTickMin();

		// check timeactions
		if (actions.size() > 0)
			for (YTimeAction a : actions.toArray(new YTimeAction[] {})) {
				try {
					if (a.getRunNext() <= timeClock && a.run()) {
						actions.remove(a);
					}
				} catch (Exception e) {
					YConfig.error(e, false);
					actions.remove(a);
				}
			}
	}

	/**
	 * @param timeSpeed
	 *            the timeSpeed to set
	 */
	public void setTimeSpeed(int timeSpeed) {
		// stopped?
		if (this.timeSpeed > 0 && timeSpeed == 0) {
			MapScreen.get().getEvents().perform(MapEventHandlerTyp.TIME_BREAK);
		}

		// strted?
		if (this.timeSpeed == 0 && timeSpeed > 0) {
			MapScreen.get().getEvents().perform(MapEventHandlerTyp.TIME_PLAY);
		}

		this.timeSpeed = timeSpeed;

	}

	/**
	 * Get the time label
	 * 
	 * @return
	 */
	public String getTimeLabel() {
		return String.format("Day %d, %02d:%02d", getDay(), getHour(), getMin());
	}

	/**
	 * Get the time label
	 * 
	 * @return
	 */
	public String getShortTimeLabel() {
		return String.format("%02d:%02d", getHour(), getMin());
	}

	/**
	 * @return the timeClock
	 */
	public int getTickMin() {
		return 2;
	}

	/**
	 * Return the day hour
	 * 
	 * @return
	 */
	public int getDay() {
		return (int) (timeClock / 1440) + 1;
	}

	/**
	 * Return the day hour
	 * 
	 * @return
	 */
	public int getHour() {
		return (int) (timeClock / 60 % 24);
	}

	/**
	 * Return the day hour
	 * 
	 * @return
	 */
	public int getMin() {
		return (int) (timeClock % 60);
	}

	/**
	 * Add the hours to the clock.
	 * 
	 * @return
	 */
	public void addHour(int hour) {
		this.timeClock += 60 * hour;
	}

	/**
	 * @return the timeClock
	 */
	public long getTimeStamp() {
		return timeClock;
	}

	/**
	 * Add a new time action
	 * 
	 * @param e
	 */
	public void addTimeAction(YTimeAction e) {
		actions.add(e);
	}

	/**
	 * Calculate the timestamp
	 * 
	 * @param timestamp
	 * @param d
	 * @param h
	 * @param m
	 * @return
	 */
	public static long addCalcTimestamp(long timestamp, int d, int h, int m) {
		return timestamp + d * 1440 + h * 60 + m;
	}

}
