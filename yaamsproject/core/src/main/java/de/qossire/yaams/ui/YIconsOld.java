/**
 *
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YStatic;

/**
 * @author sven
 *
 */
@Deprecated
public class YIconsOld {

	// TODO new
	@Deprecated
	public enum QIType {
		QUEST, RESS, SPRING, SUMMER, AUTUMN, WINTER, TIME, LEX, MENU, RESEARCH, KINGDOM, MONEY
	}

	/**
	 *
	 */
	@Deprecated
	public static TextureRegion get(QIType type) {
		switch (type) {
		case QUEST:
			return YIconsOld.getRessIcon(10, 14);
		case RESEARCH:
			return YIconsOld.getRessIcon(5, 13);
		case RESS:
			return YIconsOld.getRessIcon(14, 16);
		case SPRING:
			return YIconsOld.getRessIcon(12, 6);
		case SUMMER:
			return YIconsOld.getRessIcon(14, 6);
		case AUTUMN:
			return YIconsOld.getRessIcon(15, 6);
		case WINTER:
			return YIconsOld.getRessIcon(9, 6);
		case TIME:
			return YIconsOld.getRessIcon(8, 17);
		case LEX:
			return YIconsOld.getRessIcon(5, 14);
		case MENU:
			return YIconsOld.getRessIcon(11, 11);
		case KINGDOM:
			return YIconsOld.getRessIcon(4, 10);
		case MONEY:
			return YIconsOld.getRessIcon(9, 22);
		default:
			YConfig.error(new IllegalArgumentException("Icon" + type + " not exist"), false);
			return YIconsOld.getRessIcon(0, 0);
		}
	}

	/**
	 *
	 */
	@Deprecated
	public static TextureRegionDrawable get(QIType type, boolean TextureRegionDrawable) {
		return new TextureRegionDrawable(get(type));
	}

	/**
	 *
	 * @param file
	 * @param x
	 *            multiplies with 24
	 * @param y
	 *            multiplies with 24
	 */
	@Deprecated
	public static TextureRegion getIcon(int x, int y, int basis, String file) {
		Texture texture = YStatic.gameAssets.get(file, Texture.class);
		return new TextureRegion(texture, x * basis, y * basis, basis, basis);
	}

	/**
	 * Get a Area special for the buildings
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	@Deprecated
	public static TextureRegion getUnitIcon(int id) {
		return new TextureRegion(YStatic.gameAssets.get("system/tileset/units.png", Texture.class), id % 32 * 32, id / 32 * 32, 32, 32);
	}

	/**
	 * Get a Area special for the buildings
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	public static Image getIcon(String title) {
		return new Image(getIconT(title));
	}

	/**
	 * Get a Area special for the buildings
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	public static Drawable getIconD(String title) {
		return new TextureRegionDrawable(getIconT(title));
	}

	/**
	 * Get a Area special for the buildings
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	public static TextureRegion getIconT(String title) {
		return new TextureRegion(YStatic.gameAssets.get("system/icons/" + title + ".png", Texture.class));
	}

	/**
	 * Get a Area special for the buildings
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	public static TextureRegionDrawable getOverlayIcon(int id) {
		return new TextureRegionDrawable(
				new TextureRegion(YStatic.gameAssets.get("system/tileset/overlay.png", Texture.class), id % 32 * 32, id / 32 * 32, 32, 32));
	}

	/**
	 *
	 * @param file
	 * @param x
	 *            multiplies with 24
	 * @param y
	 *            multiplies with 24
	 */
	@Deprecated
	public static TextureRegion getActionIcon(int x, int y) {
		return getRessIcon(x, y);
	}

	/**
	 *
	 * @param file
	 * @param x
	 *            multiplies with 24
	 * @param y
	 *            multiplies with 24
	 */
	@Deprecated
	public static TextureRegion getActionIcon(String name) {
		int s = 1;// YSettings.getGuiScale();
		Texture texture = new Texture(Gdx.files.internal("system/" + s + "/action/" + name + ".png"));
		return new TextureRegion(texture, 0, 0, 32 * s, 32 * s);
	}

	/**
	 *
	 * @param file
	 * @param x
	 *            multiplies with 24
	 * @param y
	 *            multiplies with 24
	 */
	@Deprecated
	public static TextureRegion getRessIcon(int x, int y) {
		Texture texture = YStatic.systemAssets.get("system/icons/IconSet.png", Texture.class);
		return new TextureRegion(texture, x * 24, y * 24, 24, 24);
	}

}
