/**
 *
 */
package de.qossire.yaams.screens.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;

/**
 * @author sven
 *
 */
public abstract class BaseLoaderScreen extends BaseMenuScreen {

	protected Image loadingHour;
	protected int status;
	protected VisLabel text;
	protected String title;

	/**
	 * 
	 */
	public BaseLoaderScreen(Yaams yaams, String title) {
		super(yaams);

		int s = YSettings.isBiggerGui() ? 256 : 128;
		this.title = title;

		loadingHour = new Image(YStatic.systemAssets.get("system/logo/logo" + (s * 2) + ".png", Texture.class));
		loadingHour.setOrigin(s, s);
		loadingHour.setX(Gdx.graphics.getWidth() / 2 - s);
		loadingHour.setY(Gdx.graphics.getHeight() / 2 - s);
		loadingHour.getColor().a = 0;
		loadingHour.addAction(Actions.fadeIn(1));
		// stage.addActor(loadingHour);

		status = 0;
	}

	/**
	 * 
	 */
	protected void createLabel() {
		int s = YSettings.isBiggerGui() ? 256 : 128;

		text = new VisLabel(title);
		// text.setScale(5);
		text.setX(Gdx.graphics.getWidth() / 2 - text.getWidth() / 2);
		text.setY(Gdx.graphics.getHeight() / 2 - s * 2);
		text.setColor(0, 0, 0, 0);
		text.addAction(Actions.fadeIn(1));
		stage.addActor(text);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		super.render(delta);

		loadingHour.setRotation(loadingHour.getRotation() + delta * 10);
		loadingHour.act(delta);

		if (status == 0) {
			status++;
			return;
		}

		// init code
		if (status == 1) {
			onceCode();
			status++;
			return;
		}

		if (!isFinishLoading()) {
			return;
		}

		// init code
		if (status == 2) {
			secondCode();
			status++;
			return;
		}

		// if (loadingHour.isVisible()) {
		// loadingHour.setVisible(false);
		// }

		// load it
		// if (stage.getActors().size == 0) {
		if (status == 3) {
			nextScreen();
			status++;
		}

		// }

		// display loading information
		// float progress = qossire.getAssets().getProgress();
		// System.out.println(progress);
		// ... left to the reader ...
	}

	/**
	 * Run after loading the assets
	 */
	protected void secondCode() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.base.BaseMenuScreen#drawBatchElements(float)
	 */
	@Override
	protected void drawBatchElements(float delta) {
		batch.setColor(loadingHour.getColor());
		loadingHour.draw(batch, 1);
	}

	/**
	 * @return
	 */
	public abstract boolean isFinishLoading();

	/**
	 * Load the nextScreen
	 */
	public abstract void nextScreen();

	/**
	 * Load the loading code
	 */
	public abstract void onceCode();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		int s = YSettings.isBiggerGui() ? 256 : 128;
		loadingHour.setX(Gdx.graphics.getWidth() / 2 - s);
		loadingHour.setY(Gdx.graphics.getHeight() / 2 - s);

		if (text == null)
			return;

		text.setX(Gdx.graphics.getWidth() / 2 - text.getWidth() / 2);
		text.setY(Gdx.graphics.getHeight() / 2 - s * 2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * @return the loadingHour
	 */
	public Image getLoadingHour() {
		return loadingHour;
	}

	/**
	 * @return the text
	 */
	public VisLabel getText() {
		return text;
	}

}
