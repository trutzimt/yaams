package de.qossire.yaams.game.debug;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.layout.HorizontalFlowGroup;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.game.action.CustomerGameAction;
import de.qossire.yaams.game.player.Stats;
import de.qossire.yaams.game.player.Stats.EStats;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YTabWindow;
import de.qossire.yaams.ui.YTable;

public class DebugWindow extends YTabWindow {

	public DebugWindow() {
		super("Debug");

		tabbedPane.add(new Tab(false, false) {

			@Override
			public String getTabTitle() {
				return "Setting";
			}

			@Override
			public Table getContentTable() {
				YTable t = new YTable(), r = new YTable();

				// money
				final VisTextField names = new VisTextField(Double.toString(MapScreen.get().getPlayer().getMoney()));
				names.addCaptureListener(new YChangeListener(false) {

					@Override
					public void changedY(Actor actor) {
						MapScreen.get().getPlayer().addMoney((int) -MapScreen.get().getPlayer().getMoney());
						MapScreen.get().getPlayer().addMoney(Integer.parseInt(names.getText()));

					}
				});

				t.addL("money", names);

				VisCheckBox box = new VisCheckBox("Debug Speed", YSettings.isDebugSpeed());
				box.addCaptureListener(new ChangeListener() {

					@Override
					public void changed(ChangeEvent event, Actor actor) {
						YSounds.click();
						YSettings.getPref().putBoolean("devSpeed", ((VisCheckBox) actor).isChecked());

					}
				});
				t.addL("Build", box);

				r.add(t).growX().row();

				HorizontalFlowGroup buttonBar = new HorizontalFlowGroup(2);
				buttonBar.addActor(new VisTextButton("customer", new ChangeListener() {

					@Override
					public void changed(ChangeEvent event, Actor actor) {

						MapScreen.get().getInputs().setActiveAction(new CustomerGameAction());
						YSounds.click();
						close();
					}
				}));
				buttonBar.addActor(new VisTextButton("scenario win", new ChangeListener() {

					@Override
					public void changed(ChangeEvent event, Actor actor) {

						MapScreen.get().getScenario().setWon();
						YSounds.click();
						close();
					}
				}));

				r.add(buttonBar).growX();
				return r;
			}
		});

		tabbedPane.add(new Tab(false, false) {

			@Override
			public String getTabTitle() {
				return "Scenario";
			}

			@Override
			public Table getContentTable() {
				YTable t = new YTable();

				t.addL("Scenario", MapScreen.get().getScenario().getTitle() + " " + MapScreen.get().getScenario().getFullId());

				for (ScenConf s : ScenConf.values()) {
					t.addL(s.toString(), MapScreen.get().getSettings().isActive(s) + " ");
				}

				return t.createScrollPaneInTable();
			}
		});

		tabbedPane.add(new Tab(false, false) {

			@Override
			public String getTabTitle() {
				return "Stats";
			}

			@Override
			public Table getContentTable() {
				YTable t = new YTable();

				Stats s = MapScreen.get().getPlayer().getStats();

				// set header
				t.add(new VisLabel("Day"));
				for (EStats e : EStats.values()) {
					t.add(new VisLabel(e.toString()));
				}
				t.row();

				// add data
				for (int i = 0; i < MapScreen.get().getClock().getDay(); i++) {
					t.add(new VisLabel("" + i));
					for (EStats e : EStats.values()) {
						t.add(new VisLabel(s.get(e, i) + ""));
					}
					t.row();
				}

				// add footer
				t.add(new VisLabel("Total"));
				for (EStats e : EStats.values()) {
					t.add(new VisLabel(s.getTotal(e) + ""));
				}
				t.row();

				return t.createScrollPaneInTable();
			}
		});

		buildIt();
	}

	/**
	 * Close it
	 */
	@Override
	public void close() {
		super.close();
		YSettings.save();
	}

}
