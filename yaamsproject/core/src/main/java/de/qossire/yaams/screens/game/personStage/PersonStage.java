/**
 * 
 */
package de.qossire.yaams.screens.game.personStage;

import java.util.ArrayList;
import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.utils.Array;

import de.qossire.yaams.code.YLog;
import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.game.persons.BasePerson;
import de.qossire.yaams.game.persons.PersonManagement;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;
import de.qossire.yaams.screens.game.map.YMapMap;

/**
 * @author sven
 *
 */
public class PersonStage {

	private BasePerson[][] matrix;
	private ManhattanHeuristic heuristic;

	/**
	 * map where true is a node and false is a node.
	 */
	private YNode[][] walkNodes;

	private boolean reBuildNodes;
	private IndexedAStarPathFinder<YNode> pathfinder;

	private int nextCustomerTime;
	private int width, height;

	private PersonManagement mgmt;

	private MapScreen mapScreen;

	private ArrayList<BasePerson> persons;

	private int maxCustomer;
	private int CustomerMinWait, CustomerMaxWait;

	/**
	 * 
	 */
	public PersonStage(MapScreen screen, YMapMap map) {
		// this.camera = camera;
		persons = new ArrayList<>();

		this.mapScreen = screen;

		mgmt = new PersonManagement();

		width = map.getWidth();
		height = map.getHeight();

		matrix = new BasePerson[width][height];

		// build default nodes
		heuristic = new ManhattanHeuristic();
		walkNodes = new YNode[width][height];
		int i = 0;
		Array<YNode> node = new Array<>(height * width);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				walkNodes[x][y] = new YNode(x, y, i++);
				node.add(walkNodes[x][y]);
			}
		}
		pathfinder = new IndexedAStarPathFinder<YNode>(new Graph(node), true);

		reBuildNodes = true;

		// inform
		mapScreen.getEvents().register(MapEventHandlerTyp.FILE_LOAD, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				recalcCustomerTime();
				return false;
			}
		});
		mapScreen.getEvents().register(MapEventHandlerTyp.NEXTDAY, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				recalcCustomerTime();
				return false;
			}
		});

		maxCustomer = 10;
		CustomerMinWait = 50;
		CustomerMaxWait = 150;

	}

	/**
	 * 
	 */
	private void recalcCustomerTime() {
		int i = mapScreen.getPlayer().getRang().getRank(ERank.OVERVIEW).getLevel();
		int s = mapScreen.getData().getAllPropsPoints(MapProp.STREET, 1).size();

		maxCustomer = 10 + mapScreen.getPlayer().getRang().getRank(ERank.OVERVIEW).getLevel() * 15;
		CustomerMinWait = 5 + 100 / (s == 0 ? 1 : s);
		CustomerMaxWait = CustomerMinWait + (CustomerMinWait * (11 - i)) / 2;
	}

	/**
	 * Convert the blocked
	 * 
	 * @param x
	 * @param y
	 * @return
	 */

	private boolean getWalkTile(int x, int y) {
		return mapScreen.getTilesetHelper().isValidePosition(x, y) && mapScreen.getData().getProps(MapProp.BLOCKED, x, y) == 0;
	}

	/**
	 * Run on the thread
	 */
	public void threadCode() {

		// rebuild nodes?
		if (reBuildNodes) {
			rebuildNodes();
		}

		// TODO better formant
		for (final BasePerson person : persons.toArray(new BasePerson[] {})) {
			person.updateLogic(mapScreen.getClock().getTickMin());
			// walk somewhere?
			if (person.getGoalNode() != null) {
				// needs a new route?
				if (person.getPathIndex() == -1) {
					// found a route?
					boolean erg = pathfinder.searchNodePath(getNode(person.getGameX(), person.getGameY()), person.getGoalNode(), heuristic, person.getPath());
					// nothing found
					// Gdx.app.debug("found " + erg, "search path to " + person.getGoalNode().getX()
					// + " " + person.getGoalNode().getY());
					if (erg) {
						person.setPathIndex(0);
					} else {
						// remove node
						person.clearGoalPos();
					}
				}

				// walk on path?
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						// process the result, e.g. add it to an Array<Result> field of the
						// ApplicationListener.
						person.walk();
					}
				});
			}

		}

		// add new customers?
		if (mapScreen.getPlayer().getMuseum().isOpen()) {
			// right time?
			if (nextCustomerTime > 0) {
				nextCustomerTime -= mapScreen.getClock().getTickMin();
			} else {
				// is open?
				if (maxCustomer > persons.size() && mapScreen.getPlayer().getMuseum().getOpenFrom() <= mapScreen.getClock().getHour()
						&& mapScreen.getClock().getHour() < mapScreen.getPlayer().getMuseum().getOpenTo() - 1) {
					LinkedList<YPoint> places = mapScreen.getData().getAllPropsPoints(MapProp.STREET, 1);
					// has place?
					if (places.size() > 0) {

						// find a free place
						for (int i = 0, l = places.size(); i < l; i++) {
							int r = NamesGenerator.getIntBetween(0, places.size());
							if (!isFree(places.get(r).getX(), places.get(r).getY())) {
								places.remove(r);
								continue;
							}
							// generate a new customer
							addPerson(new Customer(places.get(r).getX(), places.get(r).getY(), this, mapScreen));
							break;
						}

						nextCustomerTime = NamesGenerator.getIntBetween(CustomerMinWait, CustomerMaxWait);
						YLog.log("next customer", nextCustomerTime);
					} else {
						nextCustomerTime = 10;
						YLog.log("no free place", nextCustomerTime);
					}

				} else {
					nextCustomerTime = 60;
				}
			}

		}

	}

	/**
	 * 
	 */
	private void rebuildNodes() {
		// rebuild nodes?
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				YNode n = walkNodes[x][y];
				// add connections?
				n.clearConnection();
				if (getWalkTile(x, y + 1)) { // N
					n.addConnection(walkNodes[x][y + 1]);
				}
				if (getWalkTile(x - 1, y)) { // W
					n.addConnection(walkNodes[x - 1][y]);
				}
				if (getWalkTile(x, y - 1)) { // S
					n.addConnection(walkNodes[x][y - 1]);
				}
				if (getWalkTile(x + 1, y)) { // E
					n.addConnection(walkNodes[x + 1][y]);
				}

				// add diagonal connection
				if (getWalkTile(x + 1, y + 1)) { // NO
					n.addConnection(walkNodes[x + 1][y + 1]);
				}
				if (getWalkTile(x - 1, y + 1)) { // NW
					n.addConnection(walkNodes[x - 1][y + 1]);
				}
				if (getWalkTile(x - 1, y - 1)) { // SW
					n.addConnection(walkNodes[x - 1][y - 1]);
				}
				if (getWalkTile(x + 1, y - 1)) { // SO
					n.addConnection(walkNodes[x + 1][y - 1]);
				}

				// Gdx.app.debug("helper",
				// "create connections for " + x + " " + y + " size:" +
				// n.getConnections().size);

			}

		}

		reBuildNodes = false;
		YLog.log("rebuild graph");

		// reset all paths
		for (BasePerson person : persons) {
			if (person.getGoalNode() != null) {
				person.setGoalPos(person.getGoalNode().getX(), person.getGoalNode().getY());
			}
		}
	}

	/**
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void addPerson(BasePerson person) {
		persons.add(person);
		MapScreen.get().getMap().getStage().addActor(person.getActor());

		if (person instanceof Customer) {
			mapScreen.getEvents().perform(MapEventHandlerTyp.CUSTOMER_NEW, person);
		}

	}

	/**
	 * @return the walkNodes
	 */
	public YNode getNode(int x, int y) {
		return walkNodes[x][y];
	}

	/**
	 * Set a person
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public void setPersonMatrix(int x, int y, BasePerson p) {
		matrix[x][y] = p;
	}

	/**
	 * Set a person
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public BasePerson getPersonMatrix(int x, int y) {
		return matrix[x][y];
	}

	/**
	 * Check if this field is free
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isFree(int x, int y) {
		return matrix[x][y] == null;
	}

	/**
	 * @return the mgmt
	 */
	public PersonManagement getMgmt() {
		return mgmt;
	}

	/**
	 * Calculate a route between the points and return the length
	 * 
	 * @param fromX
	 * @param fromY
	 * @param goalX
	 * @param goalY
	 * @return -1, no found, otherwise the length
	 */
	public int calcPath(int fromX, int fromY, int goalX, int goalY) {
		DefaultGraphPath<YNode> path = new DefaultGraphPath<>();

		boolean erg = pathfinder.searchNodePath(getNode(fromX, fromY), getNode(goalX, goalY), heuristic, path);

		if (erg) {
			return path.getCount();
		} else {
			return -1;
		}
	}

	/**
	 * This customer will go to the nearest tile, with this properties, based on
	 * path
	 * 
	 * @param customer
	 * @param points
	 * @return true, found a way or tile, false otherwise
	 */
	public boolean goToNearestTileFrom(Customer customer, LinkedList<YPoint> points) {

		// found points?
		if (points == null) {
			return false;
		}

		YPoint goalPos = null;
		int goalDis = -1;

		// find best route
		for (YPoint pos : points) {

			int dis = calcPath(customer.getGameX(), customer.getGameY(), pos.getX(), pos.getY());

			// is a better path?
			if (dis != -1) {
				if (goalDis == -1 || goalDis > dis) {
					goalDis = dis;
					goalPos = pos;
				}
			}
		}

		// found something?
		if (goalDis == -1) {
			return false;
		}

		// go their
		customer.setGoalPos(goalPos.getX(), goalPos.getY());
		return true;

	}

	/**
	 * @return the personStage
	 */
	public ArrayList<BasePerson> getPersons() {
		return persons;
	}

	/**
	 * Reset for the next day
	 * 
	 * @category thread
	 */
	public void nextDay() {
		// remove all customers
		for (BasePerson a : persons.toArray(new BasePerson[] {})) {
			if (!(a instanceof Customer)) {
				continue;
			}

			final Customer person = (Customer) a;
			person.remove();
			persons.remove(person);
		}

		// TODO reset timer?

	}

	/**
	 * @param reBuildNodes
	 *            the reBuildNodes to set
	 */
	public void setReBuildNodes(boolean reBuildNodes) {
		this.reBuildNodes = reBuildNodes;
	}

	/**
	 * @return the maxCustomer
	 */
	public int getMaxCustomer() {
		return maxCustomer;
	}

	/**
	 * @return the customerMinWait
	 */
	public int getCustomerMinWait() {
		return CustomerMinWait;
	}

	/**
	 * @return the customerMaxWait
	 */
	public int getCustomerMaxWait() {
		return CustomerMaxWait;
	}
}
