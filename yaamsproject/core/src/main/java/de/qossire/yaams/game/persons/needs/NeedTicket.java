/**
 * 
 */
package de.qossire.yaams.game.persons.needs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.game.build.BuildManagement;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.persons.PersonManagement.PersonDir;
import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.screens.game.GameData.GDC;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.AnimationHelper;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YNotification;

/**
 * @author sven
 *
 */
public class NeedTicket extends BaseNeed {

	/**
	 */
	public NeedTicket() {
		super(PersonNeeds.TICKET, 3);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#updateLogic(de.qossire.yaams.
	 * game.persons.Customer, de.qossire.yaams.screens.map.MapScreen, int)
	 */
	@Override
	public boolean updateLogic(final Customer customer, final MapScreen map, int deltaTime) {
		// has a ticket?
		if (customer.getNeed(id) >= MAX) {
			return false;
		}

		// has money for a ticket?
		if (customer.getMoney() < map.getData().getPI(GDC.TICKET_PRICE)) {
			return true;
		}

		// Gdx.app.debug(id.toString(),
		// "field: " + map.getMap().getProps(BuildProperties.TICKET,
		// customer.getGameX(), customer.getGameY()));

		// found an automat?
		if (map.getData().getProps(MapProp.TICKET, customer.getGameX(), customer.getGameY()) >= 1 && customer.getWaitMin() == 0) {

			// find automat pos
			PersonDir d = map.getData().getProps(MapProp.TICKET, customer.getGameX(), customer.getGameY() - 1) > 2 ? PersonDir.SOUTH
					: map.getData().getProps(MapProp.TICKET, customer.getGameX(), customer.getGameY() + 1) > 2 ? PersonDir.NORTH
							: map.getData().getProps(MapProp.TICKET, customer.getGameX() + 1, customer.getGameY()) > 2 ? PersonDir.WEST : PersonDir.EAST;

			customer.setWaitMin(10, d, new Runnable() {

				@Override
				public void run() {
					customer.addNeed(id, SET_MAX);
					customer.addMoney(-map.getData().getPI(GDC.TICKET_PRICE));

					Gdx.app.debug("TICKET", "buy new ticket " + customer.getNeed(id));

					AnimationHelper.moneyAnimation(map.getData().getPI(GDC.TICKET_PRICE), customer.getGameX(), customer.getGameY(), true);

					// TODO add joy modification for ticket price
				}

			});
			return false;
		}

		// want to buy a ticket
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#tryToFix(de.qossire.yaams.game.
	 * persons.Customer, de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void tryToFix(Customer customer, MapScreen map) {
		// find nearst automat
		if (!map.getMap().goToNearestTileFrom(customer, MapProp.TICKET, 1)) {
			// noti user
			map.getMapgui().addNotification(new YNotification("Ticket", customer.getPersonName() + " can not find a Ticket Machine or the way is blocked. "
					+ customer.getPersonNameProp() + " will enter for free.", new Image(YIcons.getBuildIcon(BuildManagement.TICKET, true))));

			// add ticket
			customer.addNeed(id, SET_MAX);

			// add joy
			customer.addNeed(PersonNeeds.JOY, 300);
			return;

		}

		// has not money for a ticket?
		if (customer.getMoney() < map.getData().getPI(GDC.TICKET_PRICE)) {
			customer.addNeed(PersonNeeds.LEAVE, SET_MAX);
		}

	}

}
