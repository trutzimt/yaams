/**
 *
 */
package de.qossire.yaams.screens.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.level.LevelWindow;
import de.qossire.yaams.level.ScenarioManagement;
import de.qossire.yaams.level.endless.EndlessGameWindow;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.saveload.SaveLoadManagement;
import de.qossire.yaams.ui.YTextButton;
import de.qossire.yaams.ui.YWindow;
import de.qossire.yaams.ui.YWindowHelper;
import de.qossire.yaams.ui.actions.AboutAction;
import de.qossire.yaams.ui.actions.FeedbackAction;
import de.qossire.yaams.ui.actions.LoadAction;
import de.qossire.yaams.ui.actions.OptionsAction;

/**
 * @author sven
 *
 */
public class MainMenuWindow extends YWindow {

	/**
	 * 
	 */
	public MainMenuWindow(final Yaams qossire, final Stage stage) {
		super("yaaMs");

		// new game
		if (ScenarioManagement.getCampaignIds().size() > 1)
			add(new YTextButton("Scenarios") {

				@Override
				public void perform() {
					YSounds.click();
					stage.addActor(new LevelWindow(qossire));

				}
			}).fillX().row();

		// new game
		add(new YTextButton("Endless Game") {

			@Override
			public void perform() {
				YSounds.click();
				stage.addActor(new EndlessGameWindow(qossire));
				// qossire.setScreen(new EndlessGameScreen(qossire));

			}
		}).fillX().row();

		// load
        if (SaveLoadManagement.getSaves().size()>0)
		    add(new LoadAction().getButton(qossire)).fillX().row();

		// options
		add(new OptionsAction().getButton(qossire)).fillX().row();

		// about game
		add(new AboutAction().getButton(qossire)).fillX().row();

		// send feedback
		add(new FeedbackAction().getButton(qossire)).fillX().row();

		// quit game
		add(new YTextButton("Quit") {

			@Override
			public void perform() {
				YSounds.click();
				Gdx.app.exit();

			}
		}).fillX().row();

		// setSize(133 * QSettings.getGuiScale(), 203 * QSettings.getGuiScale());
        setCenterOnAdd(false);
		pack();
		// setSize(getWidth()+40, getHeight()+);

		YWindowHelper.setCenterX(this);
		setY(Gdx.graphics.getHeight() / 3 - getHeight() / 2);
		fadeIn();

		addTitleIcon(new Image(new TextureRegion(YStatic.systemAssets.get("system/logo/logo" +(YSettings.isBiggerGui() ? "48.png" : "32.png"), Texture.class))));

	}

}
