/**
 *
 */
package de.qossire.yaams.level.single;

import java.util.HashMap;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.quest.BaseQuest;
import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.QuestActionWin;
import de.qossire.yaams.game.quest.req.QuestReqCustomerCount;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.level.ScenarioSettings;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class Customer101GameScenario extends BaseScenario {

	protected HashMap<String, String> settings;

	/**
	 */
	public Customer101GameScenario() {
		super("cus101", "Hundert and one customer", "map/40.tmx");

		// set the settings
		desc = "Create a art gallery and attract 101 customer.";

		baseData.put(ScenarioSettings.getKey(ScenConf.ART_ONTABLE), false);
		baseData.put(ScenarioSettings.getKey(ScenConf.ART_STATURE), false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(final MapScreen screen) {
		super.start(screen);

		for (int i = 0, l = NamesGenerator.getIntBetween(2, 6); i < l; i++) {
			screen.getPlayer().getArtwork().addArt(screen.getPlayer().getArtwork().generateArt(), ArtStatus.DEPOT);
		}

		// start money
		screen.getPlayer().addMoney(50_000);

		// add quests
		BaseQuest b = new BaseQuest(getTitle());
		BaseQuestItem i = new BaseQuestItem();
		i.setDesc(desc);
		i.addReq(new QuestReqCustomerCount(101));
		i.addAction(new QuestActionWin());
		b.addItem(i);
		screen.getPlayer().getQuests().addQuest(b);

	}

}
