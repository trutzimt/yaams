package de.qossire.yaams.music;

import java.io.FileNotFoundException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.code.YLog;
import de.qossire.yaams.generator.NamesGenerator;

public class YMusic {

	private Music music;
	private String actSong;
	private String newSong;
	private boolean newLoop;
	private Thread musicthread;
	private boolean playList;

	public YMusic() {
		// has music?
		if (!Gdx.files.internal("music").exists())
			return;

		musicthread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					while (true) {
						Thread.sleep(1000);

						// ingame?
						if (playList && music != null && !music.isPlaying()) {
							generateNewSong();
						}

						// has a new song?
						if (newSong == null || newSong == actSong) {
							continue;
						}

						// has old music? //fade out
						if (music != null) {
							// fade out
							float v = music.getVolume();
							for (int i = 0; i < 10; i++) {
								music.setVolume(v * (1 - i / 10f));
								Thread.sleep(100);
							}
							stop();
						}

						// start new music
						if (YSettings.getMusicVolume() > 0) {
							FileHandle f = Gdx.files.internal("music/" + newSong + ".mp3");
							if (!f.exists()) {
								actSong = null;
								newSong = null;
								playList = false;
								YConfig.error(new FileNotFoundException("music/" + newSong + ".mp3"), false);

								continue;
							}

							music = Gdx.audio.newMusic(Gdx.files.internal("music/" + newSong + ".mp3"));
							music.setLooping(newLoop);
							music.setVolume(YSettings.getMusicVolume());
							music.play();
							actSong = newSong;
							newSong = null;
						} else {
							music = null;
						}
					}
				} catch (Throwable e) {
					YConfig.error(e, false);
				}

			}
		});
		musicthread.start();
	}

	/**
	 * Return a sound
	 * 
	 * @param name
	 * @return
	 */
	private void play(String name, boolean loop) {
		playList = false;
		newLoop = loop;
		newSong = name;
	}

	/**
	 * Stop the music
	 */
	public void stop() {
		// has old music?
		if (music != null) {
			music.stop();
			music.dispose();
		}
	}

	/**
	 * Stop the music
	 */
	public static void updateVol() {
		if (YSettings.getMusicVolume() == 0)
			YStatic.music.stop();

		if (YStatic.music.music != null)
			YStatic.music.music.setVolume(YSettings.getMusicVolume());
	}

	/**
	 * Start the ingame music
	 */
	public static void startInGameMusic() {
		// has a folder?
		if (!Gdx.files.internal("music").isDirectory()) {
			return;
		}

		YStatic.music.playList = true;
		YStatic.music.generateNewSong();

	}

	/**
	 * Generate a new song
	 */
	private void generateNewSong() {
		newSong = "game/" + NamesGenerator.getRndA(Gdx.files.internal("music/game/list.txt").readString().split(","));
		YLog.log("next song: " + newSong);
		newLoop = false;
	}

	/**
	 * Play the click sound
	 */
	public static void mainmenu() {
		YStatic.music.play("Our Tragic Kingdom", true);
	}

	/**
	 * Play the click sound
	 */
	public static void winScenario() {
		YStatic.music.play("The Succesor", false);
	}

	/**
	 * Play the click sound
	 */
	public static void loseScenario() {
		YStatic.music.play("Sleep and Rest", false);
	}

	/**
	 * Play the click sound
	 */
	public static void clickForOptions() {
		YSounds.play("Cursor", YSettings.getMusicVolume());
	}

	/**
	 * @return the actSong
	 */
	public String getActSong() {
		return actSong;
	}

}
