package de.qossire.yaams.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.rafaskoberg.gdx.typinglabel.TypingLabel;

import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.music.YSounds;

public class YMessageDialog extends YWindow {

	private YChangeListener action;
	protected VisTable buttons;

	public YMessageDialog(Yaams yaams, String title, String message, YChangeListener action) {
		super(title);

		this.action = action;

		// add content
		TypingLabel t = new TypingLabel(message, VisUI.getSkin());
		t.setWrap(message.length() > 50);
		VisScrollPane scrollPane = new VisScrollPane(t);
		scrollPane.setFadeScrollBars(false);
		scrollPane.setFlickScroll(false);
		scrollPane.setOverscroll(false, false);
		scrollPane.setScrollingDisabled(true, false);
		add(scrollPane).align(Align.left).grow().row();

		buttons = new VisTable(true);

		add(buttons).align(Align.right);

		if (message.length() < 50) {
			pack();
		} else {
			setWidth(Gdx.graphics.getWidth() / 2);
			setHeight(Gdx.graphics.getHeight() / 2);
		}

	}

	/**
	 * Build it to the end
	 * 
	 * @param stage
	 */
	public void build(Stage stage) {
		YTextButton t = new YTextButton("Close") {

			@Override
			public void perform() {
				YSounds.click();
				close();

			}
		};
		buttons.add(t);

		stage.addActor(this);
	}

	/**
	 *
	 * @param title
	 * @param action
	 */
	public void addButton(YTextButton button) {
		buttons.add(button);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		super.close();
		if (action != null) {
			action.changed(null, this);
		}
	}
}
