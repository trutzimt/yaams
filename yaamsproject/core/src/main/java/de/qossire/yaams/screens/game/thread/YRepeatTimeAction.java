package de.qossire.yaams.screens.game.thread;

import de.qossire.yaams.screens.game.MapScreen;

public abstract class YRepeatTimeAction extends YTimeAction {

	protected int start, end;
	protected long started, estimated;

	/**
	 * 
	 * @param runNext
	 */
	public YRepeatTimeAction(int start, int end, int estimated) {
		super(0);

		started = MapScreen.get().getClock().getTimeStamp();
		this.start = start;
		this.end = end;
		this.estimated = estimated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.screens.game.thread.YTimeAction#run()
	 */
	@Override
	public boolean run() {

		long diff = MapScreen.get().getClock().getTimeStamp() - started;
		long step = (long) (diff * 1.f / estimated * (end - start));

		// call it
		runStep(start + step);

		return step >= end;
	}

	/**
	 * Call the action with the correct step count, between start and end
	 * 
	 * @param step
	 */
	public abstract void runStep(long step);
}
