/**
 * 
 */
package de.qossire.yaams.game.persons.customer;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.qossire.yaams.game.persons.BasePerson;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;
import de.qossire.yaams.screens.game.personStage.PersonStage;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YProgressBar;
import de.qossire.yaams.ui.YSplitTab;
import de.qossire.yaams.ui.YTable;
import de.qossire.yaams.ui.YTextButton;

/**
 * @author sven
 *
 */
public class CustomerTab extends YSplitTab {

	private VisTextButton remove;
	private MapEventHandlerAction cNew, cLea;

	public CustomerTab() {
		super("Customer", "At the moment you have no customer in your museum. Open the museum and place some art.");

		remove = new YTextButton("Ask the customer to leave") {

			@Override
			public void perform() {
				// leave him
				doubleClickElement(remove);
			}
		};
		remove.setDisabled(true);
		buttonBar.addActor(remove);

		cNew = new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				// need to set?
				if (window.getTabbedPane().getActiveTab() == CustomerTab.this)
					rebuild();
				return false;
			}
		};
		cLea = new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				// need to set?
				if (window.getTabbedPane().getActiveTab() == CustomerTab.this) {
					reset();
					rebuild();
				}
				return false;
			}
		};

		// register for changed
		MapScreen.get().getEvents().register(MapEventHandlerTyp.CUSTOMER_NEW, cNew);
		MapScreen.get().getEvents().register(MapEventHandlerTyp.CUSTOMER_LEAVE, cLea);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(Button btn) {
		Customer c = ((Customer) btn.getUserObject());
		if (c != null) {
			return c.getInfoPanel();
		}

		return getDefaultPanel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#getDefaultPanel()
	 */
	@Override
	protected Actor getDefaultPanel() {
		PersonStage p = MapScreen.get().getMap().getPersonStage();

		YTable t = new YTable();
		t.addH("General");
		t.addL("Customer", new YProgressBar(p.getPersons().size(), p.getMaxCustomer()));
		t.addL("Awareness", "10%");
		t.addL("New Customer", "Every " + p.getCustomerMinWait() + " to " + p.getCustomerMaxWait() + " minutes");

		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#reset()
	 */
	@Override
	protected void reset() {
		super.reset();
		remove.setDisabled(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#clickElement(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected void clickElement(Button btn) {
		remove.setDisabled(btn.getUserObject() == null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		elements.clear();

		addElement("Overview", YIcons.getIconD(YIType.INFOABOUT), null);

		// add all elements
		for (final BasePerson b : MapScreen.get().getMap().getPersonStage().getPersons()) {
			if (!(b instanceof Customer)) {
				continue;
			}

			Customer c = (Customer) b;
			addElement(c.getPersonName(), new TextureRegionDrawable(c.getActor().getPreviewRegion()), c);
		}

		super.rebuild();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#doubleClickElement(com.badlogic.gdx.scenes.
	 * scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		Customer c = ((Customer) btn.getUserObject());
		if (c != null) {
			c.askToLeave();
			return;
		}

		right.clear();
		right.add(getDefaultPanel());

	}

}