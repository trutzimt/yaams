/**
 * 
 */
package de.qossire.yaams.game.rooms;

import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.rooms.RoomMgmt.RoomTyp;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public abstract class BaseRoom {

	protected final RoomTyp typ;
	protected final int id;
	protected int x, y;
	protected String name;
	protected boolean toCheck;
	protected boolean hasFloor, hasWall;
	protected int size;
	protected int bonus;

	/**
	 * @param iD
	 */
	public BaseRoom(RoomTyp typ, int uid, int x, int y) {
		super();
		this.typ = typ;
		this.id = uid;
		this.x = x;
		this.y = y;
		name = typ.toString().toLowerCase() + " " + uid;
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	public VisTable getInfoPanel() {
		YTable table = new YTable();
		table.addL("Name", name).setWrap(true);
		table.addL("Typ", typ.toString().toLowerCase());

		return table;
	}

	/**
	 * Reset for the next day, Check the condition of this room
	 * 
	 * @category thread
	 */
	public void nextDay() {
		toCheck = true;
		updateLogic();
	}

	/**
	 * Get the icon
	 * 
	 * @return
	 */
	public abstract TextureRegionDrawable getIcon();

	/**
	 * @return the title
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setName(String title) {
		this.name = title;
	}

	/**
	 * @return the iD
	 */
	public RoomTyp getTyp() {
		return typ;
	}

	/**
	 * @return the uid
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param toCheck
	 *            the toCheck to set
	 */
	public void setToCheck(boolean toCheck) {
		this.toCheck = toCheck;
	}

	/**
	 * Update in the threads the player logic
	 * 
	 * @category thread
	 */
	public void updateLogic() {
		// check it
		if (!toCheck) {
			return;
		}

		size = 0;
		hasFloor = true;
		hasWall = true;
		checkTile(x, y, new boolean[MapScreen.get().getMap().getWidth()][MapScreen.get().getMap().getHeight()]);

		toCheck = false;

	}

	/**
	 * Look if everything exist
	 * 
	 * @param x
	 * @param y
	 * @param fields
	 */
	protected void checkTile(int x, int y, boolean[][] fields) {
		// was visit?
		if (!MapScreen.get().getTilesetHelper().isValidePosition(x, y) || fields[x][y]) {
			return;
		}

		// right typ?
		if (MapScreen.get().getData().getProps(MapProp.ROOMID, x, y) != id) {

			// is a wall?
			if (hasWall && MapScreen.get().getData().getProps(MapProp.WALL, x, y) == 0) {
				hasWall = false;
			}
			return;
		}

		fields[x][y] = true;
		size++;

		// has a floor?
		if (hasFloor && MapScreen.get().getData().getProps(MapProp.FOUNDATION, x, y) == 0) {
			hasFloor = false;
		}

		// ask neighboors
		checkTile(x - 1, y, fields);
		checkTile(x, y - 1, fields);
		checkTile(x + 1, y, fields);
		checkTile(x, y + 1, fields);

	}

	/**
	 * @return the bonus
	 */
	public int getBonus() {
		return bonus;
	}
}
