package de.qossire.yaams.game.quest.req;

public interface IQuestRequirement {

	/**
	 * Are this req fullfilled?
	 * 
	 * @param map
	 * @return
	 */
	public boolean checkReq();

	/**
	 * Get a description for this requirement
	 * 
	 * @return
	 */
	public String getDesc();
}
