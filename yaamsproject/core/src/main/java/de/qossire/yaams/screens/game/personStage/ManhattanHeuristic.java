package de.qossire.yaams.screens.game.personStage;

import com.badlogic.gdx.ai.pfa.Heuristic;

public class ManhattanHeuristic implements Heuristic<YNode> {

	@Override
	public float estimate(YNode startNode, YNode endNode) {
		return Math.abs(startNode.getX() - endNode.getX()) + Math.abs(startNode.getY() - endNode.getY());
	}
}