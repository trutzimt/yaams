/**
 * 
 */
package de.qossire.yaams.game.museum.funding;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.game.quest.action.QuestActionGetMoney;
import de.qossire.yaams.game.quest.action.QuestActionSetPref;
import de.qossire.yaams.game.quest.req.QuestReqMaxMoney;
import de.qossire.yaams.game.quest.req.QuestReqMaxPref;
import de.qossire.yaams.game.quest.req.QuestReqRankMaxCount;
import de.qossire.yaams.game.quest.req.QuestReqRankMinCount;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class FundingsMgmt {

	private ArrayList<Fundings> funds;
	private int maxActive;

	/**
	 * Create it
	 */
	public FundingsMgmt() {
		maxActive = 2;

		funds = new ArrayList<>();

		// add it
		Fundings f = new Fundings() {

			@Override
			public Drawable getIcon() {
				return YIcons.getIconD(YIType.MUSEUM);
			}

		};
		f.setTitle("For small museums");
		f.addReq(new QuestReqRankMinCount(ERank.OVERVIEW, 1));
		f.addReq(new QuestReqRankMaxCount(ERank.OVERVIEW, 3));
		f.addAction(new QuestActionGetMoney(500));
		funds.add(f);

		// add it
		f = new Fundings() {

			@Override
			public Drawable getIcon() {
				return YIcons.getIconD(YIType.MUSEUM);
			}

		};
		f.setTitle("For medium museums");
		f.addReq(new QuestReqRankMinCount(ERank.OVERVIEW, 4));
		f.addReq(new QuestReqRankMaxCount(ERank.OVERVIEW, 6));
		f.addAction(new QuestActionGetMoney(1500));
		funds.add(f);

		// add it
		f = new Fundings() {

			@Override
			public Drawable getIcon() {
				return YIcons.getIconD(YIType.MUSEUM);
			}

		};
		f.setTitle("For big museums");
		f.addReq(new QuestReqRankMinCount(ERank.OVERVIEW, 7));
		f.addAction(new QuestActionGetMoney(5000));
		funds.add(f);

		// add it
		f = new Fundings() {

			@Override
			public Drawable getIcon() {
				return YIcons.getIconD(YIType.MONEY);
			}

		};
		f.setTitle("First Aid");
		f.addReq(new QuestReqMaxMoney(100));
		f.addReq(new QuestReqMaxPref(1, "fundings.firstaid", "Run First Aid"));
		f.addPreAction(new QuestActionGetMoney(5000));
		f.addPreAction(new QuestActionSetPref("fundings.firstaid", 1, "Run only once"));
		funds.add(f);

		// add it
		f = new Fundings() {

			@Override
			public Drawable getIcon() {
				return YIcons.getIconD(YIType.JOY);
			}

		};
		f.setTitle("Happiness Support");
		f.addReq(new QuestReqRankMinCount(ERank.JOY, 6));
		f.addAction(new QuestActionGetMoney(750));
		funds.add(f);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Fundings> get() {
		return funds;
	}

	/**
	 * @return the maxActive
	 */
	public int getMaxActive() {
		return maxActive;
	}

	/**
	 * Reset for the next day
	 * 
	 * @category thread
	 */
	public void nextDay() {
		for (Fundings f : funds) {
			if (f.isActive()) {
				f.updateLogic();
			}
		}
	}

}
