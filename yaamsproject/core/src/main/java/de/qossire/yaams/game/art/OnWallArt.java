/**
 * 
 */
package de.qossire.yaams.game.art;

import java.util.LinkedList;

import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.game.art.ArtworkMgmt.ArtTyp;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.map.YTilesetHelper;

/**
 * @author sven
 *
 */
public class OnWallArt extends BaseArt {

	boolean istop;

	/**
	 * @param uid
	 * @param typ
	 */
	public OnWallArt() {
		super(ArtTyp.IMAGE);
		setTId((int) NamesGenerator.getRnd(0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 64,
				66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IMgmtElement#buildAt(int, int)
	 */
	@Override
	public void buildAt(int x, int y) {
		MapScreen map = MapScreen.get();
		istop = map.getTilesetHelper().isTopWall(x, y);

		// add Properties
		map.getData().addPropsRekursive(MapProp.DECOR, istop ? x : x + 1, istop ? y - 1 : y, decor, false);

		// change lists
		setStatus(ArtStatus.DISPLAYED);

		// set it
		map.getData().addProps(MapProp.ARTID, x, y, uid);
		map.getData().setArtTile(tid + (istop ? 0 : YTilesetHelper.TILESET_LENGTH), x, y);
		setPos(x, y);

		afterLoad();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.saveload.ISave#afterLoad()
	 */
	@Override
	public void afterLoad() {
		// set viewpoints
		if (MapScreen.get().getTilesetHelper().isTopWall(pos.getX(), pos.getY())) {
			MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX() - 1, pos.getY() - 1, this);
			MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX(), pos.getY() - 1, this);
			MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY() - 1, this);
		} else {
			MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY() + 1, this);
			MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY(), this);
			MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY() - 1, this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IMgmtElement#removeAt(int, int)
	 */
	@Override
	public void removeAt(int x, int y) {
		MapScreen map = MapScreen.get();

		// add Properties
		map.getData().addPropsRekursive(MapProp.DECOR, istop ? pos.getX() : pos.getX() + 1, istop ? pos.getY() - 1 : pos.getY(), -decor, false);

		// set it
		map.getData().setArtTile(0, pos.getX(), pos.getY());
		setPos(-1, -1);

		// set viewpoints
		if (istop) {
			map.getPlayer().getArtwork().addVisibleArtAt(pos.getX() - 1, pos.getY() - 1, this);
			map.getPlayer().getArtwork().addVisibleArtAt(pos.getX(), pos.getY() - 1, this);
			map.getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY() - 1, this);
		} else {
			map.getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY() + 1, this);
			map.getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY(), this);
			map.getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY() - 1, this);
		}
	}

	/**
	 * Add the visiting points to the user list
	 * 
	 * @param points
	 */
	@Override
	public void addCustomerGoalPoint(LinkedList<YPoint> points) {
		boolean top = MapScreen.get().getTilesetHelper().isTopWall(pos.getX(), pos.getY());

		// add points
		if (top) {
			points.add(pos.addXYandClone(-1, -1));
			points.add(pos.addXYandClone(0, -1));
			points.add(pos.addXYandClone(1, -1));
		} else {
			points.add(pos.addXYandClone(1, -1));
			points.add(pos.addXYandClone(1, 0));
			points.add(pos.addXYandClone(1, 1));
		}

	}

}
