package de.qossire.yaams.screens.game.personStage;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultConnection;
import com.badlogic.gdx.utils.Array;

public class YNode implements Connection<YNode> {
	private final int x;
	private final int y;
	private int index;
	private Array<Connection<YNode>> connections;

	public YNode(int x, int y, int index) {
		this.index = index;
		this.x = x;
		this.y = y;
		connections = new Array<Connection<YNode>>();
	}

	// public void render(ShapeRenderer shapeRenderer, Color color) {
	// int x = this.x * GameInfo.ONE_TILE;
	// int y = this.y * GameInfo.ONE_TILE;
	// shapeRenderer.setColor(color);
	// shapeRenderer.line(x, y, x, y + GameInfo.ONE_TILE);
	// shapeRenderer.line(x + GameInfo.ONE_TILE, y, x + GameInfo.ONE_TILE, y +
	// GameInfo.ONE_TILE);
	// shapeRenderer.line(x, y, x + GameInfo.ONE_TILE, y);
	// shapeRenderer.line(x, y + GameInfo.ONE_TILE, x + GameInfo.ONE_TILE, y +
	// GameInfo.ONE_TILE);
	// }

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getIndex() {
		return index;
	}

	public void addConnection(YNode node) {
		if (node != null) { // Make sure there is a neighboring node
			connections.add(new DefaultConnection<YNode>(this, node));
		}
	}

	public void clearConnection() {
		connections.clear();
	}

	public Array<Connection<YNode>> getConnections() {
		return connections;
	}

	@Override
	public float getCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public YNode getFromNode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public YNode getToNode() {
		// TODO Auto-generated method stub
		return null;
	}
}
