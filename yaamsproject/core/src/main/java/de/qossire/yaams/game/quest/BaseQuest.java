/**
 * 
 */
package de.qossire.yaams.game.quest;

import java.util.LinkedList;

import de.qossire.yaams.game.quest.action.BaseQuestAction;
import de.qossire.yaams.game.quest.req.IQuestRequirement;

/**
 * @author sven
 *
 */
public class BaseQuest extends BaseQuestItem {

	private LinkedList<BaseQuestItem> items;

	/**
	 * 
	 */
	public BaseQuest(String title) {
		items = new LinkedList<>();
		req = null;

		this.title = title;
	}

	/**
	 * @return the req
	 */
	@Override
	public BaseQuestItem addReq(IQuestRequirement e) {
		throw new IllegalAccessError();
	}

	/**
	 * Update the quest
	 * 
	 * @return
	 */
	@Override
	public boolean updateLogic() {
		if (status == QuestStatus.FINISH)
			return true;

		// take a break?
		if (status == QuestStatus.PAUSE)
			return false;

		// need to start perform?
		if (status == QuestStatus.PRESTART) {
			for (BaseQuestAction bqa : preActions) {
				bqa.perform();
			}

			status = QuestStatus.RUNNING;
		}

		// ask the items
		for (BaseQuestItem r : items) {
			if (!r.updateLogic()) {
				return false;
			}
		}

		// quest is fullfilled
		for (BaseQuestAction bqa : actions) {
			bqa.perform();
		}

		status = QuestStatus.FINISH;
		return true;
	}

	/**
	 * @return the items
	 */
	public LinkedList<BaseQuestItem> getItems() {
		return items;
	}

	/**
	 * @return the items
	 */
	public LinkedList<BaseQuestItem> getVisibleItems() {
		LinkedList<BaseQuestItem> l = new LinkedList<>();
		boolean nextB = false;
		for (BaseQuestItem r : items) {
			if (r.getStatus() == QuestStatus.FINISH) {
				l.add(r);
			} else if (nextB == false) {
				l.add(r);
				nextB = true;
			} else {
				break;
			}
		}

		return l;
	}

	/**
	 * @return the items
	 */
	public BaseQuest addItem(BaseQuestItem bqi) {
		items.add(bqi);
		return this;
	}

	/**
	 * @return the title
	 */
	@Override
	public String getTitle() {
		return (status == QuestStatus.FINISH ? "(v) " : "") + title;
	}

	/**
	 * @return the desc
	 */
	@Override
	public String getDesc() {
		return desc;
	}

}
