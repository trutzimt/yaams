/**
 * 
 */
package de.qossire.yaams.screens.game.map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.thread.YRepeatTimeAction;

/**
 * @author sven
 *
 */
public abstract class MapTimeObject {

	private final VisLabel label, labelBack;

	/**
	 * 
	 */
	public MapTimeObject(int x, int y, final Image img, int time) {
		// add image
		img.getColor().a = 0f;
		img.setPosition(x * 32, y * 32);

		MapScreen.get().getMap().getStage().addActor(img);

		labelBack = new VisLabel("0%");
		labelBack.setColor(Color.BLACK);
		labelBack.setPosition(x * 32 + 1, y * 32 - 1);
		MapScreen.get().getMap().getStage().addActor(labelBack);

		// add label
		label = new VisLabel("0%");
		label.setPosition(x * 32, y * 32);
		MapScreen.get().getMap().getStage().addActor(label);

		// debug speed
		if (YSettings.isDebugSpeed()) {
			time = 1;
		}

		// add counter
		MapScreen.get().getClock().addTimeAction(new YRepeatTimeAction(0, 100, time) {

			@Override
			public void runStep(long step) {
				img.getColor().a = step * 0.01f;
				if (YSettings.isBiggerGui() && step >= 10) {
					labelBack.setText(step + "%");
					label.setText(step + "%");
				} else {
					labelBack.setText(step + "");
					label.setText(step + "");
				}

				// last step?
				if (step >= 100) {
					label.remove();
					labelBack.remove();
					img.remove();
					perform();
				}
			}
		});

	}

	/**
	 * Run the end position
	 */
	public abstract void perform();

}
