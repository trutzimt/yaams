/**
 *
 */
package de.qossire.yaams.screens.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.code.YLog;
import de.qossire.yaams.screens.base.BaseMenuScreen;

/**
 * @author sven
 *
 */
public class DebugScreen extends BaseMenuScreen {

	// protected String logo;
	protected Image logo;

	private int sX, sY;

	/**
	 * Create it
	 *
	 * @param qossire
	 */
	public DebugScreen(Yaams yaams) {
		super(yaams);

		logo = new Image(YStatic.systemAssets.get("system/logo/logo256.png", Texture.class));
		logo.setPosition(Gdx.graphics.getWidth() / 3 - logo.getWidth() / 2, Gdx.graphics.getHeight() / 3 - logo.getHeight() / 2);
		logo.setVisible(true);

		stage.addActor(logo);

		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(new InputAdapter() {

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				YLog.log("touchDown", screenX, screenY, pointer, button);
				sX = screenX;
				sY = screenY;
				return true;
			}

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				YLog.log("touchUp", screenX, screenY, pointer, button);
				return true;
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				// move it
				int aX = screenX - sX;
				int aY = screenY - sY;
				if (aX == 0 && aY == 0)
					return true;
				logo.setPosition(logo.getX() + aX, logo.getY() - aY);
				// YLog.log(aX, logo.getX());

				sX = screenX;
				sY = screenY;
				// YLog.log("touchDragged", screenX, screenY, pointer);
				return true;
			}
		});

		Gdx.input.setInputProcessor(multiplexer);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

}
