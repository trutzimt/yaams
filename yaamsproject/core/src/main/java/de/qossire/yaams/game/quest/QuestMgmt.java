/**
 * 
 */
package de.qossire.yaams.game.quest;

import java.util.LinkedList;

/**
 * @author sven
 *
 */
public class QuestMgmt {

	private LinkedList<BaseQuest> quests;
	private int questtime;

	/**
	 * 
	 */
	public QuestMgmt() {
		this.quests = new LinkedList<>();
		questtime = 10; // to start a beginning
	}

	/**
	 * Update the logic of the gui
	 */
	public void updateLogic() {
		// has quests?
		if (quests.size() == 0) {
			return;
		}

		// update only every 10 quest times
		questtime += 1;
		if (questtime < 10) {
			return;
		}
		questtime = 0;

		// check all quests
		for (BaseQuest quest : quests) {
			quest.updateLogic();
		}
	}

	public void addQuest(BaseQuest quest) {
		quests.add(quest);
	}

	/**
	 * @return the quests
	 */
	public LinkedList<BaseQuest> getQuests() {
		return quests;
	}

}
