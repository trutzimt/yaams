/**
 * 
 */
package de.qossire.yaams.game.persons.needs;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;

import de.qossire.yaams.code.YLog;
import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.persons.PersonManagement.PersonAnimationNeeds;
import de.qossire.yaams.game.persons.PersonManagement.PersonDir;
import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.game.rooms.BaseRoom;
import de.qossire.yaams.game.rooms.RoomArt;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class NeedJoy extends BaseNeed {

	public NeedJoy() {
		super(PersonNeeds.JOY, 7);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#updateLogic(de.qossire.yaams.
	 * game.persons.Customer, de.qossire.yaams.screens.map.MapScreen, int)
	 */
	@Override
	public boolean updateLogic(final Customer customer, MapScreen map, int deltaTime) {

		// change joy
		customer.addNeed(id, -5);
		customer.addNeed(id, map.getData().getProps(MapProp.DECOR, customer.getGameX(), customer.getGameY()));

		// has a art here
		int x = customer.getGameX();
		int y = customer.getGameY();
		for (final BaseArt art : map.getPlayer().getArtwork().getVisibleArtAt(x, y)) {
			if (!customer.getHasSeeArt().contains(art.getUId()) && customer.getWaitMin() <= 0) {

				// find art pos
				int dX = art.getPos().getX() - x, dY = art.getPos().getY() - y;
				PersonDir d = (dY > 0) ? PersonDir.NORTH : (dY < 0) ? PersonDir.SOUTH : (dX > 0) ? PersonDir.EAST : PersonDir.WEST;

				customer.setWaitMin(NamesGenerator.getIntBetween(2, 20), d, new Runnable() {

					@Override
					public void run() {
						customer.getHasSeeArt().add(art.getUId());
						YSounds.camera();

						// has a room?
						BaseRoom r = MapScreen.get().getPlayer().getRooms().getRoomAt(customer.getGameX(), customer.getGameY());
						float b = 1 + ((r != null && r instanceof RoomArt) ? r.getBonus() / 100f : 0);

						// found his art?
						if (customer.getWantSeeArt().contains(art.getUId())) {
							YLog.log(id, "customer found his art");
							customer.getWantSeeArt().remove((Object) art.getUId());
							MapScreen.get().getMap().getPersonStage().getMgmt().showNeed(customer, PersonAnimationNeeds.SUN);
							customer.addNeed(id, (int) (200 * b));
						} else {
							MapScreen.get().getMap().getPersonStage().getMgmt().showNeed(customer, PersonAnimationNeeds.SINGLENOTE);
							customer.addNeed(id, (int) (50 * b));
						}
						// reset move
						customer.clearGoalPos();
					}

				});
				break;
			}

		}

		// leaving?
		if (customer.getNeed(PersonNeeds.LEAVE) == MAX)
			return false;

		// has max joy?
		if (customer.getNeed(id) >= MAX || customer.getNeed(id) <= -600) {
			// want to leave
			Gdx.app.debug(id.toString(), "has " + customer.getNeed(id) + " want to leave");
			customer.addNeed(PersonNeeds.LEAVE, SET_MAX);
		}
		// has more to see?
		if (customer.getWantSeeArt().size() == 0) {
			// nothing to see more
			Gdx.app.debug("Joy", "has no more art. want to leave");
			customer.addNeed(PersonNeeds.LEAVE, SET_MAX);
			return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#tryToFix(de.qossire.yaams.game.
	 * persons.Customer, de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void tryToFix(Customer customer, MapScreen map) {

		// build list
		LinkedList<YPoint> points = new LinkedList<>();
		for (int i = 0; i < customer.getWantSeeArt().size(); i++) {
			map.getPlayer().getArtwork().getArt(customer.getWantSeeArt().get(i)).addCustomerGoalPoint(points);
		}

		Gdx.app.debug("joy", "go to art random " + points.size());

		// got to next art
		if (!map.getMap().getPersonStage().goToNearestTileFrom(customer, points)) {
			// no way found, customer will leave
			Gdx.app.debug("Joy", "can not find a way to the art, leave");
			customer.addNeed(PersonNeeds.LEAVE, SET_MAX);

		}

	}

}
