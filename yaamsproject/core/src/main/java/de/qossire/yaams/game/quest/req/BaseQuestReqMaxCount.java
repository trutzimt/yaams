/**
 * 
 */
package de.qossire.yaams.game.quest.req;

/**
 * @author sven
 *
 */
public abstract class BaseQuestReqMaxCount implements IQuestRequirementCount {

	protected int count;

	/**
	 * @param count
	 * @param mapScreen
	 */
	public BaseQuestReqMaxCount(int count) {
		super();
		this.count = count;
	}

	protected String getBaseDesc() {
		return "Have max. " + getActCount() + "/" + count + " ";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq() {
		return getActCount() <= count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getMaxCount()
	 */
	@Override
	public int getMaxCount() {
		return count;
	}

}
