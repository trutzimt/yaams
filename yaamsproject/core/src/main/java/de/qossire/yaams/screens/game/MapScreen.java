/**
 *
 */
package de.qossire.yaams.screens.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.game.player.Player;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.level.ScenarioSettings;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.music.YMusic;
import de.qossire.yaams.saveload.SaveLoadManagement;
import de.qossire.yaams.screens.game.events.MapEventHandler;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;
import de.qossire.yaams.screens.game.map.YMapMap;
import de.qossire.yaams.screens.game.map.YTilesetHelper;
import de.qossire.yaams.screens.game.thread.YClock;
import de.qossire.yaams.screens.game.thread.YThread;

/**
 * @author sven
 *
 */
public class MapScreen implements Screen, IScreen {

	private static MapScreen mapScreen;

	private Yaams yaams;
	private BaseScenario scenario;
	private YMapMap map;
	private MapGui mapgui;
	private Player player;
	private YTilesetHelper tilesethelper;
	private YThread thread;
	private GameData data;
	private ScenarioSettings settings;
	private MapInputAdapter inputs;
	private MapEventHandler events;

	// private SpriteBatch batch;
	// private Texture img;

	// state management
	private boolean firstRun;

	/**
	 * Create new screen
	 *
	 * @param qossire
	 * @param player
	 */
	public MapScreen(Yaams yaams, BaseScenario scenario) {
		YMusic.startInGameMusic();

		mapScreen = this;
		inputs = new MapInputAdapter(this);
		this.scenario = scenario;
		events = new MapEventHandler();
		data = new GameData();
		settings = new ScenarioSettings();
		// batch = new SpriteBatch();
		this.yaams = yaams;
		this.player = new Player(this);
		thread = new YThread(this);

		map = new YMapMap(this);
		mapgui = new MapGui(this);

		managementInput();

		map.setCameraZoom(YSettings.isBiggerGui() ? 0.5f : 1);

		this.player.init();
		this.data.init();
		this.tilesethelper = new YTilesetHelper(this);

		// start it
		scenario.start(this);

		// add save
		if (YSettings.isAutosave() && settings.isActive(ScenConf.LOADSAVE)) {
			events.register(MapEventHandlerTyp.NEXTDAY, new MapEventHandlerAction() {

				@Override
				public boolean perform(Object[] objects) {
					SaveLoadManagement.save("autosave", MapScreen.this, false);
					return false;
				}
			});
		}

		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_SAVE, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				data.preSave();
				return false;
			}
		});

		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_LOAD, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				data.afterLoad();
				return false;
			}
		});

	}

	/**
	 * Add all listener
	 */
	private void managementInput() {
		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(mapgui.getStage());
		multiplexer.addProcessor(inputs);

		yaams.getPlattform().addInputs(this, multiplexer);

		Gdx.input.setInputProcessor(multiplexer);
	}

	/**
	 * Check if the coordinates outside the map
	 *
	 * @param x
	 * @param y
	 * @return
	 * @deprecated Use
	 *             {@link de.qossire.yaams.screens.game.map.YTilesetHelper#validePosition(de.qossire.yaams.screens.game.MapScreen,int,int)}
	 *             instead
	 */
	@Deprecated
	public boolean validePosition(int x, int y) {
		return tilesethelper.isValidePosition(x, y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {

		// start of the game?
		if (!firstRun) {
			thread.start();
			firstRun = true;
		}

		Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		map.render(delta);
		mapgui.render(delta);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		mapgui.resize(width, height);
		map.resize(width, height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() {
		// save the game
		SaveLoadManagement.save("temp", this, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	@Override
	public void dispose() {
		// batch.dispose();
		map.dispose();
		mapgui.dispose();
	}

	/**
	 * @return the map
	 */
	public YMapMap getMap() {
		return map;
	}

	/**
	 * @return the mapgui
	 */
	public MapGui getMapgui() {
		return mapgui;
	}

	/**
	 * @return the qossire
	 */
	@Override
	public Yaams getYaams() {
		return yaams;
	}

	/**
	 * @return the scenario
	 */
	public BaseScenario getScenario() {
		return scenario;
	}

	/**
	 * Call after a game is loaded
	 */

	public void load(GameData data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.base.IScreen#getStage()
	 */
	@Override
	public Stage getStage() {
		return mapgui.getStage();
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @param player
	 *            the player to set
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * @return the tilesethelper
	 */
	public YTilesetHelper getTilesetHelper() {
		return tilesethelper;
	}

	/**
	 * @return the thread
	 */
	public YClock getClock() {
		return thread.getClock();
	}

	/**
	 * @return the mapScreen
	 */
	public static MapScreen get() {
		return mapScreen;
	}

	/**
	 * Stop the game
	 */
	public void stop() {
		thread.stop();
	}

	/**
	 * Will called, after the thread stoped
	 */
	public static void finishAfterStop() {
		// reset it
		mapScreen = null;

	}

	/**
	 * @return the data
	 */
	public GameData getData() {
		return data;
	}

	/**
	 * @return the settings
	 */
	public ScenarioSettings getSettings() {
		return settings;
	}

	/**
	 * @return the inputs
	 */
	public MapInputAdapter getInputs() {
		return inputs;
	}

	/**
	 * @return the events
	 */
	public MapEventHandler getEvents() {
		return events;
	}
}
