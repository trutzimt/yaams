/**
 * 
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextField;

import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public abstract class YRandomField extends VisTable {

	private VisTextField names;
	private VisImageButton btn;

	/**
	 * @param text
	 */
	public YRandomField(String text) {
		super();

		names = new VisTextField(text);
		names.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				saveText(names.getText());

			}
		});
		add(names).fill().expand().grow();

		btn = new VisImageButton(YIcons.getIconD(YIType.RANDOM));
		btn.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				YSounds.random();
				names.setText(getRndText());
				saveText(names.getText());

			}
		});
		new Tooltip.Builder("Create a new random entry.").target(btn).build();
		add(btn).align(Align.right);

		// TODO Auto-generated constructor stub
	}

	/**
	 * Generate the random text
	 * 
	 * @return
	 */
	protected abstract String getRndText();

	/**
	 * Generate the random text
	 * 
	 * @return
	 */
	public String getText() {
		return names.getText();
	}

	/**
	 * Save the text
	 * 
	 * @return
	 */
	protected abstract void saveText(String text);

}
