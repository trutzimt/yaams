/**
 *
 */
package de.qossire.yaams.level.single;

import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.art.StatureArt;
import de.qossire.yaams.game.build.BuildManagement.BuildCat;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class DebugScenario extends BaseScenario {

	/**
	 */
	public DebugScenario() {
		super("debug", "Debug Game", "map/20.tmx");

		// camp = ScenarioManagement.getCampaign("single");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(MapScreen screen) {
		super.start(screen);

		screen.getPlayer().addMoney(NamesGenerator.getIntBetween(200_000, 1_000_000));

		// add some art
		for (int i = 0, l = NamesGenerator.getIntBetween(3, 10); i < l; i++) {
			screen.getPlayer().getArtwork().addArt(screen.getPlayer().getArtwork().generateArt(), ArtStatus.DEPOT);
		}

		screen.getPlayer().getMuseum().setOpenFrom(7);
		screen.getPlayer().getMuseum().setOpenTo(17);

		YStatic.buildings.get(BuildCat.BASE).get(0).buildAt(15, 10, screen);
		YStatic.buildings.get(BuildCat.BASE).get(0).buildAt(16, 10, screen);
		YStatic.buildings.get(BuildCat.BASE).get(0).buildAt(17, 10, screen);
		YStatic.buildings.get(BuildCat.BASE).get(0).buildAt(15, 11, screen);
		YStatic.buildings.get(BuildCat.BASE).get(0).buildAt(15, 12, screen);
		YStatic.buildings.get(BuildCat.BASE).get(0).buildAt(15, 9, screen);

		YStatic.buildings.get(BuildCat.BASE).get(2).buildAt(5, 5, screen);
		YStatic.buildings.get(BuildCat.BASE).get(3).buildAt(19, 12, screen);

		// vending maschine
		YStatic.buildings.get(BuildCat.BASE).get(6).buildAt(16, 10, screen);

		BaseArt a = screen.getPlayer().getArtwork().generateArt();
		BaseArt b = new StatureArt();
		b.setUid(a.getUId());
		screen.getPlayer().getArtwork().addArt(b, ArtStatus.DEPOT);
		b.buildAt(15, 11);

		// screen.getPlayer().setPrice(200);

		// show dialogs
		// screen.getMapgui().getDialogs().add(YCharacter.FINANCE, "finance", true);
		// screen.getMapgui().getDialogs().add(YCharacter.HELPER, "helper", true);
		// screen.getMapgui().getDialogs().add(YCharacter.TOWN, "town", false);

	}

}
