/**
 *
 */
package de.qossire.yaams.level.test;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.quest.BaseQuest;
import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.QuestActionWin;
import de.qossire.yaams.game.quest.req.QuestReqMinProp;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class TestScenario extends BaseScenario {

	/**
	 */
	public TestScenario(int id) {
		super("test" + id, "test" + id, "map/40.tmx");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(final MapScreen screen) {
		super.start(screen);
		screen.getPlayer().addMoney(1_000_000);

		// add quests
		BaseQuest b = new BaseQuest("Build the museum");
		BaseQuestItem i;

		// win
		i = new BaseQuestItem();
		i.addReq(new QuestReqMinProp(MapProp.STREET, 1, 1));
		i.addAction(new QuestActionWin());
		b.addItem(i);

		screen.getPlayer().getQuests().addQuest(b);

	}

}
