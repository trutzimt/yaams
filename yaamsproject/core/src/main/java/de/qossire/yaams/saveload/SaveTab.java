/**
 * 
 */
package de.qossire.yaams.saveload;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YSplitTab;

/**
 * @author sven
 *
 */
public class SaveTab extends YSplitTab {

	private VisTextButton delete, save;
	private IScreen screen;
	private boolean exitAfterSave;

	public SaveTab(IScreen screen, boolean exitAfterSave) {
		super("Save Game", "At the moment you have no save games.");

		this.screen = screen;
		this.exitAfterSave = exitAfterSave;

		// // add warning
		// VisTable t = new VisTable(true);
		// t.add(YIcons.getIcon("hazard-sign")).align(Align.left);
		// VisLabel l = new VisLabel(
		// "Warning! Support for saving is very rudimentary and prone to error./n Only
		// general information, the map and in part of the artworks are stored.");
		// // t.add(l);// .growX();
		// topBar = t;
		resetElements();

		// add button
		save = new VisTextButton("Create new save");
		save.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				doubleClickElement(null);

			}
		});
		buttonBar.addActor(save);

		delete = new VisTextButton("Delete it");
		delete.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				for (int i = 0; i < elements.size(); i++) {
					if (elements.get(i).getUserObject() == active) {
						SaveLoadManagement.delete(((YSaveInfo) active).getFile().nameWithoutExtension());
						elements.remove(i);
						break;
					}
				}
				reset();
				YSounds.click();
				rebuild();

			}
		});
		buttonBar.addActor(delete);

		// add warning
		VisImageTextButton v = new VisImageTextButton("Warning", YIcons.getIconD(YIType.WARNING));
		v.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				Dialogs.showOKDialog(SaveTab.this.screen.getStage(), "Warning!",
						"Support for saving is very rudimentary and prone to error.\n Only general information, the map and artworks are stored.");

			}
		});
		buttonBar.addActor(v);

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		resetElements();
		super.rebuild();
	}

	public void resetElements() {
		elements.clear();
		// add all elements
		Json j = new Json();
		// add saves
		for (FileHandle f : SaveLoadManagement.getSaves()) {
			YSaveInfo q = j.fromJson(YSaveInfo.class, f.read());
			if (q.isHidden())
				continue;

			q.setFile(f);
			// TODO better icon
			addElement(q.getTitle(screen), YIcons.getIconD(YIType.WARNING), q);

		}
		addElement("New Savegame", YIcons.getIconD(YIType.WARNING), null);
	}

	/**
	 * Reset the status
	 */
	@Override
	protected void reset() {
		super.reset();
		delete.setDisabled(true);
		save.setText("Create new save");
	}

	/**
	 * Place the selected artwork
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		YSounds.click();
		// create new save?
		if ((btn != null && btn.getUserObject() == null) || active == null) {
			SaveLoadManagement.save((FileHandle) null, (MapScreen) screen, false);
		} else {
			SaveLoadManagement.save(((YSaveInfo) active).getFile(), (MapScreen) screen, false);
		}

		if (exitAfterSave) {
			Gdx.app.exit();
		} else {
			window.close();
		}

	}

	@Override
	protected void clickElement(Button btn) {
		save.setText(btn.getUserObject() == null ? "Create new save" : "Overwrite " + ((YSaveInfo) btn.getUserObject()).getTitle(screen));
		delete.setDisabled(btn.getUserObject() == null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(Button btn) {
		if (btn.getUserObject() == null) {
			return new VisLabel("");
		}
		return ((YSaveInfo) btn.getUserObject()).getInfoPanel(screen);
	}
}