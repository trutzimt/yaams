/**
 *
 */
package de.qossire.yaams.base;

import com.badlogic.gdx.InputMultiplexer;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTable;

/**
 * Management Plattform code
 *
 * @author sven
 *
 */
public interface IPlattform {

	/**
	 * Format the long to a readable date
	 *
	 * @param date
	 * @return
	 */
	public String formatDate(long date);

	/**
	 * Show a error message dialog for the user
	 *
	 * @param message
	 */
	public void showErrorDialog(String message);

	/**
	 * This method will called at the beginn of a game to add plattform spezific
	 * code
	 * 
	 * @param mapScreen
	 * @param multiplexer
	 */
	public void addInputs(MapScreen mapScreen, InputMultiplexer multiplexer);

	/**
	 * This method will called at the beginning
	 */
	public void startUpCode();

	/**
	 * Allows the platform to add platform specific graphic options
	 * 
	 * @param cont
	 */
	public void addGraphicOptions(YTable cont);

}
