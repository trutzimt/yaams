package de.qossire.yaams.game.quest.req;

import de.qossire.yaams.game.museum.funding.Fundings;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqMinFund extends BaseQuestReqMinCount {

	/**
	 * @param prop
	 * @param val
	 */
	public QuestReqMinFund(int count) {
		super(count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + "fundings";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getActCount()
	 */
	@Override
	public int getActCount() {
		int i = 0;

		for (Fundings f : MapScreen.get().getPlayer().getFundings().get()) {
			if (f.isActive()) {
				i++;
			}
		}

		return i;
	}

}