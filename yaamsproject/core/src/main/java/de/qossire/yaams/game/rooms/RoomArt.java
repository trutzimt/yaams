/**
 * 
 */
package de.qossire.yaams.game.rooms;

import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.rooms.RoomMgmt.RoomTyp;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class RoomArt extends BaseRoom {

	private boolean sameArt;
	private BaseArt firstArt;
	private boolean sameArtEpoch;
	private int artCount;

	/**
	 * @param iD
	 */
	public RoomArt(int uid, int x, int y) {
		super(RoomTyp.ART, uid, x, y);
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	@Override
	public VisTable getInfoPanel() {
		YTable table = (YTable) super.getInfoPanel();

		table.addL("Floor", hasFloor ? "+ everywhere with a floor" : "- floor is missing");
		table.addL("Art typ", sameArt ? "+ same art type" : "- Different art types");
		table.addL("Art epoch", sameArtEpoch ? "+ same art epoch" : "- Different age epoch");
		table.addL("Count of Art", artCount < 5 ? "- very few works of art" : (artCount > 10 ? "- too many works of art" : "+ good number of works of art"));

		table.addL("Joy bonus condition",
				bonus == 0 ? "Customers have no bonus when viewing the artwork." : "Customers have a " + bonus + "% bonus when viewing the artwork.");

		return table;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.rooms.BaseRoom#getIcon()
	 */
	@Override
	public TextureRegionDrawable getIcon() {
		return YIcons.getOverlayIcon(35, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.rooms.BaseRoom#updateLogic()
	 */
	@Override
	public void updateLogic() {
		// check it
		if (!toCheck) {
			return;
		}

		// reset
		firstArt = null;
		sameArtEpoch = false;
		sameArt = false;

		super.updateLogic();

		// calc
		bonus = hasFloor ? 5 : -5;
		bonus += sameArt ? 5 : -5;
		bonus += artCount >= 5 && artCount <= 10 ? 10 : (artCount < 5 ? -artCount : -(artCount - 10));
		bonus += sameArtEpoch ? 20 : -20;
		bonus = Math.max(bonus, 0);
	}

	/**
	 * Look if everything exist
	 * 
	 * @param x
	 * @param y
	 * @param fields
	 */
	@Override
	protected void checkTile(int x, int y, boolean[][] fields) {
		// was visit?
		if (!MapScreen.get().getTilesetHelper().isValidePosition(x, y) || fields[x][y]) {
			return;
		}
		super.checkTile(x, y, fields);

		if (firstArt != null && sameArt == false && sameArtEpoch == false)
			return;

		// search for art
		for (BaseArt b : MapScreen.get().getPlayer().getArtwork().getVisibleArtAt(x, y)) {
			if (firstArt == null) {
				firstArt = b;
				sameArt = true;
				sameArtEpoch = true;
				continue;
			}

			// check it
			if (sameArt && firstArt.getTyp() != b.getTyp()) {
				sameArt = false;
			}

			if (sameArtEpoch && firstArt.getEpochid() != b.getEpochid()) {
				sameArtEpoch = false;
			}
		}
	}

}
