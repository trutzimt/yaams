/**
 * 
 */
package de.qossire.yaams.base;

import com.badlogic.gdx.Gdx;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.qossire.yaams.screens.base.ErrorScreen;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class YConfig {

	public static final double VERSION = 0.1;

	private static Yaams yaams;

	public static void init(Yaams yaams) {
		YConfig.yaams = yaams;
	}

	/**
	 * Show an error
	 *
	 * @param t
	 */
	public static void error(final Throwable t, boolean fatal) {
		Gdx.app.error("yaaMs", t.getMessage(), t);

		if (yaams == null) {
			// can do nothing
			Gdx.app.error("yaaMs", "Can not show error message and exit game");
			Gdx.app.exit();
			return;
		}

		// try to save
		if (yaams.getScreen() != null && yaams.getScreen() instanceof MapScreen) {
			try {
				// TODO FIX
				// SaveLoadManagement.save("crash", (MapScreen) yaams.getScreen(), true);
			} catch (Throwable e) {
				Gdx.app.error("yaaMs", e.getMessage(), e);
			}
		}

		// Arrays.toString(t.getStackTrace())
		if (!fatal && yaams.getScreen() instanceof IScreen) {
			try {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {

						String title = t.getClass() + " " + t.getLocalizedMessage();
						if (t instanceof Exception) {
							Dialogs.showErrorDialog(((IScreen) yaams.getScreen()).getStage(), title, (Exception) t);
						} else {
							// build message
							String mess = "\n" + t.getMessage();
							for (int i = 0; i < 5; i++) {
								mess += "\n" + t.getStackTrace()[i];
							}
							Dialogs.showErrorDialog(((IScreen) yaams.getScreen()).getStage(), title, mess);
						}

					}
				});
				return;
			} catch (Exception e) {
				Gdx.app.error("yaaMs", e.getMessage(), e);
			}
		}

		if (yaams.getScreen() != null && yaams.getScreen() instanceof ErrorScreen) {
			yaams.getPlattform().showErrorDialog(t.getClass() + ":" + t.getMessage());
		} else {
			Gdx.app.postRunnable(new Runnable() {

				@SuppressWarnings("deprecation")
				@Override
				public void run() {
					yaams.setScreen(new ErrorScreen(t, yaams.getScreen()));
				}
			});

		}
	}
}
