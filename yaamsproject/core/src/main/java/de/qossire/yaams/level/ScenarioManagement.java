/**
 *
 */
package de.qossire.yaams.level;

import java.util.Set;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.code.YHashMap;
import de.qossire.yaams.level.single.SingleScenarioCampaign;
import de.qossire.yaams.level.test.TestCampaign;
import de.qossire.yaams.level.tutorial.TutorialCampaign;

/**
 * @author sven
 *
 */
public class ScenarioManagement {

	/**
	 * Load all campaigns
	 */
	public static void init() {
		YStatic.campaigns = new YHashMap<>("campaigns");

		addCampaign(new SingleScenarioCampaign());
		addCampaign(new TutorialCampaign());

		if (YSettings.isDebug())
			addCampaign(new TestCampaign());
	}

	/**
	 * Add the campaign
	 *
	 * @param cam
	 */
	public static void addCampaign(BaseCampaign cam) {
		YStatic.campaigns.addS(cam);
	}

	/**
	 * Exist Campaign
	 *
	 * @param id
	 * @return
	 */
	public static boolean existCampaign(String id) {
		// exist campaign?
		return YStatic.campaigns.containsKey(id);
	}

	/**
	 * Get a specific Campaign
	 *
	 * @param id
	 * @return
	 */
	public static BaseCampaign getCampaign(String id) {
		return YStatic.campaigns.getS(id);
	}

	/**
	 * Get the Campaign Ids
	 *
	 * @return
	 */
	public static Set<String> getCampaignIds() {
		return YStatic.campaigns.keySet();
	}
}
