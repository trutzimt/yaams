/**
 * 
 */
package de.qossire.yaams.game.museum.rank;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class RankArt extends BaseRank {

	/**
	 * @param typ
	 * @param title
	 */
	public RankArt() {
		super(ERank.ART, "Art");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return YIcons.getIconD(YIType.ARTWORK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getDesc()
	 */
	@Override
	public String getDesc() {
		return getCount() == -1 ? notImplemented
				: "For the next level you need " + getCount() + " artworks. At the moment "
						+ MapScreen.get().getPlayer().getArtwork().getArtsCount(ArtStatus.DISPLAYED) + " works of art are presented in the museum.";
	}

	/**
	 * Return the number of needed artworks
	 * 
	 * @return
	 */
	private int getCount() {
		// TODO add qualiry
		switch (level) {
		case 0:
			return 5;
		case 1:
			return 15;
		case 2:
			return 30;
		default:
			return -1;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#checkLevel()
	 */
	@Override
	public int checkLevel() {

		return MapScreen.get().getPlayer().getArtwork().getArtsCount(ArtStatus.DISPLAYED) >= getCount() ? level + 1 : level;

	}

}
