/**
 * 
 */
package de.qossire.yaams.game.action;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.persons.PersonManagement.PersonAnimationNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class CustomerGameAction extends BaseGameAction {

	/**
	 * @param type
	 * @param title
	 */
	public CustomerGameAction() {
		super("customer", "Place Customer");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.action.BaseGameAction#performClick(int, int,
	 * de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void performClick(int x, int y, MapScreen map) {
		if (map.getMap().getPersonStage().isFree(x, y)) {
			Customer c = new Customer(x, y, map.getMap().getPersonStage(), map);
			map.getMap().getPersonStage().addPerson(c);
			MapScreen.get().getMap().getPersonStage().getMgmt().showNeed(c, PersonAnimationNeeds.LIGHTBULB);

			YSounds.click();
		} else {
			YSounds.buzzer();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.action.BaseGameAction#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return YIcons.getIconD(YIType.MAINMENU);
	}

}
