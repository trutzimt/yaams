/**
 *
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.widget.VisTextButton;

/**
 * @author sven
 *
 */
public abstract class YTextButton extends VisTextButton {

	/**
	 * 
	 * @param title
	 */
	public YTextButton(String title) {
		super(title);

		addListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				perform();

			}
		});

	}

	public abstract void perform();

}
