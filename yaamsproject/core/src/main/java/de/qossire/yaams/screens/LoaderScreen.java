/**
 *
 */
package de.qossire.yaams.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.VisUI.SkinScale;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.level.ScenarioManagement;
import de.qossire.yaams.music.YMusic;
import de.qossire.yaams.screens.base.BaseLoaderScreen;
import de.qossire.yaams.screens.menu.MainMenuScreen;
import de.qossire.yaams.ui.YIcons;

/**
 * @author sven
 *
 */
public class LoaderScreen extends BaseLoaderScreen {

	/**
	 *
	 */
	public LoaderScreen(Yaams yaams) {
		super(yaams, "Loading yaaMs ");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.menu.BaseLoaderScreen#nextScreen()
	 */
	@Override
	public void nextScreen() {

		if (YSettings.isDebug()) {
			// yaams.setScreen(new GameLoaderScreen(yaams,
			// YStatic.campaigns.getS("tut").getScenario("tut1")));
			// yaams.switchScreen(new GameLoaderScreen(yaams,
			// YStatic.campaigns.getS("single").getScenario("debug")));
			// yaams.setScreen(new DebugScreen(yaams));
			// return;

		}
		yaams.switchScreen(new MainMenuScreen(yaams));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		super.render(delta);

		if (text == null)
			return;
		text.setText(title + ((int) (YStatic.systemAssets.getProgress() * 100)) + "%");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.menu.BaseLoaderScreen#onceCode()
	 */
	@Override
	public void onceCode() {

		if (YSettings.isDebug()) {
			Gdx.app.setLogLevel(Application.LOG_DEBUG);
		}

		// Gdx.app.log("Qoss",Gdx.graphics.getWidth()+" "+Gdx.graphics.getHeight());

		// load skin. Big or small?
		if (VisUI.isLoaded()) {
			VisUI.dispose();
		}

		if (!YSettings.getPref().contains("biggerGui")) {
			if (Gdx.graphics.getWidth() > 1000 && Gdx.graphics.getHeight() > 700) {
				YSettings.getPref().putBoolean("biggerGui", true);
				if (Gdx.files.internal("64").exists())
					YSettings.getPref().putInteger("guiScale", 64);
			} else {
				YSettings.getPref().putBoolean("biggerGui", false);
			}
		}
		VisUI.load(YSettings.isBiggerGui() ? SkinScale.X2 : SkinScale.X1);
		createLabel();

		YStatic.music = new YMusic();
		YMusic.mainmenu();

		ScenarioManagement.init();

		yaams.getPlattform().startUpCode();

		// add loading ressources
		// YStatic.systemAssets.load("system/logo/logo256.png", Texture.class);
		YStatic.systemAssets.load("system/icons/IconSet.png", Texture.class);
		YStatic.systemAssets.load("system/logo/logo" + (YSettings.isBiggerGui() ? "48.png" : "32.png"), Texture.class);

		// add sounds
		for (String s : new String[] { "Item", "Cursor", "72714__jankoehl__shutter-photo", "speechClosing", "Book2", "010-System10", "004-System04",
				"003-System03", "006-System06" }) {
			YStatic.systemAssets.load("system/sound/" + s + ".mp3", Sound.class);
		}
		YIcons.loadIconsS();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.base.BaseLoaderScreen#isFinishLoading()
	 */
	@Override
	public boolean isFinishLoading() {
		return YStatic.systemAssets.update();
	}

}
