package de.qossire.yaams.game.museum;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel;
import com.kotcrab.vis.ui.widget.spinner.Spinner;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.build.BuildManagement;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.museum.funding.FundingTab;
import de.qossire.yaams.game.museum.rank.RankTab;
import de.qossire.yaams.game.player.Stats.EStats;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.GameData.GDC;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.TextHelper;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YNotification;
import de.qossire.yaams.ui.YRandomField;
import de.qossire.yaams.ui.YTabWindow;
import de.qossire.yaams.ui.YTable;

public class MuseumWindow extends YTabWindow {

	protected MapScreen map;

	public MuseumWindow(final MapScreen map) {
		super(map.getPlayer().getMuseum().getName());
		this.map = map;

		tabbedPane.add(new OverviewTab());
		if (map.getSettings().isActive(ScenConf.MONEY)) {
			tabbedPane.add(new FinanceTab());
			if (map.getSettings().isActive(ScenConf.FUNDINGS))
				tabbedPane.add(new FundingTab());
		}
		if (map.getSettings().isActive(ScenConf.RANG))
			tabbedPane.add(new RankTab());

		buildIt();

		addTitleIcon(YIcons.getIconI(YIType.MUSEUM));
	}

	public class OverviewTab extends Tab {

		public OverviewTab() {
			super(false, false);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
		 */
		@Override
		public String getTabTitle() {
			return "Overview";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
		 */
		@Override
		public Table getContentTable() {

			YTable content = new YTable();

			content.addL("Museum", new YRandomField(map.getPlayer().getMuseum().getName()) {

				@Override
				protected void saveText(String text) {
					map.getPlayer().getMuseum().setName(text);

				}

				@Override
				protected String getRndText() {
					return NamesGenerator.getMuseumName(map.getPlayer().getName(), map.getPlayer().getTown().getName());
				}
			});

			// add time sliders
			final IntSpinnerModel openFromModel = new IntSpinnerModel(map.getPlayer().getMuseum().getOpenFrom(), 8, 18, 1);
			final Spinner openFromSpinner = new Spinner("From", openFromModel);
			openFromSpinner.setVisible(map.getPlayer().getMuseum().getOpenFrom() >= 0);

			final IntSpinnerModel openToModel = new IntSpinnerModel(map.getPlayer().getMuseum().getOpenTo(), 8, 18, 1);
			final Spinner openToSpinner = new Spinner("Clock until", openToModel);
			openToSpinner.addListener(new YChangeListener(false) {
				@Override
				public void changedY(Actor actor) {
					map.getPlayer().getMuseum().setOpenTo(openToModel.getValue());
					openFromModel.setMax(openToModel.getValue());
				}
			});
			openToSpinner.setVisible(map.getPlayer().getMuseum().getOpenFrom() >= 0);

			openFromSpinner.addListener(new YChangeListener(false) {
				@Override
				public void changedY(Actor actor) {
					map.getPlayer().getMuseum().setOpenFrom(openFromModel.getValue());
					openToModel.setMin(openFromModel.getValue());
				}
			});

			// add open checkbox
			final VisCheckBox openEnable = new VisCheckBox("", map.getPlayer().getMuseum().getOpenFrom() >= 0);
			openEnable.addCaptureListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					map.getPlayer().getMuseum().setOpenFrom(openEnable.isChecked() ? 9 : -1);
					openFromModel.setValue(map.getPlayer().getMuseum().getOpenFrom());
					openFromSpinner.setVisible(openEnable.isChecked());

					map.getPlayer().getMuseum().setOpenTo(openEnable.isChecked() ? 17 : -1);
					openToModel.setValue(map.getPlayer().getMuseum().getOpenTo());
					openToSpinner.setVisible(openEnable.isChecked());

					// Check if the museum is right open
					if (openEnable.isChecked()) {
						// has a street?
						if (map.getData().getAllPropsPoints(MapProp.STREET, 1).size() == 0) {
							map.getMapgui()
									.addNotification(new YNotification("No Connection",
											"There is no (finished) road on the edge, so no visitors can come. First build a street and then open the museum.",
											new Image(YIcons.getBuildIcon(BuildManagement.STREET, true))));
						}
						// has some art?
						if (map.getPlayer().getArtwork().getArtsCount(ArtStatus.DISPLAYED) == 0) {
							map.getMapgui()
									.addNotification(new YNotification("Artworks",
											"For guests to come to the museum, art should also be exhibited. Simply place it over the Artwork>Depot window.",
											YIcons.getIconI(YIType.ARTWORK)));
						}
					}

				}
			});

			VisTable opening = new VisTable();
			opening.add(openEnable);
			opening.add(openFromSpinner);
			opening.add(openToSpinner);

			content.addL("Open", opening);
			content.addL("Director", map.getPlayer().getName());
			content.addL("Town", map.getPlayer().getTown().getName());
			content.add().growY();

			return content;
		}

	}

	public class FinanceTab extends Tab {

		public FinanceTab() {
			super(false, false);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
		 */
		@Override
		public String getTabTitle() {
			return "Finance";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
		 */
		@Override
		public Table getContentTable() {

			YTable content = new YTable();
			content.addH("Prices");
			final IntSpinnerModel intModel = new IntSpinnerModel(map.getData().getPI(GDC.TICKET_PRICE), 0, 200, 1);
			Spinner intSpinner = new Spinner("", intModel);
			intSpinner.addListener(new YChangeListener(false) {
				@Override
				public void changedY(Actor actor) {
					map.getData().setP(GDC.TICKET_PRICE, intModel.getValue());
				}
			});
			content.addL("Entry", intSpinner);

			// add drinking prices?
			if (map.getData().getAllPropsPoints(MapProp.DRINK, 1).size() > 0) {
				final IntSpinnerModel waterModel = new IntSpinnerModel(map.getData().getPI(GDC.DRINK_PRICE), 0, 200, 1);
				intSpinner = new Spinner("", waterModel);
				intSpinner.addListener(new YChangeListener(false) {
					@Override
					public void changedY(Actor actor) {
						map.getData().setP(GDC.DRINK_PRICE, waterModel.getValue());
					}
				});
				content.addL("Water Bottle", intSpinner);

				final IntSpinnerModel foodModel = new IntSpinnerModel(map.getData().getPI(GDC.FOOD_PRICE), 0, 200, 1);
				intSpinner = new Spinner("", foodModel);
				intSpinner.addListener(new YChangeListener(false) {
					@Override
					public void changedY(Actor actor) {
						map.getData().setP(GDC.DRINK_PRICE, foodModel.getValue());
					}
				});
				content.addL("Food", intSpinner);
			}

			content.addH("Stats");
			if (map.getPlayer().getStats().getYesterday(EStats.MONEY) >= 0)
				content.addL("Yesterday", "" + TextHelper.getMoneyString(map.getPlayer().getStats().getYesterday(EStats.MONEY)));
			content.addL("Today", "" + TextHelper.getMoneyString((int) map.getPlayer().getMoney()));

			content.add().growY();

			return content;
		}

	}
}
