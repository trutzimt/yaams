package de.qossire.yaams.game.quest.req;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqMinProp extends BaseQuestReqMinCount {

	private MapProp prop;
	private int minVal;

	/**
	 * @param prop
	 * @param val
	 */
	public QuestReqMinProp(MapProp prop, int count, int minVal) {
		super(count);
		this.prop = prop;
		this.minVal = minVal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + prop.getNamel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getActCount()
	 */
	@Override
	public int getActCount() {
		return MapScreen.get().getData().getAllPropsPoints(prop, minVal).size();
	}

}