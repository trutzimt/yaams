/**
 * 
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.music.YSounds;

/**
 * @author sven
 *
 */
public abstract class YChangeListener extends ChangeListener {

	protected boolean playSound;

	/**
	 * 
	 */
	public YChangeListener() {
		this(true);
	}

	/**
	 * 
	 */
	public YChangeListener(boolean playSound) {
		this.playSound = playSound;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.badlogic.gdx.scenes.scene2d.utils.ChangeListener#changed(com.badlogic.gdx
	 * .scenes.scene2d.utils.ChangeListener.ChangeEvent,
	 * com.badlogic.gdx.scenes.scene2d.Actor)
	 */
	@Override
	public void changed(ChangeEvent event, Actor actor) {
		try {
			if (playSound)
				YSounds.click();
			changedY(actor);
		} catch (Exception e) {
			YConfig.error(e, false);
		}

	}

	public abstract void changedY(Actor actor);

}
