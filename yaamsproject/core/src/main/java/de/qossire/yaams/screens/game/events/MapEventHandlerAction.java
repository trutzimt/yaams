/**
 * 
 */
package de.qossire.yaams.screens.game.events;

/**
 * @author sven
 *
 */
public interface MapEventHandlerAction {

	/**
	 * Perform the action
	 * 
	 * @param objects
	 *            get the req. objects, if exist
	 * @return true > finish & remove it
	 */
	public boolean perform(Object[] objects);
}
