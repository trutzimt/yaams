/**
 * 
 */
package de.qossire.yaams.game.museum.rank;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class RankOverview extends BaseRank {

	/**
	 * @param typ
	 * @param title
	 */
	public RankOverview() {
		super(ERank.OVERVIEW, "Overview");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return YIcons.getIconD(YIType.MUSEUM);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Counts all levels and represent the average";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#checkLevel()
	 */
	@Override
	public int checkLevel() {
		int all = 0;

		for (BaseRank rang : MapScreen.get().getPlayer().getRang().getRanks()) {
			if (rang.getTyp() != ERank.OVERVIEW) {
				all += rang.getLevel();
			}
		}

		return all / (MapScreen.get().getPlayer().getRang().getRanks().size() - 1);

	}

}
