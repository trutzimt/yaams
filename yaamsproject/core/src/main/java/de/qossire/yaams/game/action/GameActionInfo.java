/**
 * 
 */
package de.qossire.yaams.game.action;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.persons.BasePerson;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class GameActionInfo extends BaseGameAction {

	/**
	 * @param type
	 * @param title
	 */
	public GameActionInfo() {
		super("info", "Get Information");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.action.BaseGameAction#performClick(int, int,
	 * de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void performClick(int x, int y, MapScreen map) {

		map.getStage().addActor(new FieldWindow(map, x, y));
		YSounds.click();

	}

	@Override
	public void mouseMoved(int x, int y, MapScreen mapScreen) {

		// show person?
		BasePerson person = mapScreen.getMap().getPersonStage().getPersonMatrix(x, y);

		if (person != null && person instanceof Customer) {
			Customer customer = (Customer) person;

			mapScreen.getMapgui().setInfo(customer.getPersonName(), new TextureRegionDrawable((customer.getActor().getPreviewRegion())));

			return;
		}

		// show art?
		final BaseArt art = mapScreen.getPlayer().getArtwork().getArtAt(x, y);
		if (art != null) {
			mapScreen.getMapgui().setInfo(art.getName(), art.getIcon(true));

			return;
		}

		// show it
		mapScreen.getMapgui().setInfo(null, null);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.action.BaseGameAction#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return YIcons.getIconD(YIType.INFOABOUT);
	}

}
