/**
 *
 */
package de.qossire.yaams.ui.actions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.ui.YChangeListener;

/**
 * @author sven
 *
 */
public class FeedbackAction extends QDefaultAction {

	public FeedbackAction() {
		super("Send Feedback");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.ui.actions.QDefaultAction#getAction()
	 */
	@Override
	public YChangeListener getAction(final Yaams yaams) {
		return new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				YSounds.click();
				Gdx.net.openURI("https://yaams.de/feedback");

			}
		};
	}

}