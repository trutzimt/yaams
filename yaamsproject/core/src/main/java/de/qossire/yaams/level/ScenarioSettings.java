/**
 * 
 */
package de.qossire.yaams.level;

import de.qossire.yaams.screens.game.GameData;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class ScenarioSettings {

	public enum ScenConf {
		LOADSAVE, MONEY, ROOM, DRINK, FOOD, TOILET, RANG, FUNDINGS, WORKSHOP, ART_BUY, ART_SELL, ART_IMAGE, ART_STATURE, ART_ONTABLE
	}

	private GameData data;

	/**
	 * 
	 */
	public ScenarioSettings() {
		data = MapScreen.get().getData();
	}

	/**
	 * Convert the conf in the key
	 * 
	 * @param conf
	 * @return
	 */
	public static String getKey(ScenConf conf) {
		return "config." + conf.toString().toLowerCase();
	}

	/**
	 * Check if the config active
	 * 
	 * @return the config
	 */
	public boolean isActive(ScenConf conf) {
		return data.getPB(getKey(conf), true);
	}

	/**
	 * Set the config active
	 * 
	 * @return the config
	 */
	public void set(ScenConf conf, boolean value) {
		data.setP(getKey(conf), value);
	}

}
