/**
 *
 */
package de.qossire.yaams.generator;

import java.util.Random;

/**
 * @author sven
 *
 */
public class NamesGenerator {

	// Source http://www.dungeoneering.net/d100-list-fantasy-town-names/
	final private static String[] townNames = { "Aerilon", "Aquarin", "Aramoor", "Azmar", "Begger’s Hole", "Black Hollow", "Blue Field", "Briar Glen",
			"Brickelwhyte", "Broken Shield", "Boatwright", "Bullmar", "Carran", "City of Fire", "Coalfell", "Cullfield", "Darkwell", "Deathfall", "Doonatel",
			"Dry Gulch", "Easthaven", "Ecrin", "Erast", "Far Water", "Firebend", "Fool’s March", "Frostford", "Goldcrest", "Goldenleaf", "Greenflower",
			"Garen’s Well", "Haran", "Hillfar", "Hogsfeet", "Hollyhead", "Hull", "Hwen", "Icemeet", "Ironforge", "Irragin", "Jarren’s Outpost", "Jongvale",
			"Kara’s Vale", "Knife’s Edge", "Lakeshore", "Leeside", "Lullin", "Marren’s Eve", "Millstone", "Moonbright", "Mountmend", "Nearon", "New Cresthill",
			"Northpass", "Nuxvar", "Oakheart", "Oar’s Rest", "Old Ashton", "Orrinshire", "Ozryn", "Pavv", "Pella’s Wish", "Pinnella Pass", "Pran", "Quan Ma",
			"Queenstown", "Ramshorn", "Red Hawk", "Rivermouth", "Saker Keep", "Seameet", "Ship’s Haven", "Silverkeep", "South Warren", "Snake’s Canyon",
			"Snowmelt", "Squall’s End", "Swordbreak", "Tarrin", "Three Streams", "Trudid", "Ubbin Falls", "Ula’ree", "Veritas", "Violl’s Garden", "Wavemeet",
			"Whiteridge", "Willowdale", "Windrip", "Wintervale", "Wellspring", "Westwend", "Wolfden", "Xan’s Bequest", "Xynnar", "Yarrin", "Yellowseed",
			"Zao Ying", "Zeffari", "Zumka" };

	// Source http://listofrandomnames.com/index.cfm?textarea
	final private static String[] maleNames = { "Ray", "Arron", "Wendell", "Werner", "Riley", "Alfonzo", "Erick", "Tyler", "Freeman", "Kenneth", "Sam", "Josh",
			"Jimmie", "Renaldo", "Ezra", "Alfredo", "Rick", "Ellsworth", "Kareem", "Gale", "Son", "Juan", "Deon", "Saul", "Rhett", "Zackary", "Malcolm",
			"Marvin", "Cecil", "Christopher", "Hector", "Jordan", "Humberto", "Reinaldo", "Eliseo", "Willie", "Lester", "Bertram", "Rupert", "Milo", "Thad",
			"Hubert", "Travis", "Rod", "Maria", "Justin", "Wilfredo", "Reggie", "Jacques", "Aron", "Rashad", "Kevin", "Noe", "Gerardo", "Rufus", "Sergio",
			"Rocco", "Spencer", "Wally", "Jayson", "Ricardo", "Claude", "Kip", "Mauricio", "Damon", "Harley", "Renato", "Foster", "Clemente", "Gil", "Jerold",
			"Laurence", "Sheldon", "Jeremiah", "Barry", "Lino", "Luis", "Andrea", "Chance", "Nigel", "Everett", "Olin", "Cedric", "Russel", "Sherwood",
			"Zachary", "Lowell", "Jason", "Marion", "Abdul", "Jerrold", "Jim", "Dennis", "Refugio", "Hosea", "Ambrose", "Sammie", "Logan", "Carlton",
			"Terrance", "Lucas", "Leif", "Columbus", "Eduardo", "Branden", "Felton", "Frederic", "Brant", "Richard", "Hal", "Wayne", "Woodrow", "Rodney",
			"Gary", "Lacy", "Jerald", "Toby", "Julius", "Ricky", "Rico", "Nicholas", "Korey", "Ulysses", "Sylvester", "Lavern", "Mathew", "Joseph", "Doyle",
			"Richie", "Otis", "Pablo", "Berry", "Jackson", "Andrew", "Dwain", "Noel", "Raymond", "Tyron", "Sydney", "Fernando", "Dalton", "Casey", "Billie",
			"Raymundo", "Linwood", "Cesar", "Earnest", "Irwin", "Len", "Shawn", "Williams", "Marco", "Keenan", "Norman", "Fausto", "Dewayne", "Angelo",
			"Christian", "Louis", "Genaro", "Benedict", "Ignacio", "Morris", "Blake", "Brendon", "Lucien", "Russ", "Earl", "Aurelio", "Carson", "Darin",
			"Giuseppe", "Devon", "Devin", "Timmy", "Raphael", "Josef", "Mel", "Grant", "Xavier", "Jewell", "Joan", "Toney", "Robby", "Todd", "Troy", "Alden",
			"Emery", "Antonio", "Terrence", "Tyrone", "Von", "Bryce", "Stanton", "Darius", "Keneth", "Glenn", "Maximo", "Josue", "Javier", "Deangelo",
			"Domenic", "Sonny", "Gavin", "Rueben", "Ethan", "Wilber", "Bernie", "Dallas", "Mitchell", "Marcelo", "Tomas", "Emmitt", "Darron", "Herman",
			"Coleman", "Buddy", "Donn", "Tuan", "Lonnie", "Colton", "Felipe", "Brain", "Ezequiel", "Julio", "Filiberto", "Carey", "Dirk", "Zachariah",
			"Patricia", "Titus", "Haywood", "Florentino", "Art", "Fletcher", "Wilmer", "Elton", "Rocky", "Granville", "Odis", "Scotty", "Alex", "Dane",
			"Douglass", "Omer", "Jeffry", "Malcom", "Long", "Ruben", "Minh", "Benito", "Gaylord", "Benton", "Timothy", "Brent", "Willy", "Hunter", "Delbert",
			"Orlando", "Nathanael", "Danial", "Jame", "Bernard", "Thanh", "Britt", "Hoyt", "Donte", "Booker", "Jonathon", "Jonah", "John", "Kent", "Benjamin",
			"Ronnie", "Erik", "Numbers", "Frederick", "Vincenzo", "Albert", "Kenny", "Evan", "Augustus", "Freddie", "Dave", "Bennie", "Michal", "Kerry", "Reid",
			"Denny", "Phillip", "Abel", "Wiley", "Dean", "Jerrod", "Weldon", "Freddy", "Chase", "Jerome", "Bryan", "Darryl", "Burt", "Tad", "Jeramy", "Ed",
			"Marshall", "Teodoro", "Lamar", "Winston", "Jan", "Gus", "Roger", "Rolland", "Brett", "Napoleon", "Herb", "Efren", "Normand", "Mickey", "Trent",
			"Johnie", "Elisha", "Don", "Jose", "Barrett", "Greg", "Clay", "Jake", "Lyndon", "Alfonso", "Sterling", "Alva", "Rolando", "Rolf", "Chong",
			"Enrique", "Derick", "Vernon", "Carmelo", "Pete", "Leonel", "Robbie", "Roscoe", "Heath", "Eldridge", "Oswaldo", "Gino", "Chas", "Jacob", "Jeffery",
			"Franklin", "Denver", "Morgan", "Bryant", "Ken", "Randy", "Carol", "Percy", "Carlos", "Cliff", "Carroll", "Roosevelt", "Isidro", "Kenton", "Milton",
			"Dudley", "Norris", "Florencio", "Ali", "Angel", "Maynard", "Nelson", "Jonathan", "Jed", "Orval", "Jacinto", "Lawrence", "Rodrick", "Leigh",
			"Whitney", "Stuart", "Mason", "Benny", "Mohammad", "Gregg", "Ollie", "Sandy", "Julian", "Michel", "Gustavo", "Adam", "Alton", "Boris", "Cordell",
			"Fermin", "Les", "Jerrell", "Otha", "Scott", "Harvey", "Donnell", "Wilson", "Jc", "Tim", "Monty", "Lane", "Elliott", "Dong", "Vito", "Mckinley",
			"Daryl", "Ahmed", "Thurman", "Emile", "Maxwell", "Salvador", "Elmer", "William", "Claudio", "Israel", "Kendall", "Chi", "Merle", "Chad", "Bob",
			"Johnnie", "Burton", "Val", "Guadalupe", "Brad", "Teddy", "Henry", "Salvatore", "Jessie", "Paris", "Frankie", "Junior", "Solomon", "Perry",
			"Antione", "Bradley", "Dee", "Craig", "Stan", "Roy", "Jay", "Ferdinand", "Sanford", "Gordon", "Manual", "Rudy", "Mack", "Erwin", "Raymon",
			"Giovanni", "Joel", "Tod", "Anderson", "Aubrey", "Connie", "Ervin", "Wm", "Stewart", "Sung", "Vincent", "Kraig", "Leonardo", "Theo", "Galen",
			"Luciano", "Doug", "Jermaine", "Harland", "Dwayne", "Victor", "Rodrigo", "Hobert", "Erasmo", "Nicky", "Tyrell", "Kennith", "Norbert", "Chester",
			"Omar", "Ronald", "Tony", "Jerry", "Clayton", "Ellis", "Garret", "Oscar", "Conrad", "Dan", "Rafael", "Edgardo", "Leon", "Rubin", "Lamont", "Murray",
			"Lesley", "Herschel", "Samuel", "Michale", "Louie", "Garrett", "Alan", "Monroe", "Kelley", "Colby", "Pat", "Randal", "Jasper", "Howard", "Octavio",
			"Prince", "Damion", "Laverne", "Gonzalo", "Eusebio", "Tom", "Amado", "Jude", "Otto", "Gabriel", "Harrison", "Arthur", "Carmine", "Maurice", "Joe",
			"Clarence", "Joey", "Edwin", "Stevie", "Raleigh", "Sang", "Emmett", "Moises", "Hugh", "Leroy", "Lyle", "Anibal", "Landon", "Abraham", "Isreal",
			"Dewitt", "Joaquin", "Boyce", "Buster", "Yong", "Alfred", "Gilbert", "Ariel", "Geraldo", "Vern", "Seth", "Allen", "Carmen", "Warner", "Jarred",
			"Blaine", "Adalberto", "Tyree", "Luther", "Trinidad", "Shelby", "Jean", "Dustin", "Brian", "Donnie", "Jordon", "Gail", "Emmanuel", "Mike", "Jared",
			"Jimmy", "Erich", "Hollis", "Leandro", "Clifton", "Zachery", "Ismael", "Russell", "Coy", "Bennett", "Mohammed", "Kurtis", "Pedro", "Beau", "Joesph",
			"Valentine", "Tanner", "Ted", "Lucio", "Nestor", "Ernie", "Carl", "Emerson", "Elroy", "German", "Tommy", "Dexter", "Chadwick", "Rodolfo",
			"Dominique", "Rickey", "Reuben", "Roderick", "Everette", "Neville", "Quentin", "Martin", "Lon", "Randall", "Forest", "Elvin", "Brice", "Aldo",
			"George", "Millard", "Cory", "Sal", "Mariano", "Hipolito", "Brock", "Quinton", "Sherman", "Roland", "Phil", "Cyril", "Amos", "Brady", "Andreas",
			"James", "Ike", "Dino", "Bobbie", "Kristofer", "Esteban", "Silas", "Caleb", "Cristopher", "Brooks", "Geoffrey", "Preston", "Jamel", "Scot", "Rene",
			"Cody", "Franklyn", "Dusty", "Myles", "Ward", "Bo", "Luke", "Bud", "Elliot", "Dion", "Anthony", "Dick", "Darren", "Edwardo", "Cary", "Fritz",
			"Derek", "Fredric", "Earle", "Vince", "Lionel", "Waldo", "Sidney", "Tyson", "Micheal", "Ernest", "Darrin", "Walton", "Jarrod", "Mario", "Alonso",
			"Miguel", "Hassan", "Rey", "Fabian", "Jamie", "Kieth", "Leo", "Alexander", "Lazaro", "Calvin", "Sean", "Duane", "Lanny", "Milford", "Roberto",
			"Kelly", "Trevor", "Gregory", "Micah", "Ira", "Zane", "Curt", "Gayle", "Chris", "Virgilio", "Curtis", "Thomas", "Bart", "Mary", "Gerry", "Felix",
			"Corey", "Heriberto", "Merrill", "Buck", "Santos", "Virgil", "Danilo", "Wesley", "Horacio", "Francisco", "Max", "Darrel", "Samual", "Clement",
			"Royal", "Arnold", "Oren", "Thaddeus", "Waylon", "Shayne", "Cristobal", "Keith", "Valentin", "Fredrick", "Jackie", "Kelvin", "Edmond", "Moshe",
			"Rosendo", "Bradly", "Sol", "Lindsey", "Deandre", "Horace", "Dario", "Cornell", "Frank", "Aaron", "Diego", "Claud", "Avery", "Broderick", "Noble",
			"Winfred", "Floyd", "Jess", "Brenton", "Cole", "Dante", "Donald", "Malik", "Melvin", "Bill", "Graham", "Kendrick", "Ryan", "Donovan", "Winford",
			"Theron", "Johnathan", "Cleveland", "Trenton", "Edmundo", "Domingo", "Wes", "Del", "Edward", "Willian", "August", "Rory", "Abe", "Clifford",
			"Moses", "Jarod", "Alphonse", "Rodger", "Rayford", "Charley", "Elmo", "Emil", "Ramiro", "Arlen", "Lincoln", "Elden", "Antonia", "Herbert", "Elwood",
			"Hershel", "Robt", "Tory", "Mark", "Cornelius", "Jamar", "Irvin", "Porfirio", "Ashley", "Jesus", "Ezekiel", "Isaiah", "Archie", "Hai", "Chang",
			"Eric", "Kirby", "Eldon", "Merlin", "Tracy", "Seymour", "Jefferey", "Marc", "Dewey", "Grover", "Byron", "Bryon", "Rudolf", "Armando", "Guillermo",
			"Jorge", "Loyd", "Chet", "Reed", "Leopoldo", "Andre", "Antoine", "Eloy", "Damien", "Dana", "Damian", "Garfield", "Palmer", "Lindsay", "Armand",
			"Alberto", "Buford", "Kasey", "Hugo", "Elijah", "Hank", "Stanford", "Douglas", "Jack", "Stefan", "Gerard", "Josiah", "Marcellus", "Tracey", "Elias",
			"Johnny", "Jarrett", "Rich", "Ty", "Jeffrey", "Neal", "Mauro", "Milan", "Harry", "Cedrick", "Bruno", "Lloyd", "Steven", "Lou", "Sebastian", "Mikel",
			"Fidel", "Odell", "Simon", "Rosario", "Chung", "Alexis", "Trey", "Pasquale", "Huey", "Demetrius", "Cameron", "Alec", "Cortez", "Luigi", "Tobias",
			"Carlo", "Glen", "Gaston", "Lorenzo", "Terrell", "Marlin", "Hong", "Jewel", "Basil", "Carrol", "Kory", "Leslie", "Daniel", "Alejandro", "Wilbert",
			"Robert", "Neil", "Young", "Michael", "Errol", "Quincy", "Duncan", "Daren", "Ralph", "Jon", "Andres", "Wilfred", "Lynn", "Lee", "Vaughn",
			"Alphonso", "Jae", "Philip", "Delmar", "Stephen", "Faustino", "Kyle", "Mitchel", "Wilford", "Shannon", "Rudolph", "Jody", "Newton", "Bernardo",
			"Emory", "Danny", "Barton", "Bobby", "Christoper", "Raul", "Miles", "Jefferson", "Lauren", "Carter", "Randolph", "Edgar", "Terence", "Eugene",
			"Federico", "Shaun", "Kermit", "Lewis", "Guy", "Arturo", "Darell", "Will", "Walker", "Dylan", "Lemuel", "Deshawn", "Terry", "Pierre", "Eli", "King",
			"Mac", "Gregorio", "Lynwood", "Marty", "Desmond", "Peter", "Bruce", "Jeremy", "Collin", "Kristopher", "Marcelino", "Loren", "Modesto", "Lance",
			"Andy", "Nathan", "Major", "Vance", "Frances", "Antwan", "Nolan", "Eugenio", "Jere", "Hilton", "Judson", "Harlan", "Ivan", "Stacy", "Dillon",
			"Nathaniel", "Homer", "Santo", "Lenard", "Billy", "Warren", "Delmer", "Mohamed", "Mitch", "Hilario", "Jaime", "Adolfo" };

	final private static String[] femaleNames = { "Jenell", "Marlys", "Tabitha", "Georgene", "Marquerite", "Ima", "Shaniqua", "Vivian", "Lavonda", "Jackeline",
			"Mozella", "Arletta", "Hee", "Sudie", "Steffanie", "Marianna", "Rivka", "Elisa", "Nedra", "Karol", "Maryrose", "Kristin", "Jeannetta", "Justa",
			"Julieann", "Reanna", "Charlene", "Karina", "Tennie", "Lisette", "Iola", "Esther", "Willia", "Kenyatta", "Marchelle", "Vickey", "Babara",
			"Catherine", "Christinia", "Candida", "Adria", "Krissy", "Lucinda", "Teodora", "Echo", "Manda", "Lorraine", "Maribeth", "Jacqui", "Trista", "Tilda",
			"Etsuko", "Soraya", "Berenice", "Luba", "Dara", "Hillary", "Dreama", "Temeka", "Marisol", "Melina", "Gena", "Tula", "Georgianna", "Maren",
			"Jacqulyn", "Estella", "Signe", "Vonnie", "Hue", "Sueann", "Bunny", "Dania", "April", "Tiffany", "Catalina", "Evelynn", "Un", "Sharan", "Kelsey",
			"Fatimah", "Tamica", "Galina", "Albertha", "Winifred", "Mariann", "Lorretta", "Carlyn", "Stella", "Halina", "Anisha", "Mercy", "Nidia", "Jana",
			"Shaunda", "Angela", "Shakia", "Reatha", "Katelin", "Reginia", "Bailey", "Danyelle", "Lashawnda", "Louvenia", "Jerrie", "Britta", "Beryl", "Alvina",
			"Trinh", "Janean", "Lili", "Particia", "Katherine", "Tori", "Bebe", "Yer", "Agnus", "Taryn", "Carolann", "Han", "Oleta", "Sherryl", "Gay", "Trina",
			"Nicolasa", "Fe", "Klara", "Camilla", "Marth", "Margery", "Ethelyn", "Verla", "Fiona", "Shalon", "Tia", "Agnes", "Kenia", "Alla", "Chasidy", "Lola",
			"Delila", "Melynda", "Paz", "Twila", "Athena", "Elna", "Shanice", "Lauri", "Leanna", "Christia", "Thao", "Catrice", "Jani", "Mahalia", "Jennette",
			"Olivia", "Quyen", "Clorinda", "Lakeesha", "Kathey", "Sophie", "Dionna", "Noriko", "Debera", "Marti", "Lina", "Venessa", "Carmelia", "Zelda",
			"Melita", "Louella", "Deena", "Jaclyn", "Orpha", "Anneliese", "Williemae", "Randee", "Lona", "Pauline", "Pamela", "Keila", "Shela", "Casimira",
			"Loraine", "Delicia", "Cyndy", "Coretta", "Kary", "Briana", "Jacalyn", "Stormy", "Arie", "Brynn", "Dorothy", "Catrina", "Annika", "Jeri", "Carline",
			"Carolynn", "Divina", "Devona", "Brenda", "Stefani", "Marta", "Ozella", "Petronila", "Sharda", "Lorriane", "Idalia", "Monnie", "Erika", "Lenita",
			"Ione", "Lore", "Inge", "Cristin", "Eva", "Sana", "Jacquline", "Dayna", "Roselia", "Gabrielle", "Melodie", "Enriqueta", "Myriam", "Jocelyn",
			"Elicia", "Myong", "Loreen", "Mariana", "Leticia", "Christen", "Eugenia", "Celine", "Valarie", "Alexandra", "Tegan", "Cecilia", "Tameka", "Linh",
			"Flora", "Christeen", "Tandra", "Nerissa", "Luna", "Arlena", "Sharilyn", "Kyra", "Silvia", "Laurena", "Vida", "Priscilla", "Monique", "Dorene",
			"Dora", "Joleen", "Sharolyn", "Vita", "Chantal", "Vinnie", "Sherrill", "Brittany", "Sylvie", "Reena", "Erica", "Suzanna", "Theodora", "Loretta",
			"Versie", "Ona", "Niesha", "Lan", "Kimiko", "Hiroko", "Valorie", "Ebonie", "Samara", "Karima", "Daphne", "Yajaira", "Roxanna", "Cheryll", "Larissa",
			"Penni", "Roseanna", "Merlyn", "Rosenda", "Cora", "Yon", "Jacquelin", "Keren", "Maxie", "Izola", "Penelope", "Gladys", "Chelsea", "Sharlene",
			"Dorothea", "Kati", "Irina", "Jessika", "Wynona", "Julene", "Julienne", "Ellamae", "Genoveva", "Adelina", "Barb", "Treva", "Karine", "Zana",
			"Patria", "Magaly", "Suk", "Chante", "Roxanne", "Shanika", "Pandora", "Meryl", "Niki", "Rebecka", "Laine", "Patience", "Shea", "Yvonne", "Alecia",
			"Shaunte", "Teri", "Tommye", "Eryn", "Rosemarie", "Danelle", "Claire", "Genie", "Adelle", "Nakita", "Kathlyn", "Mendy", "Olga", "Lyndia", "Tiffiny",
			"Cherri", "Kym", "Reynalda", "Rosann", "Kali", "Wava", "Salome", "Elizebeth", "Zofia", "Latricia", "Jillian", "Akilah", "Eilene", "Emelia",
			"Rolanda", "Ellan", "Candie", "Victorina", "Milagros", "Alesia", "Jennine", "Adella", "Detra", "Demetra", "Camie", "Ethelene", "Ana", "Twyla",
			"Lorean", "Harriett", "Lilia", "Genesis", "Fleta", "Renae", "Fabiola", "Jannie", "Elsie", "Josefina", "Cris", "Odelia", "Dorie", "Bettina", "Bok",
			"Nichole", "Hester", "Chery", "Asley", "Gillian", "Dinah", "Lawanda", "Johanna", "Brigette", "Kathlene", "Marisha", "Asha", "Kiyoko", "Celia",
			"Alexia", "Starla", "Rochell", "Sophia", "Beverly", "Ara", "Shannan", "Sheena", "Kayce", "Tammi", "Bao", "Tammara", "Allegra", "Fatima", "Terrilyn",
			"Lorelei", "Sachiko", "Eunice", "Audrea", "Anjanette", "Aurea", "Breanne", "Shara", "Estela", "Tanisha", "Amanda", "Tawanna", "Danette", "Carina",
			"Christine", "Toshiko", "Wai", "Farah", "Lynne", "Yuriko", "Lennie", "Nakisha", "Paulina", "Carma", "Anissa", "Amy", "Alejandrina", "Alessandra",
			"Coreen", "Melba", "Mattie", "Shelba", "Syble", "Jazmine", "Kelle", "Misty", "Arlyne", "Neoma", "Gregoria", "Raina", "Britney", "Shemika",
			"Juanita", "Roseline", "Danille", "Ranee", "Voncile", "Liberty", "Monserrate", "Faustina", "Josefine", "Anabel", "Sharmaine", "Cindy", "Kylie",
			"Sanda", "Tania", "Flo", "Dung", "Dell", "Myrtis", "Kamilah", "Pamala", "Nieves", "Ellyn", "Honey", "Fidela", "Phoebe", "Tama", "Marquetta",
			"Cassaundra", "Gemma", "Martine", "Zetta", "Santina", "Lecia", "Terra", "Alida", "Lucille", "Delpha", "Karri", "Belen", "Vernita", "Delois",
			"Janise", "Lori", "Amber", "Genevie", "Roslyn", "Cherryl", "Modesta", "Lexie", "Domonique", "Rosalba", "Saundra", "Novella", "Aura", "Roseann",
			"Carlie", "Hertha", "Mavis", "Annetta", "Lucia", "Lashawn", "Elin", "Loni", "Corliss", "Brittny", "Janiece", "Lashay", "Winnie", "Deandra", "Lula",
			"Freda", "Carisa", "Terrie", "Leann", "Maureen", "Eartha", "Shae", "Aline", "Elois", "Rosalina", "Florine", "Kristina", "Reva", "Jaimie", "Geri",
			"Mollie", "Clementina", "Arianna", "Augustina", "Mellissa", "Ciera", "Benita", "Tyesha", "Carleen", "Karla", "Marisela", "Adell", "Rosanne",
			"Roselle", "Keturah", "Rose", "Mireille", "Karyn", "Danae", "Zola", "Pauletta", "Roxie", "Marilu", "Lavonia", "Tuyet", "Raymonde", "Arnette",
			"Arlene", "Brittani", "Bianca", "Tam", "Brittanie", "Darlena", "Ashleigh", "Nelida", "Bryanna", "Zenaida", "Alfredia", "Elke", "Jalisa", "Heide",
			"Mariam", "Zulma", "Felisha", "Darcie", "Nora", "Felipa", "Maye", "Tracie", "Annett", "Danica", "Samira", "Evia", "Lucilla", "Madeline", "Christal",
			"Kanisha", "Sima", "Lilly", "Liliana", "Leigha", "Launa", "Carlita", "Amberly", "Despina", "Emiko", "Kacie", "Melda", "Penney", "Valene", "Clelia",
			"Leonida", "Edyth", "Kristyn", "Krystina", "Toshia", "Rhoda", "Annis", "Meaghan", "Katerine", "Natalie", "Jena", "Salina", "Ewa", "Bridgett",
			"Miguelina", "Yessenia", "Lezlie", "Takisha", "Dorinda", "Kacey", "Loida", "Hortencia", "Imelda", "Melodi", "Nguyet", "Laurene", "Annette",
			"Rosita", "Amal", "Mallory", "Pam", "Tonisha", "Zoila", "Samella", "Tillie", "Providencia", "Genny", "Adriana", "Carli", "Debbra", "Margy",
			"Carolyn", "Alane", "Deana", "Margo", "Althea", "Thalia", "Shavonne", "Katy", "Irena", "Doreen", "Maile", "Chu", "Leeanna", "Luella", "Sharita",
			"Kathie", "Melia", "Damaris", "Crystal", "Annice", "Myrtle", "Lolita", "Marlena", "Fernanda", "Vilma", "Vernie", "Angeles", "Polly", "Elsa",
			"Alberta", "Zulema", "Odessa", "Nellie", "Celestina", "Oneida", "Alaine", "Ila", "Marsha", "Celesta", "Chan", "Marget", "Iris", "Maryjo", "Aleen",
			"Karmen", "Jaymie", "Li", "Ramona", "Roxane", "Khadijah", "Idella", "Stephaine", "Ariane", "Mara", "Cristy", "Jong", "Malka", "Torrie", "Clora",
			"Keesha", "Earlene", "Madelene", "Lesa", "Cinthia", "Laurice", "Rosia", "Nan", "Trisha", "Dorla", "Barbara", "Yvone", "Crissy", "Aisha", "Magda",
			"Francoise", "Taina", "Isis", "Maximina", "Joette", "Albertina", "Lara", "Randi", "Towanda", "Ruby", "Celina", "Maira", "Lillie", "Donella", "Lise",
			"Lu", "Chaya", "Roxana", "Rachell", "Cordia", "Fernande", "Ryann", "Angeline", "Deneen", "Tiara", "Taunya", "Mei", "Sherie", "Arnita", "Felicita",
			"Davida", "Lorna", "Inger", "Maple", "Latasha", "May", "Lucina", "Miss", "Shenita", "Cyrstal", "Kaleigh", "Valery", "Amee", "Anja", "Otelia",
			"Mikki", "Marcie", "Lida", "Junita", "Jama", "Gertha", "Renay", "Selma", "Ozie", "Audra", "Shantelle", "Cristal", "Viviana", "Shaunna", "Annita",
			"Georgiana", "Karyl", "Merri", "Tianna", "Ula", "Wilhelmina", "Jene", "Ming", "Madelyn", "Elanor", "Cathrine", "Fran", "Debi", "Madison",
			"Carolina", "Olimpia", "Rosalyn", "Deane", "Herma", "Elana", "Shaneka", "Terresa", "Shantell", "Shira", "Luise", "Kathyrn", "Rana", "Summer",
			"Ngoc", "Jenine", "Cheree", "Bethann", "Sherri", "Chassidy", "Jeannie", "Renea", "Noelia", "Tyra", "Carman", "Imogene", "Melisa", "Ma", "Pennie",
			"Gracia", "Ammie", "Tamera", "Hien", "Emerald", "Eura", "Brande", "Cassidy", "Sunni", "Lisha", "Shery", "Tawanda", "Krystal", "Shan", "Chantel",
			"Denyse", "Annalee", "Arla", "Eliza", "Maragret", "Felicitas", "Dianne", "Judith", "Ariana", "Hermine", "Tonita", "Sharla", "Eusebia", "Allison",
			"Khalilah", "Arvilla", "Jesica", "Portia", "Latoya", "Zona", "My", "Belle", "Macy", "Kera", "Sona", "Leonia", "Cara", "Tynisha", "Leonora", "Letha",
			"Eleanora", "Pamelia", "Alyssa", "Eulah", "Susanna", "Rocio", "Joye", "Rochel", "Belinda", "Buffy", "Rolande", "Mozelle", "Rasheeda", "Yi", "Mabel",
			"Larae", "Loma", "Tanika", "Sigrid", "Madge", "Francine", "Angelic", "Louise", "Leola", "Chelsie", "Alysha", "Ester", "Lakeisha", "Brook",
			"Shaquana", "Cindie", "Xenia", "Marisa", "Faviola", "Roselyn", "Edelmira", "Hyo", "Babette", "Lyndsey", "Nadine", "Neomi", "Moon", "Iliana",
			"Marivel", "Pearlene", "Chanel", "Kaley", "Maryellen", "Sandi", "Laci", "Kathryne", "Kirstie", "Etha", "Jacquetta", "Lashonda", "Dinorah",
			"Valeria", "Clotilde", "Tarah", "Olevia", "Lakiesha", "Nga", "Cecila", "Denisse", "Sherilyn", "Lizbeth", "Shantel", "Jerrica", "Claudie", "Barbera",
			"Kristan", "Mariko", "Jeraldine", "Abby", "Delinda", "Kristel", "Ja", "Karren", "Jacquelyne", "Dedra", "Tiffaney", "Abbey", "Mitzie", "Wanetta",
			"Necole", "Meta", "Lanell", "Lauralee", "Eveline", "Annamarie", "Mamie", "Lia", "Tonja", "Vanesa", "Mistie", "Mechelle", "Inez", "Mina",
			"Kassandra", "Raelene", "Corina", "Bernadine", "Mafalda", "Abigail", "Keshia", "Robyn", "Rosemary", "Ling", "Rufina", "Meridith", "Florene",
			"Susanne", "Ellen", "Vivienne", "Zina", "Simonne", "Tamisha", "Jenette", "Beaulah", "Hui", "Tisha", "Dianna", "Mickie", "Susan", "Piedad",
			"Glynda" };

	final private static String[] surName = { "Stutes", "Simon", "Rendell", "Tolar", "Meneses", "Deibler", "Tores", "Bratten", "Berns", "Kehl", "Striegel",
			"Marzette", "Demoss", "Weideman", "Wyant", "Cagney", "Lichtman", "Gott", "Jeffers", "Ducker", "Broomfield", "Calo", "Montenegro", "Auzenne",
			"Whitesell", "Poteete", "Ocheltree", "Ivey", "Vansickle", "Manske", "Wernick", "Troncoso", "Armijo", "Lomanto", "Schutz", "Pekar", "Brimmer",
			"Schild", "Byram", "Vining", "Chung", "Meggs", "Weyandt", "Sonntag", "Zerbe", "Holiman", "Goodrum", "Bustamante", "Wisener", "Steinbach", "Nilsson",
			"Pearlman", "Elswick", "Northup", "Duquette", "Ames", "Ferreira", "Lamanna", "Luth", "Sherburne", "Wilderman", "Amendola", "Pickle", "Walden",
			"Godard", "Wahl", "Hipple", "Toto", "Ronco", "Mclennan", "Hagar", "Vigna", "Fishburn", "Varona", "Abad", "Peak", "Speier", "Napolitano", "Gosier",
			"Cerrato", "Bloch", "Nelligan", "Boelter", "Bouldin", "Parkin", "Sanner", "Neves", "Reidy", "Searight", "Grinstead", "Leard", "Pettus", "Vega",
			"Holden", "Wolery", "Scull", "Shoup", "Mahurin", "Langenfeld", "Meany", "Millward", "Goodwin", "Mcnamee", "Bargas", "Reineke", "Mcbee", "Alfano",
			"Souliere", "Mcmanus", "Badeaux", "Iglesias", "Keesler", "Perdue", "Pelt", "Callejas", "Priestley", "Mishoe", "Baty", "Brenton", "Alexandria",
			"Landa", "Bohanon", "Outlaw", "Hickson", "Rosenquist", "Wilborn", "Challis", "Riggio", "Alden", "Jenney", "Savant", "Wingate", "Burkes", "Goll",
			"Clevenger", "Hillin", "Bortle", "Real", "Mares", "Mickley", "Pearsall", "Renfro", "Bequette", "Caudillo", "Swiger", "Nold", "King", "Baine",
			"Liakos", "Deckman", "Lachowicz", "Sandoz", "Kober", "Meszaros", "Cauble", "Bullock", "Mowen", "Reddin", "Guptill", "Avis", "Su", "Lobue", "Bilger",
			"Novick", "Tippin", "Canady", "Neary", "Whipple", "Breit", "Cremin", "Hedgecock", "Slaymaker", "Schloss", "Adolphson", "Westrick", "Noto",
			"Mcchesney", "Hernandez", "Badilla", "Segars", "Nathaniel", "Guidotti", "Peavey", "Neff", "Moreau", "Valderrama", "Vannoy", "Mazzei", "Hsu",
			"Mcdonell", "Dombrowski", "Nevarez", "Alfred", "Tankersley", "Mosser", "Keppel", "Bavaro", "Palomares", "Mona", "Gilchrest", "Landrum",
			"Summerford", "Collie", "Lessley", "Blackman", "Berkman", "Westphal", "Mckissick", "Newlon", "Figueredo", "Windle", "Winters", "Schwager", "Troche",
			"Lindbloom", "Mainor", "Geraci", "Coon", "Pilgrim", "Flink", "Kulik", "Divito", "Vanwingerden", "Mattis", "Dundon", "Orrell", "Gheen", "Langstaff",
			"Demarest", "Langsam", "Cotten", "Failla", "Burnside", "Son", "Kaul", "Rulison", "Yonker", "Monica", "Cotnoir", "Higa", "Coward", "Sergent", "Zell",
			"Janssen", "Talavera", "Edmiston", "Salsbury", "Stonesifer", "Bartz", "Haycock", "Olinger", "Tingey", "Shortridge", "Nitta", "Islas", "Hamman",
			"Stolte", "Weiler", "Levins", "Lambert", "Haskins", "Danielsen", "Rambin", "Mozee", "Seybert", "Milardo", "Lintz", "Toothman", "Rodriguz", "Shams",
			"Mcmann", "Tandy", "Etzel", "Mcie", "Koval", "Hunsaker", "Brasil", "Kerry", "Trigg", "Gullatt", "Vance", "Tiedemann", "Mazzola", "Cargo", "Haffner",
			"Pitzen", "Harig", "Curran", "Monette", "Brehm", "Veras", "Lemke", "Lindenberg", "Morell", "Daub", "Arzate", "Cashman", "Tenney", "Ritter", "Kasel",
			"Kestner", "Saulter", "Mcmasters", "Patterson", "Branch", "Boeck", "Hochmuth", "Cusick", "Mcguirk", "Helvey", "Purvis", "Plate", "Fitzsimmons",
			"Masten", "Stelly", "Buel", "Quon", "Pleiman", "Sim", "Tisher", "Pfeifer", "Stoney", "Clune", "Mariscal", "Rothwell", "Carrillo", "Vossler",
			"Fresquez", "Jeansonne", "Eicher", "Tienda", "Weishaupt", "Hunsinger", "Rome", "Giesen", "Rust", "Tibbs", "Beckham", "Helper", "Maria", "Yarbro",
			"Kardos", "Polk", "Vankeuren", "Gros", "Mcentee", "Huggard", "Boll", "Lingle", "Lasker", "Champ", "Rarick", "Buchholtz", "Yellowhair", "Peak",
			"Mckissack", "Gaulin", "Hollifield", "Direnzo", "Derouen", "Tengan", "Schurman", "Fraley", "Chavis", "Frith", "Pelaez", "Stabler", "Rexroat",
			"Skeete", "Jemison", "Schnitzer", "Kroeker", "Overbey", "Summerlin", "Bilodeau", "Lumpkins", "Linch", "Sancho", "Hicklin", "Groll", "Calahan",
			"Jaques", "Bourg", "Billy", "Spagnola", "Mccary", "Bugarin", "Johnstone", "Barbieri", "Sitler", "Collman", "Peaden", "Dolan", "Ferra", "Shoults",
			"Riggins", "Hymas", "Gerry", "Helbig", "Suman", "Sloop", "Myron", "Orloff", "Markle", "Dames", "Sturdevant", "Levan", "Midgette", "Studebaker",
			"Braham", "Collinsworth", "Hotard", "Corral", "Streit", "Epting", "Blandford", "Wilhelm", "Dix", "Benfer", "Burge", "Akbar", "Horstman", "Soper",
			"Marine", "Bradeen", "Creasy", "Crago", "Roeder", "Caple", "Fant", "Johanson", "Rosenthal", "Alter", "Palermo", "Fitzmaurice", "Barreras", "Royce",
			"Hochman", "Farina", "Routt", "Severino", "Monarrez", "Court", "Quiroga", "Wishon", "Ruppel", "Hyun", "Galusha", "Bright", "Dehne", "Churchwell",
			"Buchman", "Gisler", "Kent", "Stahly", "Tedesco", "Malbrough", "Bitton", "Koons", "Corman", "Rutten", "Talamantes", "Omarah", "Guglielmo",
			"Jakubowski", "Knickerbocker", "Anstett", "Leiva", "Londono", "Hackworth", "Froman", "Deno", "Loken", "Shives", "Argo", "Isham", "Knepper",
			"Wheless", "Monge", "Swicegood", "Brockwell", "Horsman", "Plumley", "Cowart", "Esposito", "Pooley", "Kellner", "Youngblood", "Strite", "Strang",
			"Craven", "Carriere", "Abbey", "Rients", "Winward", "East", "Weisman", "Licari", "Deforge", "Dematteo", "Pillow", "Kuck", "Wooldridge", "Beiler",
			"Mathias", "Harrill", "Dandridge", "Draughn", "Lamer", "Eberhart", "Dejong", "Towe", "Archey", "Jaynes", "Hoskins", "Weiss", "Ramsden", "Wageman",
			"Conigliaro", "Herman", "Labriola", "Eubanks", "Sundquist", "Zamarripa", "Pecor", "Shine", "Merlo", "Archibald", "Faires", "Folden", "Klumpp",
			"Westcott", "Escovedo", "Hendon", "Martines", "Yeaman", "Kua", "Meints", "Genao", "Saffell", "Dampier", "Hurt", "Byers", "Salvas", "Yoshioka",
			"Giraldo", "Buffum", "Beaupre", "Wragg", "Shute", "Voisin", "Koch", "Heise", "Albus", "Doster", "Hittle", "Valencia", "Macklin", "Michelson",
			"Crumrine", "Pella", "Laforest", "Grenz", "Hurtado", "Manzella", "Mandell", "Wetherbee", "Eisner", "Maland", "Kneip", "Shorts", "Daulton", "Maxton",
			"Curl", "Dore", "Jarrell", "Sabado", "Lao", "Fitchett", "Cummings", "Mccalla", "Levar", "Villafuerte", "Cavallo", "Luman", "Haight", "Upchurch",
			"Dore", "Loveall", "Mcquarrie", "Plaster", "Outler", "Marth", "Camargo", "Shifflett", "Crossland", "Ringel", "Witherow", "Royall", "Rentfro",
			"Zeng", "Finger", "Plaisance", "Immel", "Portis", "Chabot", "Pineau", "Miro", "Francese", "Sites", "Mroz", "Chamblee", "Felan", "Pullen", "Toone",
			"Storie", "Varghese", "Litteral", "Frias", "Wight", "Lucchesi", "Gosse", "Trostle", "Lain", "Gonyea", "Deal", "Foland", "Bencomo", "Mccleery", "Do",
			"Pintor", "Jock", "Hatch", "Pisani", "Zack", "Sher", "Vandehey", "Binion", "Thoreson", "Waddy", "Renegar", "Huddle", "Hyde", "Buckle", "Garon",
			"Elkins", "Platts", "Spiva", "Mcgahey", "Bartholomew", "Zinke", "Kaminski", "Bayes", "Konen", "Hoadley", "Meagher", "Sillman", "Carnegie", "Kontos",
			"Tschanz", "Fluellen", "Pollitt", "Golding", "Dilley", "Cadden", "Moeller", "Demma", "Wagnon", "Valderrama", "Hanger", "Punch", "Michel", "Bolger",
			"Lutz", "Bartell", "Muise", "Boshart", "Crays", "Saulsbury", "Box", "Barrie", "Gunter", "Rathbun", "Angers", "Sibert", "Markwell", "Duty", "Daily",
			"Roe", "Mcmorris", "Woodruff", "Booker", "Lapointe", "Heflin", "Pigford", "Paden", "Beans", "Underdahl", "Player", "Hudnall", "Wotring", "Schon",
			"Mattocks", "Greenwald", "Malloy", "Rosenbaum", "Cora", "Bias", "Wohl", "Schulman", "Furlong", "Tookes", "Ptacek", "Spring", "Heitkamp", "Bulman",
			"Cadle", "Valdovinos", "Jansky", "Dart", "Coman", "Ferrell", "Vecchio", "Mattos", "Shirah", "Keown", "Mulligan", "Fuchs", "Sinegal", "Larocco",
			"Jilek", "Kenan", "Ortis", "Stellhorn", "Whisenhunt", "Hoose", "Yokley", "Mor", "Mcmillen", "Lockhart", "Cutting", "Condit", "Mcneill", "Byram",
			"Niemeyer", "Bulluck", "Devalle", "Gamino", "Messinger", "Renfroe", "Rosenstein", "Stembridge", "Turrentine", "Knoles", "Sidoti", "Gaunce",
			"Mcclaine", "Chamberland", "Mcloughlin", "Whittle", "Hewes", "Talton", "Postma", "Dammann", "Oslund", "Holmstrom", "Bubb", "Fuson", "Hilts",
			"Tighe", "Mahan", "Frederickson", "Enterline", "Stifter", "Sparkman", "Metzinger", "Eckler", "Cookingham", "Fredette", "Rago", "Puthoff", "Classen",
			"Kennerly", "Breslin", "Fargo", "Bowler", "Meneely", "Candler", "Errico", "Lehto", "Engen", "Wittrock", "Parrales", "Bilderback", "Peng", "Lutes",
			"Corns", "Hopp", "Crofts", "Neloms", "Krzeminski", "Martelli", "Bell", "Stockwell", "Vanderpool", "Homes", "Casaus", "Heskett", "Ortez", "Provost",
			"Edie", "Hutter", "Vanduyne", "Wetherington", "Cummins", "Lenig", "Majeed", "Croghan", "Reimers", "Swinford", "Mccay", "Gaertner", "Berge", "Derry",
			"Butterfield", "Eggers", "Arellano", "Wiggins", "Hudkins", "Keebler", "Rayos", "Mattera", "Mott", "Hitchings", "Kulp", "Hyre", "Caine", "Calnan",
			"Verduzco", "Aust", "Maloy", "Guyton", "Palin", "Krantz", "Watts", "Zenz", "Updike", "Lukowski", "Faler", "Tuthill", "Stegner", "Touchstone",
			"Hutzler", "Wallner", "Lange", "Timko", "Gingerich", "Lovett", "Wind", "Cumbie", "Shealey", "Schlichting", "Kinchen", "Callihan", "Gaus", "Haynie",
			"Abler", "Harwell", "Coltrane", "Roque", "Hartz", "Tilson", "Bail", "Pariseau", "Reinbold", "Droz", "Emmanuel", "Minich", "Ragsdale", "Giunta",
			"Chery", "Mcnulty", "Hornstein", "Gerace", "Marlette", "Santerre", "Windsor", "Dais", "Heeren", "Gelb", "Dolph", "Strawder", "Granillo", "Cannaday",
			"Sutter", "Pacetti", "Foose", "Mahnke", "Phifer", "Mean", "Barwick", "Rostad", "Kolodziej", "Jasper", "Heal", "Loyola", "Avilez", "Broussard",
			"Fails", "Popek", "Diede", "Jarrard", "Percy", "Kubala", "Mount", "Dahm", "Angel", "Ledezma", "Knutsen", "Devall", "Sawyer", "Bisbee", "Olin",
			"Reiff", "Tolley", "Hyler", "Crum", "Donato", "Zabala", "Strout", "Depuy", "Lusher", "Weddle", "Canavan", "Beshears", "Prendergast", "Bourke",
			"Davila", "Khalaf", "Febus", "Helsley", "Zaleski", "Curiel", "Morriss", "Bruss", "Charette", "Vanhouten", "Woodbury", "Mailhot", "Desjardins",
			"Ingle", "Lui", "Maciel", "Moy", "Harkless", "Mascia", "Besecker", "Snelgrove", "Gazaway", "Hodge", "Bornstein", "Bulloch", "Giddings", "Dahmen",
			"Dawn", "Zehnder", "Otoole", "Dahl", "Parkhurst", "Mennenga", "Gerdts", "Falgoust", "Salmeron", "Batey", "Rapoza", "Reeve", "Quinney", "Schmoll",
			"Shuler", "Wanke", "Bolyard", "Punch", "Mcnally", "Johson", "Walz", "Winchenbach", "Greene", "Haith", "Darrell", "Mcclelland", "Fedler", "Lemarr",
			"Schaefer", "Poppell", "Larrison", "Towle", "Gauthier", "Villanueva", "Thong", "Hille", "Leeder", "Tippetts", "Colston", "Lampman", "Wymore",
			"Capron", "Watters", "Pop", "Beers", "Krach", "Obey", "Misner", "Varley", "Valtierra", "Florentino", "Boudreau", "Montalvo", "Decosta", "Spilman",
			"Mayen", "Ruark", "Beebe", "Holz", "Sheedy", "Sterns", "Cedeno", "Malan", "Starnes", "Bosak", "Piermarini", "Aldinger", "Meldrum", "Neyman",
			"Kirkham", "Denmon", "Palmateer", "Holtzen", "Higgins", "Omar", "Mabon", "Campana", "Cevallos", "Takacs", "Bulfer", "Hofmeister", "Holthaus",
			"Carns", "Grizzle", "Reny", "Aumick", "Mcaninch", "Fleury", "Heyer", "Truelove", "Linnen", "Biggers", "Gunderman", "Fluker", "Baumgarten", "Helms",
			"Kranz", "Welton", "Greaney", "Petri", "Strachan", "Vo", "Goding", "Bogner", "Herter", "Eisenhower", "Vides", "Feltner", "Montalvan", "Cayton",
			"Palko", "Sweeten", "Kemp", "Bordeau", "Churchill", "Rosinski", "Klemme", "Aye", "Goins", "Jefcoat", "Bale", "Beaman", "Everson", "Marietta",
			"Klopp", "Bullion", "Garney", "Estelle", "Stottlemyer", "Just", "Tipps", "Ancheta", "Sharer", "Stingley", "Minard", "Dedmon", "Crater", "Trembath",
			"Germain", "Bierbaum", "Rahm", "Boerner", "Rucks", "Randles", "Hara", "Delamater", "Giffin", "Sambrano", "Waggoner", "Zayac", "Bockman", "Dittrich",
			"Haddix", "Kelling", "Bearse", "Orton", "Bautista", "Placek", "Hardwick", "Claflin", "Mashburn", "Ely", "Marino", "Witte", "Wortman", "Mccarley",
			"Burges", "Charlie", "Crean", "Cadorette", "Goree", "Lennon", "Elsea", "Fein", "Tincher", "Dunaway", "Newcombe", "Bruneau", "Quattlebaum", "Tsao",
			"Herrington", "Alvarenga", "Benton", "Brenes", "Skillings", "Pack", "Carbo", "Jefferson", "Hubbert", "Longerbeam", "Amann", "Vancleve", "Councill",
			"Mota", "Reller", "Leverette", "Reichard", "Lezama", "Caya", "Schank", "Poppe", "Hudgens", "Shattuck", "Woodruff", "Vasquez", "Maxim", "Bernstein",
			"Gannon", "Park", "Rondon", "Quintana", "Bowersox", "Motton", "Maysonet", "Hotaling", "Hernadez", "Dahlstrom", "Clemens", "Melchior", "Nevitt",
			"Studivant", "Frisch", "Moniz", "Hamed", "Confer", "Acy", "Eversole", "Stalker", "Messersmith", "Propst", "Baskin", "Heeren", "Nass", "Greenberg",
			"Pfister", "Strahan", "Borum", "Riker", "Clyburn", "Arata", "Saunders", "Peltz", "Manfredi", "Captain", "Skoog", "Loo", "Carbonneau", "Madill",
			"Reichman", "Baden", "Crawshaw", "Sieber", "Oriol", "Rodi", "Bare", "Scher", "Revel", "Pedigo", "Cundiff", "Lesage", "Tavares", "Wickwire",
			"Sumpter", "Lusk", "Sibrian", "Waldon", "Dykstra", "Rosin", "Swank", "Pando", "Penaflor", "Steves", "Nogueira", "Rodas", "Saylors", "Maughan",
			"Binion", "Bendickson", "Deloney", "Respass", "Pinkerton", "Carolina", "Hukill", "Berthelot", "Mangus", "Percy", "Gracey", "Grossman", "Shipley",
			"Gibbons", "Tripp", "Wheeler", "Pelley", "Kadel", "Isabell", "Wong", "Weatherby", "Monterrosa", "Bowler", "Fairbairn", "Tusing", "Bellah", "Dan",
			"Wilk", "Leamon", "Purtee", "Erler", "Goree", "Manalo", "Bakker", "Joslyn", "Sulser", "Quigley", "Denis", "Hooks", "Garwood", "Negrin", "Lavallee",
			"Yuhas", "Wildey", "Keisler", "Bickers", "Malmberg", "Doner", "Otte", "Whisenant", "Paynter", "Huges", "Giannini", "Bushong", "Galentine", "Baity",
			"Feth", "Grondin", "Fiorito", "Keeble", "Deschaine", "Runk", "Wedell", "Regis", "Michelin", "Bogart", "Siewert", "Pascale", "Seevers", "Rosendahl",
			"Knott", "Bergh", "Hardee", "Tefft", "Belgrave", "Honea", "Hamp", "Vinton", "Donovan", "Samms", "Ginn", "Huston", "Sobczak", "Todd", "Walker",
			"Fredricks", "Futrell", "Menzies", "Stricklin", "Conant", "Saville", "Mceuen", "Schneider", "Spady", "Lefkowitz", "Mcmakin", "Gottfried", "Flavors",
			"Dively", "Hynes", "Brandstetter", "Partridge", "Schrage", "Massi", "Normandeau", "Macon", "Uchida", "Dike", "Blais", "Searfoss", "Blakes",
			"Gallager", "Boxer", "Bolds", "Rohlfing", "Opitz", "Samora", "Rehbein", "Demark", "Bull", "Daves", "Bartolome", "Hathcock", "Bicknell", "Couts",
			"Goldberg", "Lawver", "Bixby", "Azevedo", "Bechtold", "Hui", "Duffie", "Busby", "Pattee", "Shehan", "Nail", "Ostlund", "Ibarra", "Norgard",
			"Mcclane", "Buffington", "Samuel", "Markle", "Seager", "Goodreau", "Hipsher", "Abron", "Layman", "Efird", "Turney", "Walch", "Shehorn", "Citizen",
			"Stejskal", "Hanger", "Siler", "Ellen", "Wenzel", "File", "Christy", "Shott", "Judon", "Heiner", "Orozco", "Simard", "Kolb", "Nugent", "Odum",
			"Smithson", "Suazo", "Vanepps", "Arechiga", "Munger", "Adorno", "Gust", "Marcotte", "Pick", "Sorenson", "Evitt", "Rasch", "Schulz", "Whittington",
			"Slaughter", "Lyda", "Eggebrecht", "Hickox", "Coffey", "Bauer", "Tumlinson", "Bartkowski", "Porche", "Klemm", "Pipes", "Bernard", "Siebert",
			"Cartlidge", "Hernandes", "Schaible", "Vandyke", "Steed", "Isaman", "Lipford", "Medrano", "Groseclose", "Roussel", "Moffat", "Harju", "Dant",
			"Villeda", "Ciampa", "Culp", "Doverspike", "Bickford", "Lambright", "Mcreynolds", "Rowser", "Shurtleff", "Sumler", "Patague", "Doig", "Landwehr",
			"Blakeney", "Natale", "Silsby", "Zaragoza", "Shipe", "Lescarbeau", "Cerezo", "Cheshire", "Balbuena", "Pichon", "Pursley", "Wolfram", "Carrizales",
			"Schacht", "Brazzell", "Kelliher", "Chaffins", "Schissler", "Corbeil", "Loch", "Goggin", "Zawacki", "Hess", "Eriksson", "Beegle", "Horiuchi",
			"Plain", "Mccranie", "Holladay", "Joesph", "Hamlin", "Uren", "Lore", "Lazar", "Ehrenberg", "Danielson", "Pomeroy", "Rowles", "Alger", "Kesterson",
			"Cowling", "Bloom", "Meyer", "Wiren", "Cheslock", "Arline", "Durr", "Morency", "Gandara", "Kroger", "Chico", "Dubrey", "Gabor", "Wegener",
			"Brannan", "Schram", "Check", "Ducharme", "Haakenson", "Forney", "Newlin", "Tannehill", "Estabrook", "Barrs", "Felker", "Hinshaw", "Odowd",
			"Wakefield", "Solem", "Bogert", "Heckart", "Parkins", "Shenkel", "Lade", "Rivenbark", "Fonville", "Hyatt", "Sakata", "Hartl", "Meade", "Erby",
			"Moring", "Sullivan", "Preciado", "Hadlock", "Smalls", "Guidi", "Kagawa", "Sarabia", "Leno", "Przybyla", "Hodapp", "Mccabe", "Antle", "Vedder",
			"Deschamp", "Keplin", "Kirby", "Racanelli", "Vizcarrondo", "Sellman", "Balmer", "Troxler", "Borth", "Garr", "Botta", "Hodgins", "Rochford", "Grace",
			"Warden", "Lake", "Lesley", "Nussbaum", "Carmichael", "Moret", "Scot", "Pippin", "Tadeo", "Rosel", "Test", "Binford", "Steinberger", "Duff",
			"Cashen", "Humfeld", "Aldana", "Seymour", "Ziemba", "Delaune", "Oyola", "Fridley", "Huls", "Recker", "Hey", "Maxson", "Owens", "Dearing", "Fusaro",
			"Bade", "Craner", "Coronado", "Glad", "Flax", "Wallach", "Macomber", "Jesse", "Abels", "Uhrich", "Courts", "Poisson", "Winnie", "Gilham", "Bartel",
			"Niblett", "Burdine", "Grissett", "Lanning", "Barrington", "Ditullio", "Bredeson", "Schommer", "Cape", "Osuna", "Carlo", "Stepney", "Riviera",
			"Peyton", "Bollin", "Alcock", "Gaddis", "Wickline", "Tryon", "Tarwater", "Nakasone", "Dacus", "Roemer", "Hirst", "Burgard", "Kong", "Braziel",
			"Filler", "Normandin", "Perillo", "Yamada", "Hands", "Wulf", "Reuss", "Pinson", "Scheid", "Akerley", "Greenspan", "Toussaint", "Deak", "Grosch",
			"Auclair", "Suire", "Dahlstrom", "Koenig", "Ferrara", "Aumiller", "Harms", "Magrath", "Thistle", "Vivanco", "Monroe", "Spieker", "Teamer",
			"Flesher", "Lama", "Mccorkle", "Sheikh", "Holtz", "Durfey", "Midgett", "Minto", "Quaid", "Evins", "Hatt", "Keesling", "Vangorder", "Zeh", "Shutter",
			"Gaymon", "Martyn", "Bolander", "Wycoff", "Duran", "Hurt", "Vanfossen", "Holton", "Gao", "Ong", "Roop", "Speller", "Lanctot", "Gales", "Descoteaux",
			"Farrow", "Speidel", "Book", "Soliman", "Hurley", "Ronk", "Kerwin", "Eoff", "Matte", "Wilmeth", "Lundquist", "Balderas", "Weideman", "Spradling",
			"Chadwell", "Fellers", "Cokley", "Smythe", "Conlan", "Buchholz", "Knell", "Villegas", "Paschke", "Lenahan", "Hon", "Milhorn", "Klahn", "Debow",
			"Devries", "Loux", "Reisman", "Lukasik", "Montford", "Vanwingerden", "Curren", "Roehl", "Viars", "Belew", "Alcon", "Spofford", "Neer", "Yepez",
			"Fetty", "Westley", "Gamblin", "Amoroso", "Burks", "Chew", "Hinerman", "Dreiling", "Ludlow", "Turrentine", "Kendrick", "Marten", "Desouza",
			"Hankinson", "Pigman", "Mcmillan", "Kreidler", "Dorris", "Kam", "Yarger", "Holifield", "Boelter", "Durgin", "Amundsen", "Arcand", "Viands",
			"Makuch", "Jimmerson", "Murdoch", "Eakes", "Ebbert", "Ingle", "Bollinger", "Hewett", "Bartlow", "Canty", "Smock", "Lollar", "Sinclair", "Birch",
			"Frenkel", "Bratcher", "Saechao", "Morrell", "Letchworth", "Abbott", "Sensabaugh", "Meehan", "Jezierski", "Reisinger", "Capell", "Theriot",
			"Grissom", "Hellums", "Christ", "Melara", "Gile", "Yawn", "Hartman", "Shockley", "Sistrunk", "Giltner", "Mehan", "Sirmans", "Downing", "Grayson",
			"Infante", "Reynaga", "Kong", "Mapes", "Casebolt", "Ticknor", "Bruening", "Crow", "Meszaros", "Hubbs", "Duchene", "Cavallaro", "Dole", "Harner",
			"Lemmon", "Chichester", "Antunez", "Gartner", "Arledge", "Pennell", "Voris", "Clinton", "Gaeta", "Tee", "Hershman", "Shawver", "Tworek", "Haddox",
			"Blythe", "Harward", "Cueto", "Bejarano", "Fusco", "Vang", "Pellegren", "Wattley", "Matranga", "Fike", "Waldow", "Halliwell", "Welk", "Tucci",
			"Parson", "Stich", "Alberti", "Deharo", "Lepore", "Sackrider", "Tedesco", "Ishee", "Reith", "Petitt", "Claypool", "Critchlow", "Mailloux",
			"Arguelles", "Sheffler", "Plourde", "Haugh", "Tinkler", "Golder", "Frenzel", "Meinhardt", "Blythe", "Serafini", "Bevers", "Hinderliter",
			"Rodriguez", "Ott", "Schmuck", "Lafond", "Savell", "Russo", "Keltner", "Chaloux", "Schrecengost", "Mertes", "Witham", "Orcutt", "Connor",
			"Tichenor", "Erskine", "Rainbolt", "Schamber", "Lagos", "Kimery", "Kinch", "Mckinnie", "Eagan", "Kridler", "Overman", "Maranto", "Haddad",
			"Coppinger", "Mingo", "Stills", "Sturgeon", "Halpin", "Farrelly", "Sidebottom", "Luebbers", "Mcdill", "Carver", "Dyson", "Milazzo", "Spratt",
			"Valderas", "Volz", "Poirrier", "Craine", "Flemister", "Spagnola", "Gallegos", "Jo", "Kopp", "Lan", "Currin", "Plowman", "Jobst", "Johannsen",
			"Norvell", "Dettman", "Gibney", "Mcnerney", "Stanger", "Fast", "Hohman", "Cavins", "Fogarty", "Havard", "Donoghue", "Priolo", "Balducci", "Pastore",
			"Servin", "Kirkley", "Gerrish", "Eaker", "Mosby", "Sommers", "Kaiser", "Hubler", "Terrell", "Zeno", "Fiala", "Gangemi", "Capps", "Brosnahan",
			"Brogden", "Connelly", "Duhe", "Clement", "Zimmerman", "Nakken", "Sharper", "Rickard", "Brodnax", "Modlin", "Autrey", "Arledge", "Westray",
			"Harbin", "Trawick", "Farquharson", "Dietrick", "Fagen", "Buechner", "Jurgens", "Macaulay", "Moultrie", "Bayard", "Ingram", "Popek", "Granberry",
			"Wilhoit", "Burrows", "Chapel", "Cayetano", "Hagedorn", "Jobst", "Nailor", "Schaber", "Rando", "Waybright", "Weakley", "Rand", "Moulder",
			"Burroughs", "Kettner", "Bumbalough", "Hilaire", "Klosterman", "Westhoff", "Weisel", "Fancher", "Sartori" };

	final static String[] museumNames1 = { "Accident", "Aerial", "Aerospace", "Amphibian", "Analysis", "Anatomy", "Ancestry", "Anthropology", "Apparatus",
			"Aquatic", "Archaeology", "Architecture", "Art", "Artillery", "Astronomy", "Audiovisual", "Auditory", "Blossoms", "Carnival", "Changing",
			"Childhood", "Children", "Chrono", "Cryonic", "Circus", "Contemporary", "Cooking", "Costume", "Creativity", "Cryptology", "Crystal", "Cultural",
			"Culture", "Curiosity", "Dance", "Darkness", "Data", "Decorative", "Design", "Digital", "Dinosaur", "Disaster", "Discovery", "Dream", "Dynamics",
			"Earth", "Electric", "Emergency", "Estate", "Eternity", "Expedition", "Experiment", "Experimentation", "Exploration", "Explorers", "Fame",
			"Fantasy", "Fashion", "Fear", "Fiction", "Film", "Fire", "Firepower", "Forest", "Fortune", "Frost", "Future", "Gadget", "Galaxy", "Game", "Garden",
			"Genius", "Gift", "Gizmo", "Glass", "Globe", "Gold", "Guardian", "Happiness", "Harmony", "Hazard", "Heritage", "History", "Hope", "Horror",
			"Illusion", "Imagination", "Immersion", "Infinity", "Ingenuity", "Inheritance", "Innovation", "Insect", "Inspiration", "Instrument", "Invention",
			"Jewel", "Kinetics", "Language", "Learning", "Legacy", "Liberty", "Light", "Literature", "Living", "Locomotion", "Love", "Lunar", "Machine",
			"Magic", "Magnetic", "Manor", "Manuscript", "Maritime", "Master", "Mechanical", "Medical", "Micro", "Mind", "Mineral", "Mistake", "Movement",
			"Music", "Musical", "Natural", "Nature", "Neurological", "Night", "Nightingale", "Nightmare", "Novelty", "Open", "Optical", "Origin", "Origins",
			"Party", "Passage", "Passion", "People", "Perception", "Pet", "Petrified", "Pinnacle", "Play", "Portrait", "Prediction", "Puppet", "Pyro",
			"Reconnaissance", "Religion", "Reptile", "Research", "Revelation", "Rush", "Sail", "Science", "Science Fiction", "Secrets", "Sensory", "Serpent",
			"Sight", "Sightings", "Silent", "Skeleton", "Snow", "Solar", "Space", "Speech", "Spinning", "Sports", "Stimulation", "Submarine", "Submerged",
			"Sugar", "Summer", "Surprise", "Tactile", "Taste", "Testing", "Theater", "Time", "Toy", "Tradition", "Tragedy", "Transport", "Travel", "Treasure",
			"Trinket", "Underground", "Universe", "Video", "Virtual", "Virtuoso", "Visual", "War", "Water", "Weirdness", "Wild", "Wind", "Winter", "Youth",
			"Zoology" };
	final static String[] museumNames2 = { "Centre", "Centre", "Center", "Center", "Exhibition", "Gallery", "Gallery", "Hall", "Hall", "Institute",
			"Institution", "Museum", "Museum", "Museum", "Treasury", "Vault" };
	final static String[] museumNames3 = { "National", "International", "Grand", "Great", "Central", "Royal" };
	final static String[] museumNames4 = { "Accidents", "Aerial Technology", "Aerospace", "Amphibians", "Analysis", "Anatomy", "Ancestry", "Anthropology",
			"Aquatic Life", "Archaeology", "Architecture", "Art", "Artillery", "Astronomy", "Audio", "Audiovisual Arts", "Blossoms", "Carnivals", "Change",
			"Childhood", "Children", "Cryonic Technology", "Circuses", "Contemporary Arts", "Cooking", "Costumes", "Creativity", "Cryptology", "Crystals",
			"Cultural History", "Culture", "Curiosity", "Dance", "Darkness", "Data", "Decoration", "Design", "Digital Arts", "Dinosaurs", "Disaster",
			"Discovery", "Dreams", "Dynamics", "Earth", "Electricity", "Emergencies", "Eternity", "Expeditions", "Experimentations", "Experiments",
			"Exploration", "Explorers", "Fame", "Fantasy", "Fashion", "Fear", "Fiction", "Film", "Fire", "Firepower", "Forests", "Fortune", "Frost", "Gadgets",
			"Games", "Gardens", "Geniuses", "Gifts", "Gizmos", "Glass", "Gold", "Guardians", "Happiness", "Harmony", "Hazards", "Heritage", "History", "Hope",
			"Horrors", "Illusions", "Imagination", "Immersion", "Infinity", "Ingenuity", "Inheritance", "Innovation", "Insects", "Inspiration", "Instruments",
			"Inventions", "Jewels", "Kinetics", "Language", "Learning", "Legacies", "Liberty", "Light", "Literature", "Living", "Locomotion", "Love",
			"Lunar History", "Machines", "Magic", "Magnetics", "Manors", "Manuscripts", "Maritime History", "Masters", "Mechanics", "Medicine", "Micro Life",
			"Minerals", "Mistakes", "Movement", "Music", "Musicals", "Natural History", "Nature", "Neurological Science", "Nightmares", "Novelties", "Openness",
			"Optical Illusions", "Origins", "Parties", "Passages", "Passion", "People", "Perception", "Petrification", "Pets", "Pinnacles", "Play", "Portraits",
			"Predictions", "Puppets", "Pyromania", "Reconnaissance", "Religion", "Reptiles", "Research", "Revelations", "Rush Hours", "Sailing", "Science",
			"Science Fiction", "Secrets", "Sensory Science", "Serpents", "Sight", "Sightings", "Silence", "Skeletons", "Snow", "Solar History", "Space",
			"Speech", "Sports", "Stimulation", "Submarines", "Submersion", "Sugar", "Summer", "Surprises", "Tactics", "Taste", "Testing", "Theater", "Time",
			"Toys", "Traditions", "Tragedy", "Transport", "Travel", "Treasures", "Trinkets", "Video", "Virtual Reality", "Visual Arts", "War", "Water",
			"Weirdness", "Wind", "Winter", "Youth", "Zoology", "the Future", "the Galaxy", "the Globe", "the Mind", "the Night", "the Nightingale",
			"the Underground", "the Universe", "the Wild" };

	final static String[] artwork1 = { "Abandoned", "Abstract", "Active", "Amusing", "Anchored", "Ancient", "Arctic", "Aspiring", "Attractive", "Auspicious",
			"Awoken", "Belated", "Bewildered", "Bewitched", "Bizarre", "Blank", "Bountiful", "Bright", "Brilliant", "Broken", "Calamitous", "Capricious",
			"Carefree", "Celebrated", "Changing", "Composed", "Conscious", "Contemplating", "Courteous", "Creative", "Crowded", "Crushing", "Dapper", "Defiant",
			"Delicate", "Delightful", "Diligent", "Disguised", "Distorted", "Divine", "Dramatic", "Dynamic", "Ecstatic", "Elated", "Electric", "Elementary",
			"Eminent", "Enchanted", "Enchanting", "Enlightened", "Equable", "Esteemed", "Ethereal", "Euphoric", "Evanescent", "Everlasting", "Exalted",
			"Exasperated", "Exclusive", "Exhilarated", "Exuberant", "Exultant", "False", "Familiar", "Fanatical", "Fickle", "Fluttering", "Forsaken", "Fragile",
			"Frightening", "Frivolous", "Future", "Generous", "Glorious", "Gracious", "Gregarious", "Grieving", "Grim", "Grounded", "Guilty", "Guiltless",
			"Hallowed", "Holistic", "Hollow", "Honorable", "Hypnotic", "Illustrious", "Imminent", "Impure", "Infernal", "Infinite", "Internal", "Invincible",
			"Invisible", "Jealous", "Juvenile", "Lamentable", "Living", "Lone", "Lonely", "Macabre", "Majestic", "Malicious", "Marked", "Merciful", "Monstrous",
			"Muddled", "Nonchalant", "Nonsensical", "Nostalgic", "Noxious", "Obedient", "Oblivious", "Obsequious", "Omniscient", "Onerous", "Outrageous",
			"Overdue", "Paltry", "Peaceful", "Perfect", "Perpetual", "Picayune", "Playful", "Pointless", "Pristine", "Psychedelic", "Punctual", "Putrid",
			"Quiet", "Redundant", "Repulsive", "Rightful", "Ruthless", "Scandalous", "Scintillating", "Serene", "Shallow", "Shameless", "Silent", "Sniveling",
			"Spectacular", "Spirited", "Spiritual", "Spurious", "Stained", "Striking", "Stunning", "Tainted", "Tasteless", "Tawdry", "Temporary", "Tender",
			"Thundering", "Tranquil", "Trivial", "Troubled", "Trusting", "Turbulent", "Uncovered", "Undesirable", "Unfortunate", "Unwelcome", "Unwritten",
			"Utopian", "Veiled", "Vicious", "Voiceless", "Volatile", "Wandering", "Warped", "Wicked", "Wilted", "Worthless", "Wretched" };
	final static String[] artwork2 = { "Accident", "Action", "Adventure", "Aftermath", "Afternoon", "Afterthought", "Ambition", "Anger", "Animal", "Answer",
			"Anxiety", "Apartment", "Appearance", "Appointment", "Approval", "Arrival", "Assistant", "Audience", "Autumn", "Balance", "Basis", "Battle",
			"Beach", "Beast", "Beauty", "Beggar", "Belief", "Blame", "Blood", "Body", "Border", "Bother", "Boy", "Boyfriend", "Brain", "Breath", "Brother",
			"Bubble", "Business", "Candle", "Canvas", "Catch", "Celebration", "Cemetery", "Chain", "Chains", "Challenge", "Champion", "Change", "Chaos",
			"Charity", "Child", "Childhood", "City", "Clarity", "Clerk", "Client", "Climate", "Clock", "Clocks", "Cloud", "Clouds", "Coast", "Cobweb", "Collar",
			"Command", "Compassion", "Confidence", "Consequence", "Consequences", "Contentment", "Contract", "Conversation", "Country", "Couple", "Courage",
			"Cover", "Crash", "Creator", "Creature", "Crime", "Crown", "Curiosity", "Curtain", "Daughter", "Death", "Deceit", "Decision", "Defeat", "Delight",
			"Demand", "Democracy", "Depression", "Desire", "Despair", "Destruction", "Development", "Dictator", "Dictatorship", "Dimension", "Direction",
			"Discovery", "Display", "Disturbance", "Divide", "Division", "Doll", "Doubt", "Drama", "Dream", "Dreams", "Drive", "Dust", "Duty", "Earth", "Edge",
			"Election", "Emergency", "End", "Enhancement", "Entrance", "Entry", "Environment", "Eternity", "Excuse", "Expansion", "Expression", "Extreme",
			"Extremes", "Faith", "Fall", "Family", "Fate", "Father", "Feast", "Feeling", "Fight", "Figure", "Flame", "Flight", "Flock", "Flower", "Fluke",
			"Fortune", "Freedom", "Friction", "Friend", "Friends", "Friendship", "Front", "Future", "Garden", "Gift", "Girl", "Girlfriend", "Goal", "Grace",
			"Guidance", "Guide", "Harmony", "Hatred", "Honesty", "Honor", "Humor", "Idea", "Ideal", "Imagination", "Impact", "Incident", "Independence",
			"Infatuation", "Influence", "Insanity", "Instrument", "Integrity", "Invention", "Invitation", "Jewel", "Judgment", "Justice", "King", "Knowledge",
			"Leader", "Leadership", "Lesson", "Lessons", "Liberty", "Love", "Loyalty", "Luck", "Luxury", "Magic", "Mask", "Master", "Mercy", "Message", "Might",
			"Miracle", "Mirror", "Misery", "Mistake", "Mistress", "Moment", "Morning", "Mother", "Motion", "Movement", "Nerve", "Network", "Night", "Nobody",
			"Oath", "Obligation", "Omen", "Opinion", "Option", "Parent", "Parents", "Passage", "Perception", "Permission", "Perseverance", "Perspective",
			"Pollution", "Possession", "Possessions", "Potential", "Practice", "Preparation", "Pressure", "Price", "Pride", "Priority", "Problem", "Problems",
			"Process", "Profile", "Protest", "Punishment", "Queen", "Question", "Questions", "Reaction", "Reality", "Reception", "Recognition", "Redemption",
			"Reflection", "Refusal", "Release", "Relief", "Request", "Requiem", "Revolution", "Riddle", "Riddles", "Romance", "Sanity", "Satisfaction",
			"Secret", "Security", "Sentence", "Servant", "Service", "Shadow", "Shadows", "Shame", "Shelter", "Sign", "Signal", "Singer", "Sister", "Slave",
			"Slavery", "Smile", "Smoke", "Solution", "Son", "Song", "Sorrow", "Spoils", "Spring", "Storm", "Stranger", "Strength", "Stress", "Stretch",
			"Struggle", "Submission", "Success", "Summer", "Support", "Surprise", "Suspect", "Switch", "Sympathy", "Temper", "Tension", "Territory", "Thrill",
			"Throne", "Tolerance", "Tradition", "Traditions", "Treatment", "Union", "Vacation", "Variety", "Vessel", "Visitor", "Voyage", "Web", "Weekend",
			"Weight", "Wild", "Wilderness", "Winter", "Wisdom", "Wonder", "Wound", "Wounds" };
	final static String[] artwork3 = { "Accidents", "Action", "Adventure", "Aftermath", "Afternoon", "Afterthoughts", "Ambitions", "Anger", "Animals",
			"Answers", "Anxiety", "Approval", "Arrival", "Assistants", "Audience", "Autumn", "Balance", "Basis", "Battles", "Beach", "Beasts", "Beauty",
			"Belief", "Blood", "Bodies", "Borders", "Breath", "Brothers", "Business", "Candles", "Canvas", "Celebrations", "Cemetery", "Chain", "Chains",
			"Challenges", "Champions", "Change", "Chaos", "Charity", "Child", "Childhood", "City", "Cities", "Clarity", "Clouds", "Collar", "Command",
			"Compassion", "Confidence", "Consequence", "Consequences", "Contentment", "Contracts", "Conversation", "Conversations", "Country", "Courage",
			"Cover", "Crash", "Creator", "Creature", "Creatures", "Crime", "Crown", "Curiosity", "Curtain", "Curtains", "Daughter", "Death", "Deceit",
			"Decision", "Decisions", "Defeat", "Delight", "Delights", "Demand", "Demands", "Democracy", "Depression", "Depressions", "Desire", "Desires",
			"Despair", "Destruction", "Development", "Dictator", "Dictatorship", "Dimension", "Dimensions", "Direction", "Directions", "Discovery", "Display",
			"Disturbance", "Divide", "Division", "Divisions", "Doll", "Doubt", "Drama", "Dream", "Dreams", "Dust", "Duty", "Edge", "Emergency", "End",
			"Enhancement", "Eternity", "Excuse", "Expansion", "Expression", "Extremes", "Faith", "Fall", "Family", "Fate", "Father", "Feast", "Feeling",
			"Fights", "Figures", "Flames", "Flights", "Flock", "Flowers", "Fortunes", "Freedom", "Friend", "Friends", "Front", "Fronts", "Future", "Futures",
			"Garden", "Gardens", "Gift", "Gifts", "Goal", "Goals", "Grace", "Guidance", "Guide", "Harmony", "Hatred", "Honesty", "Honor", "Humor", "Idea",
			"Ideas", "Ideal", "Ideals", "Imagination", "Impact", "Incident", "Independence", "Infatuation", "Influence", "Insanity", "Instrument",
			"Instruments", "Integrity", "Invention", "Inventions", "Invitation", "Jewel", "Jewels", "Judgment", "Justice", "King", "Kings", "Knowledge",
			"Leader", "Leaders", "Leadership", "Lesson", "Lessons", "Liberty", "Love", "Loyalty", "Luck", "Luxury", "Mask", "Masks", "Master", "Masters",
			"Mercy", "Message", "Messages", "Might", "Miracle", "Miracles", "Mirror", "Mirrors", "Misery", "Miseries", "Mistake", "Mistakes", "Mistress",
			"Moment", "Moments", "Morning", "Mother", "Motion", "Motions", "Movement", "Network", "Night", "Obligation", "Omen", "Passage", "Perception",
			"Permission", "Perseverance", "Perspective", "Pollution", "Possession", "Possessions", "Potential", "Pressure", "Price", "Pride", "Priority",
			"Problem", "Problems", "Process", "Profile", "Profiles", "Protest", "Protests", "Punishment", "Queen", "Queens", "Question", "Questions", "Reality",
			"Recognition", "Redemption", "Reflection", "Refusal", "Release", "Relief", "Request", "Requiem", "Revolution", "Satisfaction", "Secret", "Secrets",
			"Servant", "Service", "Shadow", "Shadows", "Shame", "Shelter", "Sign", "Signs", "Signal", "Signals", "Slave", "Slavery", "Smile", "Smoke", "Son",
			"Song", "Sorrow", "Spoils", "Spring", "Storm", "Stranger", "Strangers", "Strength", "Struggle", "Submission", "Summer", "Support", "Surprise",
			"Suspect", "Switch", "Sympathy", "Temper", "Tension", "Territory", "Thrill", "Thrills", "Throne", "Tolerance", "Tradition", "Traditions",
			"Treatment", "Union", "Vessel", "Voyage", "Web", "Weight", "Wilderness", "Winter", "Wisdom", "Wonder", "Wound", "Wounds" };
	final static String[] artwork4 = { "Abuse", "Adventure", "Amazement", "Ambition", "Amusement", "Anger", "Anxiety", "Attraction", "Authority", "Balance",
			"Battle", "Beauty", "Belief", "Birth", "Blood", "Bravery", "Brilliance", "Brutality", "Celebration", "Change", "Chaos", "Charity", "Chemistry",
			"Childhood", "Choice", "Clarity", "Confusion", "Courage", "Creation", "Crime", "Culture", "Curiosity", "Darkness", "Death", "Deceit", "Dedication",
			"Delight", "Design", "Desire", "Despair", "Disaster", "Discovery", "Dreams", "Earth", "Education", "Emotion", "Empathy", "Employment", "Eternity",
			"Evil", "Existence", "Faith", "Fate", "Finance", "Fire", "Flame", "Forever", "Fortune", "Fragility", "Frailty", "Freedom", "Generosity", "Grace",
			"Harmony", "Honesty", "Horror", "Ice", "Imagination", "Independence", "Infatuation", "Insanity", "Invention", "Jealousy", "Justice", "Language",
			"Laughter", "Liberty", "Life", "Love", "Luck", "Misery", "Mystery", "Nobody", "Opportunity", "Panic", "Parenthood", "Passion", "Patience",
			"Patriotism", "Perseverance", "Perspective", "Philosophy", "Pleasure", "Poverty", "Pride", "Prison", "Psychology", "Purpose", "Reality",
			"Redemption", "Reflection", "Religion", "Rhythm", "Romance", "Self-Control", "Sensitivity", "Service", "Significance", "Solitude", "Spirits",
			"Storms", "Struggle", "Surprise", "Sympathy", "Technology", "Time", "Tomorrow", "Uncertainty", "War", "Weakness", "Wonder" };

	final public static Random r = new Random();

	private static String getRndAry(String[] ary) {
		return ary[r.nextInt(ary.length)];
	}

	/**
	 * Get an random element from this array
	 * 
	 * @param ary
	 * @return
	 */
	public static Object getRnd(Object... ary) {
		return ary[r.nextInt(ary.length)];
	}

	/**
	 * Get an random element from this array
	 * 
	 * @param ary
	 * @return
	 */
	public static Object getRndA(Object[] ary) {
		return ary[r.nextInt(ary.length)];
	}

	/**
	 * Get a Town Name
	 * 
	 * @return
	 */
	public static String getTown() {
		return getRndAry(townNames);
	}

	/**
	 * Get a Male Name
	 * 
	 * @return
	 */
	public static String getMale() {
		return getRndAry(maleNames);
	}

	/**
	 * Get a FeMale Name
	 * 
	 * @return
	 */
	public static String getFemale() {
		return getRndAry(femaleNames);
	}

	/**
	 * Get a FeMale Name
	 * 
	 * @return
	 */
	public static String getSurname() {
		return getRndAry(surName);
	}

	/**
	 * Get a FeMale Name
	 * 
	 * @return
	 */
	public static String getName(boolean sex) {
		return sex ? getFemale() : getMale() + " " + getSurname();
	}

	/**
	 * Return a random int
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getIntBetween(int min, int max) {
		return r.nextInt(max - min) + min;
	}

	/**
	 * Generate a name for the museum
	 * 
	 * @param player
	 * @param town
	 * @author http://www.fantasynamegenerators.com/museum-names.php
	 * @return
	 */

	public static String getMuseumName(String player, String town) {
		String name;

		if (r.nextBoolean()) {
			name = getRndAry(museumNames2) + " of " + getRndAry(museumNames4);
		} else {
			name = getRndAry(museumNames1) + " " + getRndAry(museumNames2);
		}

		if (r.nextInt(10) < 4) {
			name = getRndAry(museumNames3) + " " + name;
		}

		if (player != null && NamesGenerator.r.nextInt(10) < 3) {
			name = player + "'s " + name;
		}

		if (town != null && NamesGenerator.r.nextInt(10) < 3) {
			name = name + " of " + town;
		}

		return name;
	}

	/**
	 * Generate a name for an artwork
	 * 
	 * @param player
	 * @param town
	 * @author http://www.fantasynamegenerators.com/artwork-names.php
	 * @return
	 */

	public static String getArtWorkName() {

		if (r.nextBoolean()) {
			if (r.nextBoolean()) {
				return getRndAry(artwork2);
			} else {
				return getRndAry(artwork1) + " " + getRndAry(artwork2);
			}
		} else {
			return getRndAry(artwork3) + " of " + getRndAry(artwork4);
		}
	}
}
