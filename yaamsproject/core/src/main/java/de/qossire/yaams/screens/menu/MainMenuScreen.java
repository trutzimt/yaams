/**
 *
 */
package de.qossire.yaams.screens.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.level.ScenarioManagement;
import de.qossire.yaams.music.YMusic;
import de.qossire.yaams.saveload.SaveLoadManagement;
import de.qossire.yaams.saveload.YSaveInfo;
import de.qossire.yaams.screens.base.BaseMenuScreen;
import de.qossire.yaams.screens.game.GameLoaderScreen;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YMessageDialog;
import de.qossire.yaams.ui.YTextButton;
import de.qossire.yaams.ui.actions.FeedbackAction;

/**
 * @author sven
 *
 */
public class MainMenuScreen extends BaseMenuScreen {

	private Image menuBack;
	// protected String logo;

	/**
	 * Create it
	 *
	 * @param qossire
	 */
	public MainMenuScreen(Yaams yaams) {
		super(yaams);
		YMusic.mainmenu();

		menuBack = new Image(YStatic.systemAssets.get("system/menu/Castle2.png", Texture.class));
		// menuBack.setScale(Gdx.graphics.getWidth() / menuBack.getWidth(),
		// Gdx.graphics.getHeight() / menuBack.getHeight());
		menuBack.setFillParent(true);
		// menuBack.setY(Gdx.graphics.getHeight() - menuBack.getHeight());
		stage.addActor(menuBack);

		stage.addActor(new MainMenuWindow(yaams, stage));
		stage.addActor(new VisLabel(YConfig.VERSION + (YSettings.isDebug() ? "-DEBUG" : "")));
		stage.getRoot().getColor().a = 0;
		stage.getRoot().addAction(Actions.fadeIn(1));

		startupCheck();
	}

	/**
	 * First run
	 */
	protected void startupCheck() {
		// first run?
		if (YSettings.getPref().getFloat("firstRun", 0f) != (float) YConfig.VERSION) {
			YMessageDialog q = new YMessageDialog(yaams, "Welcome to yaaMs V" + YConfig.VERSION, CreditsWindow.getWelcomeMsg(), new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					YSettings.getPref().putFloat("firstRun", (float) YConfig.VERSION);
					YSettings.save();

				}
			});
			q.addButton(new FeedbackAction().getButton(yaams));
			q.addButton(new YTextButton("Start Tutorial") {

				@Override
				public void perform() {
					yaams.switchScreen(new GameLoaderScreen(yaams, ScenarioManagement.getCampaign("tut").getScenario("tut1")));
				}
			});
			q.setHeight(Gdx.graphics.getHeight() * 2 / 3);
			q.build(stage);

			return;
		}

		// has a temp file?
		if (SaveLoadManagement.exist("temp")) {
			YSaveInfo q = SaveLoadManagement.loadConfig("temp");
			final YMessageDialog m = new YMessageDialog(yaams, "Load last game?", "Museum: " + q.getSettings().get("museum"), null);
			m.addButton(new YTextButton("Delete and Close") {

				@Override
				public void perform() {
					SaveLoadManagement.delete("temp");
					m.close();

				}
			});
			m.addButton(new YTextButton("Load save game") {

				@Override
				public void perform() {
					SaveLoadManagement.load("temp", yaams, stage);

				}
			});
			m.pack();
			m.build(stage);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		// menuBack.setScale(width / menuBack.getWidth(), height /
		// menuBack.getHeight());
		// menuBack.setY(height - menuBack.getHeight());
	}

}
