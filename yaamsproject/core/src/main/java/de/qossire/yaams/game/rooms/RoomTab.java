/**
 * 
 */
package de.qossire.yaams.game.rooms;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YSplitTab;

/**
 * @author sven
 *
 */
public class RoomTab extends YSplitTab {

	public RoomTab() {
		super("Rooms", "At the moment you have no room. You can construct some in the building menu.");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(Button btn) {
		return ((BaseRoom) btn.getUserObject()).getInfoPanel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		elements.clear();

		// add all elements
		for (BaseRoom room : MapScreen.get().getPlayer().getRooms().getRooms()) {
			addElement(room.getName(), room.getIcon(), room);
		}

		super.rebuild();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#doubleClickElement(com.badlogic.gdx.scenes.
	 * scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {}

}