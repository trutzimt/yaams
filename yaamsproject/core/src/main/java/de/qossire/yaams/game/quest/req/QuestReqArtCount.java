package de.qossire.yaams.game.quest.req;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqArtCount extends BaseQuestReqMinCount {

	private ArtStatus status;

	/**
	 * @param prop
	 * @param val
	 */
	public QuestReqArtCount(ArtStatus status, int count) {
		super(count);

		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + "Artworks";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getActCount()
	 */
	@Override
	public int getActCount() {
		return MapScreen.get().getPlayer().getArtwork().getArtsCount(status);
	}

}