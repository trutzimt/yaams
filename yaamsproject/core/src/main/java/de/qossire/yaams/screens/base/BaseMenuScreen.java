/**
 *
 */
package de.qossire.yaams.screens.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;

/**
 * @author sven
 *
 */
public class BaseMenuScreen implements Screen, IScreen {

	protected Stage stage;
	protected Yaams yaams;
	protected SpriteBatch batch;
	protected Texture background;
	protected OrthographicCamera camera;

	public BaseMenuScreen(Yaams yaams) {
		batch = new SpriteBatch();
		background = YStatic.systemAssets.get("system/menu/DecorativeTile.png", Texture.class);

		camera = new OrthographicCamera();
		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		// stage.getRoot().getColor().a = 0;
		// stage.getRoot().addAction(Actions.fadeIn(0.8f));

		this.yaams = yaams;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// tell the camera to update its matrices.
		// camera.update();

		// tell the SpriteBatch to render in the
		// coordinate system specified by the camera.
		batch.setProjectionMatrix(camera.combined);

		batch.begin();
		batch.setColor(Color.WHITE);
		batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0, background.getWidth(), background.getHeight(), false, false);

		drawBatchElements(delta);

		// img.setSize(2f, 2f * img.getHeight() / img.getWidth());

		batch.end();

		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();

	}

	/**
	 * Helpermethod
	 */
	protected void drawBatchElements(float delta) {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, width, height);
		stage.getViewport().update(width, height, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	@Override
	public void dispose() {
		batch.dispose();
		stage.dispose();

	}

	/**
	 * @return the stage
	 */
	@Override
	public Stage getStage() {
		return stage;
	}

	@Override
	public Yaams getYaams() {
		return yaams;
	}

}
