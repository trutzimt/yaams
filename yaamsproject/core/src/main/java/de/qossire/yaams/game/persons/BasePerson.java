/**
 * 
 */
package de.qossire.yaams.game.persons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.persons.PersonManagement.PersonDir;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;
import de.qossire.yaams.screens.game.personStage.PersonStage;
import de.qossire.yaams.screens.game.personStage.YNode;

/**
 * @author sven
 *
 */
public class BasePerson {

	protected int gameX, gameY;

	/**
	 * Has the person a goal? null=no Goal
	 */
	protected YNode goalNode;
	protected DefaultGraphPath<YNode> path;
	/**
	 * Who is standing this person at his goal? -1=need new rendering
	 */
	protected int pathIndex;

	protected PersonStage persons;

	protected YAnimationPersonActor actor;

	protected String file;
	protected PersonDir dir;

	/**
	 * Track the time, after 60 m not moving, make a random move
	 */
	protected long lastMove;

	protected MapEventHandlerAction aniStop;

	/**
	 * @param region
	 */
	public BasePerson(int x, int y, String file, PersonStage persons) {

		actor = new YAnimationPersonActor(file);

		this.persons = persons;
		this.gameX = -1;
		this.file = file;
		setGamePos(x, y);

		pathIndex = 1;
		path = new DefaultGraphPath<YNode>();

		aniStop = new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				actor.setStop(true);
				return false;
			}
		};
		MapScreen.get().getEvents().register(MapEventHandlerTyp.TIME_BREAK, aniStop);

	}

	/**
	 * Update the person logic
	 */
	public void updateLogic(int deltaMin) {

		// has a path?
		if (goalNode != null && pathIndex != -1 && lastMove + 60 < MapScreen.get().getClock().getTimeStamp()) {
			randomWalk();
		}
	}

	/**
	 * Try to move to a random field
	 */
	public void randomWalk() {
		Gdx.app.debug("BasePerson", "Random walk");
		// limit try
		for (int i = 0; i < 10; i++) {
			int x = NamesGenerator.r.nextBoolean() ? -1 : 1;
			int y = NamesGenerator.r.nextBoolean() ? -1 : 1;
			// is field free
			// can walk?
			if (MapScreen.get().getTilesetHelper().isValidePosition(gameX + x, gameX + y) && MapScreen.get().getData().getProps(MapProp.BLOCKED, x, y) == 0
					&& persons.isFree(gameX + x, gameY + y)) {
				clearGoalPos();
				setGamePos(gameX + x, gameX + y);
				return;
			}
		}
	}

	/**
	 * @return the gameX
	 */
	public int getGameX() {
		return gameX;
	}

	/**
	 * @param gameX
	 *            the gameX to set
	 * @param gameY
	 *            the gameY to set
	 */
	public void setGamePos(int gameX, int gameY) {
		if (this.gameX != -1)
			persons.setPersonMatrix(this.gameX, this.gameY, null);
		persons.setPersonMatrix(gameX, gameY, this);
		this.gameX = gameX;
		this.gameY = gameY;

		if (MapScreen.get().getClock().getTimeSpeed() == 0 || (actor.getX() == 0 && actor.getY() == 0))
			actor.setPosition(gameX * 32, gameY * 32);
		else {
			actor.setStop(false);
			actor.addAction(Actions.sequence(Actions.moveTo(gameX * 32, gameY * 32, MapScreen.get().getClock().getTickTimeMillis() / 1000f), new Action() {

				@Override
				public boolean act(float delta) {
					// end of path?
					if (pathIndex + 1 >= path.nodes.size)
						BasePerson.this.actor.setStop(true);
					return true;
				}
			}));

		}

	}

	/**
	 * @param gameX
	 *            the gameX to set
	 * @param gameY
	 *            the gameY to set
	 */
	public void setGoalPos(int goalX, int goalY) {
		goalNode = persons.getNode(goalX, goalY);
		pathIndex = -1;
		path.clear();
	}

	/**
	 */
	public void clearGoalPos() {
		goalNode = null;
		pathIndex = -1;
		path.clear();
	}

	/**
	 * @return the gameY
	 */
	public int getGameY() {
		return gameY;
	}

	/**
	 * @return the path
	 */
	public DefaultGraphPath<YNode> getPath() {
		return path;
	}

	/**
	 * @return the pathIndex
	 */
	public int getPathIndex() {
		return pathIndex;
	}

	/**
	 * @param pathIndex
	 *            the pathIndex to set
	 */
	public void setPathIndex(int pathIndex) {
		this.pathIndex = pathIndex;
	}

	/**
	 * @return the goalNode
	 */
	public YNode getGoalNode() {
		return goalNode;
	}

	public YNode getNextNode() {
		return path.nodes.get(++pathIndex);
	}

	public void walk() {
		// has a path?
		if (goalNode != null && pathIndex != -1) {
			YNode n = path.nodes.get(pathIndex);
			// can walk?
			if (persons.isFree(n.getX(), n.getY()) || persons.getPersonMatrix(n.getX(), n.getY()) == this) {
				pathIndex++;
				// set dir
				int x = gameX - n.getX();
				int y = gameY - n.getY();
				PersonDir d;
				if (y == 0) {
					d = x < 0 ? PersonDir.EAST : PersonDir.WEST;
				} else {
					d = y > 0 ? PersonDir.SOUTH : PersonDir.NORTH;
				}

				// need to chance?
				if (d != dir) {
					actor.setAnimation(persons.getMgmt().getChar(file, d));
					dir = d;
				}

				lastMove = MapScreen.get().getClock().getTimeStamp();

				// set new pos
				setGamePos(n.getX(), n.getY());

				// end of path?
				if (pathIndex >= path.nodes.size) {
					clearGoalPos();
				}
			}

		}

	}

	/**
	 * remove this baseperson
	 */
	public boolean remove() {
		// remove it
		persons.setPersonMatrix(gameX, gameY, null);
		MapScreen.get().getMap().getPersonStage().getPersons().remove(this);
		MapScreen.get().getEvents().remove(MapEventHandlerTyp.TIME_BREAK, aniStop);
		return actor.remove();
	}

	/**
	 * @return the actor
	 */
	public YAnimationPersonActor getActor() {
		return actor;
	}

}
