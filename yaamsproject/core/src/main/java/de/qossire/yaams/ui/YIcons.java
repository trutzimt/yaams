/**
 *
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.screens.game.map.YTilesetHelper;

/**
 * @author sven
 *
 */
public class YIcons {

	public enum YIType {
		MUSEUM, MONEY, RANDOM, ARTWORK, ROOM, BUILD, TASK, DESTROY, MAINMENU, PLAY, PAUSE, INFOABOUT, WARNING, PERSONS, NEXTDAY, SAFETY, JOY
	}

	/**
	 *
	 */
	private static String getFileName(YIType type) {
		switch (type) {
		case MUSEUM:
			return "greek-temple";
		case RANDOM:
			return "perspective-dice-six-faces-random";
		case BUILD:
			return "tinker";
		case TASK:
			return "files";
		case ARTWORK:
			return "mona-lisa";
		case WARNING:
			return "hazard-sign";
		case ROOM:
			return "cubes";
		case MONEY:
			return "money-stack";
		case DESTROY:
			return "bright-explosion";
		case MAINMENU:
			return "hamburger-menu";
		case PLAY:
			return "pause-button";
		case PAUSE:
			return "play-button";
		case INFOABOUT:
			return "magnifying-glass";
		case PERSONS:
			return "ages";
		case NEXTDAY:
			return "sunrise";
		case SAFETY:
			return "checked-shield";
		case JOY:
			return "duality-mask";
		default:
			throw new IllegalArgumentException("Icon" + type + " not exist");
		}
	}

	/**
	 *
	 */
	private static AssetManager getAsset(YIType type) {
		switch (type) {
		case RANDOM:
		case WARNING:
			return YStatic.systemAssets;
		default:
			return YStatic.gameAssets;
		}
	}

	/**
	 *
	 */
	public static void loadIcons() {
		String e = YSettings.getGuiScaleFolder() + "/icons/";
		for (YIType t : YIType.values()) {
			String s = getFileName(t);

			// TODO find better way
			if (getAsset(t) == YStatic.gameAssets)
				getAsset(t).load(e + s + ".png", Texture.class);
		}
	}

	/**
	 *
	 */
	public static void loadIconsS() {
		String e = YSettings.getGuiScaleFolder() + "/icons/";
		for (YIType t : YIType.values()) {
			String s = getFileName(t);

			// TODO find better way
			if (getAsset(t) == YStatic.systemAssets) {
				YStatic.systemAssets.load(e + s + ".png", Texture.class);
			}
		}
	}

	/**
	 * Get the build icon
	 * 
	 * @param id
     * @param scale Scaled the icon?
	 * @return
	 */
	public static TextureRegionDrawable getBuildIcon(int id, boolean scale) {
		return getTilesetIcon("tileset", id,scale);
	}

	/**
	 * Get the build icon
	 * 
	 * @param id
     * @param scale Scaled the icon?
	 * @return
	 */
	public static TextureRegionDrawable getOverlayIcon(int id, boolean scale) {
		return getTilesetIcon("overlay", id,scale);
	}

	/**
	 * Get the build icon
	 * 
	 * @param id
	 * @return
	 */
	public static TextureRegionDrawable getTilesetIcon(String tileset, int id, boolean scale) {
		int s = scale?YSettings.getGuiScale():32;
		String file = (scale?YSettings.getGuiScaleFolder():"system") + "/tileset/" + tileset + ".png";

		return new TextureRegionDrawable(
				new TextureRegion(YStatic.gameAssets.get(file, Texture.class),
						id % YTilesetHelper.TILESET_LENGTH * s, id / YTilesetHelper.TILESET_LENGTH * s, s, s));
	}

	/**
	 * Get the art icon
	 * 
	 * @param id
     * @param scale Scaled the icon?
	 * @return
	 */
	public static TextureRegionDrawable getArtIcon(int id, boolean scale) {
		return getTilesetIcon("art", id, scale);
	}

	/**
	 * Get the icon as image
	 * 
	 * @param type
	 * @return
	 */
	public static Image getIconI(YIType type) {
		return new Image(getIconT(type));
	}

	/**
	 * Get the icon as drawable
	 * 
	 * @param type
	 * @return
	 */
	public static Drawable getIconD(YIType type) {
		return new TextureRegionDrawable(getIconT(type));
	}

	/**
	 * Get the icon as texture region
	 * 
	 * @param type
	 * @return
	 */
	public static TextureRegion getIconT(YIType type) {
		return new TextureRegion(getAsset(type).get(YSettings.getGuiScaleFolder() + "/icons/" + getFileName(type) + ".png", Texture.class));
	}

}
