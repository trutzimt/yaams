/**
 *
 */
package de.qossire.yaams.level.endless;

import java.util.HashMap;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.level.ScenarioManagement;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class EndlessGameScenario extends BaseScenario {

	protected String map;
	protected HashMap<String, String> settings;

	/**
	 */
	public EndlessGameScenario(String map, HashMap<String, String> settings) {
		super("endlessgame", "Endless Game", "map/" + map + ".tmx");

		camp = ScenarioManagement.getCampaign("single");

		this.map = map;
		this.settings = settings;

		// TODO find better way
		// setConfig(ScenarioConfig.LOADSAVE, 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(MapScreen screen) {
		super.start(screen);

		if (settings != null) {
			// save the settings
			screen.getData().setP("map", map);

			screen.getPlayer().addMoney(Integer.parseInt(settings.get("money")));
			// TODO
			screen.getPlayer().setName(settings.get("player"));
			screen.getPlayer().getMuseum().setName(settings.get("museum"));
			screen.getPlayer().getTown().setName(settings.get("town"));
		}

		// add some art
		for (int i = 0, l = NamesGenerator.getIntBetween(3, 5); i < l; i++) {
			screen.getPlayer().getArtwork().addArt(screen.getPlayer().getArtwork().generateArt(), ArtStatus.DEPOT);
		}
	}

}
