/**
 *
 */
package de.qossire.yaams.level.single;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.level.BaseCampaign;

/**
 * @author sven
 *
 */
public class SingleScenarioCampaign extends BaseCampaign {

	/**
	 */
	public SingleScenarioCampaign() {
		super("single", "Single Scenarios");

		desc = "Collections of single missions";

		addScenario(new Customer50GameScenario());
		addScenario(new Customer101GameScenario());
		addScenario(new Level1GameScenario());
		addScenario(new HerLastWillGameScenario());

		if (YSettings.isDebug())
			addScenario(new DebugScenario());

		isCampaign = false;
	}

}
