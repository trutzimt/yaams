package de.qossire.yaams.code;

import java.util.HashMap;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.generator.NamesGenerator;

@SuppressWarnings("serial")
public class YHashMap<T extends IHashMapID> extends HashMap<String, T> {

	protected String type;

	/**
	 * @param type
	 */
	public YHashMap(String type) {
		super();
		this.type = type;
	}

	/**
	 * Add a Single entry, if it exist a error will occure
	 *
	 * @param cam
	 */
	public void addS(T obj) {
		// exist campaign?
		if (containsKey(obj.getID())) {
			YConfig.error(new IllegalArgumentException(
					type + " " + get(obj.getID()).getClass().getSimpleName() + "(" + obj.getID() + ") exist. Overwrite with " + obj.getClass().getSimpleName()),
					false);
		}

		put(obj.getID(), obj);
	}

	/**
	 * Get a specific object, or the first, if exist
	 *
	 * @param id
	 * @return
	 */
	public T getS(String id) {
		// exist campaign?
		if (!containsKey(id)) {
			try {
				// get object
				if (isEmpty()) {
					YConfig.error(new IllegalArgumentException(type + " (" + id + ") not exist. Null will returned."), false);
					return null;
				} else {
					T nobj = values().iterator().next();
					YConfig.error(new IllegalArgumentException(
							type + " (" + id + ") not exist. " + nobj.getClass().getSimpleName() + " (" + nobj.getID() + ") will returned"), false);
					return nobj;
				}
			} catch (Exception e) {
				YConfig.error(new IllegalArgumentException("ID " + id + " not exist and code is corrupt."), true);
				return null;
			}
		}

		return get(id);
	}

	/**
	 * Return a random object
	 * 
	 * @return
	 */
	public T getRandom() {
		Object a[] = keySet().toArray();
		return get(a[NamesGenerator.getIntBetween(0, a.length)]);
	}

	/**
	 * Check if the research exist, otherwise notice the player
	 *
	 * @param type
	 */
	public void check(String id) {
		if (!containsKey(id)) {
			YConfig.error(new IllegalArgumentException("Check:" + type + " (" + id + ") not exist."), true);
		}
	}
}
