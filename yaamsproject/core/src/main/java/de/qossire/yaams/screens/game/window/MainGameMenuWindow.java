/**
 *
 */
package de.qossire.yaams.screens.game.window;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.game.debug.DebugWindow;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.saveload.SaveLoadWindow;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.menu.MainMenuScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YMessageDialog;
import de.qossire.yaams.ui.YTextButton;
import de.qossire.yaams.ui.YWindow;
import de.qossire.yaams.ui.actions.AboutAction;
import de.qossire.yaams.ui.actions.FeedbackAction;
import de.qossire.yaams.ui.actions.LoadAction;
import de.qossire.yaams.ui.actions.OptionsAction;

/**
 * @author sven
 *
 */
public class MainGameMenuWindow extends YWindow {

	/**
	 *
	 */
	public MainGameMenuWindow(final Yaams yaams, final Stage stage, final MapScreen map) {
		super("Main menu");

		// load it?
		if (map.getSettings().isActive(ScenConf.LOADSAVE)) {
			add(new LoadAction().getButton(yaams)).fillX().row();

			// save it
			add(new YTextButton("Save") {

				@Override
				public void perform() {
					YSounds.click();
					stage.addActor(new SaveLoadWindow(map, false, false));
					close();

				}
			}).fillX().row();
		}

		// about game
		add(new AboutAction().getButton(yaams)).fillX().row();

		// zoom in
		add(new YTextButton("Zoom in") {

			@Override
			public void perform() {
				YSounds.click();
				map.getMap().zoomIn();

			}
		}).fillX().row();

		// zoom out
		add(new YTextButton("Zoom out") {

			@Override
			public void perform() {
				YSounds.click();
				map.getMap().zoomOut();

			}
		}).fillX().row();

		// send feedback
		add(new FeedbackAction().getButton(yaams)).fillX().row();

		// options
		add(new OptionsAction().getButton(yaams)).fillX().row();

		// zoom out
		if (YSettings.isDebug())
			add(new YTextButton("Debug Window") {

				@Override
				public void perform() {
					MapScreen.get().getMapgui().getStage().addActor(new DebugWindow());

				}
			}).fillX().row();

		// quit game
		add(new YTextButton("Quit") {

			@Override
			public void perform() {
				YMessageDialog m = new YMessageDialog(yaams, "You really want to quit?", "", null);
				m.addButton(new YTextButton("Main Menu") {

					@Override
					public void perform() {
						YSounds.click();
						yaams.switchScreen(new MainMenuScreen(yaams));

					}
				});

				m.addButton(new YTextButton("Exit Game") {

					@Override
					public void perform() {
						YSounds.click();
						Gdx.app.exit();

					}
				});

				m.addButton(new YTextButton("Save & Exit") {

					@Override
					public void perform() {
						YSounds.click();
						stage.addActor(new SaveLoadWindow(map, false, true));
						close();

						// TODO better warning
						Dialogs.showOKDialog(map.getStage(), "Warning!",
								"Support for saving is very rudimentary and prone to error.\n Only general information, the map and in part of the artworks are stored.");

					}
				});

				m.build(stage);
				m.pack();

				YSounds.click();

			}
		}).fillX().row();

		pack();

		addTitleIcon(YIcons.getIconI(YIType.MAINMENU));
	}

}
