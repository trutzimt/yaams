/**
 *
 */
package de.qossire.yaams.level.endless;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.util.Validators;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSelectBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisValidatableTextField;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.GameLoaderScreen;
import de.qossire.yaams.ui.YRandomField;
import de.qossire.yaams.ui.YTable;
import de.qossire.yaams.ui.YTextButton;
import de.qossire.yaams.ui.YWindow;

/**
 * @author sven
 *
 */
public class EndlessGameWindow extends YWindow {

	protected Yaams yaams;
	protected HashMap<String, String> settings;

	/**
	 * @param title
	 */
	public EndlessGameWindow(final Yaams yaams) {
		super("Endless game");

		this.yaams = yaams;
		YTable table = new YTable();
		settings = new HashMap<>();

		// add names
		final YRandomField name, town, museum;
		name = new YRandomField(YSettings.getUserName()) {

			@Override
			protected void saveText(String text) {
				YSettings.getPref().putString("playername", text);
			}

			@Override
			protected String getRndText() {
				return NamesGenerator.getName(NamesGenerator.r.nextBoolean());
			}
		};
		table.addL("Player Name", name);

		town = new YRandomField(NamesGenerator.getTown()) {

			@Override
			protected void saveText(String text) {}

			@Override
			protected String getRndText() {
				return NamesGenerator.getTown();
			}
		};
		table.addL("Town Name", town);

		museum = new YRandomField(NamesGenerator.getMuseumName(null, null)) {

			@Override
			protected void saveText(String text) {}

			@Override
			protected String getRndText() {
				return NamesGenerator.getMuseumName(name.getText(), town.getText());
			}
		};
		table.addL("Museum Name", museum);

		table.addSeparator().colspan(2);

		// add map
		final VisSelectBox<String> map = new VisSelectBox<>();
		// TODO dynamic
		map.setItems("20", "40", "80");
		table.addL("Map Size", map);

		final VisValidatableTextField money = new VisValidatableTextField(Validators.INTEGERS);
		money.setText("200000");
		table.addL("Start Money", money);

		// add settings
		// addC(table, ScenarioConfig.FOGOFWAR, "endlessgame.fogofwar");
		// addC(table, ScenarioConfig.RESEARCH, "research");
		// addC(table, ScenarioConfig.WEATHER, "endlessgame.weather");
		// addC(table, ScenarioConfig.WEALTHSYSTEM, "ress.wealth");

		// add last
		table.addSeparator().colspan(2);
		YTextButton menu = new YTextButton("Start") {

			@Override
			public void perform() {
				// save everything
				settings.put("town", town.getText());
				settings.put("player", name.getText());
				settings.put("museum", museum.getText());
				settings.put("money", money.getText());

				yaams.switchScreen(new GameLoaderScreen(yaams, new EndlessGameScenario(map.getSelected(), settings)));
				YSounds.click();
			}
		};
		// table.add();
		table.add(menu).colspan(2).align(Align.right);

		// set position
		add(table).grow();
		setResizable(true);

		pack();
		setWidth(Gdx.graphics.getWidth() / 2);

	}

	/**
	 * Helper add
	 *
	 * @param table
	 * @param label
	 * @param a
	 */
	protected void add(VisTable table, String label, Actor a) {
		table.add(new VisLabel((label)));
		table.add(a).row();
	}

	/**
	 * Helper add
	 *
	 * @param table
	 * @param label
	 * @param a
	 */
	// @Deprecated
	// protected void addC(VisTable table, final ScenarioConfig conf, String title)
	// {
	// final VisCheckBox c = new VisCheckBox((title));
	//
	// c.addListener(new ChangeListener() {
	// @Override
	// public void changed(ChangeEvent event, Actor actor) {
	// settingsConf.put(conf, c.isChecked() ? 1 : 0);
	// }
	// });
	// // start value
	// c.setChecked(true);
	// settingsConf.put(conf, 1);
	//
	// table.add();
	// table.add(c).align(Align.left).row();
	// }

	/**
	 * Helper add
	 *
	 * @param table
	 * @param label
	 * @param a
	 */
	protected void addS(VisTable table, final String key, String title) {
		final VisCheckBox c = new VisCheckBox((title));

		c.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				settings.put(key, c.isChecked() ? "1" : "0");
			}
		});
		// start value

		table.add();
		table.add(c).align(Align.left).row();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		super.close();
		// qossire.setScreen(new MainMenuScreen(qossire));
	}

}
