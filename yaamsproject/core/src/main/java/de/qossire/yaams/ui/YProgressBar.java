/**
 *
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisProgressBar;

/**
 * @author sven
 *
 */
public class YProgressBar extends Stack {

	protected VisLabel label;
	protected VisProgressBar progress;

	/**
	 * Create a new
	 *
	 * @param act
	 * @param max
	 */
	public YProgressBar(int act, int max) {
		this(act, max, act + "/" + max);
	}

	/**
	 * Create a new
	 *
	 * @param act
	 * @param max
	 */
	public YProgressBar(int act, int max, String text) {

		// add bar
		progress = new VisProgressBar(0, max, 1, false);
		progress.setAnimateDuration(5);
		progress.setValue(act);
		add(progress);

		// add text
		label = new VisLabel(text);
		label.setAlignment(Align.center);
		add(label);
	}

	/**
	 * @return the label
	 */
	public VisLabel getLabel() {
		return label;
	}

	/**
	 * @return the progress
	 */
	public VisProgressBar getProgress() {
		return progress;
	}
}
