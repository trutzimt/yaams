/**
 *
 */
package de.qossire.yaams.code;

/**
 * @author sven
 *
 */
public interface IHashMapID {

	/**
	 * Get the id of the element
	 *
	 * @return
	 */
	public String getID();

	/**
	 * Get the title of the element
	 *
	 * @return
	 */
	public String getTitle();
}
