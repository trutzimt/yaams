package de.qossire.yaams.game.rooms;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YTabWindow;

public class RoomWindow extends YTabWindow {

	/**
	 * 
	 * @param map
	 */
	public RoomWindow() {
		super("Rooms");

		tabbedPane.add(new RoomTab());

		buildIt();

		addTitleIcon(YIcons.getIconI(YIType.ROOM));
		MapScreen.get().getMap().setShowOverlay(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		super.close();
		MapScreen.get().getMap().setShowOverlay(false);
	}

}
