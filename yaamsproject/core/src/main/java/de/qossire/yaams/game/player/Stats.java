/**
 * 
 */
package de.qossire.yaams.game.player;

import java.util.ArrayList;
import java.util.HashMap;

import de.qossire.yaams.code.YLog;
import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;

/**
 * @author sven
 *
 */
public class Stats {

	public enum EStats {
		MONEY, CUSTOMERCOUNT, CUSTOMERJOY
	}

	private HashMap<EStats, ArrayList<Integer>> stats;
	private int day;

	/**
	 * 
	 */
	public Stats() {
		stats = new HashMap<>();

		for (EStats key : EStats.values()) {
			stats.put(key, new ArrayList<Integer>());
		}

		// register for new customer
		MapScreen.get().getEvents().register(MapEventHandlerTyp.CUSTOMER_NEW, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				add(EStats.CUSTOMERCOUNT, 1);
				YLog.log("Add", EStats.CUSTOMERCOUNT, stats.get(EStats.CUSTOMERCOUNT));
				return false;
			}
		});

		// register for leaving
		MapScreen.get().getEvents().register(MapEventHandlerTyp.CUSTOMER_LEAVE, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				add(EStats.CUSTOMERJOY, ((Customer) objects[0]).getNeed(PersonNeeds.JOY));
				return false;
			}
		});

		// register for nextDay
		MapScreen.get().getEvents().register(MapEventHandlerTyp.NEXTDAY, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				add(EStats.MONEY, (int) MapScreen.get().getPlayer().getMoney());
				day++;
				return false;
			}
		});

		// register for saving
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_SAVE, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				// build array
				Object[][] erg = new Object[EStats.values().length][day + 1];
				for (EStats e : EStats.values()) {
					erg[e.ordinal()] = stats.get(e).toArray();
				}
				MapScreen.get().getData().setStats(erg);
				MapScreen.get().getData().setP("stats_day", day);
				return false;
			}
		});

		// register for loading
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_LOAD, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				Object[][] erg = MapScreen.get().getData().getStats();
				// load array
				for (EStats e : EStats.values()) {

					ArrayList<Integer> intList = new ArrayList<Integer>();
					for (Object i : erg[e.ordinal()]) {
						intList.add((int) i);
					}
					stats.put(e, intList);
				}
				day = MapScreen.get().getData().getPI("stats_day", 2);
				return false;
			}
		});
	}

	/**
	 * Get the stats for this day
	 * 
	 * @param key
	 * @return
	 */
	public int get(EStats key) {
		return stats.get(key).size() <= day ? 0 : stats.get(key).get(day);
	}

	/**
	 * Get the stats for this day
	 * 
	 * @param key
	 * @return
	 */
	public int getTotal(EStats key) {
		int t = 0;
		for (int i = 0; i <= day; i++) {
			t += get(key);
		}
		return t;
	}

	/**
	 * Get the stats for yesterday, if not exist return -1
	 * 
	 * @param key
	 * @return
	 */
	public int getYesterday(EStats key) {
		return day == 0 ? -1 : stats.get(key).get(day - 1);
	}

	/**
	 * Get the stats for this special day, if not exist return -1
	 * 
	 * @param key
	 * @return
	 */
	public int get(EStats key, int day) {
		return day < 0 || day > this.day ? -1 : (day >= stats.get(key).size() ? 0 : stats.get(key).get(day));
	}

	/**
	 * Add a special value for the day
	 * 
	 * @param key
	 * @param value
	 */
	public void add(EStats key, int value) {
		int old = stats.get(key).size() <= day ? 0 : stats.get(key).get(day);
		if (old == 0)
			stats.get(key).add(value);
		else
			stats.get(key).set(day, old + value);
	}

}
