package de.qossire.yaams.game.rooms;

import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.build.BuildConfig;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.build.requirement.IRequirement;
import de.qossire.yaams.screens.game.MapScreen;

public class ReqNoRoom implements IRequirement {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#checkField(int,
	 * int, de.qossire.yaams.screens.map.MapScreen,
	 * de.qossire.yaams.game.build.BuildConfig)
	 */
	@Override
	public boolean checkField(int x, int y, MapScreen map, BuildConfig build) {
		return map.getMap().getRoomTile(x, y) == -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#checkField(int,
	 * int, de.qossire.yaams.screens.map.MapScreen,
	 * de.qossire.yaams.game.art.BaseArt)
	 */
	@Override
	public boolean checkField(int x, int y, MapScreen map, BaseArt art) {
		return checkField(x, y, map, (BuildConfig) null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc(int, int,
	 * de.qossire.yaams.screens.map.MapScreen,
	 * de.qossire.yaams.game.build.BuildConfig)
	 */
	@Override
	public String getDesc(int x, int y, MapScreen map, BuildConfig buildConfig) {
		return "The room " + map.getPlayer().getRooms().getRoom(map.getData().getProps(MapProp.ROOMID, x, y)).getName() + " is already here.";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc(int, int,
	 * de.qossire.yaams.screens.map.MapScreen, de.qossire.yaams.game.art.BaseArt)
	 */
	@Override
	public String getDesc(int x, int y, MapScreen map, BaseArt baseArt) {
		return getDesc(x, y, map, (BuildConfig) null);
	}
}