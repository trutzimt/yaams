/**
 * 
 */
package de.qossire.yaams.game.art.window;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.TextHelper;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YSplitTab;

/**
 * @author sven
 *
 */
public class BuyTab extends YSplitTab {

	private VisTextButton buy, newArt;
	private BaseArt active;

	public BuyTab() {
		super("Buy", "At the moment there is no art to buy. Come back tomorrow.");

		// add button
		buy = new VisTextButton("");
		buy.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				buy();

			}
		});
		buttonBar.addActor(buy);

		// add button
		newArt = new VisTextButton("Look for new Art (" + TextHelper.getMoneyString(-100) + ")");
		newArt.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				MapScreen.get().getPlayer().getArtwork().regAvaibleArt();

				MapScreen.get().getPlayer().addMoney(-100);
				YSounds.buy();
				reset();
				resetElements();
				rebuild();
			}
		});
		newArt.setDisabled(MapScreen.get().getPlayer().getMoney() < 100);
		buttonBar.addActor(newArt);

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		resetElements();
		super.rebuild();
	}

	public void resetElements() {
		elements.clear();
		// add all elements
		for (final BaseArt art : MapScreen.get().getPlayer().getArtwork().getAvaibleArt()) {
			addElement(art.getName(), art.getIcon(true), art);
		}
	}

	/**
	 * Reset the status
	 */
	@Override
	protected void reset() {
		super.reset();
		active = null;
		buy.setDisabled(true);
		buy.setText("Buy it");
		newArt.setDisabled(MapScreen.get().getPlayer().getMoney() < 100);

	}

	/**
	 * Place the selected artwork
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		buy();
	}

	/**
	 * Buy the selected artwork
	 */
	protected void buy() {
		if (MapScreen.get().getPlayer().getMoney() < active.getPrice()) {
			YSounds.buzzer();
			return;
		}
		MapScreen.get().getPlayer().getArtwork().removeArt(active);
		MapScreen.get().getPlayer().getArtwork().addArt(active, ArtStatus.DEPOT);
		MapScreen.get().getPlayer().addMoney(-active.getPrice());
		YSounds.buy();
		resetElements();
		reset();
		rebuild();
	}

	@Override
	protected void clickElement(Button btn) {
		active = ((BaseArt) btn.getUserObject());
		buy.setDisabled(MapScreen.get().getPlayer().getMoney() < active.getPrice());
		buy.setText("Buy " + active.getName() + " for " + TextHelper.getMoneyString(active.getPrice()));
	}

	@Override
	protected Actor getInfoPanel(Button btn) {
		return ((BaseArt) btn.getUserObject()).getInfoPanel();
	}
}