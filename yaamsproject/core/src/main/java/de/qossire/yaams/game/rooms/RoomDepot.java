package de.qossire.yaams.game.rooms;

import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.rooms.RoomMgmt.RoomTyp;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YTable;

public class RoomDepot extends BaseRoom {

	public RoomDepot(int uid, int x, int y) {
		super(RoomTyp.DEPOT, uid, x, y);
		bonus = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.rooms.BaseRoom#getIcon()
	 */
	@Override
	public TextureRegionDrawable getIcon() {
		return YIcons.getOverlayIcon(33, true);
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	@Override
	public VisTable getInfoPanel() {
		YTable table = (YTable) super.getInfoPanel();

		table.addL("Wall", hasWall ? "+ completely surrounded" : "- not completely surrounded");

		table.addL("Floor", hasFloor ? "+ everywhere with a floor" : "- floor is missing");

		table.addL("Art condition", bonus == 0 ? "Everything ok" : "Break " + bonus + "% every day.");

		return table;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.rooms.BaseRoom#nextDay()
	 */
	@Override
	public void nextDay() {
		// rebuild the room
		toCheck = true;
		updateLogic();

		// perform it
		if (bonus != 0)
			performTile(x, y, new boolean[MapScreen.get().getMap().getWidth()][MapScreen.get().getMap().getHeight()]);

	}

	/**
	 * Update the art
	 * 
	 * @param x
	 * @param y
	 * @param fields
	 */
	private void performTile(int x, int y, boolean[][] fields) {
		// was visit?
		if (!MapScreen.get().getTilesetHelper().isValidePosition(x, y) || fields[x][y]) {
			return;
		}

		// right typ?
		if (MapScreen.get().getData().getProps(MapProp.ROOMID, x, y) != id) {
			return;
		}

		fields[x][y] = true;

		// update art
		BaseArt a = MapScreen.get().getPlayer().getArtwork().getArtAt(x, y);
		if (a != null) {
			a.addCondition(bonus);
		}

		// ask neighboors
		performTile(x - 1, y, fields);
		performTile(x, y - 1, fields);
		performTile(x + 1, y, fields);
		performTile(x, y + 1, fields);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.rooms.BaseRoom#updateLogic()
	 */
	@Override
	public void updateLogic() {
		// check it
		if (!toCheck) {
			return;
		}

		super.updateLogic();

		// set it
		if (hasFloor && hasWall) {
			bonus = 0;
		} else if (hasFloor || hasWall) {
			bonus = -1;
		} else {
			bonus = -3;
		}

	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}
}
