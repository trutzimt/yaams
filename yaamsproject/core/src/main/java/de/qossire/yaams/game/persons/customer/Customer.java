/**
 * 
 */
package de.qossire.yaams.game.persons.customer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.code.YLog;
import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.persons.BasePerson;
import de.qossire.yaams.game.persons.PersonManagement.PersonAnimationNeeds;
import de.qossire.yaams.game.persons.PersonManagement.PersonDir;
import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.needs.BaseNeed;
import de.qossire.yaams.game.player.Stats.EStats;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.personStage.PersonStage;
import de.qossire.yaams.ui.TextHelper;
import de.qossire.yaams.ui.YProgressBar;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class Customer extends BasePerson {

	private HashMap<PersonNeeds, Integer> needs;
	private HashMap<String, Object> data;
	private int money;
	private BaseNeed activeNeed;
	private MapScreen map;
	/**
	 * WHich Art he want to see?
	 */
	private LinkedList<Integer> wantSeeArt, hasSeeArt;

	/**
	 * Wait the amount in Min
	 */
	private int waitMin;
	private Runnable waitMinRun;

	private String name;

	/**
	 * Sexuality. True > Woman
	 */
	private boolean sex;

	/**
	 * @param x
	 * @param y
	 * @param id
	 */
	public Customer(int x, int y, PersonStage persons, MapScreen map) {
		super(x, y, String.valueOf(NamesGenerator.getIntBetween(0, (YSettings.isDebug() ? 4 : 135))), persons);
		// TODO gener llok

		// setGoalPos(0, 0);
		this.map = map;
		this.needs = new HashMap<>();
		data = new HashMap<>();
		addNeed(PersonNeeds.TOILET, NamesGenerator.getIntBetween(-1000, 500));
		addNeed(PersonNeeds.DRINK, NamesGenerator.getIntBetween(-1000, 500));
		addNeed(PersonNeeds.FOOD, NamesGenerator.getIntBetween(-1000, 500));

		sex = Math.random() > 0.5f;
		name = (sex ? NamesGenerator.getFemale() : NamesGenerator.getMale()) + " " + NamesGenerator.getSurname();
		money = NamesGenerator.getIntBetween(30, 70);

		//

		// TODO more natural generation
		// add some wantSeeArt
		hasSeeArt = new LinkedList<>();
		wantSeeArt = new LinkedList<>();
		int s = map.getPlayer().getArtwork().getArts(ArtStatus.DISPLAYED).size();
		for (int i = 0, l = Math.min(NamesGenerator.getIntBetween(4, 6), s); i < l;) {
			int n = map.getPlayer().getArtwork().getArts(ArtStatus.DISPLAYED).get(NamesGenerator.getIntBetween(0, s)).getUId();
			// new? want to see
			if (!wantSeeArt.contains(n)) {
				wantSeeArt.add(n);
				i++;
			}
		}

	}

	/**
	 * Update the person logic
	 */
	@Override
	public void updateLogic(int deltaMin) {
		super.updateLogic(deltaMin);

		// need to wait?
		if (waitMin > 0) {
			waitMin -= deltaMin;
			if (waitMin <= 0) {
				waitMin = 0;
				waitMinRun.run();
				activeNeed = null;
			}
		}

		boolean changes = false;
		// run over the needs
		for (PersonNeeds need : PersonNeeds.values()) {
			BaseNeed b = persons.getMgmt().getNeed(need);
			boolean erg = b.updateLogic(this, map, deltaMin);

			// if (erg && activeNeed != b) {
			// Gdx.app.debug("Customer", "want new need:" + need + " activeNeed " +
			// activeNeed);
			// }

			// has a new need?
			if (erg && activeNeed != b && (activeNeed == null || b.getImpLevel() < activeNeed.getImpLevel())) {
				activeNeed = b;
				changes = true;
				// Gdx.app.debug("Customer", "new need:" + activeNeed.getId());
			}
		}

		// try to fix the need
		if (changes) {
			Gdx.app.debug("Customer", "try to fix new need:" + activeNeed.getId());
			// customer will leave
			activeNeed.tryToFix(this, map);
		}
	}

	/**
	 * @return the needs
	 */
	public int getNeed(PersonNeeds need) {
		if (!needs.containsKey(need)) {
			return 0;
		}
		return needs.get(need);
	}

	/**
	 * @return the needs
	 */
	public void addNeed(PersonNeeds need, int val) {
		// Gdx.app.debug(getPersonName(), need + " add " + val, new
		// IllegalArgumentException(needs.get(need) + " "));
		if (!needs.containsKey(need)) {
			needs.put(need, val);
		} else {
			needs.put(need, needs.get(need) + val);
		}

		// check area?
		if (needs.get(need) > BaseNeed.MAX) {
			needs.put(need, BaseNeed.MAX);
		}
		if (needs.get(need) < BaseNeed.MIN) {
			needs.put(need, BaseNeed.MIN);
		}

		// if (need == PersonNeeds.JOY && needs.get(need) >= BaseNeed.MAX) {
		// Gdx.app.debug(getPersonName(), need + " add " + val, new
		// IllegalArgumentException(needs.get(need) + " "));
		// }
	}

	/**
	 * @return the money
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * @param money
	 *            the money to set
	 */
	public void addMoney(int money) {
		this.money += money;
	}

	/**
	 * @return the waitMin
	 */
	public int getWaitMin() {
		return waitMin;
	}

	/**
	 * @param waitMin
	 *            the waitMin to set
	 * @param runnable
	 */
	public void setWaitMin(int waitMin, PersonDir dir, Runnable runnable) {
		this.waitMin = waitMin;
		this.waitMinRun = runnable;

		// show it
		MapScreen.get().getMap().getPersonStage().getMgmt().showNeed(this, PersonAnimationNeeds.DOTDOT);
		actor.setDirection(dir);
	}

	/**
	 * @return the name
	 */
	public String getPersonName() {
		return name;
	}

	/**
	 * @return the name
	 */
	public String getPersonNameProp() {
		return sex ? "she" : "he";
	}

	/**
	 * remove this customer
	 */
	public void removeWithAnimation() {
		// set leave animation
		Image i = new Image(actor.getPreviewRegion());
		i.setPosition(actor.getX(), actor.getY());
		i.addAction(Actions.sequence(Actions.fadeOut(1f), Actions.removeActor()));
		map.getMap().getStage().addActor(i);

		// add feedback
		if (needs.get(PersonNeeds.JOY) >= 750)
			map.getMap().getPersonStage().getMgmt().showNeed(this, PersonAnimationNeeds.HEART);
		else if (needs.get(PersonNeeds.JOY) >= 400)
			map.getMap().getPersonStage().getMgmt().showNeed(this, PersonAnimationNeeds.SINGLENOTE);
		else if (needs.get(PersonNeeds.JOY) >= -400)
			map.getMap().getPersonStage().getMgmt().showNeed(this, PersonAnimationNeeds.SLEEPING);
		else
			map.getMap().getPersonStage().getMgmt().showNeed(this, PersonAnimationNeeds.ANGER);

		remove();
	}

	/**
	 * 
	 */
	public void askToLeave() {
		addNeed(PersonNeeds.LEAVE, BaseNeed.SET_MAX);
		addNeed(PersonNeeds.JOY, (int) (-getNeed(PersonNeeds.JOY) * (getNeed(PersonNeeds.JOY) > 0 ? 0.5 : 2)));
		map.getMap().getPersonStage().getMgmt().showNeed(this, PersonAnimationNeeds.ANGER);
	}

	/**
	 * remove this customer
	 */
	@Override
	public boolean remove() {
		YLog.log("Customer", getPersonName() + " is destroyed");
		boolean s = super.remove();
		// TODO add review points ...
		map.getEvents().perform(MapEventHandlerTyp.CUSTOMER_LEAVE, this);
		map.getPlayer().getStats().add(EStats.CUSTOMERJOY, needs.get(PersonNeeds.JOY));

		return s;
	}

	/**
	 * @return the activeNeed
	 */
	public BaseNeed getActiveNeed() {
		return activeNeed;
	}

	/**
	 * @return the wantSeeArt
	 */
	public LinkedList<Integer> getWantSeeArt() {
		return wantSeeArt;
	}

	/**
	 * @return the hasSeeArt
	 */
	public LinkedList<Integer> getHasSeeArt() {
		return hasSeeArt;
	}

	/**
	 * Sexuality. True > Woman
	 * 
	 * @return the sex
	 */
	public boolean isSex() {
		return sex;
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	public YTable getInfoPanel() {
		YTable t = new YTable();
		t.addH("Basics");
		t.addL("Name", name);
		t.addL("Money", TextHelper.getMoneyString(money));
		if (waitMin > 0)
			t.addL("Wait", TextHelper.getLongTimeString(waitMin));
		t.addL("Happiness", persons.getMgmt().getNeedsText(PersonNeeds.JOY, getNeed(PersonNeeds.JOY)));
		if (MapScreen.get().getSettings().isActive(ScenConf.TOILET))
			t.addL("Toilet", persons.getMgmt().getNeedsText(PersonNeeds.TOILET, getNeed(PersonNeeds.TOILET)));
		if (MapScreen.get().getSettings().isActive(ScenConf.DRINK))
			t.addL("Thirsty", persons.getMgmt().getNeedsText(PersonNeeds.DRINK, getNeed(PersonNeeds.DRINK)));
		if (MapScreen.get().getSettings().isActive(ScenConf.FOOD))
			t.addL("Hungry", persons.getMgmt().getNeedsText(PersonNeeds.FOOD, getNeed(PersonNeeds.FOOD)));
		// add images
		if (wantSeeArt.size() > 0) {
			t.addH("Want to see");
			for (int i : wantSeeArt) {
				BaseArt a = map.getPlayer().getArtwork().getArt(i);
				t.add(new Image(a.getIcon(true))).align(Align.right);
				t.add(new VisLabel(a.getName())).align(Align.left).fillX().row();
			}
		}

		if (!YSettings.isDebug()) {
			return t;
		}

		t.addH("Debug Infos");
		t.addL("Image", new Image(getActor().getPreviewRegion()));

		// add needs
		for (PersonNeeds need : PersonNeeds.values()) {
			t.addL(need.toString().toLowerCase(), new YProgressBar(getNeed(need) + BaseNeed.MAX, BaseNeed.MAX - BaseNeed.MIN, getNeed(need) + ""));
		}

		if (getGoalNode() != null) {
			t.addL("Goal", "X: " + getGoalNode().getX() + "Y: " + getGoalNode().getY());
		}

		if (getActiveNeed() != null) {
			t.addL("Active Need", getActiveNeed().getId().toString().toLowerCase());
		}

		t.addL("Art Seen", Arrays.toString(getHasSeeArt().toArray()));
		t.addL("Art Want See", Arrays.toString(getWantSeeArt().toArray()));
		t.addL("Actor", "Color: " + getActor().getColor() + " Count: " + getActor().getActions().size + " Ani:" + getActor().isStop());

		return t;
	}

	/**
	 * @return the data
	 */
	public HashMap<String, Object> getData() {
		return data;
	}

}