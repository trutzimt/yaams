package de.qossire.yaams.game.museum.rank;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YSplitTab;

public class RankTab extends YSplitTab {

	/**
	 * Create a new RangTab
	 */
	public RankTab() {
		super("Rating", "?");
		// add all rangs
		for (BaseRank rang : MapScreen.get().getPlayer().getRang().getRanks()) {
			addElement(rang.getTitle(), rang.getIcon(), rang);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#doubleClickElement(com.badlogic.gdx.scenes.
	 * scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(Button btn) {
		VisLabel l = new VisLabel(((BaseRank) btn.getUserObject()).getDesc());
		l.setWrap(true);
		return l;
	}

}
