/**
 * 
 */
package de.qossire.yaams.ui;

import java.util.LinkedList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.layout.HorizontalFlowGroup;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.qossire.yaams.music.YSounds;

/**
 * @author sven
 *
 */
public abstract class YSplitTab extends Tab {

	protected String title, error;
	protected LinkedList<Button> elements;
	protected HorizontalFlowGroup buttonBar;
	protected Actor topBar;
	protected VisTable content;
	protected Object active;
	protected final VisTable right;
	protected YTabWindow window;

	/**
	 * 
	 */
	public YSplitTab(String title, String error) {
		super(false, false);
		this.title = title;
		this.error = error;
		elements = new LinkedList<>();
		content = new VisTable();
		right = new VisTable();
		buttonBar = new HorizontalFlowGroup(2);
		active = null;
	}

	/**
	 * This windows & tab will be closed
	 */
	public void close() {}

	/**
	 * Add a new button
	 * 
	 * @param title
	 * @param icon
	 * @param o
	 */
	protected void addElement(String title, Drawable icon, Object o) {
		final VisImageTextButton t = new VisImageTextButton(title, icon);
		t.setUserObject(o);
		t.getImage().setAlign(Align.left);
		t.getLabel().setAlignment(Align.left);
		t.addCaptureListener(new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				if (t.getUserObject() != active) {
					active = t.getUserObject();
					// show the art and set the buttons
					YSounds.click();
					right.clear();
					right.add(getInfoPanel(t)).grow();
					clickElement(t);

				} else {
					doubleClickElement(t);
				}

			}
		});
		elements.add(t);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
	 */
	@Override
	public String getTabTitle() {
		return title;
	}

	/**
	 * Reset the status
	 */
	protected void reset() {
		right.clear();
		active = null;
	}

	/**
	 * Rebuild the gui
	 */
	protected void rebuild() {
		content.clear();

		// has some avaible?
		if (elements.size() == 0) {
			VisLabel l = new VisLabel(error);
			l.setWrap(true);
			VisScrollPane pL = new VisScrollPane(l);
			pL.setScrollingDisabled(true, false);
			pL.setFadeScrollBars(false);
			pL.setScrollbarsOnTop(true);
			content.add(pL).align(Align.left).grow();
			return;
		}

		VisTable left = new VisTable();

		// list every object
		for (final Button btn : elements) {
			// btn.clearListeners();

			left.add(btn).growX().row();

		}
		left.add().growY();
		// left.setFillParent(true);

		VisScrollPane paneL = new VisScrollPane(left);
		paneL.setScrollingDisabled(true, false);
		paneL.setFadeScrollBars(false);
		paneL.setScrollbarsOnTop(true);

		right.clear();
		right.add(getDefaultPanel());
		VisScrollPane paneR = new VisScrollPane(right);
		paneR.setScrollingDisabled(true, false);
		paneR.setFadeScrollBars(false);
		paneR.setScrollbarsOnTop(true);

		// has children?
		if (topBar != null) {
			content.add(topBar).growX().colspan(2);
			content.addSeparator().colspan(2);
		}

		content.add(paneL).width(Value.percentWidth(.5F, content)).align(Align.topLeft);
		// content.addSeparator(true);
		content.add(paneR).width(Value.percentWidth(.5F, content)).growY().row();
		// has children?
		if (buttonBar.getChildren().size > 0) {
			content.addSeparator().colspan(2);
			content.add(buttonBar).growX().colspan(2);
		}

	}

	/**
	 * Second click on the list element
	 * 
	 * @param btn
	 */
	protected abstract void doubleClickElement(Button btn);

	/**
	 * First click on the list element
	 * 
	 * @param btn
	 */
	protected void clickElement(Button btn) {}

	/**
	 * 
	 * @param btn
	 * @return
	 */
	protected abstract Actor getInfoPanel(Button btn);

	/**
	 * Get the default panel, if nothing clicked
	 */
	protected Actor getDefaultPanel() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
	 */
	@Override
	public Table getContentTable() {
		rebuild();

		return content;
	}

	/**
	 * @param window
	 *            the window to set
	 */
	public void setWindow(YTabWindow window) {
		this.window = window;
	}

}
