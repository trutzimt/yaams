/**
 * 
 */
package de.qossire.yaams.game.museum;

import de.qossire.yaams.generator.NamesGenerator;

/**
 * @author sven
 *
 */
public class Town {

	protected String name;

	/**
	 * 
	 */
	public Town() {
		name = NamesGenerator.getTown();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
