/**
 * 
 */
package de.qossire.yaams.game.persons;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import de.qossire.yaams.game.persons.PersonManagement.PersonDir;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YAnimationActor;

/**
 * @author sven
 *
 */
public class YAnimationPersonActor extends YAnimationActor {

	private String file;

	/**
	 * @param animation
	 */
	public YAnimationPersonActor(String file) {
		super(MapScreen.get().getMap().getPersonStage().getMgmt().getChar(file, PersonDir.SOUTH));

		getColor().a = 0;
		addAction(Actions.fadeIn(1f));
		setStop(true);
		this.file = file;
	}

	/**
	 * @param animation
	 *            the animation to set
	 */
	public void setAnimation(Animation<TextureRegion> animation) {
		this.animation = animation;
		currentRegion = animation.getKeyFrame(0, true);

		// rebuild ani
		if (stop)
			setStop(true);
	}

	/**
	 * @param animation
	 *            the animation to set
	 */
	public void setDirection(PersonDir dir) {
		setAnimation(MapScreen.get().getMap().getPersonStage().getMgmt().getChar(file, dir));
	}

}
