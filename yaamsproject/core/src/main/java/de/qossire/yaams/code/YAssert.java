/**
 * 
 */
package de.qossire.yaams.code;

import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class YAssert {

	/**
	 * 
	 */
	public YAssert() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Check the position
	 * 
	 * @param x
	 * @param y
	 */
	public static void isValidePosition(int x, int y) {
		if (!MapScreen.get().getTilesetHelper().isValidePosition(x, y))
			new IllegalArgumentException("Wrong Position (" + x + "," + y + ")");

	}

}
