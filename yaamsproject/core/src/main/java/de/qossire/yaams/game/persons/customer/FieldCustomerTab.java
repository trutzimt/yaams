package de.qossire.yaams.game.persons.customer;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.layout.HorizontalFlowGroup;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.qossire.yaams.ui.YTable;
import de.qossire.yaams.ui.YTextButton;

public class FieldCustomerTab extends Tab {

	private Customer customer;

	/**
	 * 
	 * @param customer
	 */
	public FieldCustomerTab(Customer customer) {
		super(false, false);
		this.customer = customer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
	 */
	@Override
	public String getTabTitle() {
		return customer.getPersonName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
	 */
	@Override
	public Table getContentTable() {
		YTable t = new YTable();
		t.add(customer.getInfoPanel().createScrollPane()).expand().row();

		HorizontalFlowGroup buttonBar = new HorizontalFlowGroup(2);
		buttonBar.addActor(new YTextButton("Ask the customer to leave") {

			@Override
			public void perform() {
				// leave him
				customer.askToLeave();
			}
		});
		t.add(buttonBar);

		return t;
	}
}
