/**
 * 
 */
package de.qossire.yaams.game.art;

import de.qossire.yaams.game.art.ArtworkMgmt.ArtTyp;
import de.qossire.yaams.generator.NamesGenerator;

/**
 * @author sven
 *
 */
public class OnTableArt extends BaseArt {

	/**
	 * @param uid
	 * @param typ
	 */
	public OnTableArt() {
		super(ArtTyp.ONTABLE);
		setTId((int) NamesGenerator.getRnd(386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428,
				429));
	}

}
