/**
 *
 */
package de.qossire.yaams.saveload;

import java.util.HashMap;
import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.level.ScenarioManagement;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;

/**
 * @author svenå
 *
 */
public class SaveLoadManagement {

	private final static String FOLDER = "saves";

	/**
	 * Get all Saves
	 *
	 * @return
	 */
	public static LinkedList<FileHandle> getSaves() {
		LinkedList<FileHandle> files = new LinkedList<>();

		// add local files
		if (Gdx.files.isLocalStorageAvailable()) {
			FileHandle file = Gdx.files.local(FOLDER);
			if (file.isDirectory()) {
				for (FileHandle f : file.list()) {
					if (f.nameWithoutExtension().endsWith("data")) {
						continue;
					}

					files.add(f);
				}

			}
		}

		// add external files
		if (Gdx.files.isExternalStorageAvailable()) {
			FileHandle file = Gdx.files.external(FOLDER);
			if (file.isDirectory()) {
				for (FileHandle f : file.list()) {
					if (!f.nameWithoutExtension().endsWith("data")) {
						files.add(f);
					}
				}

			}
		}

		return files;
	}

	/**
	 * Save the game under a spezific file name
	 *
	 * @param filename
	 * @param map
	 */
	public static boolean exist(String filename) {
		FileHandle active = createFile(filename);

		return !(active == null) && active.exists();
	}

	/**
	 * delete the game under a spezific file name
	 *
	 * @param filename
	 * @param map
	 */
	public static boolean delete(String filename) {
		FileHandle active = createFile(filename);
		FileHandle data = createFile(filename + "data");

		return !(active == null) && active.delete() && !(data == null) && data.delete();
	}

	private static FileHandle createFile(String filename) {
		// save local?
		if (Gdx.files.isLocalStorageAvailable()) {
			return Gdx.files.local(FOLDER + "/" + filename);
		}
		if (Gdx.files.isExternalStorageAvailable()) {
			return Gdx.files.external(FOLDER + "/" + filename);
		}
		return null;
	}

	/**
	 * Save the game under a spezific file name
	 *
	 * @param filename
	 * @param map
	 */
	public static boolean save(String filename, MapScreen map, boolean hidden) {
		FileHandle active = createFile(filename);

		if (active == null) {
			return false;
		}

		save(active, map, hidden);
		return true;
	}

	/**
	 * Save the game
	 *
	 * @param active
	 * @param map
	 */
	public static void save(FileHandle active, MapScreen map, boolean hidden) {

		// new game?
		if (active == null) {
			// find next free spot
			int c = 1;
			String name = FOLDER + "/";

			if (Gdx.files.isLocalStorageAvailable()) {

				// find free file?
				do {
					active = Gdx.files.local(name + c);
					c++;
				} while (active.exists());
			} else if (Gdx.files.isExternalStorageAvailable()) {

				// find free file
				do {
					active = Gdx.files.external(name + c);
					c++;
				} while (active.exists());
			} else {
				Dialogs.showErrorDialog(map.getMapgui().getStage(), "No place or space for saving available.");
				return;
			}
		}

		// inform
		MapScreen.get().getEvents().perform(MapEventHandlerTyp.FILE_SAVE);

		// build save game object
		YSaveInfo qi = new YSaveInfo();
		qi.setVersion(YConfig.VERSION);
		qi.setSettings(map.getData().getCoreSettings());
		qi.setHidden(hidden);

		// TODO use all

		// save the game data
		Json json = new Json();
		active.sibling(active.name() + "data").writeString(json.toJson(map.getData()), false);

		// save the game info
		active.writeString(json.toJson(qi), false);

		// active.writeString(json.toJson(map.getActivePlayer()), true);
		// active.writeString(json.toJson(map.getPlayers()), true);

	}

	/**
	 * Load a game
	 *
	 * @param active
	 * @param qossire
	 */
	public static boolean load(String filename, Yaams yaams, Stage stage) {
		FileHandle active = createFile(filename);

		if (active == null) {
			return false;
		}

		return load(active, yaams, stage);
	}

	/**
	 * Load a game
	 *
	 * @param active
	 * @param qossire
	 */
	public static boolean load(FileHandle saveGame, Yaams yaams, Stage stage) {
		// check can load?
		if (!saveGame.exists()) {
			Dialogs.showOKDialog(stage, "Load game error", "Can not load game: No save file exist.");
			return false;
		}

		// data exist?
		if (!saveGame.sibling(saveGame.nameWithoutExtension() + "data").exists()) {
			Dialogs.showOKDialog(stage, "Load game error", "Can not load game: No save game exist.");
			return false;
		}

		try {
			YSaveInfo q = new Json().fromJson(YSaveInfo.class, saveGame.read());

			// right version?
			if (q.getVersion() != YConfig.VERSION) {
				Dialogs.showOKDialog(stage, "Load game error",
						"Can not load game: It was saved with version " + q.getVersion() + ". You are using version " + YConfig.VERSION + ".");
				return false;
			}

			String c = q.getSettings().get("campaign");
			String s = q.getSettings().get("scenario");

			// camp exist??
			if (!ScenarioManagement.existCampaign(c)) {
				Dialogs.showOKDialog(stage, "Load game error", "Can not load game: The campaign " + c + " not exist.");
				return false;
			}

			// scenario exist??
			// TODO find better way
			if (!"endlessgame".equals(s) && !"debug".equals(s) && !ScenarioManagement.getCampaign(c).existScenario(s)) {
				Dialogs.showOKDialog(stage, "Load game error", "Can not load game: The scenario " + s + " not exist.");
				return false;
			}

			yaams.switchScreen(new GameLoadLoaderScreen(yaams, saveGame));
		} catch (Exception e) {
			YConfig.error(e, false);
		}

		return true;
	}

	/**
	 * Fix a bug from loading
	 *
	 * @param map
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	public static HashMap<Integer, Boolean> convert(HashMap<Integer, Boolean> map) {
		for (Object key : ((HashMap<Integer, Boolean>) map.clone()).keySet()) {
			if (key instanceof String) {
				map.put(Integer.parseInt((String) key), true);
			}
		}
		return map;
	}

	/**
	 * Load the config file
	 *
	 * @param filename
	 * @return
	 */
	public static YSaveInfo loadConfig(String filename) {
		FileHandle active = createFile(filename);

		if (active == null) {
			return null;
		}

		return new Json().fromJson(YSaveInfo.class, active.read());
	}

}
