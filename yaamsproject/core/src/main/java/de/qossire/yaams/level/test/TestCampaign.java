/**
 *
 */
package de.qossire.yaams.level.test;

import de.qossire.yaams.level.BaseCampaign;

/**
 * @author sven
 *
 */
public class TestCampaign extends BaseCampaign {

	/**
	 */
	public TestCampaign() {
		super("test", "test");

		addScenario(new TestScenario(1));
		addScenario(new TestScenario(2));
		addScenario(new TestScenario(3));
	}

}
