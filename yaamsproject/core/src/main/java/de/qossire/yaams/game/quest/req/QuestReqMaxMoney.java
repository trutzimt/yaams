package de.qossire.yaams.game.quest.req;

import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqMaxMoney extends BaseQuestReqMaxCount {

	/**
	 * @param prop
	 * @param val
	 */
	public QuestReqMaxMoney(int count) {
		super(count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + "Money";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getActCount()
	 */
	@Override
	public int getActCount() {
		return (int) MapScreen.get().getPlayer().getMoney();
	}

}