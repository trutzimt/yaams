/**
 * 
 */
package de.qossire.yaams.game.museum.rank;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.game.player.Stats.EStats;
import de.qossire.yaams.game.rooms.RoomToilet;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class RankUsability extends BaseRank {

	/**
	 * @param typ
	 * @param title
	 */
	public RankUsability() {
		super(ERank.USABILITY, "Usability");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return YIcons.getIconD(YIType.PERSONS);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getDesc()
	 */
	@Override
	public String getDesc() {
		if (getCustomer() == -1) {
			return notImplemented;
		}

		String c = "For the next level you need " + getCustomer() + " customer per day. At the moment you have "
				+ MapScreen.get().getPlayer().getStats().get(EStats.CUSTOMERCOUNT) + ". ";

		switch (level) {
		case 0:
			return c;
		case 1:
			return c + " Also you need one toilet. ";
		case 2:
			return c + " Also you need a vending machine. ";
		default:
			return c;
		}
	}

	/**
	 * Return the number of needed artworks
	 * 
	 * @return
	 */
	private int getCustomer() {
		switch (level) {
		case 0:
			return 10;
		case 1:
			return 15;
		case 2:
			return 30;
		default:
			return -1;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#checkLevel()
	 */
	@Override
	public int checkLevel() {
		boolean nextLevel = getCustomer() >= MapScreen.get().getPlayer().getStats().get(EStats.CUSTOMERCOUNT);
		switch (level) {
		case 0:
			break;
		case 1:
			nextLevel = nextLevel && MapScreen.get().getPlayer().getRooms().getRooms(RoomToilet.class).size() >= 1;
			break;
		case 2:
			nextLevel = nextLevel && MapScreen.get().getData().getAllPropsPoints(MapProp.DRINK, 1).size() >= 1;
			break;
		default:
			nextLevel = false;
		}

		return nextLevel ? level + 1 : level;

	}

}
