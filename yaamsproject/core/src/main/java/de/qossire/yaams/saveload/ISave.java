/**
 *
 */
package de.qossire.yaams.saveload;

/**
 * @author sven
 *
 */
@Deprecated
public interface ISave {

	/**
	 * Will call after a game is loaded
	 */
	public void afterLoad();

	/**
	 * Will call before a game is saved
	 */
	public void preSave();
}
