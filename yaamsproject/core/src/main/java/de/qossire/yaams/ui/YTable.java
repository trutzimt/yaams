/**
 * 
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;

/**
 * @author sven
 *
 */
public class YTable extends VisTable {

	/**
	 * Create with a litte space
	 */
	public YTable() {
		super(true);
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public Widget addL(String label, Widget content) {
		add(label + ":").align(Align.right);
		add(content).align(Align.left).growX().row();

		return content;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public VisLabel addL(String label, String content) {
		VisLabel l = new VisLabel(content);
		l.setWrap(true);
		return (VisLabel) addL(label, l);
	}

	/**
	 * Helpermethod to add a Header
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public VisLabel addH(String label) {
		VisLabel l = new VisLabel(label);
		l.setWrap(true);
		add(l).colspan(2).growX().row();
		addSeparator().colspan(2);
		return l;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public WidgetGroup addL(String label, WidgetGroup content) {
		if (label == null || label.length() == 0)
			add();
		else
			add(label + ":").align(Align.right);
		add(content).align(Align.left).growX().row();

		return content;
	}

	/**
	 * Create a scrollY pane and return it
	 * 
	 * @return
	 */
	public VisScrollPane createScrollPane() {
		VisScrollPane pane = new VisScrollPane(this);
		pane.setScrollingDisabled(true, false);
		pane.setFadeScrollBars(false);
		pane.setScrollbarsOnTop(true);
		// pane.setFillParent(true);
		return pane;
	}

	/**
	 * Create a scrollY pane and return it in a table
	 * 
	 * @return
	 */
	public YTable createScrollPaneInTable() {
		YTable t = new YTable();
		t.add(createScrollPane()).grow().row();
		return t;
	}

}
