package de.qossire.yaams.screens.game.thread;

public abstract class YTimeAction {

	protected long runNext;

	/**
	 * 
	 * @param runNext
	 */
	public YTimeAction(long runNext) {
		this.runNext = runNext;
	}

	/**
	 * Return the timestamp of the next run
	 * 
	 * @return
	 */
	public long getRunNext() {
		return runNext;
	}

	/**
	 * Run it
	 * 
	 * @return true = finish with run
	 */
	public abstract boolean run();
}
