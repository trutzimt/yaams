/**
 *
 */
package de.qossire.yaams.ui.actions;

import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YTextButton;

/**
 * @author sven
 *
 */
public abstract class QDefaultAction {

	protected String title;

	/**
	 * @param title
	 */
	public QDefaultAction(String title) {
		super();
		this.title = title;
	}

	/**
	 * Generate the Text button
	 *
	 * @return
	 */
	public YTextButton getButton(final Yaams yaams) {
		return new YTextButton(title) {

			@Override
			public void perform() {
				getAction(yaams).changed(null, null);
			}
		};
	}

	/**
	 * Get the Action
	 *
	 * @param stage
	 * @return
	 */
	public abstract YChangeListener getAction(Yaams yaams);

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
}
