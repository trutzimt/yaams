package de.qossire.yaams.ui;

import com.badlogic.gdx.Gdx;
import com.kotcrab.vis.ui.widget.VisWindow;

public class YWindowHelper {

	public static VisWindow setPositionX(VisWindow w, float x) {
		w.setX(x);
		return w;
	}

	public static VisWindow setPositionY(VisWindow w, float y) {
		w.setY(y);
		return w;
	}

	public static VisWindow setCenterX(VisWindow w) {
		setPositionX(w, Gdx.graphics.getWidth() / 2 - w.getWidth() / 2);
		return w;
	}

	public static VisWindow setCenterY(VisWindow w) {
		setPositionY(w, Gdx.graphics.getHeight() / 2 - w.getHeight() / 2);
		return w;
	}

	public static VisWindow setRight(VisWindow w, int columns) {
		setPositionX(w, Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / columns
				+ Gdx.graphics.getWidth() / columns / 2 - w.getWidth() / 2);
		return w;
	}

	public static VisWindow setBottom(VisWindow w, int rows) {
		setPositionY(w, Gdx.graphics.getHeight() / rows - Gdx.graphics.getHeight() / rows / 2 - w.getHeight() / 2);
		return w;

		//
	}

	public static VisWindow setLeft(VisWindow w, int columns) {
		setPositionX(w, Gdx.graphics.getWidth() / columns / 2 - w.getWidth() / 2);
		return w;
	}
}
