/**
 * 
 */
package de.qossire.yaams.game.quest.action;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTextDialogMgmt.YCharacter;

/**
 * @author sven
 *
 */
public class QuestActionDisplayMessage extends BaseQuestAction {

	private YCharacter character;
	private String message;

	/**
	 * @param character
	 * @param message
	 */
	public QuestActionDisplayMessage(YCharacter character, String message) {
		super("Displays a message");
		this.character = character;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.action.BaseQuestAction#perform(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public void perform() {
		MapScreen.get().getMapgui().getDialogs().add(character, message);

	}

}
