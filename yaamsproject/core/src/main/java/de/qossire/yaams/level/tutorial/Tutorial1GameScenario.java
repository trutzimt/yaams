/**
 *
 */
package de.qossire.yaams.level.tutorial;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.build.BuildManagement;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.quest.BaseQuest;
import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.QuestActionDisplayMessage;
import de.qossire.yaams.game.quest.action.QuestActionGetArt;
import de.qossire.yaams.game.quest.action.QuestActionWin;
import de.qossire.yaams.game.quest.req.QuestReqArtCount;
import de.qossire.yaams.game.quest.req.QuestReqDailyCustomerCount;
import de.qossire.yaams.game.quest.req.QuestReqMinProp;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.level.ScenarioSettings;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YTextDialogMgmt.YCharacter;

/**
 * @author sven
 *
 */
public class Tutorial1GameScenario extends BaseScenario {

	/**
	 */
	public Tutorial1GameScenario() {
		super("tut1", "Basic Tutorial", "map/40.tmx");

		// set the settings
		desc = "You will learn the basics of how to build a museum, open it and attract the first customers.";

		// remove everything
		for (ScenConf s : ScenConf.values()) {
			baseData.put(ScenarioSettings.getKey(s), false);
		}
		baseData.put(ScenarioSettings.getKey(ScenConf.MONEY), true);
		baseData.put(ScenarioSettings.getKey(ScenConf.ART_IMAGE), true);
		baseData.put(ScenarioSettings.getKey(ScenConf.ART_STATURE), true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(final MapScreen screen) {
		super.start(screen);
		screen.getPlayer().addMoney(1_000_000);

		String street = "First, we have to plan from where the customers \n" + "should come. Call from the top right the build menu (third icon or press B) \n"
				+ "and place 2 roads at the edge of the map. After being \n" + "placed, start the game from the top left (or press Space) and wait \n"
				+ "for the roads to finish. \n" + "You can use your gems to speed up the construction.";
		String fund = "Customers can come now, but where should they go?\n" + "In order for us to show great art, we need a place where we can present it. \n"
				+ "First build the foundation 15x. \nOnly on the foundation can we later build something.";
		String wall = "Perfect. Now build 5x the wall on the edge, so it is not so drafty.";
		String art = "The building is standing. It is now time to present our art. \n"
				+ "Open the arts window (forth icon or press C) and select 2 artworks from the depot and place them in the museum. \n"
				+ "You can only hang paintings on the wall and place objects on the foundation.\n"
				+ "Allow enough space for customers to look at the art as well.";
		String ticket = "We are nearly finished. At the end, we have to take care of revenue. Build a ticket machine.";
		String customer = "Now open the museum in the museum window (first icon or press M) and wait until 5 customers arrive. \n"
				+ "More customers come when the museum is bigger and shows more art.";

		// add quests
		BaseQuest b = new BaseQuest("Build the museum");
		BaseQuestItem i = new BaseQuestItem();

		// Street
		i.setDesc(street);
		i.addPreAction(new QuestActionDisplayMessage(YCharacter.HELPER,
				"But before we have to worry about it, \n" + "we should first build our museum. \n"
						+ "Through the map you can scroll with WASD or the arrows. \n" + "If you want to read my message again, \n"
						+ "click on the task icon in the upper right corner (fifth icon or press T)"));
		i.addPreAction(new QuestActionDisplayMessage(YCharacter.HELPER, street));
		i.addPreAction(new QuestActionDisplayMessage(YCharacter.HELPER, "Na, just kidding. Press the multipler near the time or press ,"));
		i.setIcon(YIcons.getBuildIcon(BuildManagement.STREET, true));
		i.addReq(new QuestReqMinProp(MapProp.STREET, 2, 1));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, fund));
		b.addItem(i);

		// Fundament
		i = new BaseQuestItem();
		i.setDesc(fund);
		i.setIcon(YIcons.getBuildIcon(BuildManagement.FOUNDATION, true));
		i.addReq(new QuestReqMinProp(MapProp.FOUNDATION, 15, 1));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, wall));
		b.addItem(i);

		// Wall
		i = new BaseQuestItem();
		i.setDesc(wall);
		i.setIcon(YIcons.getBuildIcon(BuildManagement.WALL, true));
		i.addReq(new QuestReqMinProp(MapProp.WALL, 5, 1));
		// add some art
		i.addAction(new QuestActionGetArt(5));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, art));
		b.addItem(i);

		// Art
		i = new BaseQuestItem();
		i.setDesc(art);
		i.setIcon(YIcons.getIconD(YIType.ARTWORK));
		i.addReq(new QuestReqArtCount(ArtStatus.DISPLAYED, 2));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, ticket));
		b.addItem(i);

		// Ticket
		i = new BaseQuestItem();
		i.setDesc(ticket);
		i.setIcon(YIcons.getBuildIcon(BuildManagement.TICKET, true));
		i.addReq(new QuestReqMinProp(MapProp.TICKET, 1, 1));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, customer));
		b.addItem(i);

		// win
		i = new BaseQuestItem();
		i.setDesc(customer);
		i.setIcon(YIcons.getIconD(YIType.PERSONS));
		i.addReq(new QuestReqDailyCustomerCount(5));
		i.addAction(new QuestActionWin());
		b.addItem(i);

		screen.getPlayer().getQuests().addQuest(b);

		// add some art
		// for (int i = 0, l = NamesGenerator.getIntBetween(3, 5); i < l; i++) {
		// screen.getPlayer().getArtwork().getOwnedInDepot().add(screen.getPlayer().getArtwork().generateArt());
		// }

	}

}
