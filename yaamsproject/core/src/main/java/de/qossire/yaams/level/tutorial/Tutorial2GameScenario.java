/**
 *
 */
package de.qossire.yaams.level.tutorial;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.build.BuildManagement;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.game.quest.BaseQuest;
import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.QuestActionDisplayMessage;
import de.qossire.yaams.game.quest.action.QuestActionGetArt;
import de.qossire.yaams.game.quest.action.QuestActionWin;
import de.qossire.yaams.game.quest.req.QuestReqArtCount;
import de.qossire.yaams.game.quest.req.QuestReqMinFund;
import de.qossire.yaams.game.quest.req.QuestReqMinProp;
import de.qossire.yaams.game.quest.req.QuestReqRankMinCount;
import de.qossire.yaams.game.quest.req.QuestReqTile;
import de.qossire.yaams.game.rooms.RoomToilet;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YTextDialogMgmt.YCharacter;

/**
 * @author sven
 *
 */
public class Tutorial2GameScenario extends BaseScenario {

	/**
	 */
	public Tutorial2GameScenario() {
		super("tut2", "Advanced Tutorial", "map/40.tmx");

		// set the settings
		desc = "You will to make the customers happy and generate a nice profit.";

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(final MapScreen screen) {
		super.start(screen);
		screen.getPlayer().addMoney(1_000_000);

		String start = "At the beginning we need a small museum. Use your knowledge from the first tutorial. \n"
				+ "Build a ticket machine, a street and show 5 artworks.";
		String toilet = "A basic need is the toilet. Build a room and a men's and women's toilets.";
		String automat = "Another basic need is to eat and drink. The easiest way is to set up a vending machine.";
		String rank = "How successful our museum is, we can see in the overview (first icon, m) in the rating tab. \n"
				+ "The task now is to improve the joy rating. It makes sense to open the museum and to speed up the game until the next day.";
		String artrank = "Very nice. There are ranks are a bit more difficult. Now it's time to improve on the artwork. \n"
				+ "According to the description, we need 15 artworks. Time to buy a few more in the art overview. \n"
				+ "You probably also need to enlarge the museum.";
		String fund = "With all the preparation, we have definitely acquired the first rank for our museum. \n"
				+ "That mean, we are entitled to the promotion of the city. Apply now for the promotion.";

		// add quests
		BaseQuest b = new BaseQuest("Build the museum");
		BaseQuestItem i = new BaseQuestItem();

		// Street
		i.setDesc(start);
		i.setIcon(YIcons.getIconD(YIType.MUSEUM));
		i.addPreAction(new QuestActionDisplayMessage(YCharacter.HELPER, start));
		i.addPreAction(new QuestActionGetArt(5));
		i.addReq(new QuestReqMinProp(MapProp.STREET, 1, 1));
		i.addReq(new QuestReqMinProp(MapProp.TICKET, 1, 1));
		i.addReq(new QuestReqArtCount(ArtStatus.DISPLAYED, 5));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, "Our museum thrives on customer satisfaction. Every customer has different needs. \n"
				+ "In the overview (4th icon or button p) you can see the needs."));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, toilet));
		b.addItem(i);

		// Fundament
		i = new BaseQuestItem();
		i.setDesc(toilet);
		i.setIcon(YIcons.getBuildIcon(RoomToilet.WOMEN, true));
		i.addReq(new QuestReqTile(1, RoomToilet.MAN, "men toilet"));
		i.addReq(new QuestReqTile(1, RoomToilet.WOMEN, "women toilet"));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, "In addition to the toilet there is the depot room (works of art are stored better) \n"
				+ "and the art room (exhibition bonus for the same works of art). \n" + "The requirements can be displayed in the room overview (3th, r)."));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, automat));
		b.addItem(i);

		// Wall
		i = new BaseQuestItem();
		i.setDesc(automat);
		i.setIcon(YIcons.getBuildIcon(BuildManagement.DRINK, true));
		i.addReq(new QuestReqTile(1, BuildManagement.DRINK, "vending machine"));
		// add some art
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, "The (basic) needs are satisfied. Guests have works of art that they like to see. \n"
				+ "Accordingly, we should build the museum so that there is always enough space."));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER,
				"If the customers have seen enough art, they are happy and hopefully tell their friends that they will come. \n"
						+ "More customers will come, if your museum is big enough, you have enough streets and the old customers are happy"));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, rank));
		b.addItem(i);

		// Art
		i = new BaseQuestItem();
		i.setDesc(rank);
		i.setIcon(YIcons.getIconD(YIType.JOY));
		i.addReq(new QuestReqRankMinCount(ERank.JOY, 4));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, artrank));
		b.addItem(i);

		// Ticket
		i = new BaseQuestItem();
		i.setDesc(artrank);
		i.addReq(new QuestReqRankMinCount(ERank.ART, 2));
		i.addAction(new QuestActionDisplayMessage(YCharacter.HELPER, fund));
		b.addItem(i);

		// win
		i = new BaseQuestItem();
		i.setDesc(fund);
		i.addReq(new QuestReqMinFund(1));
		i.addAction(new QuestActionWin());
		b.addItem(i);

		screen.getPlayer().getQuests().addQuest(b);

	}

}
