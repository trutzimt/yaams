/**
 *
 */
package de.qossire.yaams.level.single;

import java.util.HashMap;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.quest.BaseQuest;
import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.QuestActionWin;
import de.qossire.yaams.game.quest.req.QuestReqDailyCustomerCount;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class Customer50GameScenario extends BaseScenario {

	protected HashMap<String, String> settings;

	/**
	 */
	public Customer50GameScenario() {
		super("cus50", "50 Customer per day", "map/40.tmx");

		// set the settings
		desc = "Build a museum and attract 50 customer per day.";

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(final MapScreen screen) {
		super.start(screen);

		for (int i = 0, l = NamesGenerator.getIntBetween(2, 6); i < l; i++) {
			screen.getPlayer().getArtwork().addArt(screen.getPlayer().getArtwork().generateArt(), ArtStatus.DEPOT);
		}

		// start money
		screen.getPlayer().addMoney(50_000);

		// add quests
		BaseQuest b = new BaseQuest(getTitle());
		BaseQuestItem i = new BaseQuestItem();
		i.setDesc(desc);
		i.addReq(new QuestReqDailyCustomerCount(50));
		i.addAction(new QuestActionWin());
		b.addItem(i);
		screen.getPlayer().getQuests().addQuest(b);

	}

}
