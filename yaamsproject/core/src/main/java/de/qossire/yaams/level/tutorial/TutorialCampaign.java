/**
 *
 */
package de.qossire.yaams.level.tutorial;

import de.qossire.yaams.level.BaseCampaign;

/**
 * @author sven
 *
 */
public class TutorialCampaign extends BaseCampaign {

	/**
	 */
	public TutorialCampaign() {
		super("tut", "Tutorial");

		desc = "Learn to play the game.";

		addScenario(new Tutorial1GameScenario());
		addScenario(new Tutorial2GameScenario());
	}

}
