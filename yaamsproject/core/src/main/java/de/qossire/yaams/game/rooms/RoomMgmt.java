package de.qossire.yaams.game.rooms;

import java.util.LinkedList;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.screens.game.MapScreen;

public class RoomMgmt {

	public enum RoomTyp {
		DEPOT, ART, TOILET;
	}

	private LinkedList<BaseRoom> rooms;
	private int uid;

	public RoomMgmt() {
		rooms = new LinkedList<>();
		uid++;
	}

	/**
	 * @return the rooms
	 */
	public BaseRoom getRoom(int id) {

		for (BaseRoom art : rooms) {
			if (art.getId() == id) {
				return art;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return Get the room or null
	 */
	public BaseRoom getRoomAt(int x, int y) {
		if (MapScreen.get().getData().getProps(MapProp.ROOMID, x, y) > 0) {
			return getRoom(MapScreen.get().getData().getProps(MapProp.ROOMID, x, y));
		}
		return null;
	}

	/**
	 * Reset for the next day
	 * 
	 * @category thread
	 */
	public void nextDay() {
		for (BaseRoom b : rooms) {
			b.nextDay();
		}
	}

	/**
	 * Create a new room
	 * 
	 * @param room
	 */
	public BaseRoom createRoom(RoomTyp room, int sX, int sY) {
		// create it
		BaseRoom r = null;
		switch (room) {
		case DEPOT:
			r = new RoomDepot(uid++, sX, sY);
			break;
		case ART:
			r = new RoomArt(uid++, sX, sY);
			break;
		case TOILET:
			r = new RoomToilet(uid++, sX, sY);
			break;
		}
		rooms.add(r);

		return r;

	}

	/**
	 * @return the rooms
	 */
	public LinkedList<BaseRoom> getRooms() {
		return rooms;
	}

	/**
	 * @return the rooms
	 */
	@SuppressWarnings("unchecked")
	public <T extends BaseRoom> LinkedList<T> getRooms(Class<T> type) {
		LinkedList<T> rooms = new LinkedList<>();
		for (BaseRoom r : this.rooms) {
			if (r.getClass().equals(type)) {
				rooms.add((T) r);
			}
		}
		return rooms;
	}

	/**
	 * Update in the threads the player logic
	 * 
	 * @category thread
	 */
	public void updateLogic() {
		for (BaseRoom b : rooms) {
			b.updateLogic();
		}
	}

}
