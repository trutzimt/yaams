/**
 * 
 */
package de.qossire.yaams.game.art.window;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.game.art.ArtTimeAction;
import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.art.GameActionPlaceArt;
import de.qossire.yaams.game.rooms.BaseRoom;
import de.qossire.yaams.game.rooms.RoomDepot;
import de.qossire.yaams.game.rooms.RoomMgmt.RoomTyp;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.thread.YClock;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YNotification;
import de.qossire.yaams.ui.YProgressBar;

/**
 * @author sven
 *
 */
public class DepotTab extends ArtworkTab {

	private VisTextButton sell, place, debugAdd, workshop;
	private YProgressBar depot;
	private ArtworkWindow window;

	public DepotTab(ArtworkWindow window) {
		super("Depot", "At the moment you have no art in the camp. You can buy some, if you want.");

		this.window = window;

		// add button
		place = new VisTextButton("Place it");
		place.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				doubleClickElement(null);

			}
		});
		buttonBar.addActor(place);

		if (MapScreen.get().getSettings().isActive(ScenConf.ART_SELL)) {
			sell = new VisTextButton("Sell it");
			sell.addCaptureListener(new YChangeListener(false) {

				@Override
				public void changedY(Actor actor) {

					MapScreen.get().getPlayer().getArtwork().removeArt((BaseArt) active);
					MapScreen.get().getPlayer().addMoney(((BaseArt) active).getPrice() / 2);
					for (int i = 0; i < elements.size(); i++) {
						if (elements.get(i).getUserObject() == active) {
							elements.remove(i);
							break;
						}
					}
					reset();
					YSounds.buy();
					rebuild();

				}
			});
			buttonBar.addActor(sell);
		}

		if (MapScreen.get().getSettings().isActive(ScenConf.WORKSHOP)) {
			workshop = new VisTextButton("Send to workshop");
			workshop.addCaptureListener(new YChangeListener(false) {

				@Override
				public void changedY(Actor actor) {

					final BaseArt a = ((BaseArt) active);
					a.setStatus(ArtStatus.WORKSHOP);

					for (int i = 0; i < elements.size(); i++) {
						if (elements.get(i).getUserObject() == active) {
							elements.remove(i);
							break;
						}
					}

					// add moving option
					ArtTimeAction t = new ArtTimeAction("Moving to Workshop", YClock.addCalcTimestamp(MapScreen.get().getClock().getTimeStamp(), 0, 0, 10)) {

						@Override
						public boolean run() {
							a.setTimeAction(null);
							MapScreen.get().getMapgui()
									.addNotification(new YNotification("Workshop", a.getName() + " arrived in the workshop.", new Image(a.getIcon(true))));
							// just wait
							return true;
						}
					};

					a.setTimeAction(t);
					MapScreen.get().getClock().addTimeAction(t);

					reset();
					YSounds.click();
					rebuild();

				}
			});
			buttonBar.addActor(workshop);
		}

		if (YSettings.isDebug()) {
			debugAdd = new VisTextButton("Add Art");
			debugAdd.addCaptureListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					BaseArt b = MapScreen.get().getPlayer().getArtwork().generateArt();

					MapScreen.get().getPlayer().getArtwork().addArt(b, ArtStatus.DEPOT);
					rebuild();

				}
			});
			buttonBar.addActor(debugAdd);
		}

		// add progress
		depot = new YProgressBar(1, 1);
		buttonBar.addActor(depot);
		reset();

	}

	/**
	 * Reset the status
	 */
	@Override
	protected void reset() {
		super.reset();
		if (sell != null)
			sell.setDisabled(true);
		if (workshop != null) {
			workshop.setDisabled(true);
			workshop.setText("Send to workshop");
		}

		place.setDisabled(true);
		place.setText("Place it");
	}

	/**
	 * Rebuild the gui
	 */
	@Override
	protected void rebuild() {

		// calc depot size
		int size = 0;
		for (BaseRoom b : MapScreen.get().getPlayer().getRooms().getRooms()) {
			if (b.getTyp() == RoomTyp.DEPOT) {
				size += ((RoomDepot) b).getSize();
			}
		}

		elements.clear();
		// add all elements
		for (final BaseArt art : MapScreen.get().getPlayer().getArtwork().getArts(ArtStatus.DEPOT)) {
			addElement(art.getName(), art.getIcon(true), art);
		}

		depot.getProgress().setRange(0, size);
		depot.getProgress().setValue(elements.size());
		depot.getLabel().setText("Depot: " + elements.size() + "/" + size);

		super.rebuild();
	}

	/**
	 * Place the selected artwork
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		YSounds.click();
		MapScreen.get().getInputs().setActiveAction(new GameActionPlaceArt((BaseArt) active, MapScreen.get()));
		window.close();

	}

	@Override
	protected void clickElement(Button btn) {
		place.setText("Place " + ((BaseArt) btn.getUserObject()).getName());
		place.setDisabled(false);
		if (workshop != null) {
			workshop.setDisabled(false);
			if (MapScreen.get().getPlayer().getArtwork().getWorkshopCapacity() > MapScreen.get().getPlayer().getArtwork().getArtsCount(ArtStatus.WORKSHOP))
				workshop.setText("Workshop is full");
		}
		if (sell != null)
			sell.setDisabled(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#addElement(java.lang.String,
	 * com.badlogic.gdx.scenes.scene2d.utils.Drawable, java.lang.Object)
	 */
	@Override
	public void addElement(String title, Drawable icon, Object o) {
		super.addElement(title, icon, o);
	}
}