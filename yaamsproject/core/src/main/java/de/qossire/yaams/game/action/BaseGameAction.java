package de.qossire.yaams.game.action;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;

public abstract class BaseGameAction {

	protected String type, title;

	/**
	 * Create a new action
	 *
	 * @param type
	 * @param x
	 * @param y
	 */
	public BaseGameAction(String type, String title) {
		this.type = type;
		this.title = title;
	}

	/**
	 * Will all, when the action is active on a map and get every click. Do not
	 * forgot to remove the action after finish
	 *
	 * @param activeSelected
	 *
	 * @param screenXToMapX
	 * @param screenYToMapY
	 */
	public abstract void performClick(int x, int y, MapScreen map);

	/**
	 * Will called, if a mouse was moved, overwrite if want to implement
	 * 
	 * @param x
	 * @param y
	 * @param mapScreen
	 */
	public void mouseMoved(int x, int y, MapScreen mapScreen) {
	}

	/**
	 * Will called, when the action will be destroyed
	 */
	public void destroy(MapScreen mapScreen) {
		YSounds.buzzer();
	}

	/**
	 * Return the icon, who will be shown
	 * 
	 * @return
	 */
	public abstract Drawable getIcon();

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
}
