/**
 * 
 */
package de.qossire.yaams.game.quest.action;

/**
 * @author sven
 *
 */
public abstract class BaseQuestAction {

	protected String title;

	/**
	 * @param title
	 */
	public BaseQuestAction(String title) {
		super();
		this.title = title;
	}

	/**
	 * Perform this action
	 */
	public abstract void perform();

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

}