/**
 *
 */
package de.qossire.yaams.level;

import java.util.HashMap;

import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class BaseScenario {

	protected String mapLink;
	protected String title;
	protected String desc;
	protected String id;
	protected BaseCampaign camp;
	protected HashMap<String, Object> baseData;

	/**
	 *
	 * @param mapLink
	 */
	public BaseScenario(String id, String title, String mapLink) {
		super();
		this.id = id;
		this.mapLink = mapLink;
		this.title = title;
		baseData = new HashMap<>();
		desc = "No Desc";

	}

	/**
	 * Overwrite to implement own scenario logic
	 *
	 * @param player
	 */
	public void start(MapScreen screen) {
		screen.getData().setP("campaign", camp.getID());
		screen.getData().setP("scenario", id);
	}

	/**
	 * @return the mapLink
	 */
	public String getMapLink() {
		return mapLink;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	@Override
	public String toString() {
		return getTitle();
	}

	/**
	 * Get a node for the level tree
	 *
	 * @return
	 */
	public Node getNode() {
		Node n = new Node(new VisLabel(isWon() ? "(x) " + getTitle() : getTitle()));
		n.setObject(this);
		return n;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the id
	 */
	public String getFullId() {
		return camp.getID() + "." + getId();
	}

	/**
	 * @return the camp
	 */
	public BaseCampaign getCamp() {
		return camp;
	}

	/**
	 * @param camp
	 *            the camp to set
	 */
	public void setCamp(BaseCampaign camp) {
		this.camp = camp;
	}

	/**
	 * Load the settings, will call after loading
	 *
	 * @param map
	 */
	public void load(MapScreen map) {}

	/**
	 * Set if the scenario is won
	 * 
	 * @return
	 */
	public boolean isWon() {
		return YSettings.getPref().getBoolean("scen." + getFullId(), false);
	}

	/**
	 * Set the scenario is won
	 */
	public void setWon() {
		YSettings.getPref().putBoolean("scen." + getFullId(), true);
		YSettings.save();
	}

	/**
	 * Load the basics for the game, before the game data are loaded.
	 */
	public void preLoad(MapScreen map) {}

	/**
	 * @return the baseData
	 */
	public HashMap<String, Object> getBaseData() {
		return baseData;
	}
}
