/**
 *
 */
package de.qossire.yaams.level.single;

import java.util.HashMap;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.game.quest.BaseQuest;
import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.QuestActionDisplayMessage;
import de.qossire.yaams.game.quest.action.QuestActionWin;
import de.qossire.yaams.game.quest.req.QuestReqArtCount;
import de.qossire.yaams.game.quest.req.QuestReqRankMinCount;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.level.ScenarioSettings;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YTextDialogMgmt.YCharacter;

/**
 * @author sven
 *
 */
public class HerLastWillGameScenario extends BaseScenario {

	protected HashMap<String, String> settings;

	/**
	 */
	public HerLastWillGameScenario() {
		super("herlastwill", "Her last will", "map/40.tmx");

		// set the settings
		desc = "Your grandmother inherited various works of art. However, in her will she decreed that at least half of the works of art should be exhibited in a new museum.";

		baseData.put(ScenarioSettings.getKey(ScenConf.ART_BUY), false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(final MapScreen screen) {
		super.start(screen);

		for (int i = 0, l = 30; i < l; i++) {
			screen.getPlayer().getArtwork().addArt(screen.getPlayer().getArtwork().generateArt(), ArtStatus.DEPOT);
		}

		// add quests
		BaseQuest b = new BaseQuest("Tasks");
		BaseQuestItem i = new BaseQuestItem();
		i.setTitle(getTitle());

		i.setDesc(desc);
		i.setIcon(YIcons.getIconD(YIType.ARTWORK));
		i.addReq(new QuestReqArtCount(ArtStatus.DISPLAYED, 15));
		i.addReq(new QuestReqRankMinCount(ERank.OVERVIEW, 2));
		i.addPreAction(new QuestActionDisplayMessage(YCharacter.FINANCE, desc));
		i.addAction(new QuestActionWin());
		b.addItem(i);
		screen.getPlayer().getQuests().addQuest(b);

	}

}
