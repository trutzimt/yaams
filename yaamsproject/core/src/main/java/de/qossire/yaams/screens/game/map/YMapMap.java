/**
 *
 */
package de.qossire.yaams.screens.game.map;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;

import de.qossire.yaams.code.YLog;
import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.personStage.PersonStage;

/**
 * Management the map
 *
 * @author sven
 *
 */
public class YMapMap {

	// private int mapspeed;
	private boolean showOverlay;

	private TiledMap tiledMap, overlayMap;// , unitMap, actionMap;
	private TiledMapTileSet artTileset;// , unitMap, actionMap;
	private TiledMapRenderer tiledMapRenderer, overlayRenderer;// , unitMapRenderer, actionMapRenderer;
	private TiledMapTileLayer buildLayer, artLayer, floorLayer, roomLayer;
	// private TiledMapTileLayer unitLayer;
	// private TiledMapTileLayer actionLayer;
	// private TiledMapTileLayer fogLayer;

	private PersonStage personStage;
	private Stage stage;
	private Image cursor;
	private MapScreen mapScreen;

	private int width, height;

	private OrthographicCamera camera;
	private Vector3 dstCamera;

	// OrthoCamController cameraController;

	/**
	 *
	 */
	public YMapMap(MapScreen screen) {
		mapScreen = screen;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		dstCamera = camera.position.cpy();

		// cameraController = new OrthoCamController(camera);
		// Gdx.input.setInputProcessor(cameraController);

		stage = new Stage();
		stage.setViewport(new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera));

		createMap();

		personStage = new PersonStage(screen, this);

	}

	/**
	 * Move the map to this coordinates
	 *
	 * @param x
	 * @param y
	 */
	public void moveMapView(float x, float y) {

		// TODO check border

		// TODO add ZOOM
		dstCamera.x += x;
		dstCamera.y += y;
	}

	/**
	 * Load the map and everything
	 */
	private void createMap() {
		TmxMapLoader tmx = new TmxMapLoader();

		tiledMap = tmx.load(mapScreen.getScenario().getMapLink());
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

		// load layers
		TiledMapTileLayer t = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		width = t.getWidth();
		height = t.getHeight();
		int tWidth = (int) t.getTileWidth();
		int tHeight = (int) t.getTileHeight();

		// add floor layer
		floorLayer = new TiledMapTileLayer(width, height, tWidth, tHeight);
		tiledMap.getLayers().add(floorLayer);

		// add build layer
		buildLayer = new TiledMapTileLayer(width, height, tWidth, tHeight);
		tiledMap.getLayers().add(buildLayer);

		// add art layer
		artTileset = tmx.load("system/tileset/art.tmx").getTileSets().getTileSet(0);
		artLayer = new TiledMapTileLayer(width, height, tWidth, tHeight);
		tiledMap.getLayers().add(artLayer);

		// add overlay layer
		overlayMap = tmx.load("system/tileset/overlay.tmx");
		overlayRenderer = new OrthogonalTiledMapRenderer(overlayMap);

		roomLayer = new TiledMapTileLayer(width, height, tWidth, tHeight);
		overlayMap.getLayers().add(roomLayer);

		// unitMap = tmx.load("system/tileset/units.tmx");
		// unitMapRenderer = new OrthogonalTiledMapRenderer(unitMap);

		// add unit layer
		// unitLayer = new TiledMapTileLayer(width, height, tWidth, tHeight);
		// unitMap.getLayers().add(unitLayer);

		// add action layer
		// actionMap = tmx.load("system/tileset/fog.tmx");
		// actionMapRenderer = new OrthogonalTiledMapRenderer(actionMap);

		// add action layer
		// actionLayer = new TiledMapTileLayer(width, height, tWidth, tHeight);
		// actionMap.getLayers().add(actionLayer);

		// add fog layer?
		// if (mapScreen.getScenario().isConfig(ScenarioConfig.FOGOFWAR)) {
		// fogLayer = new TiledMapTileLayer(width, height, tWidth, tHeight);
		// actionMap.getLayers().add(fogLayer);
		//
		// }
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	public void render(float delta) {
		camera.position.lerp(dstCamera, 0.075f);// vector of the camera desired position and smoothness of the movement

		// tell the camera to update its matrices.
		camera.update();

		// show it
		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();

		// unitMapRenderer.setView(camera);
		// unitMapRenderer.render();
		//
		// actionMapRenderer.setView(camera);
		// actionMapRenderer.render();

		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();

		// show it?
		if (showOverlay) {
			overlayRenderer.setView(camera);
			overlayRenderer.render();
		}

	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id
	 */
	@Deprecated
	public int getBuildTile(int x, int y) {
		return mapScreen.getData().getBuildTile(x, y);
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id
	 */
	@Deprecated
	public int getFloorTile(int x, int y) {
		return mapScreen.getData().getFloorTile(x, y);
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id
	 */
	@Deprecated
	public int getArtTile(int x, int y) {
		return mapScreen.getData().getArtTile(x, y) - 1;
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id -2 = out of map
	 */
	public int getRoomTile(int x, int y) {
		return getLayerTile(roomLayer, x, y);
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id
	 */
	private int getLayerTile(TiledMapTileLayer layer, int x, int y) {
		if (!mapScreen.getTilesetHelper().isValidePosition(x, y)) {
			return -2;
		}

		if (layer.getCell(x, y) == null)
			return -1;

		return layer.getCell(x, y).getTile().getId() - 1;
	}

	/**
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void setRoomTile(int id, int x, int y) {
		roomLayer.setCell(x, y, new Cell());
		roomLayer.getCell(x, y).setTile(overlayMap.getTileSets().getTileSet(0).getTile(id + 1));
	}

	/**
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void setTile(MapProp l, int id, int x, int y) {
		TiledMapTileLayer lay;
		TiledMapTileSet set = tiledMap.getTileSets().getTileSet(0);
		switch (l) {
		case ARTTILE:
			lay = artLayer;
			set = artTileset;
			break;
		case BUILDTILE:
			lay = buildLayer;
			break;
		case FLOORTILE:
			lay = floorLayer;
			break;
		default:
			throw new IllegalArgumentException(l + " is not supported");
		}

		if (lay.getCell(x, y) == null)
			lay.setCell(x, y, new Cell());
		lay.getCell(x, y).setTile(set.getTile(id + 1));
	}

	/**
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void removeTile(MapProp l, int x, int y) {
		TiledMapTileLayer lay;
		switch (l) {
		case ARTTILE:
			lay = artLayer;
			break;
		case BUILDTILE:
			lay = buildLayer;
			break;
		case FLOORTILE:
			lay = floorLayer;
			break;
		default:
			throw new IllegalArgumentException(l + " is not supported");
		}

		lay.setCell(x, y, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
		camera.setToOrtho(false, width, height);
		setCameraZoom(camera.zoom);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	public void dispose() {
		tiledMap.dispose();
		// unitMap.dispose();
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param zoom
	 *            the zoom to set
	 */
	public void zoomIn() {
		if (camera.zoom > 0.1f) {
			setCameraZoom(camera.zoom - 0.1f);// / 2f);
		}
	}

	/**
	 * @param zoom
	 *            the zoom to set
	 */
	public void zoomOut() {
		setCameraZoom(camera.zoom + 0.1f);// * 2f);
	}

	/**
	 * @param zoom
	 *            the zoom to set
	 */
	public void setCameraZoom(float zoom) {
		camera.zoom = zoom;
	}

	/**
	 * 
	 * @return
	 */
	public OrthographicCamera getCamera() {
		return camera;
	}

	/**
	 * @return the personStage
	 */
	public PersonStage getPersonStage() {
		return personStage;
	}

	/**
	 * 
	 * @return the props, if not exist return 0, if no valid coordinates return 0
	 */
	@Deprecated
	public int getProps(MapProp prop, int x, int y) {
		return mapScreen.getData().getProps(prop, x, y);
	}

	/**
	 * @param props
	 *            the props to set
	 */
	@Deprecated
	public void addProps(MapProp prop, int x, int y, int value) {
		mapScreen.getData().addProps(prop, x, y, value);
	}

	/**
	 * Set Rekursivlie the settings
	 * 
	 * @param prop
	 * @param x
	 * @param y
	 * @param value
	 * @param stopOnWall
	 */
	@Deprecated
	public void addPropsRekursive(MapProp prop, int x, int y, int value, boolean stopOnWall) {
		mapScreen.getData().addPropsRekursive(prop, x, y, value, stopOnWall);
	}

	/**
	 * Return a list with all coordinates, who have this prop
	 * 
	 * @param customer
	 * @param prop
	 * @param min,
	 *            min value
	 * @return the list, if nothing was found, return null
	 */
	@Deprecated
	public LinkedList<YPoint> getAllBuildTilePoints(int id) {
		return mapScreen.getData().getAllBuildTilePoints(id);
	}

	/**
	 * This customer will go to the nearest tile, with this properties, based on
	 * path
	 * 
	 * @param customer
	 * @param prop
	 * @param min,
	 *            min value
	 * @return true, found a way or tile, false otherwise
	 */
	public boolean goToNearestTileFrom(Customer customer, MapProp prop, int min) {
		YLog.log("goToNearestTileFrom", customer.getPersonName() + " looks to find " + prop.getNamel());

		// found points?
		LinkedList<YPoint> points = MapScreen.get().getData().getAllPropsPoints(prop, min);
		return personStage.goToNearestTileFrom(customer, points);

	}

	/**
	 * @param showOverlay
	 *            the showOverlay to set
	 */
	public void setShowOverlay(boolean showOverlay) {
		this.showOverlay = showOverlay;
	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @return the cursor
	 */
	public Image getCursor() {
		return cursor;
	}

	/**
	 * @param cursor
	 *            the cursor to set
	 */
	public void setCursor(Image cursor) {
		// has a temp?
		if (this.cursor != null) {
			this.cursor.remove();
		}

		this.cursor = cursor;

		// has a new?
		if (cursor != null) {
			cursor.setZIndex(100000);
			stage.addActor(cursor);
		}
	}
}
