/**
 * 
 */
package de.qossire.yaams.game.rooms;

import de.qossire.yaams.game.build.BaseBuildAction;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.rooms.RoomMgmt.RoomTyp;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class BuildActionRoom extends BaseBuildAction {

	private RoomTyp room;

	/**
	 * @param room
	 */
	public BuildActionRoom(RoomTyp room) {
		super();
		this.room = room;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.build.BaseBuildAction#perform(de.qossire.yaams.screens.
	 * map.MapScreen, int, int)
	 */
	@Override
	public void build(MapScreen mapScreen, int x, int y) {
		BaseRoom b = null;

		// has a room near?
		b = getRoom(mapScreen, x - 1, y);
		b = b == null ? getRoom(mapScreen, x, y - 1) : b;
		b = b == null ? getRoom(mapScreen, x + 1, y) : b;
		b = b == null ? getRoom(mapScreen, x, y + 1) : b;

		// has a room?
		if (b == null) {
			// create a new one
			b = mapScreen.getPlayer().getRooms().createRoom(room, x, y);
		} else {
			mapScreen.getMap().setRoomTile(mapScreen.getMap().getRoomTile(x, y) - 1, x, y);
		}
		b.setToCheck(true);

		// set it
		mapScreen.getData().addProps(MapProp.ROOMID, x, y, b.getId());
	}

	private BaseRoom getRoom(MapScreen mapScreen, int x, int y) {
		if (mapScreen.getData().getProps(MapProp.ROOMID, x, y) > 0) {
			BaseRoom b = mapScreen.getPlayer().getRooms().getRoom(mapScreen.getData().getProps(MapProp.ROOMID, x, y));
			if (b.getTyp() != room) {
				return null;
			}
			return b;
		}
		return null;
	}

	@Override
	public void destroy(MapScreen mapScreen, int x, int y) {
		// TODO Auto-generated method stub

	}

}
