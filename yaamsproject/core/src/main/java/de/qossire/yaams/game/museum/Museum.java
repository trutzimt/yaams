/**
 * 
 */
package de.qossire.yaams.game.museum;

import de.qossire.yaams.game.player.Player;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;

/**
 * @author sven
 *
 */
public class Museum {

	private String name;
	private MapScreen mapScreen;
	private int openFrom, openTo;
	private boolean playedClosing;

	/**
	 * 
	 */
	public Museum(MapScreen mapScreen, Player player) {
		name = NamesGenerator.getMuseumName(name, player.getTown().getName());

		this.mapScreen = mapScreen;

		openFrom = -1;

		// save it
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_SAVE, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				MapScreen.get().getData().setP("museum_openFrom", openFrom);
				MapScreen.get().getData().setP("museum_openTo", openTo);
				return false;
			}
		});

		// load it
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_LOAD, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				openFrom = MapScreen.get().getData().getPI("museum_openFrom");
				openTo = MapScreen.get().getData().getPI("museum_openTo");
				return false;
			}
		});

		// show next day
		MapScreen.get().getEvents().register(MapEventHandlerTyp.NEXTDAY, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				playedClosing = false;
				return false;
			}
		});
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the openFrom
	 */
	public int getOpenFrom() {
		return openFrom;
	}

	/**
	 * @param openFrom
	 *            the openFrom to set
	 */
	public void setOpenFrom(int openFrom) {
		this.openFrom = openFrom;
	}

	/**
	 * @return the openTo
	 */
	public int getOpenTo() {
		return openTo;
	}

	/**
	 * @param openTo
	 *            the openTo to set
	 */
	public void setOpenTo(int openTo) {
		this.openTo = openTo;
	}

	/**
	 * Check if the museum is open
	 * 
	 * @return
	 */
	public boolean isOpen() {
		return !(openFrom == -1);
	}

	/**
	 * Update in the threads the player logic
	 * 
	 * @category thread
	 */
	public void updateLogic() {

		// check if the museum is open & it is the right time
		if (isOpen() && mapScreen.getClock().getHour() >= openTo - 1 && !playedClosing) {
			YSounds.speechClosing();

			playedClosing = true;
		}
	}

}
