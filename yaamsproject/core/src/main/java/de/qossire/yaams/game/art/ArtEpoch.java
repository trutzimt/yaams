/**
 * 
 */
package de.qossire.yaams.game.art;

import de.qossire.yaams.code.IHashMapID;

/**
 * @author sven
 *
 */
public class ArtEpoch implements IHashMapID {

	private int minAge, maxAge;
	private String name, region;

	/**
	 * @param minAge
	 * @param maxAge
	 * @param id
	 * @param name
	 * @param region
	 */
	public ArtEpoch(String name, int minAge, int maxAge, String region) {
		super();
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.name = name;
		this.region = region;
	}

	/**
	 * @return the minAge
	 */
	public int getMinAge() {
		return minAge;
	}

	/**
	 * @return the maxAge
	 */
	public int getMaxAge() {
		return maxAge;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.code.IHashMapID#getID()
	 */
	@Override
	public String getID() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.code.IHashMapID#getTitle()
	 */
	@Override
	public String getTitle() {
		return name;
	}

}
