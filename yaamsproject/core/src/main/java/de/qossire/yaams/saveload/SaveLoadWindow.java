/**
 * 
 */
package de.qossire.yaams.saveload;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTabWindow;

/**
 * @author sven
 *
 */
public class SaveLoadWindow extends YTabWindow {

	/**
	 * @param name
	 */
	public SaveLoadWindow(IScreen screen, boolean load, boolean exitAfterSave) {
		super("Save & Load");

		if (screen instanceof MapScreen)
			tabbedPane.add(new SaveTab(screen, exitAfterSave));
		if (!exitAfterSave)
			tabbedPane.add(new LoadTab(screen));

		buildIt();

		if (load && tabbedPane.getTabs().size >= 2)
			tabbedPane.switchTab(tabbedPane.getTabs().size - 1);

	}

}
