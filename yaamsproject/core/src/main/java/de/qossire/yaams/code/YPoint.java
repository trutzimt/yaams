package de.qossire.yaams.code;

public class YPoint {

	private int x, y;

	public YPoint() {}

	public YPoint(YPoint p) {
		this(p.getX(), p.getY());
	}

	public YPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Add this coordinate
	 * 
	 * @param x
	 * @return
	 */
	public YPoint addX(int x) {
		this.x += x;
		return this;
	}

	/**
	 * Add this coordinate
	 * 
	 * @param y
	 * @return
	 */
	public YPoint addY(int y) {
		this.y += y;
		return this;
	}

	/**
	 * Add this coordinate
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public YPoint addXY(int x, int y) {
		this.x += x;
		this.y += y;
		return this;
	}

	/**
	 * Create a new point, with this difference
	 * 
	 * @param y
	 * @return
	 */
	public YPoint addXandClone(int x) {
		return new YPoint(this.x + x, y);
	}

	/**
	 * Create a new point, with this difference
	 * 
	 * @param x
	 * @return
	 */
	public YPoint addYandClone(int y) {
		return new YPoint(x, this.y + y);
	}

	/**
	 * Create a new point, with this difference
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public YPoint addXYandClone(int x, int y) {
		return new YPoint(this.x + x, this.y + y);
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

}
