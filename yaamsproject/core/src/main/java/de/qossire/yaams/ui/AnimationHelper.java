/**
 * 
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class AnimationHelper {

	/**
	 * 
	 * @param value
	 * @param x
	 * @param y
	 * @param map,
	 *            Show at which stage? true = map
	 */
	public static void moneyAnimation(int value, int x, int y, boolean map) {
		// has money?
		if (!MapScreen.get().getSettings().isActive(ScenConf.MONEY))
			return;

		VisLabel top = new VisLabel(TextHelper.getMoneyString(value));
		top.setColor(value > 0 ? Color.LIME : Color.RED);
		top.getColor().a = 0;
		// add animation
		top.addAction(Actions.parallel(Actions.sequence(Actions.moveBy(0, 80, 3.5f)),
				Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(1), Actions.fadeOut(2f), Actions.removeActor())));

		VisLabel bottom = new VisLabel(TextHelper.getMoneyString(value));
		bottom.setColor(Color.BLACK);
		bottom.getColor().a = 0;
		// // add animation
		bottom.addAction(Actions.parallel(Actions.sequence(Actions.moveBy(0, 80, 3.5f)),
				Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(1), Actions.fadeOut(2f), Actions.removeActor())));

		// give money
		YSounds.buy();
		MapScreen.get().getPlayer().addMoney(value);

		// set position and add it to stage
		if (map) {
			bottom.setPosition(x * 32 + 1, y * 32 + 23);
			MapScreen.get().getMap().getStage().addActor(bottom);

			top.setPosition(x * 32, y * 32 + 24);
			MapScreen.get().getMap().getStage().addActor(top);
		} else {
			bottom.setPosition(x + 1, y - 1);
			MapScreen.get().getStage().addActor(bottom);

			top.setPosition(x, y);
			MapScreen.get().getStage().addActor(top);
		}

	}

}
