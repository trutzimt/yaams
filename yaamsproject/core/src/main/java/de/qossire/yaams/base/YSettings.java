/**
 *
 */
package de.qossire.yaams.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import de.qossire.yaams.code.YLog;

/**
 * @author sven
 *
 */
public class YSettings {

	private static Preferences pref;

	/**
	 * Get it
	 *
	 * @return
	 */
	public static Preferences getPref() {

		if (pref == null) {
			load();
		}

		return pref;
	}

	/**
	 * Get it
	 *
	 * @return
	 */
	public static void save() {
		if (pref != null) {
			pref.putInteger("screen.width", Gdx.graphics.getWidth());
			pref.putInteger("screen.height", Gdx.graphics.getHeight());

			pref.flush();

			YLog.log("Settings", "Save it");
		}
	}

	/**
	 * Get it
	 *
	 * @return
	 */
	public static void load() {
		if (pref == null) {
			pref = Gdx.app.getPreferences("de.qossire.yaams");
		}
	}

	/**
	 * Return if the user select the bigger gui
	 *
	 * @return
	 */
	public static boolean isBiggerGui() {
		return getPref().getBoolean("biggerGui", true);
	}

	/**
	 * Return if the user select the bigger gui
	 *
	 * @return
	 */
	public static int getGuiScale() {
		return getPref().getInteger("guiScale", 32);
	}

	/**
	 * Return the folder for the gui scale
	 *
	 * @return
	 */
	public static String getGuiScaleFolder() {
		return getGuiScale() == 32 ? "system" : Integer.toString(getGuiScale());
	}

	/**
	 * Get the selected language
	 *
	 * @return
	 */
	@Deprecated
	public static String getLang() {
		return getPref().getString("lang", "def");
	}

	/**
	 * Enabled Autosave
	 *
	 * @return
	 */
	public static boolean isAutosave() {
		return getPref().getBoolean("autosave", true);
	}

	/**
	 * Enabled Fullscreen
	 *
	 * @return
	 */
	public static boolean isFullscreen() {
		return getPref().getBoolean("fullscreen", false);
	}

	/**
	 * Get sound volume
	 *
	 * @return
	 */
	public static float getSoundVolume() {
		return getPref().getFloat("sound", 0.5f);
	}

	/**
	 * Get music volume
	 *
	 * @return
	 */
	public static float getMusicVolume() {
		return getPref().getFloat("music", 0.5f);
	}

	/**
	 * Is the game in the dev mode?
	 * 
	 * @return
	 */
	public static boolean isDebug() {
		return getPref().getBoolean("dev", false);
	}

	/**
	 * Is the game in the dev mode?
	 * 
	 * @return
	 */
	public static boolean isDebugSpeed() {
		return getPref().getBoolean("devSpeed", false);
	}

	/**
	 * Setting for vsync
	 * 
	 * @return
	 */
	public static boolean isVSync() {
		return getPref().getBoolean("vsync", false);
	}

	/**
	 * Get the selected language
	 *
	 * @return
	 */
	public static String getUserName() {
		return getPref().getString("playername", System.getProperties().getProperty("user.name", "Player"));
	}

}
