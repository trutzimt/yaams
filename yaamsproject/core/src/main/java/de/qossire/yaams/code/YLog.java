package de.qossire.yaams.code;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;

public class YLog {

	public static void log(Object... objects) {
		Throwable e = new Throwable();

		String mess = "";

		for (Object o : objects) {
			if (mess.length() > 0) {
				mess += ", ";
			}

			mess += getName(o);
		}

		// create
		Gdx.app.log(getLine(1, e), mess);
	}

	/**
	 * @param e
	 * @return
	 */
	private static String getLine(int id, Throwable e) {
		String f = e.getStackTrace()[id].getFileName();
		int l = e.getStackTrace()[id].getLineNumber();
		String t = "(" + (f != null && l >= 0 ? f + ":" + l : (f != null ? f : "Unknown Source")) + ")";
		return t;
	}

	public static void log2(Object... objects) {
		Throwable e = new Throwable();

		String mess = getLine(2, e);

		for (Object o : objects) {
			if (mess.length() > 0) {
				mess += ", ";
			}

			mess += getName(o);
		}

		// create
		Gdx.app.log(getLine(1, e), mess);
	}

	private static String getName(Object o) {
		if (o == null) {
			return "null";
		} else if (o.getClass().isArray()) {

			try {
				Object[] oa = (Object[]) o;
				return oa.getClass().getSimpleName().toString() + oa.length + ": " + Arrays.toString(oa);
			} catch (Exception e) {
				e.printStackTrace();
				return "Array error " + e.getClass().getSimpleName() + " " + e.getLocalizedMessage();
			}

		} else {
			return o.toString();
		}
	}

}
