/**
 * 
 */
package de.qossire.yaams.game.museum.rank;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YNotification;

/**
 * @author sven
 *
 */
public abstract class BaseRank {

	protected int level;
	protected ERank typ;
	protected String title;

	protected final String notImplemented = "The next level is not implemented yet.";

	/**
	 * 
	 */
	public BaseRank(ERank typ, String title) {
		this.typ = typ;
		this.title = title;
	}

	/**
	 * Get the icon
	 * 
	 * @return
	 */
	public abstract Drawable getIcon();

	/**
	 * Get the description, how to reach the next level
	 * 
	 * @return
	 */
	public abstract String getDesc();

	/**
	 * Reset for the next day
	 * 
	 * @category thread
	 */
	public void nextDay() {
		int nLevel = checkLevel();
		if (nLevel != level) {
			level = nLevel;
			// inform player

			MapScreen.get().getMapgui()
					.addNotification(new YNotification(title, "Rank " + title + " has the new Level " + nLevel + " of 10.", new Image(getIcon())));
		}
	}

	/**
	 * Check the status and return the new level
	 * 
	 * @return
	 */
	public abstract int checkLevel();

	/**
	 * @return the value
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @return the typ
	 */
	public ERank getTyp() {
		return typ;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title + " " + level + "/10";
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}
}
