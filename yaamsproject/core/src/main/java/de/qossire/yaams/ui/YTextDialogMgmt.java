/**
 * 
 */
package de.qossire.yaams.ui;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class YTextDialogMgmt {

	public enum YCharacter {
		TOWN("Edith Binglefinger", "town", true), HELPER("Zigor Ibarra", "helper", false), FINANCE("Felix Archibald Billings", "finance", false);
		private String file, name;
		private boolean left;

		private YCharacter(String name, String file, boolean left) {
			this.file = file;
			this.name = name;
			this.left = left;
		}
	}

	private LinkedList<YTextDialog> dialogs;
	private int act;
	private MapScreen mapScreen;
	private Image image, bottom, top;
	private int oldTimeSpeed;

	/**
	 * 
	 */
	public YTextDialogMgmt(MapScreen map) {
		dialogs = new LinkedList<>();
		this.mapScreen = map;
		act = -1;

	}

	/**
	 * Check if their is some dialogs and show them
	 */
	public void checkToShow() {
		// need to show?
		if (act == -1 && dialogs.size() == 0) {
			return;
		}

		// is showing?
		if (image != null) {
			return;
		}

		// stop the time
		oldTimeSpeed = mapScreen.getClock().getTimeSpeed();
		mapScreen.getClock().setTimeSpeed(0);

		// show the dialogs
		show();
	}

	/**
	 * Add a new dialog
	 * 
	 * @param character
	 * @param message
	 * @param more,
	 *            follow more messages?
	 * @return
	 */
	public YTextDialog add(YCharacter character, String message) {

		// add welcome message?
		try {
			addWelcome(character);

			dialogs.add(new YTextDialog(character, message));
		} catch (Exception e) {
			YConfig.error(e, false);
		}

		return dialogs.get(dialogs.size() - 1);
	}

	/**
	 * Check if to add a welcome message
	 * 
	 * @param character
	 */
	private void addWelcome(YCharacter character) {
		if (!mapScreen.getData().existP("welcomescreen." + character.file)) {

			String welcome = "?";

			// TODO add player gender
			switch (character) {
			case TOWN:
				welcome = "Good day Mr. " + mapScreen.getPlayer().getName() + ",\n" + "Thank God we have not been introduced yet, my name is " + character.name
						+ ". \nSince my husband died under mysterious circumstances, I dedicate myself fully \nto the beautification and improvement of our city "
						+ mapScreen.getPlayer().getTown().getName()
						+ ". \nThe mayor has recently promoted me. Since then, I lead the cultural affairs for the city. \nUnfortunately, I am also responsible for you and your 'museum' "
						+ mapScreen.getPlayer().getMuseum().getName() + ".\n" + "I hope you will not cause me any problems.";
				break;
			case HELPER:
				welcome = "Hello boss,\n" + "I officially work as a gardener for the Museum " + mapScreen.getPlayer().getMuseum().getName()
						+ ". \nIf you know the cultural area, you know that sometimes it can be very dirty. My job is to keep you clean. \nIf you ever have problems, let me know and I will clear the tracks. I am looking forward to a great cooperation.";
				break;
			case FINANCE:
				welcome = "Good day " + mapScreen.getPlayer().getName() + ",\n" + character.name
						+ " here. I do not have time at the moment. I have to go over the books. \nBut if you have any questions about your finances, let me know.";
				break;
			}

			dialogs.add(new YTextDialog(character, welcome));
			mapScreen.getData().setP("welcomescreen." + character.file, "1");
		}
	}

	/**
	 * Show the dialogs
	 */
	public void show() {
		// get act dialog
		act++;
		YTextDialog d = dialogs.get(act);

		// hide old image?
		if (image != null) {
			// last the same?
			if (dialogs.get(act - 1).character != d.character) {
				hideImage();
			}
		}
		// show new image
		if (act == 0 || dialogs.get(act - 1).character != d.character) {
			// top
			top = new Image(YStatic.gameAssets.get("system/person/black.png", Texture.class));
			top.setScale(Gdx.graphics.getWidth() / 10f, (Gdx.graphics.getHeight() / 6f) / 10f);
			top.setY(Gdx.graphics.getHeight() - top.getHeight() * top.getScaleY());
			top.getColor().a = 0;
			top.addAction(Actions.fadeIn(1));
			mapScreen.getStage().addActor(top);

			// bottom
			bottom = new Image(YStatic.gameAssets.get("system/person/black.png", Texture.class));
			bottom.setScale(Gdx.graphics.getWidth() / 10f, (Gdx.graphics.getHeight() / 6f) / 10f);
			bottom.getColor().a = 0;
			bottom.addAction(Actions.fadeIn(1));
			mapScreen.getStage().addActor(bottom);

			// person
			image = new Image(YStatic.gameAssets.get("system/person/" + d.character.file + ".png", Texture.class));
			image.setScale(Gdx.graphics.getHeight() / image.getHeight());
			image.getColor().a = 0;
			image.setX(d.character.left ? 0 : Gdx.graphics.getWidth() - (image.getWidth() * image.getScaleX()));
			image.addAction(Actions.fadeIn(1));
			mapScreen.getStage().addActor(image);
		}

		// last screen?
		if (act + 1 == dialogs.size()) {
			Dialogs.showOKDialog(mapScreen.getStage(), d.character.name, d.message).addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					clear();
				}
			});
			return;
		}

		// TODO show normal dialog
		Dialogs.showOKDialog(mapScreen.getStage(), d.character.name, d.message).addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				show();
			}
		});

	}

	private void hideImage() {
		image.addAction(Actions.sequence(Actions.fadeOut(1), Actions.addListener(new EventListener() {

			@Override
			public boolean handle(Event event) {
				event.getListenerActor().remove();
				return true;
			}
		}, true, image)));

		top.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		bottom.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
	}

	/**
	 * Clear everything
	 */
	private void clear() {
		act = -1;
		dialogs.clear();
		hideImage();
		image = null;
		mapScreen.getClock().setTimeSpeed(oldTimeSpeed);
	}

	private class YTextDialog {
		YCharacter character;
		String message;

		/**
		 * @param character
		 * @param message
		 */
		public YTextDialog(YCharacter character, String message) {
			this.character = character;
			this.message = message;
		}
	}
}
