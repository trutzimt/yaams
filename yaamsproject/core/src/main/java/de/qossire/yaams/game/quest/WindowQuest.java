package de.qossire.yaams.game.quest;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YSplitTab;
import de.qossire.yaams.ui.YTabWindow;

public class WindowQuest extends YTabWindow {

	protected MapScreen map;

	public WindowQuest(final MapScreen map) {
		super("Tasks");
		this.map = map;

		if (map.getPlayer().getQuests().getQuests().size() != 1)
			tabbedPane.add(new OverviewTab());
		// add quests
		for (final BaseQuest quest : map.getPlayer().getQuests().getQuests()) {
			tabbedPane.add(new QuestTab(quest));
		}

		buildIt();

		addTitleIcon(YIcons.getIconI(YIType.TASK));
	}

	public class OverviewTab extends Tab {

		public OverviewTab() {
			super(false, false);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
		 */
		@Override
		public String getTabTitle() {
			return "Overview";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
		 */
		@Override
		public Table getContentTable() {

			VisTable content = new VisTable();

			// has quests?
			if (map.getPlayer().getQuests().getQuests().size() == 0) {
				content.add(new VisLabel("At the moment there are no quests."));
				return content;
			}

			// list every quest
			for (final BaseQuest quest : map.getPlayer().getQuests().getQuests()) {
				content.add(new VisLabel(quest.getTitle())).align(Align.left).growX().row();

			}

			return content;
		}
	}

	public class QuestTab extends YSplitTab {

		protected BaseQuest quest;

		public QuestTab(BaseQuest quest) {
			super(quest.getTitle(), "This Task has nothing to do at the moment.");

			// list every quest
			for (final BaseQuestItem item : quest.getVisibleItems()) {
				addElement(item.getTitle(), item.getIcon(), item);
			}

		}

		@Override
		protected Actor getInfoPanel(Button btn) {
			return ((BaseQuestItem) btn.getUserObject()).getInfoPanel();
		}

		@Override
		protected void doubleClickElement(Button btn) {}
	}
}
