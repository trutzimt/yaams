package de.qossire.yaams.game.quest.req;

import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqRankMaxCount extends BaseQuestReqMaxCount {

	private ERank rang;

	/**
	 * @param prop
	 * @param val
	 */
	public QuestReqRankMaxCount(ERank rang, int count) {
		super(count);
		this.rang = rang;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + rang.toString().toLowerCase() + " Rang";
	}

	@Override
	public int getActCount() {
		return MapScreen.get().getPlayer().getRang().getRank(rang).getLevel();
	}

}