/**
* 
*/
package de.qossire.yaams.game.art;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.action.BaseGameAction;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.map.MapTimeObject;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class GameActionPlaceArt extends BaseGameAction {

	protected BaseArt art;
	protected Image im;

	/**
	 * @param type
	 * @param title
	 */
	public GameActionPlaceArt(BaseArt art, MapScreen map) {
		super("build", "Place " + art.getName());
		this.art = art;

		im = new Image(art.getIcon(false));
		im.setColor(1f, 0.5f, 0.5f, 0.5f);

		map.getMap().setCursor(im);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.action.BaseGameAction#performClick(int, int,
	 * de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void performClick(final int x, final int y, final MapScreen map) {
		// map.getMap().setBuildTile(buildtile, x, y);

		if (art.checkField(x, y)) {
			YSounds.click();
			map.getInputs().setActiveAction(null);
			map.getData().setArtTile(128, x, y);
			art.setStatus(ArtStatus.DISPLAYED);
			art.setTimeAction(new ArtTimeAction("Place in the museum", art.getDecor() * 3) {

				@Override
				public boolean run() {
					return true;
				}
			});
			new MapTimeObject(x, y, new Image(art.getIcon(false)), art.getDecor() * 3) {

				@Override
				public void perform() {
					art.buildAt(x, y);

				}
			};

		} else {
			YSounds.buzzer();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.action.BaseGameAction#mouseMoved(int, int,
	 * de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void mouseMoved(int x, int y, MapScreen mapScreen) {
		im.setPosition(x * 32, y * 32);
		// can build?
		if (art.checkField(x, y)) {
			im.getColor().g = 1f;
			im.getColor().r = 0.5f;
			mapScreen.getMapgui().setInfo(art.getErrorMessage(x, y, mapScreen), art.getIcon(true));
		} else {
			im.getColor().r = 1f;
			im.getColor().g = 0.5f;
			// set info
			mapScreen.getMapgui().setInfo(art.getErrorMessage(x, y, mapScreen), YIcons.getIconD(YIType.WARNING), Color.SALMON);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.action.BaseGameAction#destroy(de.qossire.yaams.screens.
	 * map.MapScreen)
	 */
	@Override
	public void destroy(MapScreen mapScreen) {
		super.destroy(mapScreen);
		mapScreen.getMap().setCursor(null);
		mapScreen.getMapgui().setInfo(null, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.action.BaseGameAction#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return art.getIcon(true);
	}

}
