package de.qossire.yaams.game.action;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.persons.BasePerson;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.game.persons.customer.FieldCustomerTab;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YTabWindow;
import de.qossire.yaams.ui.YTable;

public class FieldWindow extends YTabWindow {

	public FieldWindow(final MapScreen map, final int x, final int y) {
		super("Info about Field " + x + "/" + y);

		// show person?
		BasePerson person = map.getMap().getPersonStage().getPersonMatrix(x, y);

		if (person != null && person instanceof Customer) {
			tabbedPane.add(new FieldCustomerTab((Customer) person));
		}

		// show art?
		final BaseArt art = map.getPlayer().getArtwork().getArtAt(x, y);

		if (art != null) {

			tabbedPane.add(new Tab(false, false) {

				@Override
				public String getTabTitle() {
					return art.getName();
				}

				@Override
				public Table getContentTable() {
					return art.getInfoPanel();
				}
			});
		}

		if (YSettings.isDebug())
			tabbedPane.add(new Tab(false, false) {

				@Override
				public String getTabTitle() {
					return "Field " + x + "/" + y;
				}

				@Override
				public Table getContentTable() {

					YTable t = new YTable();

					t.addL("Floor", map.getData().getFloorTile(x, y) + " ");
					t.addL("Build", map.getData().getBuildTile(x, y) + " ");
					t.addL("Art", map.getData().getArtTile(x, y) + " ");
					// add props
					for (MapProp prop : MapProp.values()) {
						t.addL(prop.toString().toLowerCase(), map.getData().getProps(prop, x, y) + " ");
					}
					if (map.getMap().getPersonStage().getPersonMatrix(x, y) != null)
						t.addL("Person", map.getMap().getPersonStage().getPersonMatrix(x, y) + " ");

					return t.createScrollPaneInTable();

				}
			});

		setResizable(true);
		buildIt();

	}
}
