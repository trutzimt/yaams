/**
 * 
 */
package de.qossire.yaams.game.museum.funding;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YProgressBar;
import de.qossire.yaams.ui.YSplitTab;
import de.qossire.yaams.ui.YTextButton;

/**
 * @author sven
 *
 */
public class FundingTab extends YSplitTab {

	private YTextButton enable;
	private YProgressBar max;

	/**
	 * @param title
	 * @param error
	 */
	public FundingTab() {
		super("Fundings", "?");

		FundingsMgmt fm = MapScreen.get().getPlayer().getFundings();

		int a = 0;
		// add every programm
		for (Fundings f : fm.get()) {
			addElement(f.getTitle(), f.getIcon(), f);
			if (f.isActive())
				a++;
		}

		enable = new YTextButton("Enable Funding") {

			@Override
			public void perform() {
				endisable();

			}
		};
		enable.setDisabled(true);
		buttonBar.addActor(enable);

		max = new YProgressBar(a, fm.getMaxActive(), "Programs: " + a + "/" + fm.getMaxActive());
		buttonBar.addActor(max);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#doubleClickElement(com.badlogic.gdx.scenes.
	 * scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		endisable();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#clickElement(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected void clickElement(Button btn) {
		updateButtonText();
	}

	private void updateButtonText() {
		Fundings f = ((Fundings) active);
		enable.setDisabled(false);
		if (f.isActive()) {
			enable.setText("Disable Funding");
		} else if (max.getProgress().getValue() < max.getProgress().getMaxValue() && f.checkReq()) {
			enable.setText("Enable Funding");
		} else {
			enable.setDisabled(true);
			enable.setText("Can not enable Funding");
		}
	}

	/**
	 * En or disable it
	 */
	private void endisable() {
		Fundings f = ((Fundings) active);
		if (f.isActive()) {
			f.setActive(false);
			max.getProgress().setValue(max.getProgress().getValue() - 1);
			max.getLabel().setText("Programs: " + max.getProgress().getValue() + "/" + max.getProgress().getMaxValue());
			YSounds.click();
		} else if (max.getProgress().getValue() < max.getProgress().getMaxValue() && f.checkReq()) {
			f.setActive(true);
			max.getProgress().setValue(max.getProgress().getValue() + 1);
			max.getLabel().setText("Programs: " + max.getProgress().getValue() + "/" + max.getProgress().getMaxValue());
			f.start();
			YSounds.click();
		} else {
			YSounds.buzzer();
		}
		updateButtonText();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(Button btn) {
		return ((Fundings) btn.getUserObject()).getInfoPanel();
	}

}
