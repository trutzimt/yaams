/**
 *
 */
package de.qossire.yaams.saveload;

import java.util.HashMap;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.level.ScenarioManagement;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class YSaveInfo {

	private HashMap<String, String> settings;
	private double version;
	private boolean hidden;
	private transient FileHandle file;

	/**
	 * @return the version
	 */
	public double getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(double version) {
		this.version = version;
	}

	/**
	 * @return the settings
	 */
	public HashMap<String, String> getSettings() {
		return settings;
	}

	/**
	 * @param settings
	 *            the settings to set
	 */
	public void setSettings(HashMap<String, String> settings) {
		this.settings = settings;
	}

	/**
	 * @return the hidden
	 */
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * Get a description
	 *
	 * @return
	 */
	public String getDesc() {
		String c = settings.get("campaign");
		String s = settings.get("scenario");

		String conf = ScenarioManagement.existCampaign(c) && ScenarioManagement.getCampaign(c).existScenario(s)
				? ScenarioManagement.getCampaign(c).getScenario(s).getTitle()
				: "?";

		return "files.format.desc" + conf;
	}

	/**
	 * @param hidden
	 *            the hidden to set
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	public VisTable getInfoPanel(IScreen screen) {

		YTable table = new YTable();
		if (file.nameWithoutExtension().length() > 4)
			table.addL("File", getTitle(screen));
		table.addL("Museum", settings.get("museum"));
		table.addL("Time", settings.get("time") + ("1".equals(settings.get("time")) ? " Day" : " Days"));

		String c = settings.get("campaign");
		String s = settings.get("scenario");
		if (ScenarioManagement.existCampaign(c)) {
			table.addL("Campaign", ScenarioManagement.getCampaign(c).getTitle());
			if (ScenarioManagement.getCampaign(c).existScenario(s))
				table.addL("Scenario", ScenarioManagement.getCampaign(c).getScenario(s).getTitle());
		} else {
			table.addL("Game", s);
		}
		if (file != null)
			table.addL("Saved", screen.getYaams().getPlattform().formatDate(file.lastModified()));

		if (YSettings.isDebug()) {
			table.addL("Debug Path", file.path());
			if (Gdx.app.getType() == ApplicationType.Desktop)
				table.addL("Debug Absolut Path", file.file().getAbsolutePath());
		}

		return table;
	}

	/**
	 * @return the file
	 */
	public FileHandle getFile() {
		return file;
	}

	/**
	 * @return the file
	 */
	public String getTitle(IScreen screen) {
		return (file.nameWithoutExtension().length() < 4 ? screen.getYaams().getPlattform().formatDate(file.lastModified()) : file.nameWithoutExtension());
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(FileHandle file) {
		this.file = file;
	}

}
