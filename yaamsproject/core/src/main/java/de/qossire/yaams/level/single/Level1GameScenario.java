/**
 *
 */
package de.qossire.yaams.level.single;

import java.util.HashMap;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.game.quest.BaseQuest;
import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.QuestActionWin;
import de.qossire.yaams.game.quest.req.QuestReqRankMinCount;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class Level1GameScenario extends BaseScenario {

	protected HashMap<String, String> settings;

	/**
	 */
	public Level1GameScenario() {
		super("level1", "1 Level", "map/40.tmx");

		// set the settings
		desc = "Build a small museum and reach a overall rang of 1.";

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void start(final MapScreen screen) {
		super.start(screen);

		for (int i = 0, l = NamesGenerator.getIntBetween(2, 6); i < l; i++) {
			screen.getPlayer().getArtwork().addArt(screen.getPlayer().getArtwork().generateArt(), ArtStatus.DEPOT);
		}

		// start money
		screen.getPlayer().addMoney(50_000);

		// add quests
		BaseQuest b = new BaseQuest(getTitle());
		BaseQuestItem i = new BaseQuestItem();
		i.setDesc(desc);
		i.addReq(new QuestReqRankMinCount(ERank.OVERVIEW, 1));
		i.addAction(new QuestActionWin());
		b.addItem(i);
		screen.getPlayer().getQuests().addQuest(b);

	}

}
