/**
 * 
 */
package de.qossire.yaams.game.rooms;

import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.game.rooms.RoomMgmt.RoomTyp;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class RoomToilet extends BaseRoom {

	public static final int MAN = 198, WOMEN = 199;

	private int genderTile;

	/**
	 * @param iD
	 */
	public RoomToilet(int uid, int x, int y) {
		super(RoomTyp.TOILET, uid, x, y);
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	@Override
	public VisTable getInfoPanel() {
		YTable table = (YTable) super.getInfoPanel();

		table.addL("Floor", hasFloor ? "+ everywhere with a floor" : "- floor is missing");
		table.addL("Wall", hasWall ? "+ completely surrounded" : "- not completely surrounded");
		table.addL("Typ", genderTile > 0 ? "+ only one gender" : "- gender mixed");

		table.addL("Speed bonus", "Customers " + bonus + "% faster .");

		return table;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.rooms.BaseRoom#getIcon()
	 */
	@Override
	public TextureRegionDrawable getIcon() {
		return YIcons.getOverlayIcon(37, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.rooms.BaseRoom#updateLogic()
	 */
	@Override
	public void updateLogic() {
		// check it
		if (!toCheck) {
			return;
		}

		genderTile = 0;

		super.updateLogic();

		// calc
		bonus = hasFloor ? 20 : -20;
		bonus += hasFloor ? 20 : -20;
		bonus += genderTile > 0 ? 50 : -50;
	}

	/**
	 * Look if everything exist
	 * 
	 * @param x
	 * @param y
	 * @param fields
	 */
	@Override
	protected void checkTile(int x, int y, boolean[][] fields) {
		// was visit?
		if (!MapScreen.get().getTilesetHelper().isValidePosition(x, y) || fields[x][y]) {
			return;
		}
		super.checkTile(x, y, fields);

		// has a toilet?
		int id = MapScreen.get().getData().getBuildTile(x, y);
		if ((id == MAN || id == WOMEN))
			if (genderTile == 0) {
				genderTile = id;
			} else if (genderTile != id) {
				genderTile = -1;
			}
	}

}
