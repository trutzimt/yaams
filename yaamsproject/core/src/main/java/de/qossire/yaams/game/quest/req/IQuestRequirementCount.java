package de.qossire.yaams.game.quest.req;

public interface IQuestRequirementCount extends IQuestRequirement {

	/**
	 * Return the act number of items
	 * 
	 * @return
	 */
	public int getActCount();

	/**
	 * Return the max number of items
	 * 
	 * @return
	 */
	public int getMaxCount();
}
