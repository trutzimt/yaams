/**
 * 
 */
package de.qossire.yaams.game.museum.rank;

import java.util.ArrayList;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;

/**
 * @author sven
 *
 */
public class RankMgmt {

	public enum ERank {
		OVERVIEW, ART, SAFETY, USABILITY, JOY
	}

	private ArrayList<BaseRank> rank;

	/**
	 * 
	 */
	public RankMgmt() {
		rank = new ArrayList<>();

		rank.add(new RankOverview());
		rank.add(new RankArt());
		rank.add(new RankSafety());
		rank.add(new RankUsability());
		rank.add(new RankJoy());

		// save it
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_SAVE, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				for (int i = 0; i < rank.size(); i++) {
					MapScreen.get().getData().setP("rank_" + i, rank.get(i).getLevel());
				}

				return false;
			}
		});

		// load it
		MapScreen.get().getEvents().register(MapEventHandlerTyp.FILE_LOAD, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				for (int i = 0; i < rank.size(); i++) {
					rank.get(i).setLevel(MapScreen.get().getData().getPI("rank_" + i));
				}
				return false;
			}
		});

		// show next day
		MapScreen.get().getEvents().register(MapEventHandlerTyp.NEXTDAY, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				for (BaseRank r : rank) {
					r.nextDay();
				}
				return false;
			}
		});
	}

	/**
	 * @return the rang
	 */
	public ArrayList<BaseRank> getRanks() {
		return rank;
	}

	/**
	 * Return the rang or null
	 * 
	 * @param er
	 * @return
	 */
	public BaseRank getRank(ERank er) {
		for (BaseRank r : rank) {
			if (r.getTyp() == er) {
				return r;
			}
		}
		return null;
	}

}
