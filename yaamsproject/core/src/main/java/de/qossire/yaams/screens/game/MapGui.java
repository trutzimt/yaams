/**
 *
 */
package de.qossire.yaams.screens.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisWindow;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.game.action.BaseGameAction;
import de.qossire.yaams.game.action.GameActionInfo;
import de.qossire.yaams.game.art.window.ArtworkWindow;
import de.qossire.yaams.game.build.BuildWindow;
import de.qossire.yaams.game.build.DestroyGameAction;
import de.qossire.yaams.game.museum.MuseumWindow;
import de.qossire.yaams.game.persons.PersonsWindow;
import de.qossire.yaams.game.quest.WindowQuest;
import de.qossire.yaams.game.rooms.RoomWindow;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.window.MainGameMenuWindow;
import de.qossire.yaams.ui.TextHelper;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YGameImageButton;
import de.qossire.yaams.ui.YGameTextButton;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YNotification;
import de.qossire.yaams.ui.YTextDialogMgmt;

/**
 * @author sven
 *
 */
public class MapGui {

	private Stage stage;
	private MapScreen mapScreen;
	private YTextDialogMgmt dialogs;

	// gui elements

	// buttons
	private VisImageTextButton btnActionCancel;
	private VisLabel lblTime, lblMoney, lblInfo;
	private Image imgInfo, imgBottomBack;

	/**
	 * @param mapScreen
	 */
	public MapGui(MapScreen mapScreen) {
		this.mapScreen = mapScreen;
		stage = new Stage();
		dialogs = new YTextDialogMgmt(mapScreen);

		createGui();
	}

	/**
	 * Show a notification
	 * 
	 * @param note
	 */
	public void addNotification(YNotification note) {
		int y = 42;
		// find next pos
		for (Actor a : stage.getActors()) {
			if (a instanceof YNotification) {
				y += 10 + a.getHeight();
			}
		}

		note.setY(Gdx.graphics.getHeight() - y - note.getHeight());
		stage.addActor(note);

	}

	/**
	 * Check the notificationPos and move then
	 */
	public void recalcNotificationPos(YNotification origin) {
		int f = YSettings.isBiggerGui() ? 2 : 1;
		int y = 42 * f;
		// find next pos
		for (Actor a : stage.getActors()) {
			if (a instanceof YNotification && a != origin) {
				a.addAction(Actions.moveTo(10 * f, Gdx.graphics.getHeight() - y - a.getHeight(), 1));
				y += 10 * f + a.getHeight();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
	}

	/**
	 * Create the Game Gui
	 */
	private void createGui() {
		createBars();

		Table root = new Table();

		// build top
		Table top = new Table();
		// top.setFillParent(true);
		top.add(createTimeMenu()).align(Align.topLeft);

		if (mapScreen.getSettings().isActive(ScenConf.MONEY)) {
			top.add(createMoneyLabel()).align(Align.top).expandX();
		} else {
			top.add().expandX();
		}
		top.add(createMainButtons()).align(Align.topRight);

		// build button
		Table bottom = new Table();
		// bottom.setFillParent(true);
		// info button
		imgInfo = new Image();
		imgInfo.setVisible(false);
		lblInfo = new VisLabel("?");
		lblInfo.setVisible(false);
		bottom.add(imgInfo).align(Align.bottomLeft);
		bottom.add(lblInfo).align(Align.bottomLeft).growX();

		// cancel button
		btnActionCancel = new VisImageTextButton("?", YIcons.getBuildIcon(0, true));
		btnActionCancel.addCaptureListener(new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				mapScreen.getInputs().setActiveAction(null);

			}
		});
		btnActionCancel.setVisible(false);
		bottom.add(btnActionCancel).align(Align.bottomRight);

		// build
		root.add(top).align(Align.top).growX().row();
		root.add().expand().row();
		root.add(bottom).growX();
		root.setFillParent(true);
		stage.addActor(root);

	}

	private void createBars() {
		float b = YSettings.getGuiScale();
		String e = YSettings.getGuiScaleFolder();

		// build base top
		Image top = new Image(YStatic.gameAssets.get(e + "/icons/verlauf.png", Texture.class));
		top.setScale((Gdx.graphics.getWidth() + 1) / b, 1);
		top.setY(Gdx.graphics.getHeight() - b);
		stage.addActor(top);

		// build base bottom
		imgBottomBack = new Image(YStatic.gameAssets.get(e + "/icons/verlaufB.png", Texture.class));
		imgBottomBack.setScale((Gdx.graphics.getWidth() + 1) / b, 1);
		imgBottomBack.setVisible(false);
		stage.addActor(imgBottomBack);
	}

	private Table createMainButtons() {
		VisTable gamemenu = new VisTable();

		// add the buttons
		gamemenu.add(new YGameImageButton("Museum overview", YIType.MUSEUM, Keys.M) {

			@Override
			public void pressAction() {
				if (CheckToOpenWindow(MuseumWindow.class)) {
					stage.addActor(new MuseumWindow(mapScreen));
				}
			}
		});

		// add the buttons
		gamemenu.add(new YGameImageButton("Artworks", YIType.ARTWORK, Keys.C) {

			@Override
			public void pressAction() {
				if (CheckToOpenWindow(ArtworkWindow.class)) {
					stage.addActor(new ArtworkWindow());
				}
			}
		});

		// add the buttons
		if (mapScreen.getSettings().isActive(ScenConf.ROOM))
			gamemenu.add(new YGameImageButton("Rooms", YIType.ROOM, Keys.R) {

				@Override
				public void pressAction() {
					if (CheckToOpenWindow(RoomWindow.class)) {
						stage.addActor(new RoomWindow());
					}
				}
			});

		// add the buttons
		gamemenu.add(new YGameImageButton("Persons", YIType.PERSONS, Keys.P) {

			@Override
			public void pressAction() {
				if (CheckToOpenWindow(PersonsWindow.class)) {
					stage.addActor(new PersonsWindow());
				}
			}
		});

		// add the buttons
		gamemenu.add(new YGameImageButton("Build", YIType.BUILD, Keys.B) {

			@Override
			public void pressAction() {
				if (CheckToOpenWindow(BuildWindow.class)) {
					stage.addActor(new BuildWindow(mapScreen));
				}
			}
		});

		// add the buttons
		gamemenu.add(new YGameImageButton("Tasks", YIType.TASK, Keys.T) {

			@Override
			public void pressAction() {
				if (CheckToOpenWindow(WindowQuest.class)) {
					stage.addActor(new WindowQuest(mapScreen));
				}
			}
		});

		// add the buttons
		gamemenu.add(new YGameImageButton("Info about ...", YIType.INFOABOUT, Keys.I) {

			@Override
			public void pressAction() {
				mapScreen.getInputs().setActiveAction(new GameActionInfo());
				YSounds.click();
			}
		});

		// add the buttons
		gamemenu.add(new YGameImageButton("Exterminate/Destroy", YIType.DESTROY, Keys.E) {

			@Override
			public void pressAction() {
				if (mapScreen.getInputs().getActiveAction() instanceof DestroyGameAction) {
					mapScreen.getInputs().setActiveAction(null);
					YSounds.cancel();
				} else {
					mapScreen.getInputs().setActiveAction(new DestroyGameAction());
					YSounds.click();
				}
			}
		});

		// add the buttons
		gamemenu.add(new YGameImageButton("Main Menu", YIType.MAINMENU, Keys.ESCAPE) {

			@Override
			public void pressAction() {
				if (CheckToOpenWindow(MainGameMenuWindow.class)) {
					openMainMenu();
				}
			}
		});

		return gamemenu;
	}

	private Table createMoneyLabel() {
		// create time
		Table money = new Table();
		money.add(YIcons.getIconI(YIType.MONEY));
		lblMoney = new VisLabel();
		refreshMoney();
		money.add(lblMoney);
		return money;
	}

	/**
	 * Build the time
	 * 
	 * @return
	 */
	private Table createTimeMenu() {

		// create time
		Table time = new Table();

		lblTime = new VisLabel();

		final YGameTextButton speed = new YGameTextButton("Speed up", "  x1", Keys.COMMA) {

			@Override
			public void pressAction() {
				if (mapScreen.getClock().getTimeSpeed() == 0) {
					YSounds.buzzer();
				} else {
					YSounds.click();
					mapScreen.getClock().setTimeSpeed(mapScreen.getClock().getTimeSpeed() == 5 ? 1 : (mapScreen.getClock().getTimeSpeed() + 1) % 6);
					setText("  x" + mapScreen.getClock().getTimeSpeed());
				}

			}
		};
		speed.setVisible(false);

		YGameImageButton y = new YGameImageButton("Play/Stop", YIType.PAUSE, Keys.SPACE) {

			@Override
			public void pressAction() {
				YSounds.click();
				// check it
				if (mapScreen.getClock().getTimeSpeed() == 0) {
					mapScreen.getClock().setTimeSpeed(1);
					setDrawable(YIcons.getIconD(YIType.PLAY));
					lblTime.setColor(Color.WHITE);
					speed.setVisible(true);
					speed.setText("  x1");

					// museum is closed?
					if (!mapScreen.getPlayer().getMuseum().isOpen()) {
						// something under construction?
						for (Actor p : mapScreen.getMap().getStage().getActors()) {
							// TODO find better way
							if (p instanceof VisLabel) {
								return;
							}
						}
						addNotification(
								new YNotification("Museum", "The museum is closed. There will be no visitors. (You can open it in the museum administration.)",
										YIcons.getIconI(YIType.MUSEUM)));

					}
				} else {
					mapScreen.getClock().setTimeSpeed(0);
					setDrawable(YIcons.getIconD(YIType.PAUSE));
					speed.setVisible(false);
					lblTime.setColor(Color.GRAY);
				}
			}
		};

		time.add(y);
		time.add(lblTime);
		time.add(speed);
		return time;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	public void render(float delta) {
		// update time?
		if (mapScreen.getClock().getTimeSpeed() > 0) {
			lblTime.setText(mapScreen.getClock().getShortTimeLabel());
		}

		dialogs.checkToShow();

		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}

	/**
	 * Checks if the selected window is open
	 *
	 * @param clas
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private boolean WindowIsOpen(Class clas) {
		for (Actor actor : stage.getActors()) {
			if (actor.getClass().equals(clas)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Checks if the selected window is open
	 *
	 * @param clas
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private boolean CheckToOpenWindow(Class clas) {
		if (!WindowIsOpen(clas)) {
			YSounds.click();
			return true;
		} else {
			YSounds.cancel();
			closeWindow(clas);
			return false;
		}

	}

	/**
	 * Close the speficic window
	 *
	 * @param clas
	 */
	@SuppressWarnings("rawtypes")
	private void closeWindow(Class clas) {
		for (Actor actor : stage.getActors()) {
			if (actor.getClass().equals(clas)) {
				actor.remove();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	public void dispose() {
		stage.dispose();
	}

	/**
	 * Checks if any window is open
	 *
	 * @param clas
	 * @return
	 */
	public boolean AnyWindowIsOpen() {
		for (Actor actor : stage.getActors()) {
			if (actor instanceof VisWindow) {
				return true;
			}
		}
		return false;

	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * Helpermethod to refresh the view
	 */
	public void refreshMoney() {
		if (lblMoney == null)
			return;

		// TODO allow options
		lblMoney.setText(TextHelper.getMoneyString((int) mapScreen.getPlayer().getMoney()));
	}

	/**
	 * Show the button to cancel the action
	 * 
	 * @param baseAction
	 */
	public void setActiveAction(BaseGameAction baseAction) {
		if (baseAction == null) {
			btnActionCancel.setVisible(false);
			return;
		}

		btnActionCancel.setText("Cancel '" + baseAction.getTitle() + "'");
		btnActionCancel.getStyle().imageUp = baseAction.getIcon();
		btnActionCancel.getStyle().imageDown = baseAction.getIcon();
		btnActionCancel.setVisible(true);
	}

	/**
	 * Show the info for some thing
	 * 
	 * @param baseAction
	 */
	public void setInfo(String text, Drawable ico) {
		setInfo(text, ico, null);
	}

	/**
	 * Show the info for some thing
	 * 
	 * @param baseAction
	 */
	public void setInfo(String text, Drawable ico, Color c) {
		if (text == null) {
			lblInfo.setVisible(false);
			imgInfo.setVisible(false);
			imgBottomBack.setVisible(false);
			return;
		}

		lblInfo.setText(text);
		lblInfo.setVisible(true);
		lblInfo.setColor(c != null ? c : Color.WHITE);

		imgInfo.setDrawable(ico);
		imgInfo.setVisible(true);
		imgInfo.setColor(c != null ? c : Color.WHITE);

		imgBottomBack.setVisible(true);
	}

	/**
	 * @return the dialogs
	 */
	public YTextDialogMgmt getDialogs() {
		return dialogs;
	}

	public void openMainMenu() {
		stage.addActor(new MainGameMenuWindow(mapScreen.getYaams(), stage, mapScreen));
	}

	/**
	 * Show the action menu for the selected point
	 *
	 * @param x
	 * @param y
	 */
	// public void showActionMenuAt(int x, int y) {
	// mapScreen.removeActiveAction();
	//
	// actionMenu.clearChildren();
	// // move map
	// mapScreen.getMap().setCenterMapView(x, y);
	//
	// // is not explored?
	// if (!mapScreen.getMap().isFogTile(x, y)) {
	//
	// // can show a unit menu?
	// ActiveUnit unit = getUnitAt(x, y);
	// if (unit != null && unit.getPlayer() == mapScreen.getActivePlayer()) {
	// actionMenu.add(unit.getMenuButton(stage)).fillX();
	// // add actions
	// if (unit.isFinish() && unit.getBase().getActions().size() > 0) {
	// for (String type : unit.getBase().getActions()) {
	// if (BaseBaseManager.getAction(type).canShowMenuButton(mapScreen, unit)) {
	// actionMenu.add(BaseBaseManager.getAction(type).getMenuButton(mapScreen,
	// unit));
	// }
	// }
	// }
	//
	// // perform default action
	// // ((MoveAction)
	// BaseBaseManager.getAction("move")).pressMenuButton(mapScreen,
	// // unit);
	// mapScreen.getActivePlayer().setActiveUnit(unit);
	// }
	//
	// // can show a build menu?
	// ActiveBuilding build = getBuildingAt(x, y);
	// if (build != null && build.getOwner().getOwner() ==
	// mapScreen.getActivePlayer()) {
	// actionMenu.add(build.getMenuButton(stage)).fillX();
	// // add actions
	// if (build.isFinish() && build.getBase().getActions().size() > 0) {
	// for (String type : build.getBase().getActions()) {
	// if (BaseBaseManager.getAction(type).canShowMenuButton(mapScreen, build)) {
	// actionMenu.add(BaseBaseManager.getAction(type).getMenuButton(mapScreen,
	// build)).fillY();
	// }
	// }
	// }
	// }
	// }
	// if (actionMenu.getChildren().size > 0) {
	// actionMenu.setVisible(true);
	// actionMenu.setBackground(QUiHelper.getGameMenuBackground(false,
	// actionMenu.getChildren().size <= 5));
	// } else {
	// actionMenu.setVisible(false);
	// }
	//
	// }
}
