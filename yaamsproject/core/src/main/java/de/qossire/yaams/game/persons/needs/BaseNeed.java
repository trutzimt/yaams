/**
 * 
 */
package de.qossire.yaams.game.persons.needs;

import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public abstract class BaseNeed {

	public static final int MAX = 1000, MIN = -1000, SET_MAX = 2000, SET_MIN = -2000;
	/**
	 * How important is this need? 1=yes, 9=none
	 */
	protected int impLevel;

	protected PersonNeeds id;

	/**
	 * 
	 */
	public BaseNeed(PersonNeeds id, int impLevel) {
		this.impLevel = impLevel;
		this.id = id;
	}

	/**
	 * Update the logic
	 * 
	 * @param customer
	 * @param map
	 * @param deltaTime
	 * @return true=important to fix?
	 */
	public abstract boolean updateLogic(Customer customer, MapScreen map, int deltaTime);

	/**
	 * @return the impLevel
	 */
	public int getImpLevel() {
		return impLevel;
	}

	/**
	 * Try to fix this need
	 * 
	 * @param customer
	 * @param map
	 */
	public abstract void tryToFix(Customer customer, MapScreen map);

	/**
	 * @return the id
	 */
	public PersonNeeds getId() {
		return id;
	}
}
