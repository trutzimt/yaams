/**
 * 
 */
package de.qossire.yaams.game.persons.needs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.persons.PersonManagement.PersonAnimationNeeds;
import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YNotification;

/**
 * @author sven
 *
 */
public class NeedLeave extends BaseNeed {

	public NeedLeave() {
		super(PersonNeeds.LEAVE, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#updateLogic(de.qossire.yaams.
	 * game.persons.Customer, de.qossire.yaams.screens.map.MapScreen, int)
	 */
	@Override
	public boolean updateLogic(Customer customer, MapScreen mapScreen, int deltaTime) {
		// want leaving?
		if (customer.getNeed(id) >= MAX && mapScreen.getData().getProps(MapProp.STREET, customer.getGameX(), customer.getGameY()) >= 1) {
			customer.removeWithAnimation();
			// TODO add animation
		}

		// museum close? Will leave
		if (mapScreen.getPlayer().getMuseum().isOpen() && mapScreen.getClock().getHour() >= mapScreen.getPlayer().getMuseum().getOpenTo() - 1) {
			customer.addNeed(id, SET_MAX);
		}

		// want to leave?
		return (customer.getNeed(id) >= MAX);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#tryToFix(de.qossire.yaams.game.
	 * persons.Customer, de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void tryToFix(Customer customer, MapScreen map) {

		Gdx.app.debug("Customer", "customer will leave");
		// find nearst exit
		if (!map.getMap().goToNearestTileFrom(customer, MapProp.STREET, 1)) {
			// noti user
			map.getMapgui()
					.addNotification(new YNotification("Leaving",
							customer.getPersonName() + " can not find a street to exit. " + customer.getPersonNameProp() + " will die.",
							new Image(customer.getActor().getPreviewRegion())));

			// remove all joy
			customer.addNeed(PersonNeeds.JOY, SET_MIN);
			MapScreen.get().getMap().getPersonStage().getMgmt().showNeed(customer, PersonAnimationNeeds.EXCLAMATION);

			// die
			customer.remove();

		}

	}

}
