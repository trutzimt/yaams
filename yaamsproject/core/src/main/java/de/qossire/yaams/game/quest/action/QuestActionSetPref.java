/**
 * 
 */
package de.qossire.yaams.game.quest.action;

import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class QuestActionSetPref extends BaseQuestAction {

	private String key;
	private Object value;

	/**
	 * @param prop
	 * @param val
	 */
	public QuestActionSetPref(String key, Object value, String title) {
		super(title);
		this.key = key;
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.action.BaseQuestAction#perform(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public void perform() {
		MapScreen.get().getData().setP(key, value);

	}

}
