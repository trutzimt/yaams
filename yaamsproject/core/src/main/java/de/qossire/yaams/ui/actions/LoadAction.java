/**
 *
 */
package de.qossire.yaams.ui.actions;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.saveload.SaveLoadWindow;
import de.qossire.yaams.ui.YChangeListener;

/**
 * @author sven
 *
 */
public class LoadAction extends QDefaultAction {

	public LoadAction() {
		super("Load");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.ui.actions.QDefaultAction#getAction()
	 */
	@Override
	public YChangeListener getAction(final Yaams yaams) {
		return new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				YSounds.click();
				Stage stage = ((IScreen) yaams.getScreen()).getStage();
				stage.addActor(new SaveLoadWindow((IScreen) yaams.getScreen(), true, false));

			}
		};
	}

}