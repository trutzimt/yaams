package de.qossire.yaams.screens.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.game.build.BuildManagement;
import de.qossire.yaams.level.BaseScenario;
import de.qossire.yaams.screens.base.BaseLoaderScreen;
import de.qossire.yaams.ui.YIcons;

public class GameLoaderScreen extends BaseLoaderScreen {

	protected BaseScenario scenario;

	/**
	 * Load everything for the game
	 *
	 * @param qossire
	 * @param scenario
	 */
	public GameLoaderScreen(Yaams yaams, BaseScenario scenario) {
		super(yaams, "Loading " + (scenario == null ? "savegame" : scenario.getTitle()) + " ");

		this.scenario = scenario;

		createLabel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		super.render(delta);

		text.setText(title + ((int) (YStatic.gameAssets.getProgress() * 100)) + "%");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.menu.BaseLoaderScreen#nextScreen()
	 */
	@Override
	public void nextScreen() {
		yaams.switchScreen(new MapScreen(yaams, scenario));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.menu.BaseLoaderScreen#onceCode()
	 */
	@Override
	public void onceCode() {
		YStatic.gameAssets.clear();

		String e = YSettings.getGuiScaleFolder();

		YStatic.buildings = new BuildManagement();

		YStatic.gameAssets.load(e + "/tileset/tileset.png", Texture.class);
		YStatic.gameAssets.load(e + "/tileset/art.png", Texture.class);
		YStatic.gameAssets.load(e + "/tileset/overlay.png", Texture.class);

		//load system tileset?
        if (!"system".equals(e)) {
            YStatic.gameAssets.load("system/tileset/tileset.png", Texture.class);
            YStatic.gameAssets.load("system/tileset/art.png", Texture.class);
            YStatic.gameAssets.load("system/tileset/overlay.png", Texture.class);
        }

		YStatic.gameAssets.load("system/person/finance.png", Texture.class);
		YStatic.gameAssets.load("system/person/helper.png", Texture.class);
		YStatic.gameAssets.load("system/person/town.png", Texture.class);
		YStatic.gameAssets.load("system/person/black.png", Texture.class);

		// add emtions
		for (String s : new String[] { "orb-direction", "verlauf", "verlaufB", "hazard-sign", "sunrise" }) {
			YStatic.gameAssets.load(e + "/icons/" + s + ".png", Texture.class);
		}

		// add icons
		for (int i = 0; i < 11; i++) {
			YStatic.gameAssets.load("system/emotion/" + i + ".png", Texture.class);
		}
		YIcons.loadIcons();

		// add character
		for (int i = 0; i < (YSettings.isDebug() ? 5 : 136); i++) {
			YStatic.gameAssets.load("system/character/" + i + ".png", Texture.class);
		}

		Gdx.app.debug("Game Loader", title);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.base.BaseLoaderScreen#isFinishLoading()
	 */
	@Override
	public boolean isFinishLoading() {
		return YStatic.gameAssets.update() && YStatic.systemAssets.update();
	}

}
