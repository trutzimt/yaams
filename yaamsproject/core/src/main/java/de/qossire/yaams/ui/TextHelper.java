/**
 * 
 */
package de.qossire.yaams.ui;

import java.text.NumberFormat;

/**
 * @author sven
 *
 */
public class TextHelper {

	/**
	 * Format the value to money
	 * 
	 * @param value
	 * @return
	 */
	public static String getMoneyString(int value) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		String val = currencyFormatter.format(value);
		return val.substring(0, val.length() - 3);
	}

	/**
	 * Format the age and add bc, if negative
	 * 
	 * @param age
	 * @return
	 */
	public static String getYearString(int age) {
		return age < 0 ? age + " b.c." : "a.d. " + age;
	}

	/**
	 * Get the time stamp
	 * 
	 * @param l
	 * @return
	 */
	public static String getLongTimeString(long l) {
		long d = l / 1440;
		long h = (l / 60) % 24;
		long m = l % 60;

		String erg = "";
		if (d > 0)
			erg += d + " day" + (d == 1 ? "" : "s");
		if (h > 0)
			erg += (erg.length() > 1 ? ", " : "") + h + " hour" + (h == 1 ? "" : "s");
		if (m > 0)
			erg += (erg.length() > 1 ? ", " : "") + m + " minute" + (m == 1 ? "" : "s");
		if (erg.length() == 0)
			erg += "0 min";
		return erg;
	}

	/**
	 * Get the time stamp
	 * 
	 * @param l
	 * @return
	 */
	public static String getShortTimeString(long l) {
		long h = (l / 60) % 24;
		long m = l % 60;

		return String.format("%02d:%02d", h, m);
	}

	/**
	 * Get the time stamp
	 * 
	 * @param l
	 * @return
	 */
	public static String getCommaSep(String... l) {
		String erg = "";
		for (String s : l) {
			if (s == null || s.length() == 0) {
				continue;
			}

			if (erg.length() > 0) {
				erg += ", ";
			}
			erg += s;
		}

		return erg;
	}
}
