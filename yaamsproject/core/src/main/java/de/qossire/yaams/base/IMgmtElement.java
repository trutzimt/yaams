/**
 * 
 */
package de.qossire.yaams.base;

/**
 * @author sven
 *
 */
public interface IMgmtElement {

	/**
	 * Build it on the map
	 * 
	 * @param x
	 * @param y
	 */
	public void buildAt(int x, int y);

	/**
	 * Remove it from the map
	 * 
	 * @param x
	 * @param y
	 */
	public void removeAt(int x, int y);
}
