/**
 * 
 */
package de.qossire.yaams.screens.game;

import java.util.HashMap;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector3;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.game.action.BaseGameAction;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.ui.YChangeListener;

/**
 * @author sven
 *
 */
public class MapInputAdapter extends InputAdapter {

	private MapScreen mapScreen;

	// managent clicks
	private BaseGameAction activeAction;
	private HashMap<Integer, YChangeListener> keyBindings;

	// important for map scrolling
	private int oldPosX, oldPosY;

	// important for action move
	private int oldX, oldY;

	/**
	 * @param mapScreen
	 * 
	 */
	public MapInputAdapter(MapScreen mapScreen) {
		this.mapScreen = mapScreen;

		// TODO plattform code
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			keyBindings = new HashMap<>();
		}
	}

	/**
	 * Convert Screen X to Map X
	 *
	 * @param x
	 */
	private int screenXToMapX(int x) {
		Vector3 touch = new Vector3(x, 0, 0);
		mapScreen.getMap().getCamera().unproject(touch);
		return (int) touch.x / 32;

	}

	/**
	 * Convert Screen Y to Map Y
	 *
	 * @param y
	 */
	private int screenYToMapY(int y) {
		Vector3 touch = new Vector3(0, y, 0);
		mapScreen.getMap().getCamera().unproject(touch);
		return (int) touch.y / 32;
	}

	/**
	 * Set an action who is respond for every click
	 *
	 * @param baseAction
	 * @param selected
	 */
	public void setActiveAction(BaseGameAction baseAction) {

		if (activeAction != null) {
			activeAction.destroy(mapScreen);
		}

		activeAction = baseAction;

		if (baseAction == null) {
			oldX = -1;
			oldY = -1;
		}

		// inform the gui
		mapScreen.getMapgui().setActiveAction(baseAction);
	}

	/**
	 * Check if an action running?
	 *
	 * @param baseAction
	 * @param selected
	 */
	public boolean hasActiveAction() {
		return activeAction != null;
	}

	/**
	 * @param keyBindings
	 *            the keyBindings to set
	 */
	public void keyBindings(int key, YChangeListener action) {
		if (keyBindings != null) {
			if (keyBindings.containsKey(key)) {
				YConfig.error(new IllegalArgumentException("Key " + Keys.toString(key) + " already exist."), false);
			}
			keyBindings.put(key, action);
		}
	}

	/**
	 * @return the keyBindings
	 */
	public HashMap<Integer, YChangeListener> getKeyBindings() {
		return keyBindings;
	}

	/**
	 * @return the activeAction
	 */
	public BaseGameAction getActiveAction() {
		return activeAction;
	}

	/**
	 * 
	 */
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		try {
			// running an action?
			if (activeAction != null && button == Buttons.LEFT) {
				x = screenXToMapX(x);
				y = screenYToMapY(y);

				if (!mapScreen.getTilesetHelper().isValidePosition(x, y)) {
					YSounds.cancel();
					setActiveAction(null);
					return false;
				}
				// for android
				activeAction.performClick(x, y, mapScreen);
                activeAction.mouseMoved(x, y, mapScreen);
				return true;
			}

			// save pos
			if (activeAction == null) {
				oldPosX = x;
				oldPosY = y;
			}
		} catch (Exception e) {
			YConfig.error(e, false);
		}
		return false;

	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// can run?
		if (activeAction != null) {
			return false;
		}

		// changed?
		int aX = screenX - oldPosX;
		int aY = screenY - oldPosY;
		if (aX == 0 && aY == 0)
			return true;

		// move map
		mapScreen.getMap().moveMapView(-aX, aY);

		// save
		oldPosX = screenX;
		oldPosY = screenY;

		return true;

	}

	@Override
	public boolean keyDown(int keycode) {
		if (keyBindings != null && keyBindings.containsKey(keycode)) {
			keyBindings.get(keycode).changedY(null);
			return true;
		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// running an action?
		if (activeAction != null) {
			try {
				int x = screenXToMapX(screenX);
				int y = screenYToMapY(screenY);

				if (!mapScreen.getTilesetHelper().isValidePosition(x, y))
					return false;

				if (oldX == x && oldY == x)
					return false;

				oldX = x;
				oldY = y;
				activeAction.mouseMoved(x, y, mapScreen);
				return true;
			} catch (Exception e) {
				YConfig.error(e, false);
				// reset action
				setActiveAction(null);
			}
		}
		return false;
	}
}
