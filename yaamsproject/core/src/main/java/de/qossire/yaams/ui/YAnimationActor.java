/**
 * 
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author sven
 *
 */
public class YAnimationActor extends Actor {

	protected Animation<TextureRegion> animation;
	protected TextureRegion currentRegion;
	protected TextureRegion previewRegion;
	protected float time = 0f;
	protected boolean stop;

	/**
	 * 
	 */
	public YAnimationActor(Animation<TextureRegion> animation) {
		this.animation = animation;
		previewRegion = animation.getKeyFrames()[1];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.scenes.scene2d.Actor#act(float)
	 */
	@Override
	public void act(float delta) {
		super.act(delta);
		time += delta;

		if (currentRegion == null || !stop)
			currentRegion = animation.getKeyFrame(time, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.badlogic.gdx.scenes.scene2d.Actor#draw(com.badlogic.gdx.graphics.g2d.
	 * Batch, float)
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		// reset color
		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

		batch.draw(currentRegion, getX(), getY());
	}

	/**
	 * @param stop
	 *            the stop to set
	 */
	public void setStop(boolean stop) {
		this.stop = stop;
		if (stop)
			currentRegion = animation.getKeyFrames()[1];
	}

	/**
	 * @return the currentRegion
	 */
	@Deprecated
	public TextureRegion getCurrentRegion() {
		return currentRegion;
	}

	/**
	 * @return the stop
	 */
	public boolean isStop() {
		return stop;
	}

	/**
	 * @return the previewRegion
	 */
	public TextureRegion getPreviewRegion() {
		return previewRegion;
	}

}
