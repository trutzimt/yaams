package de.qossire.yaams.game.art;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import de.qossire.yaams.code.YAssert;
import de.qossire.yaams.code.YHashMap;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.build.requirement.IRequirement;
import de.qossire.yaams.game.build.requirement.ReqFoundation;
import de.qossire.yaams.game.build.requirement.ReqNoActiveBuilding;
import de.qossire.yaams.game.build.requirement.ReqNoArt;
import de.qossire.yaams.game.build.requirement.ReqNoBuilding;
import de.qossire.yaams.game.build.requirement.ReqNoDoor;
import de.qossire.yaams.game.build.requirement.ReqTable;
import de.qossire.yaams.game.build.requirement.ReqWall;
import de.qossire.yaams.game.rooms.BaseRoom;
import de.qossire.yaams.game.rooms.RoomDepot;
import de.qossire.yaams.game.rooms.RoomMgmt.RoomTyp;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;
import de.qossire.yaams.screens.game.events.MapEventHandlerAction;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YNotification;

public class ArtworkMgmt {

	@Deprecated
	private BaseArt[][] artArea;

	/**
	 * List the art, who can seen from this point
	 */
	private ArrayList<BaseArt>[][] visibleArt;

	/**
	 * Req for this art type
	 */
	private HashMap<ArtTyp, ArrayList<IRequirement>> requirements;

	private MapScreen mapScreen;
	/**
	 * Set new uids
	 */
	private int uid;

	public enum ArtTyp {
		IMAGE, STATURE, ONTABLE
	}

	private ArrayList<BaseArt> avaible, artList;
	private YHashMap<ArtEpoch> epoch;
	private int workshopPrice, workshopCapacity;

	/**
	 * build it
	 */
	public ArtworkMgmt(MapScreen map) {
		avaible = new ArrayList<>();
		artList = new ArrayList<>();
		uid = 1;
		createEpoch();

		workshopCapacity = 10;
		workshopPrice = 200;

		this.mapScreen = map;

		// build req
		requirements = new HashMap<>();
		if (map.getSettings().isActive(ScenConf.ART_ONTABLE)) {
			ArrayList<IRequirement> ir = new ArrayList<>();
			ir.add(new ReqNoArt());
			ir.add(new ReqNoActiveBuilding());
			ir.add(new ReqTable());
			requirements.put(ArtTyp.ONTABLE, ir);
		}

		if (map.getSettings().isActive(ScenConf.ART_IMAGE)) {
			ArrayList<IRequirement> ir = new ArrayList<>();
			ir.add(new ReqNoArt());
			ir.add(new ReqNoActiveBuilding());
			ir.add(new ReqWall());
			ir.add(new ReqNoDoor());
			requirements.put(ArtTyp.IMAGE, ir);
		}

		if (map.getSettings().isActive(ScenConf.ART_STATURE)) {
			ArrayList<IRequirement> ir = new ArrayList<>();
			ir.add(new ReqNoArt());
			ir.add(new ReqNoActiveBuilding());
			ir.add(new ReqFoundation());
			ir.add(new ReqNoBuilding());
			requirements.put(ArtTyp.STATURE, ir);
		}

		map.getEvents().register(MapEventHandlerTyp.FILE_SAVE, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				mapScreen.getData().setP("workshopCapacity", workshopCapacity);
				mapScreen.getData().setP("workshopPrice", workshopPrice);
				mapScreen.getData().setArts(artList);
				return false;
			}
		});

		map.getEvents().register(MapEventHandlerTyp.FILE_LOAD, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				workshopCapacity = mapScreen.getData().getPI("workshopCapacity");
				workshopPrice = mapScreen.getData().getPI("workshopPrice");
				artList = MapScreen.get().getData().getArts();

				for (BaseArt a : artList) {
					if (a.getStatus() != ArtStatus.DISPLAYED)
						continue;
					a.afterLoad();
					artArea[a.getPos().getX()][a.getPos().getY()] = a;
				}
				return false;
			}
		});

		map.getEvents().register(MapEventHandlerTyp.NEXTDAY, new MapEventHandlerAction() {

			@Override
			public boolean perform(Object[] objects) {
				mapScreen.getPlayer().addMoney(-1 * workshopPrice * getArts(ArtStatus.WORKSHOP).size());
				return false;
			}
		});
	}

	public ArrayList<IRequirement> getReq(ArtTyp art) {
		return requirements.get(art);
	}

	private void createEpoch() {
		epoch = new YHashMap<>("epoch");

		// http://www.dummies.com/education/art-appreciation/art-history-timeline/

		epoch.addS(new ArtEpoch("Stone Age", -30000, -2500, "International"));
		epoch.addS(new ArtEpoch("Mesopotamian", -3500, -539, "Asia"));
		epoch.addS(new ArtEpoch("Egyptian", -3100, -30, "Africa"));
		epoch.addS(new ArtEpoch("Greek and Hellenistic", -850, -31, "Europe"));
		epoch.addS(new ArtEpoch("Roman", -500, 476, "Europe"));
		epoch.addS(new ArtEpoch("Indian, Chinese, and Japanese", -653, 1900, "Asia"));
		epoch.addS(new ArtEpoch("Byzantine and Islamic", 476, 1453, "Asia"));
		epoch.addS(new ArtEpoch("Middle Ages", 500, 1400, "Europe"));
		epoch.addS(new ArtEpoch("Early and High Renaissance", 1400, 1550, "Europe"));
		epoch.addS(new ArtEpoch("Venetian and Northern Renaissance", 1430, 1550, "Europe"));
		epoch.addS(new ArtEpoch("Mannerism", 1527, 1580, "Europe"));
		epoch.addS(new ArtEpoch("Baroque", 1600, 1750, "Europe"));
		epoch.addS(new ArtEpoch("Neoclassical", 1750, 1850, "Europe"));
		epoch.addS(new ArtEpoch("Romanticism", 1780, 1850, "Europe"));
		epoch.addS(new ArtEpoch("Realism", 1848, 1900, "Europe"));
		epoch.addS(new ArtEpoch("Impressionism", 1865, 1885, "International"));
		epoch.addS(new ArtEpoch("Post Impressionism", 1885, 1910, "International"));
		epoch.addS(new ArtEpoch("Fauvism and Expressionism", 1900, 1935, "International"));
		epoch.addS(new ArtEpoch("Cubism, Futurism, Supremativism, Constructivism, De Stijl", 1905, 1920, "International"));
		epoch.addS(new ArtEpoch("Dada and Surrealism", 1917, 1950, "International"));
		epoch.addS(new ArtEpoch("Abstract Expressionism", 1940, 1959, "International"));
		epoch.addS(new ArtEpoch("Pop Art", 1960, 1969, "International"));
		epoch.addS(new ArtEpoch("Postmodernism and Deconstructivism", 1970, 2018, "International"));

	}

	/**
	 * Generate a new art
	 * 
	 * @return
	 */
	public BaseArt generateArt() {
		ArtTyp typ = (ArtTyp) NamesGenerator.getRndA(requirements.keySet().toArray());

		BaseArt art;

		if (typ == ArtTyp.IMAGE) {
			art = new OnWallArt();
		} else if (typ == ArtTyp.STATURE) {
			art = new StatureArt();
		} else {
			art = new OnTableArt();
		}
		art.setUid(uid++);

		return art;
	}

	/**
	 * Add a new art
	 * 
	 * @param art
	 * @param status
	 */
	public void addArt(BaseArt art, ArtStatus status) {
		art.setStatus(status);
		if (status == ArtStatus.NEW) {
			avaible.add(art);
		} else {
			artList.add(art);
		}
	}

	/**
	 * Add a new art
	 * 
	 * @param art
	 * @param status
	 */
	public ArrayList<BaseArt> getArts(ArtStatus status) {
		if (status == ArtStatus.NEW) {
			return avaible;
		} else {
			ArrayList<BaseArt> arts = new ArrayList<>();

			for (BaseArt art : artList) {
				if (art.getStatus() == status) {
					arts.add(art);
				}
			}
			return arts;
		}
	}

	/**
	 * Add a new art
	 * 
	 * @param art
	 * @param status
	 */
	public int getArtsCount(ArtStatus status) {
		if (status == ArtStatus.NEW) {
			return avaible.size();
		} else {
			int i = 0;

			for (BaseArt art : artList) {
				if (art.getStatus() == status) {
					i++;
				}
			}
			return i;
		}
	}

	/**
	 * Remove the art
	 * 
	 * @param art
	 */
	@Deprecated
	public void removeArt(BaseArt art) {
		art.setStatus(null);
		if (avaible.contains(art)) {
			avaible.remove(art);
			return;
		} else {
			artList.remove(art);
		}

	}

	/**
	 * Generate new art
	 */
	public void nextDay() {

		regAvaibleArt();

		// get depots
		LinkedList<Integer> roomsize = new LinkedList<>(), roomcond = new LinkedList<>();
		for (BaseRoom b : mapScreen.getPlayer().getRooms().getRooms()) {
			if (b.getTyp() == RoomTyp.DEPOT) {
				roomsize.add(((RoomDepot) b).getSize());
				roomcond.add(b.getBonus());
			}
		}

		int free = 0;
		// destroy to much art
		for (BaseArt a : getArtInDepot()) {
			if (roomsize.size() > 0) {
				roomsize.addFirst(roomsize.get(0) - 1);
				roomsize.remove(1);
				a.addCondition(roomcond.get(0));

				// remove used room
				if (roomsize.get(0) <= 0) {
					roomsize.removeFirst();
					roomcond.removeFirst();
				}

				continue;
			}
			free++;
			a.addCondition(-5);
		}

		// inform user?
		if (free > 0) {
			mapScreen.getMapgui()
					.addNotification(new YNotification("Artworks stored outside",
							"For " + free + " artworks is not enough space in the depot. They lost 5% condition. Build or increase your depot. (Build> Rooms)",
							YIcons.getIconI(YIType.ARTWORK)));
		}
	}

	/**
	 * regenerate aviable art
	 */
	public void regAvaibleArt() {
		avaible.clear();
		for (int i = 0; i < NamesGenerator.getIntBetween(2, 4); i++) {
			addArt(generateArt(), ArtStatus.NEW);
		}
	}

	/**
	 * @return the avaible
	 */
	public ArrayList<BaseArt> getAvaibleArt() {
		return avaible;
	}

	/**
	 * @return the owned
	 */
	@Deprecated
	public ArrayList<BaseArt> getArtInDepot() {
		return getArts(ArtStatus.DEPOT);
	}

	/**
	 * @return the owned
	 */
	@Deprecated
	public ArrayList<BaseArt> getDisplayed() {
		return getArts(ArtStatus.DISPLAYED);
	}

	/**
	 * @return the owned
	 */
	@Deprecated
	public ArrayList<BaseArt> getInWorkshop() {
		return getArts(ArtStatus.WORKSHOP);
	}

	/**
	 * Get art on this point, if exist
	 * 
	 * @param x
	 * @param y
	 * @return art or null
	 */
	public BaseArt getArtAt(int x, int y) {
		if (!mapScreen.getTilesetHelper().isValidePosition(x, y))
			return null;

		return getArt(mapScreen.getData().getProps(MapProp.ARTID, x, y));
	}

	/**
	 * build it
	 * 
	 * @param x
	 * @param y
	 */
	@Deprecated
	public void buildAt(BaseArt art, int x, int y) {
		if (!mapScreen.getTilesetHelper().isValidePosition(x, y))
			new IllegalArgumentException("Wrong Position (" + x + "," + y + ")");

		// set building
		art.buildAt(x, y);
		artArea[x][y] = art;
	}

	/**
	 * remove the art from this spot
	 * 
	 * @param x
	 * @param y
	 */
	public void removeAt(int x, int y) {
		YAssert.isValidePosition(x, y);

		// get the art
		BaseArt art = getArtAt(x, y);
		if (art == null)
			throw new IllegalArgumentException("Not Art at " + x + "," + y);

		// change lists
		art.setStatus(ArtStatus.DEPOT);

		// remove building
		art.removeAt(x, y);
		artArea[x][y] = null;
		mapScreen.getData().addProps(MapProp.ARTID, x, y, 0);
	}

	/**
	 * Destroy this art
	 * 
	 * @param art
	 */
	public void destroy(BaseArt art) {
		// is on the map?
		if (art.getStatus() == ArtStatus.DISPLAYED) {
			removeAt(art.getPos().getX(), art.getPos().getY());
		}

		artList.remove(art);
	}

	/**
	 * Get a art with this uid, if exist
	 * 
	 * @param uid
	 * @return
	 */
	public BaseArt getArt(int uid) {
		for (BaseArt art : artList) {
			if (art.getUId() == uid) {
				return art;
			}
		}
		for (BaseArt art : avaible) {
			if (art.getUId() == uid) {
				return art;
			}
		}
		return null;
	}

	/**
	 * Load it
	 */
	@SuppressWarnings("unchecked")
	public void init() {
		artArea = new BaseArt[mapScreen.getMap().getWidth()][mapScreen.getMap().getHeight()];
		visibleArt = new ArrayList[mapScreen.getMap().getWidth()][mapScreen.getMap().getHeight()];

		regAvaibleArt();

	}

	/**
	 * Set visible art at this spot. Ignore, if position is invalide
	 * 
	 * @param x
	 * @param y
	 * @param art
	 */
	public void addVisibleArtAt(int x, int y, BaseArt art) {
		if (!mapScreen.getTilesetHelper().isValidePosition(x, y))
			return;

		if (visibleArt[x][y] == null) {
			visibleArt[x][y] = new ArrayList<>();
		}

		// find free spot
		visibleArt[x][y].add(art);
	}

	/**
	 * Set visible art at this spot. Ignore, if position is invalide
	 * 
	 * @param x
	 * @param y
	 * @param art
	 */
	public void removeVisibleArtAt(int x, int y, BaseArt art) {
		if (!mapScreen.getTilesetHelper().isValidePosition(x, y))
			return;

		// find free spot
		visibleArt[x][y].remove(art);

		// is empty?
		if (visibleArt[x][y].size() == 0) {
			visibleArt[x][y] = null;
		}
	}

	/**
	 * Get all art, who can seen from this spot
	 * 
	 * @param x
	 * @param y
	 * @param art
	 * @return a empty list or a list with all arts
	 */
	public ArrayList<BaseArt> getVisibleArtAt(int x, int y) {
		if (!mapScreen.getTilesetHelper().isValidePosition(x, y))
			new IllegalArgumentException("Wrong Position (" + x + "," + y + ")");

		if (visibleArt[x][y] == null) {
			return new ArrayList<>();
		}

		return visibleArt[x][y];
	}

	/**
	 * Get the art epoch
	 * 
	 * @param epochid
	 * @return
	 */
	public ArtEpoch getArtEpoch(String epochid) {
		return epoch.getS(epochid);
	}

	/**
	 * Get a Random epoch
	 * 
	 * @return
	 */
	public ArtEpoch getRandomEpoch() {
		return epoch.getRandom();
	}

	/**
	 * @return the workshopPrice
	 */
	public int getWorkshopPrice() {
		return workshopPrice;
	}

	/**
	 * @return the workshopCapacity
	 */
	public int getWorkshopCapacity() {
		return workshopCapacity;
	}
}