/**
 * 
 */
package de.qossire.yaams.game.quest.action;

import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class QuestActionGetArt extends BaseQuestAction {

	private int count;

	/**
	 * @param character
	 * @param message
	 */
	public QuestActionGetArt(int count) {
		super("Give " + count + " Artworks");
		this.count = count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.action.BaseQuestAction#perform(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public void perform() {
		for (int i = 0, l = count; i < l; i++) {
			MapScreen.get().getPlayer().getArtwork().addArt(MapScreen.get().getPlayer().getArtwork().generateArt(), ArtStatus.DEPOT);
		}

	}

}
