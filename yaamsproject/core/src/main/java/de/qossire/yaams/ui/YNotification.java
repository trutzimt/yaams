/**
 * 
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisWindow;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class YNotification extends VisWindow {

	/**
	 * @param title
	 */
	public YNotification(String title, String text, Image icon) {
		super(title);

		VisLabel l = new VisLabel(text);
		l.setWrap(true);
		add(icon).align(Align.topLeft);
		add(l).grow();

		int f = YSettings.isBiggerGui() ? 2 : 1;

		setWidth(300 * f);
		setHeight((62 + Math.max(0, ((text.length() / 30) - 2) * 20)) * f);
		setX(10 * f);
		addCloseButton();
		closeOnEscape();

		getColor().a = 0f;
		addAction(Actions.sequence(Actions.fadeIn(3), Actions.delay(10), Actions.fadeOut(3), Actions.removeActor()));
	}

	@Override
	protected void close() {
		MapScreen.get().getMapgui().recalcNotificationPos(this);
		super.close();
	}

}
