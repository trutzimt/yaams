/**
 * 
 */
package de.qossire.yaams.screens.game.events;

import java.util.ArrayList;
import java.util.HashMap;

import de.qossire.yaams.base.YConfig;

/**
 * @author sven
 *
 */
public class MapEventHandler {

	public enum MapEventHandlerTyp {
		NEXTDAY, CUSTOMER_NEW, CUSTOMER_LEAVE, FILE_LOAD, FILE_SAVE, TIME_PLAY, TIME_BREAK
	}

	private HashMap<MapEventHandlerTyp, ArrayList<MapEventHandlerAction>> actions;

	/**
	 * 
	 */
	public MapEventHandler() {
		actions = new HashMap<>();
	}

	/**
	 * Register a new listener
	 * 
	 * @param typ
	 * @param action
	 */
	public void register(MapEventHandlerTyp typ, MapEventHandlerAction action) {
		// exist?
		if (!actions.containsKey(typ))
			actions.put(typ, new ArrayList<MapEventHandlerAction>());

		// add
		actions.get(typ).add(action);
	}

	/**
	 * Remove the listener
	 * 
	 * @param typ
	 * @param action
	 */
	public void remove(MapEventHandlerTyp typ, MapEventHandlerAction action) {
		actions.get(typ).remove(action);
	}

	/**
	 * Perform this event with this objects
	 * 
	 * @param typ
	 * @param objects
	 */
	public void perform(MapEventHandlerTyp typ, Object... objects) {
		ArrayList<MapEventHandlerAction> a = actions.get(typ);
		// check timeactions
		// TODO find performter way
		if (a != null && a.size() > 0)
			for (MapEventHandlerAction action : a.toArray(new MapEventHandlerAction[] {})) {
				try {
					if (action.perform(objects))
						a.remove(action);
				} catch (Exception e) {
					YConfig.error(e, false);
					a.remove(action);
				}
			}
	}
}
