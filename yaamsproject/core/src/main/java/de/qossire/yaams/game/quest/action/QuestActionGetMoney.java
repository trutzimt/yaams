/**
 * 
 */
package de.qossire.yaams.game.quest.action;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.TextHelper;

/**
 * @author sven
 *
 */
public class QuestActionGetMoney extends BaseQuestAction {

	private int money;

	/**
	 * @param character
	 * @param message
	 */
	public QuestActionGetMoney(int money) {
		super("Give " + TextHelper.getMoneyString(money));
		this.money = money;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.action.BaseQuestAction#perform(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public void perform() {
		MapScreen.get().getPlayer().addMoney(money);

	}

}
