/**
 * 
 */
package de.qossire.yaams.game.art.window;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.art.BaseArt.ArtStatus;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YChangeListener;

/**
 * @author sven
 *
 */
public class DisplayedTab extends ArtworkTab {

	private VisTextButton remove;

	public DisplayedTab() {
		super("Displayed", "At the moment you have no art displayed in your museum. Go in depot and place some.");

		remove = new VisTextButton("");
		remove.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				moveToDepot();

			}
		});
		buttonBar.addActor(remove);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#reset()
	 */
	@Override
	protected void reset() {
		super.reset();
		remove.setDisabled(true);
		remove.setText("Move to depot");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#clickElement(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected void clickElement(Button btn) {

		BaseArt a = ((BaseArt) btn.getUserObject());
		if (a.getTimeAction() != null) {
			remove.setDisabled(true);
			remove.setText("Please wait. It is working");
		} else {
			remove.setDisabled(false);
			remove.setText("Move " + a.getName() + " to depot");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		elements.clear();

		// add all elements
		for (final BaseArt art : MapScreen.get().getPlayer().getArtwork().getArts(ArtStatus.DISPLAYED)) {
			addElement(art.getName(), art.getIcon(true), art);
		}

		super.rebuild();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#doubleClickElement(com.badlogic.gdx.scenes.
	 * scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		moveToDepot();
	}

	/**
	 * Move the selected art to depot
	 */
	private void moveToDepot() {
		MapScreen.get().getPlayer().getArtwork().removeAt(((BaseArt) active).getPos().getX(), ((BaseArt) active).getPos().getY());
		for (int i = 0; i < elements.size(); i++) {
			if (elements.get(i).getUserObject() == active) {
				elements.remove(i);
				break;
			}
		}
		reset();
		YSounds.click();
		rebuild();
	}

}