/**
 * 
 */
package de.qossire.yaams.game.museum.funding;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.game.quest.BaseQuestItem;
import de.qossire.yaams.game.quest.action.BaseQuestAction;
import de.qossire.yaams.game.quest.req.IQuestRequirement;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public abstract class Fundings extends BaseQuestItem {

	private boolean active;

	/**
	 * 
	 */
	public Fundings() {
		title = "First Aid";
	}

	public abstract Drawable getIcon();

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	public VisTable getInfoPanel() {
		YTable table = new YTable();

		// add req
		if (req.size() > 0) {
			table.addH("Requirements");
			for (IQuestRequirement r : req) {
				table.addL("[" + (r.checkReq() ? "v" : "x") + "]", r.getDesc());
			}
		}

		// add preActions
		if (preActions.size() > 0) {
			table.addH("Reward");
			for (BaseQuestAction a : preActions) {
				table.addL("", a.getTitle());
			}
		}

		// add actions
		if (actions.size() > 0) {
			table.addH("Daily Reward");
			for (BaseQuestAction a : actions) {
				table.addL("", a.getTitle());
			}
		}

		return table;
	}

	/**
	 * Start the funding
	 * 
	 * @return true > finish, false otherwise
	 */
	public boolean start() {

		if (!checkReq()) {
			active = false;
			return false;
		}

		// quest is fullfilled
		for (BaseQuestAction bqa : preActions) {
			bqa.perform();
		}

		return true;
	}

	/**
	 * Start the funding
	 * 
	 * @return true > finish, false otherwise
	 */
	@Override
	public boolean updateLogic() {
		Gdx.app.debug("Fundings", "Update " + getTitle() + " " + status);

		if (!checkReq()) {
			active = false;
			return false;
		}

		// quest is fullfilled
		for (BaseQuestAction bqa : actions) {
			bqa.perform();
		}

		return true;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

}
