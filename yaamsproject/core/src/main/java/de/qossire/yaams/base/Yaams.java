package de.qossire.yaams.base;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.code.YLog;
import de.qossire.yaams.screens.LoaderScreen;
import de.qossire.yaams.screens.game.GameLoaderScreen;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.menu.MainMenuScreen;

public class Yaams extends Game {

	private IPlattform plattform;

	public Yaams(IPlattform plattform) {
		this.plattform = plattform;
	}

	/**
	 * Load the base assests
	 */
	@Override
	public void create() {
		try {

			YConfig.init(this);

			YStatic.systemAssets = new AssetManager();
			YStatic.gameAssets = new AssetManager();

			// load for the loadingscreen:
			YStatic.systemAssets.load("system/menu/DecorativeTile.png", Texture.class);
			YStatic.systemAssets.load("system/menu/Castle2.png", Texture.class);
			YStatic.systemAssets.load("system/logo/logo256.png", Texture.class);
			YStatic.systemAssets.load("system/logo/logo512.png", Texture.class);
			// YStatic.systemAssets.load("system/menu/hourglass.png", Texture.class);
			YStatic.systemAssets.finishLoading();

			// setScreen(new ErrorScreen(new IllegalArgumentException("www")));

			super.setScreen(new LoaderScreen(this));
		} catch (Exception e) {
			YConfig.error(e, true);

			// Gdx.app.error("Main", "Can not start game", e);
			// plattform.showErrorDialog(
			// "Can not start game " + e.getClass().getSimpleName() + " " +
			// e.getLocalizedMessage());
			// Gdx.app.exit();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Game#render()
	 */
	@Override
	public void render() {
		try {
			super.render();
		} catch (Exception e) {
			YConfig.error(e, true);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Game#setScreen(com.badlogic.gdx.Screen)
	 */
	@Override
	@Deprecated
	public void setScreen(Screen screen) {
		super.setScreen(screen);
	}

	/**
	 * Switch to a new screen with transition
	 * 
	 * @param game
	 * @param newScreen
	 */
	public void switchScreen(final Screen newScreen) {
		Screen oldScreen = getScreen();

		YLog.log(oldScreen.getClass().getSimpleName(), "switch", newScreen.getClass().getSimpleName());

		// find the matches
		if (oldScreen instanceof LoaderScreen && newScreen instanceof MainMenuScreen) {
			// just remove the loading
			LoaderScreen l = (LoaderScreen) oldScreen;
			l.getLoadingHour().addAction(Actions.sequence(Actions.fadeOut(1), Actions.run(new Runnable() {

				@Override
				public void run() {
					Yaams.super.setScreen(newScreen);

				}
			})));
			l.getText().addAction(Actions.fadeOut(1));
			return;

		}

		// find the matches
		if (oldScreen instanceof LoaderScreen && newScreen instanceof GameLoaderScreen) {
			Yaams.super.setScreen(newScreen);
			return;

		}

		// find the matches
		if (oldScreen instanceof MainMenuScreen && newScreen instanceof GameLoaderScreen) {
			// just remove the loading
			MainMenuScreen m = (MainMenuScreen) oldScreen;
			m.getStage().addAction(Actions.sequence(Actions.fadeOut(1), Actions.run(new Runnable() {

				@Override
				public void run() {
					Yaams.super.setScreen(newScreen);

				}
			})));
			return;

		}

		// stop old game?
		if (oldScreen instanceof MapScreen) {
			((MapScreen) oldScreen).stop();
		}

		// default
		oldBlackScreen(newScreen, oldScreen);
		newBlackScreen(newScreen);

		// found nothing?
		// Yaams.super.setScreen(newScreen);
	}

	/**
	 * @param newScreen
	 * @param g
	 */
	private void oldBlackScreen(final Screen newScreen, Screen oldScreen) {
		Image black = new Image(YStatic.gameAssets.get("system/person/black.png", Texture.class));
		black.setFillParent(true);
		black.getColor().a = 0;
		black.addAction(Actions.sequence(Actions.fadeIn(1), Actions.run(new Runnable() {

			@Override
			public void run() {
				Yaams.super.setScreen(newScreen);

			}
		})));
		((IScreen) oldScreen).getStage().addActor(black);
	}

	/**
	 * @param newScreen
	 * @param g
	 */
	private void newBlackScreen(Screen newScreen) {
		Image black = new Image(YStatic.gameAssets.get("system/person/black.png", Texture.class));
		black.setFillParent(true);
		black.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		((IScreen) newScreen).getStage().addActor(black);
	}

	/**
	 * Save it
	 */
	@Override
	public void dispose() {
		YSettings.save();
	}

	/**
	 * @return the plattform
	 */
	public IPlattform getPlattform() {
		return plattform;
	}
}
