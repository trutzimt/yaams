package de.qossire.yaams.game.art.window;

import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YTabWindow;

public class ArtworkWindow extends YTabWindow {

	/**
	 * 
	 * @param map
	 */
	public ArtworkWindow() {
		super("Artworks");

		tabbedPane.add(new DepotTab(this));
		if (MapScreen.get().getSettings().isActive(ScenConf.ART_BUY))
			tabbedPane.add(new BuyTab());
		tabbedPane.add(new DisplayedTab());
		if (MapScreen.get().getSettings().isActive(ScenConf.WORKSHOP))
			tabbedPane.add(new WorkshopTab());

		buildIt();

		addTitleIcon(YIcons.getIconI(YIType.ARTWORK));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		super.close();
	}

}
