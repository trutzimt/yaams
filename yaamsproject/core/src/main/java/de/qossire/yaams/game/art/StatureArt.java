/**
 * 
 */
package de.qossire.yaams.game.art;

import de.qossire.yaams.game.art.ArtworkMgmt.ArtTyp;
import de.qossire.yaams.generator.NamesGenerator;

/**
 * @author sven
 *
 */
public class StatureArt extends BaseArt {

	/**
	 * @param uid
	 * @param typ
	 */
	public StatureArt() {
		super(ArtTyp.STATURE);
		setTId((int) NamesGenerator.getRnd(192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234,
				235, 384, 385));
	}

}
