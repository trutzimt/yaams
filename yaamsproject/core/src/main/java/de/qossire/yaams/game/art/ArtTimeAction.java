/**
 * 
 */
package de.qossire.yaams.game.art;

import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.thread.YTimeAction;
import de.qossire.yaams.ui.TextHelper;
import de.qossire.yaams.ui.YProgressBar;

/**
 * @author sven
 *
 */
public abstract class ArtTimeAction extends YTimeAction {

	protected long startTimeStamp;
	protected String title;

	/**
	 * @param runNext
	 */
	public ArtTimeAction(String title, long runNext) {
		super(runNext);
		startTimeStamp = MapScreen.get().getClock().getTimeStamp();
		this.title = title;
	}

	/**
	 * Generate a progress bar
	 * 
	 * @return
	 */
	public YProgressBar getProgress() {

		return new YProgressBar((int) (MapScreen.get().getClock().getTimeStamp() - startTimeStamp), (int) (runNext - startTimeStamp),
				TextHelper.getLongTimeString(runNext - MapScreen.get().getClock().getTimeStamp()));
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

}
