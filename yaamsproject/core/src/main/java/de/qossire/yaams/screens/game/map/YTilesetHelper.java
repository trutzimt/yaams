/**
 * 
 */
package de.qossire.yaams.screens.game.map;

import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public class YTilesetHelper {

	private final MapScreen mapScreen;
	public final int MAPWIDTH, MAPHEIGHT;
	public final static int TILESET_LENGTH = 32;

	/**
	 * 
	 */
	public YTilesetHelper(MapScreen mapScreen) {
		this.mapScreen = mapScreen;
		MAPWIDTH = mapScreen.getMap().getWidth();
		MAPHEIGHT = mapScreen.getMap().getHeight();
	}

	/**
	 * Check if the coordinates outside the map
	 *
	 * @param mapScreen
	 *            TODO
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isValidePosition(int x, int y) {
		return x >= 0 && y >= 0 && x < MAPWIDTH && y < MAPHEIGHT;
	}

	/**
	 * Get wall id
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public int getWallID(int x, int y, int baseid) {
		if (mapScreen.getData().getProps(MapProp.WALL, x, y) <= 0) {
			return -1;
		}

		final int NORTH = 1, EAST = 2, SOUTH = 4, WEST = 8;// , SOUTHEAST = 16;

		// boolean north, east, south, west, southeast;

		int id = 8;

		// has an old tile?
		if (mapScreen.getData().getProps(MapProp.DECOID, x, y) > 0) {
			baseid = mapScreen.getData().getProps(MapProp.DECOID, x, y);
		}

		// find neighbors
		int wallid = 0;
		wallid += (isValidePosition(x, y + 1) && mapScreen.getData().getProps(MapProp.WALL, x, y + 1) >= 1) ? NORTH : 0;
		wallid += (isValidePosition(x - 1, y) && mapScreen.getData().getProps(MapProp.WALL, x - 1, y) >= 1) ? WEST : 0;
		wallid += (isValidePosition(x, y - 1) && mapScreen.getData().getProps(MapProp.WALL, x, y - 1) >= 1) ? SOUTH : 0;
		wallid += (isValidePosition(x + 1, y) && mapScreen.getData().getProps(MapProp.WALL, x + 1, y) >= 1) ? EAST : 0;
		// TODO wallid += (isValidePosition(x + 1, y + 1) &&
		// mapScreen.getData().getProps(MapProp.WALL, x + 1, y + 1) >= 1) ? SOUTHEAST :
		// 0;
		// southeast = (isValidePosition(x + 1, y - 1) &&
		// mapScreen.getMap().getProps(BuildProperties.WALL, x + 1, y - 1) >= 1);

		// set tile
		switch (wallid) {
		case 0:
			id = baseid;
		case NORTH:
			id = baseid + TILESET_LENGTH * 3;
			break;
		case EAST:
			id = baseid;
			break;
		case NORTH + EAST:
			id = baseid;
			break;
		case SOUTH:
			id = baseid + TILESET_LENGTH;
			break;
		case NORTH + SOUTH:
			id = baseid + TILESET_LENGTH * 2;
			break;
		case EAST + SOUTH:
			id = baseid + TILESET_LENGTH + 1;
			break;
		case NORTH + EAST + SOUTH:
			id = baseid + TILESET_LENGTH + 1;
			break;
		case WEST:
			id = baseid + 2;
			break;
		case NORTH + WEST:
			id = baseid + TILESET_LENGTH * 2 + 1;
			break;
		case EAST + WEST:
			id = baseid + 1;
			break;
		case NORTH + EAST + WEST:
			id = baseid + 1;
			break;
		case SOUTH + WEST:
			id = baseid + TILESET_LENGTH;
			break;
		case NORTH + SOUTH + WEST:
			id = baseid + TILESET_LENGTH * 2;
			break;
		case EAST + SOUTH + WEST:
			id = baseid + TILESET_LENGTH + 1;
			break;
		case NORTH + EAST + SOUTH + WEST:
			id = baseid + TILESET_LENGTH + 1;
			break;
		default:
			id = 8;

		}

		// connections

		return id;
	}

	/**
	 * Check if the wall left or top
	 * 
	 * @param x
	 * @param y
	 * @param baseid
	 * @return true > top, false > left
	 */
	public boolean isTopWall(int x, int y) {
		if (mapScreen.getData().getProps(MapProp.WALL, x, y) <= 0) {
			throw new IllegalArgumentException(x + " " + y + " is not a valid wall");
		}

		int id = mapScreen.getData().getBuildTile(x, y) - mapScreen.getData().getProps(MapProp.DECOID, x, y) - 1;

		return (id <= 2 || id == TILESET_LENGTH + 2 || id == TILESET_LENGTH * 2 + 1);
	}
}