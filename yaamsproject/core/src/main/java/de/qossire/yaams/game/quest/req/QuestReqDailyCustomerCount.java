package de.qossire.yaams.game.quest.req;

import de.qossire.yaams.game.player.Stats.EStats;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqDailyCustomerCount extends BaseQuestReqMinCount {

	/**
	 * @param prop
	 * @param val
	 */
	public QuestReqDailyCustomerCount(int count) {
		super(count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + "daily Customer";
	}

	@Override
	public int getActCount() {
		return MapScreen.get().getPlayer().getStats().get(EStats.CUSTOMERCOUNT);
	}

}