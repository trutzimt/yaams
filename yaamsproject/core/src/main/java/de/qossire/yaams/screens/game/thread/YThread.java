package de.qossire.yaams.screens.game.thread;

import com.badlogic.gdx.Gdx;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.screens.game.events.MapEventHandler.MapEventHandlerTyp;

public class YThread {

	private Thread thread;
	private MapScreen mapScreen;
	private YClock clock;
	private boolean run;

	/**
	 * @param mapScreen
	 */
	public YThread(MapScreen mapScreen) {
		super();
		this.mapScreen = mapScreen;
		clock = new YClock();
		run = true;

		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				threadCodeWrapper();

			}
		});
	}

	/**
	 * Run the code endless
	 */
	private void threadCodeWrapper() {
		// long time;
		while (run) {
			// time = System.currentTimeMillis();
			try {
				if (clock.getTimeSpeed() > 0) {
					Thread.sleep(clock.getTickTimeMillis());
					threadCodeOnRun();
				} else {
					Thread.sleep(1000);
				}
				threadCodeAlways();
			} catch (Throwable t) {
				YConfig.error(t, true);
				return;
			}
		}
		MapScreen.finishAfterStop();
	}

	/**
	 * Stop the thread
	 */
	public void stop() {
		run = false;
		Gdx.app.log("Thread", "stop it");
	}

	/**
	 * Run on the thread
	 */
	private void threadCodeOnRun() {
		mapScreen.getPlayer().updateLogic();
		mapScreen.getMap().getPersonStage().threadCode();
		clock.tick();

		// next day?
		if (clock.getHour() >= 18) {
			clock.addHour(6 + 7);
			nextDay();
		}
	}

	/**
	 * Run on the thread
	 */
	private void threadCodeAlways() {
		mapScreen.getPlayer().getQuests().updateLogic();
	}

	/**
	 * Reset for the next day
	 * 
	 * @category thread
	 */
	private void nextDay() {
		mapScreen.getEvents().perform(MapEventHandlerTyp.NEXTDAY);

		mapScreen.getPlayer().nextDay();
		mapScreen.getMap().getPersonStage().nextDay();

	}

	/**
	 * @return the clock
	 */
	public YClock getClock() {
		return clock;
	}

	/**
	 * Start the thread
	 */
	public void start() {
		thread.start();
	}
}
