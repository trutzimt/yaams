/**
 * 
 */
package de.qossire.yaams.screens.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.game.art.BaseArt;
import de.qossire.yaams.game.build.BuildManagement.MapProp;

/**
 * @author sven
 *
 */
public class GameData {

	private HashMap<String, Object> pref;
	private Object[][] stats;

	private int[][][] mapProps;

	private int width, height;

	private ArrayList<BaseArt> arts;

	/**
	 * Hold const for specific data
	 * 
	 * @author sven
	 *
	 */
	public class GDC {
		public final static String TICKET_PRICE = "ticketprice", DRINK_PRICE = "drink_price", FOOD_PRICE = "food_price";
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public GameData() {

		// TODO find better solution
		if (MapScreen.get() == null)
			return;

		pref = (HashMap<String, Object>) MapScreen.get().getScenario().getBaseData().clone();

		// set defaults
		setP(GDC.TICKET_PRICE, 20);
		setP(GDC.DRINK_PRICE, 5);
		setP(GDC.FOOD_PRICE, 10);

	}

	/**
	 * 
	 */
	public void init() {
		width = MapScreen.get().getMap().getWidth();
		height = MapScreen.get().getMap().getHeight();
		mapProps = new int[MapProp.values().length][width][height];

		// set start value
		for (MapProp m : new MapProp[] { MapProp.ARTTILE, MapProp.FLOORTILE, MapProp.BUILDTILE }) {
			int[][] s = new int[width][height];
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					s[x][y] = -1;
				}
			}
			mapProps[m.ordinal()] = s;
		}
	}

	/**
	 * Set the settings
	 * 
	 * @param key
	 * @return
	 */
	public void setP(String key, Object o) {
		pref.put(key, o);
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public Object getP(String key) {
		return pref.get(key);
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public String getPS(String key) {
		if (pref.get(key) == null)
			return null;
		return pref.get(key).toString();
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public int getPI(String key) {
		try {
			return Integer.valueOf(pref.get(key).toString());
		} catch (Exception e) {
			YConfig.error(new IllegalArgumentException("Data for Key " + key + " is missing.", e), false);
			return 0;
		}
	}

	/**
	 * Get the settings or the standard value
	 * 
	 * @param key
	 * @return
	 */
	public int getPI(String key, int standard) {
		if (existP(key))
			return getPI(key);
		else
			return standard;
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public boolean getPB(String key) {
		return Boolean.valueOf(pref.get(key).toString());
	}

	/**
	 * Get the settings or the standard value
	 * 
	 * @param key
	 * @return
	 */
	public boolean getPB(String key, boolean standard) {
		if (existP(key))
			return getPB(key);
		else
			return standard;
	}

	/**
	 * Check the settings
	 * 
	 * @param key
	 * @return
	 */
	public boolean existP(String key) {
		return pref.containsKey(key);
	}

	/**
	 * 
	 * @return the props, if not exist return 0, if no valid coordinates return 0
	 */
	public int getProps(MapProp prop, int x, int y) {

		if (!MapScreen.get().getTilesetHelper().isValidePosition(x, y)) {
			return 0;
		}

		return mapProps[prop.ordinal()][x][y];
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id
	 */
	public int getBuildTile(int x, int y) {
		return mapProps[MapProp.BUILDTILE.ordinal()][x][y];
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id
	 */
	public int getFloorTile(int x, int y) {
		return mapProps[MapProp.FLOORTILE.ordinal()][x][y];
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return -1, if nothing was found or the build id
	 */
	public int getArtTile(int x, int y) {
		return mapProps[MapProp.ARTTILE.ordinal()][x][y];
	}

	/**
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void setBuildTile(int id, int x, int y) {
		setProps(MapProp.BUILDTILE, x, y, id);
		MapScreen.get().getMap().setTile(MapProp.BUILDTILE, id, x, y);
	}

	/**
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void setFloorTile(int id, int x, int y) {
		setProps(MapProp.FLOORTILE, x, y, id);
		MapScreen.get().getMap().setTile(MapProp.FLOORTILE, id, x, y);
	}

	/**
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void setArtTile(int id, int x, int y) {
		setProps(MapProp.ARTTILE, x, y, id);
		MapScreen.get().getMap().setTile(MapProp.ARTTILE, id, x, y);
		addProps(MapProp.BLOCKED, x, y, 1);
	}

	/**
	 *
	 * @param x
	 * @param y
	 */
	public void removeBuildTile(int x, int y) {
		MapScreen.get().getMap().removeTile(MapProp.BUILDTILE, x, y);
		setProps(MapProp.BUILDTILE, x, y, -1);
	}

	/**
	 *
	 * @param x
	 * @param y
	 */
	public void removeFloorTile(int x, int y) {
		MapScreen.get().getMap().removeTile(MapProp.FLOORTILE, x, y);
		setProps(MapProp.FLOORTILE, x, y, -1);
	}

	/**
	 *
	 * @param x
	 * @param y
	 */
	public void removeArtTile(int x, int y) {
		MapScreen.get().getMap().removeTile(MapProp.ARTTILE, x, y);
		setProps(MapProp.ARTTILE, x, y, -1);
		addProps(MapProp.BLOCKED, x, y, -1);
	}

	/**
	 * @param props
	 *            the props to set
	 */
	public void addProps(MapProp prop, int x, int y, int value) {
		mapProps[prop.ordinal()][x][y] += value;

		if (prop == MapProp.BLOCKED)
			MapScreen.get().getMap().getPersonStage().setReBuildNodes(true);
	}

	/**
	 * @param props
	 *            the props to set
	 */
	public void setProps(MapProp prop, int x, int y, int value) {

		mapProps[prop.ordinal()][x][y] = value;

		if (prop == MapProp.BLOCKED)
			MapScreen.get().getMap().getPersonStage().setReBuildNodes(true);
	}

	/**
	 * Set Rekursivlie the settings
	 * 
	 * @param prop
	 * @param x
	 * @param y
	 * @param value
	 * @param stopOnWall
	 */
	public void addPropsRekursive(MapProp prop, int x, int y, int value, boolean stopOnWall) {
		// Gdx.app.debug("PROP", "set prop " + prop + " on " + x + "/" + y + " width " +
		// value);
		addPropsRekursive(prop, x, y, value, new boolean[width][height], stopOnWall);
	}

	/**
	 * Set Rekursivlie the settings
	 * 
	 * @param prop
	 * @param x
	 * @param y
	 * @param value
	 * @param stopOnWall
	 */
	private void addPropsRekursive(MapProp prop, int x, int y, int value, boolean[][] fields, boolean stopOnWall) {
		// stops on wall?
		if (stopOnWall && getProps(MapProp.BLOCKED, x, y) >= 1) {
			return;
		}

		// has visit?
		if (fields[x][y])
			return;

		// set value
		addProps(prop, x, y, value);
		fields[x][y] = true;

		// change kind?
		value = value / 2;
		if (value == 0) {
			return;
		}

		if (MapScreen.get().getTilesetHelper().isValidePosition(x - 1, y)) {
			addPropsRekursive(prop, x - 1, y, value, fields, stopOnWall);
		}
		if (MapScreen.get().getTilesetHelper().isValidePosition(x, y - 1)) {
			addPropsRekursive(prop, x, y - 1, value, fields, stopOnWall);
		}
		if (MapScreen.get().getTilesetHelper().isValidePosition(x + 1, y)) {
			addPropsRekursive(prop, x + 1, y, value, fields, stopOnWall);
		}
		if (MapScreen.get().getTilesetHelper().isValidePosition(x, y + 1)) {
			addPropsRekursive(prop, x, y + 1, value, fields, stopOnWall);
		}
	}

	/**
	 * Return a list with all coordinates, who have this prop
	 *
	 * @param prop
	 * @param min,
	 *            min value
	 * @return the list, if nothing was found, return null
	 */
	public LinkedList<YPoint> getAllPropsPoints(MapProp prop, int min) {

		LinkedList<YPoint> points = new LinkedList<>();

		// find all valid tiles
		int[][] field = mapProps[prop.ordinal()];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (field[x][y] >= min) {
					points.add(new YPoint(x, y));
				}
			}
		}

		return points;
	}

	/**
	 * Return a list with all coordinates, who have this prop
	 * 
	 * @param customer
	 * @param prop
	 * @param min,
	 *            min value
	 * @return the list, if nothing was found, return null
	 */
	public LinkedList<YPoint> getAllBuildTilePoints(int id) {
		LinkedList<YPoint> points = new LinkedList<>();

		// find all valid tiles

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (getBuildTile(x, y) == id) {
					points.add(new YPoint(x, y));
				}
			}
		}

		return (points.size() > 0) ? points : null;
	}

	/**
	 * Return the basic settings for the save game
	 * 
	 * @return
	 */
	public HashMap<String, String> getCoreSettings() {
		HashMap<String, String> sett = new HashMap<>();
		sett.put("campaign", getPS("campaign"));
		sett.put("scenario", getPS("scenario"));
		sett.put("time", Integer.toString(MapScreen.get().getClock().getDay()));
		sett.put("museum", MapScreen.get().getPlayer().getMuseum().getName());
		sett.put("map", MapScreen.get().getScenario().getMapLink());
		return sett;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.saveload.ISave#afterLoad()
	 */
	public void afterLoad() {
		// load data
		MapScreen.get().getPlayer().addMoney(-(int) MapScreen.get().getPlayer().getMoney());

		MapScreen.get().getPlayer().addMoney(getPI("money"));
		MapScreen.get().getPlayer().setName(getPS("player"));
		MapScreen.get().getPlayer().getMuseum().setName(getPS("museum"));
		MapScreen.get().getPlayer().getTown().setName(getPS("town"));

		// build map
		for (MapProp m : new MapProp[] { MapProp.ARTTILE, MapProp.FLOORTILE, MapProp.BUILDTILE }) {
			int[][] s = mapProps[m.ordinal()];
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (s[x][y] >= 0) {
						MapScreen.get().getMap().setTile(m, s[x][y], x, y);
					}
				}
			}
		}
		MapScreen.get().getMap().getPersonStage().setReBuildNodes(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.saveload.ISave#preSave()
	 */
	public void preSave() {
		// save data
		setP("money", (int) MapScreen.get().getPlayer().getMoney());
		setP("player", MapScreen.get().getPlayer().getName());
		setP("museum", MapScreen.get().getPlayer().getMuseum().getName());
		setP("town", MapScreen.get().getPlayer().getTown().getName());
	}

	/**
	 * @return the arts
	 */
	public ArrayList<BaseArt> getArts() {
		return arts;
	}

	/**
	 * @param arts
	 *            the arts to set
	 */
	public void setArts(ArrayList<BaseArt> arts) {
		this.arts = arts;
	}

	/**
	 * @return the stats
	 */
	public Object[][] getStats() {
		return stats;
	}

	/**
	 * @param stats
	 *            the stats to set
	 */
	public void setStats(Object[][] stats) {
		this.stats = stats;
	}

	/**
	 * @return the pref
	 */
	public HashMap<String, Object> getPref() {
		return pref;
	}
}
