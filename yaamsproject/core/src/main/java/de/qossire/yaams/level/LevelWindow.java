/**
 *
 */
package de.qossire.yaams.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.Selection;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTree;
import com.rafaskoberg.gdx.typinglabel.TypingLabel;

import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.screens.game.GameLoaderScreen;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YTextButton;
import de.qossire.yaams.ui.YWindow;

/**
 * @author sven
 *
 */
public class LevelWindow extends YWindow {

	private Yaams yaams;
	private TypingLabel label;
	private VisTree tree;
	private YTextButton button;

	private BaseScenario selected;

	/**
	 *
	 * @param qossire
	 */
	public LevelWindow(Yaams yaams) {
		super("Scenarios");

		this.yaams = yaams;

		tree = new VisTree();
		button = new YTextButton("Start it") {

			@Override
			public void perform() {
				startScenario(selected);
			}
		};
		button.setDisabled(true);

		// add all campaigns
		for (String id : ScenarioManagement.getCampaignIds()) {
			BaseCampaign cam = ScenarioManagement.getCampaign(id);

			tree.add(cam.getNode());
		}

		// show desc
		tree.addListener(new YChangeListener() {
			@Override
			public void changedY(Actor actor) {
				Selection<Node> y = ((VisTree) actor).getSelection();

				if (y.size() == 0) {
					// nothing new happend
					if (selected != null) {
						startScenario(selected);
					}
					return;
				}

				button.setDisabled(true);
				button.setText("Select a Scenario first");

				Object o = y.getLastSelected().getObject();

				if (o instanceof BaseScenario) {
					selected = (BaseScenario) o;
					label.restart(selected.getDesc() + (selected.isWon() ? " Scenario was won." : ""));
					button.setDisabled(false);
					button.setText("Start " + selected.getTitle());
				} else if (o instanceof BaseCampaign) {
					selected = null;
					BaseCampaign b = (BaseCampaign) o;
					((VisTree) actor).getSelection().getLastSelected().setExpanded(true);
					label.restart(b.getDesc());

				} else {
					selected = null;
					label.restart("Please select a scenario or campaign.");
				}

			}
		});

		setWidth(Gdx.graphics.getWidth() / 3 * 2);
		setHeight(Gdx.graphics.getHeight() / 3 * 2);

		VisTable content = new VisTable();

		// Add tree
		VisScrollPane scrollPane = new VisScrollPane(tree);
		scrollPane.setFlickScroll(false);
		scrollPane.setFadeScrollBars(false);
		content.add(scrollPane).align(Align.topLeft).width(Value.percentWidth(.3F, content)).growY();

		// Add description
		label = new TypingLabel("Please select a scenario or campaign.", VisUI.getSkin());
		label.setWrap(true);

		content.add(label).align(Align.top).expand().growY().width(Value.percentWidth(.7F, content)).row();

		content.addSeparator().colspan(2);
		content.add();
		content.add(button).align(Align.right);

		add(content).grow();

	}

	/**
	 * Start it
	 *
	 * @param scen
	 */
	protected void startScenario(BaseScenario scen) {
		yaams.switchScreen(new GameLoaderScreen(LevelWindow.this.yaams, scen));
	}

}
