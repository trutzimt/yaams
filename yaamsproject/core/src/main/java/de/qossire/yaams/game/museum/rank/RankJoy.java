/**
 * 
 */
package de.qossire.yaams.game.museum.rank;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.game.player.Stats.EStats;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class RankJoy extends BaseRank {

	/**
	 * @param typ
	 * @param title
	 */
	public RankJoy() {
		super(ERank.JOY, "Joy");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return YIcons.getIconD(YIType.JOY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getDesc()
	 */
	@Override
	public String getDesc() {
		double avgJoy = ((MapScreen.get().getPlayer().getStats().get(EStats.CUSTOMERJOY) * 1f)
				/ MapScreen.get().getPlayer().getStats().get(EStats.CUSTOMERCOUNT)) / 100;

		String erg = "Your actuel level will be " + (avgJoy < 0 ? " decrease" : ((int) avgJoy) + "") + ".";

		return "The fun factor indicates how much the public liked your museum. On leaving, it is measured how satisfied the visitor was. At the end of the day the average is formed. When all are maximally satisfied, you receive maximum points. "
				+ erg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#checkLevel()
	 */
	@Override
	public int checkLevel() {
		double avgJoy = ((MapScreen.get().getPlayer().getStats().getYesterday(EStats.CUSTOMERJOY) * 1f)
				/ MapScreen.get().getPlayer().getStats().getYesterday(EStats.CUSTOMERCOUNT)) / 100;

		if (avgJoy < 0) {
			if (level > 0) {
				level--;
			}
			return level;
		}

		return (int) avgJoy;

	}

}
