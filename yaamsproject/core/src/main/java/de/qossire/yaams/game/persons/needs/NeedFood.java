/**
 * 
 */
package de.qossire.yaams.game.persons.needs;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.qossire.yaams.code.YLog;
import de.qossire.yaams.game.build.BuildManagement;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.persons.PersonManagement.PersonAnimationNeeds;
import de.qossire.yaams.game.persons.PersonManagement.PersonDir;
import de.qossire.yaams.game.persons.PersonManagement.PersonNeeds;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.screens.game.GameData.GDC;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.AnimationHelper;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YNotification;

/**
 * @author sven
 *
 */
public class NeedFood extends BaseNeed {

	/**
	 */
	public NeedFood() {
		super(PersonNeeds.FOOD, 5);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#updateLogic(de.qossire.yaams.
	 * game.persons.Customer, de.qossire.yaams.screens.map.MapScreen, int)
	 */
	@Override
	public boolean updateLogic(final Customer customer, final MapScreen map, int deltaTime) {

		// change thirsty
		customer.addNeed(id, 2);

		// not thirsty?
		if (customer.getNeed(id) <= 0) {
			return false;
		}

		// Gdx.app.debug(id.toString(),
		// "field: " + map.getMap().getProps(BuildProperties.TICKET,
		// customer.getGameX(), customer.getGameY()));

		// found an automat?
		if (map.getData().getProps(MapProp.DRINK, customer.getGameX(), customer.getGameY()) >= 1 && customer.getWaitMin() == 0
				&& customer.getMoney() >= map.getData().getPI(GDC.FOOD_PRICE)) {
			customer.setWaitMin(5, PersonDir.NORTH, new Runnable() {

				@Override
				public void run() {
					customer.addNeed(id, SET_MIN);
					customer.getData().remove("food");

					// TODO add joy modification for ticket price

					MapScreen.get().getMap().getPersonStage().getMgmt().showNeed(customer, PersonAnimationNeeds.SUN);
					AnimationHelper.moneyAnimation(map.getData().getPI(GDC.FOOD_PRICE), customer.getGameX(), customer.getGameY(), true);
				}

			});
			return false;
		}

		// remove?
		if (customer.getData().containsKey("food")) {
			customer.addNeed(PersonNeeds.LEAVE, 50);
			// TODO check for automats
			return false;
		}

		// need an automat?
		if (customer.getNeed(id) >= MAX / 2) {
			return true;
		}

		// want to buy a ticket
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.persons.needs.BaseNeed#tryToFix(de.qossire.yaams.game.
	 * persons.Customer, de.qossire.yaams.screens.map.MapScreen)
	 */
	@Override
	public void tryToFix(Customer customer, MapScreen map) {
		// find nearst automat
		if (!map.getMap().goToNearestTileFrom(customer, MapProp.DRINK, 1)) {
			// noti user
			if (!customer.getData().containsKey("food")) {
				map.getMapgui().addNotification(new YNotification("Hungry", customer.getPersonName() + " can not find a Vending Machine or the way is blocked. "
						+ customer.getPersonNameProp() + " will leave soon.", new Image(YIcons.getBuildIcon(BuildManagement.DRINK, true))));
				map.getMap().getPersonStage().getMgmt().showNeed(customer, 7);
				customer.getData().put("food", 1);
				YLog.log(id, "food machine missing, leaving", customer.getPersonName());
			}

			return;

		}

		// has no money for a bottle?
		if (customer.getMoney() < map.getData().getPI(GDC.DRINK_PRICE)) {
			// noti user
			if (!customer.getData().containsKey("food")) {
				map.getMapgui()
						.addNotification(new YNotification("Hungry", customer.getPersonName() + " can not buy at a Vending Machine. The price is to high. "
								+ customer.getPersonNameProp() + " will leave soon.", new Image(YIcons.getBuildIcon(BuildManagement.DRINK, true))));
				map.getMap().getPersonStage().getMgmt().showNeed(customer, 7);
				customer.getData().put("food", 1);
				YLog.log(id, "food machine to expensive, leaving", customer.getPersonName());
			}
		}

	}

}
