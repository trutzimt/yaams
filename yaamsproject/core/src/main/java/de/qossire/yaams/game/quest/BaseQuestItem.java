/**
 * 
 */
package de.qossire.yaams.game.quest;

import java.util.LinkedList;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.code.YLog;
import de.qossire.yaams.game.quest.action.BaseQuestAction;
import de.qossire.yaams.game.quest.req.IQuestRequirement;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public class BaseQuestItem {

	public enum QuestStatus {
		PRESTART, RUNNING, PAUSE, FINISH;
	}

	protected LinkedList<IQuestRequirement> req;
	protected LinkedList<BaseQuestAction> actions, preActions;
	protected String title, desc;
	protected QuestStatus status;
	protected Drawable icon;
	protected boolean showDetail;

	/** 
	 * 
	 */
	public BaseQuestItem() {
		req = new LinkedList<>();
		actions = new LinkedList<>();
		preActions = new LinkedList<>();
		status = QuestStatus.PRESTART;
		icon = YIcons.getIconD(YIType.TASK);
		showDetail = true;
	}

	/**
	 * Update the quest
	 * 
	 * @return true > finish, false otherwise
	 */
	public boolean updateLogic() {
		if (status != QuestStatus.FINISH)
			YLog.log("Quest", "Update " + getTitle() + " " + status);

		if (status == QuestStatus.FINISH)
			return true;

		// take a break?
		if (status == QuestStatus.PAUSE)
			return false;

		// need to start perform?
		if (status == QuestStatus.PRESTART) {
			for (BaseQuestAction bqa : preActions) {
				bqa.perform();
			}

			status = QuestStatus.RUNNING;
		}

		if (!checkReq())
			return false;

		// quest is fullfilled
		for (BaseQuestAction bqa : actions) {
			bqa.perform();
		}

		status = QuestStatus.FINISH;
		return true;
	}

	/**
	 * Check the req
	 * 
	 * @return true, will pass
	 */
	public boolean checkReq() {
		// ask the items
		for (IQuestRequirement r : req) {
			if (!r.checkReq()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return (status == QuestStatus.FINISH ? "(v) " : "") + (title == null ? req.get(0).getDesc() : title);
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	public VisTable getInfoPanel() {

		YTable table = new YTable();
		if (showDetail && desc != null) {
			table.addH("Description");

		}
		if (desc != null) {
			VisLabel l = new VisLabel(desc);
			l.setWrap(true);
			table.add(l).fill().colspan(2).row();
		}
		if (showDetail) {
			if (req.size() > 0) {
				table.addH("Tasks");
				for (IQuestRequirement r : req) {
					table.add(r.getDesc()).fill().colspan(2).row();
				}
			}
			if (actions.size() > 0) {
				table.addH("Actions");
				for (BaseQuestAction a : actions) {
					table.add(a.getTitle()).fill().colspan(2).row();
				}
			}
		}

		return table;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		if (desc == null) {
			// ask the req
			StringBuilder b = new StringBuilder();
			for (IQuestRequirement r : req) {
				if (b.length() > 0)
					b.append("/n");
				b.append(r.getDesc());
			}
			return b.toString();
		}
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the req
	 */
	public BaseQuestItem addReq(IQuestRequirement e) {
		req.add(e);
		return this;
	}

	/**
	 * @return the actions
	 */
	public BaseQuestItem addAction(BaseQuestAction bqa) {
		actions.add(bqa);
		return this;
	}

	/**
	 * @return the preActions
	 */
	public BaseQuestItem addPreAction(BaseQuestAction bqa) {
		preActions.add(bqa);
		return this;
	}

	/**
	 * @return the status
	 */
	public QuestStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(QuestStatus status) {
		this.status = status;
	}

	/**
	 * @return the icon
	 */
	public Drawable getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

	/**
	 * @param showDetail
	 *            the showDetail to set
	 */
	public void setShowDetail(boolean showDetail) {
		this.showDetail = showDetail;
	}

}
