package de.qossire.yaams.game.quest.req;

import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqMaxPref extends BaseQuestReqMaxCount {
	private String title, key;

	/**
	 * @param prop
	 * @param val
	 */
	public QuestReqMaxPref(int count, String key, String title) {
		super(count);
		this.title = title;
		this.key = key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getActCount()
	 */
	@Override
	public int getActCount() {
		return MapScreen.get().getData().existP(key) ? Integer.valueOf(MapScreen.get().getData().getPS(key)) : 0;
	}

}