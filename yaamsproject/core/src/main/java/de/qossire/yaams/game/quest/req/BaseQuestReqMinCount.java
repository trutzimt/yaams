/**
 * 
 */
package de.qossire.yaams.game.quest.req;

/**
 * @author sven
 *
 */
public abstract class BaseQuestReqMinCount implements IQuestRequirementCount {

	protected int count;

	/**
	 * @param count
	 * @param mapScreen
	 */
	public BaseQuestReqMinCount(int count) {
		super();
		this.count = count;
	}

	protected String getBaseDesc() {
		return "Have " + getActCount() + "/" + count + " ";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq() {
		return getActCount() >= count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getMaxCount()
	 */
	@Override
	public int getMaxCount() {
		return count;
	}

}
