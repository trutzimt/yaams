/**
 * 
 */
package de.qossire.yaams.base;

import java.util.HashMap;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;

import de.qossire.yaams.code.YHashMap;
import de.qossire.yaams.game.build.BuildManagement;
import de.qossire.yaams.level.BaseCampaign;
import de.qossire.yaams.music.YMusic;

/**
 * @author sven
 *
 */
public class YStatic {

	public static AssetManager systemAssets, gameAssets;
	public static YMusic music;
	@Deprecated
	public static HashMap<String, Sound> sounds;
	public static YHashMap<BaseCampaign> campaigns;
	public static BuildManagement buildings;

}
