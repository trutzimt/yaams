/**
 *
 */
package de.qossire.yaams.screens.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.LinkLabel;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YTabWindow;
import de.qossire.yaams.ui.actions.FeedbackAction;

/**
 * @author sven
 *
 */
public class CreditsWindow extends YTabWindow {

	/**
	 * @param title
	 */
	public CreditsWindow(final Yaams yaams) {
		super("Welcome to yaaMs V" + YConfig.VERSION);

		tabbedPane.add(new Tab(false, false) {

			@Override
			public String getTabTitle() {
				return "Overview";
			}

			@Override
			public Table getContentTable() {
				VisTable content = new VisTable();

				// add label
				VisLabel label = new VisLabel(getWelcomeMsg());
				label.setWrap(true);

				// Add scrollpane
				VisScrollPane pL = new VisScrollPane(label);
				pL.setScrollingDisabled(true, false);
				pL.setFadeScrollBars(false);
				pL.setScrollbarsOnTop(true);
				content.add(pL).colspan(2).grow().row();

				content.add(new FeedbackAction().getButton(yaams));
				content.add(new LinkLabel("Visit the homepage", "https://yaams.de"));

				return content;
			}
		});

		tabbedPane.add(new Tab(false, false) {

			@Override
			public String getTabTitle() {
				return "Details";
			}

			@Override
			public Table getContentTable() {
				FileHandle file = Gdx.files.internal("credits.txt");

				VisTable content = new VisTable();

				VisTable together = new VisTable();
				VisLabel label = new VisLabel(file.readString());
				label.setWrap(true);
				together.add(label).grow().row();

				if (yaams.getScreen() instanceof MainMenuScreen) {
					final VisCheckBox debug = new VisCheckBox("Enable Dev Mode", YSettings.isDebug());
					debug.addCaptureListener(new YChangeListener() {

						@Override
						public void changedY(Actor actor) {
							YSettings.getPref().putBoolean("dev", debug.isChecked());

						}
					});
					together.add(debug).growX();
				}

				VisScrollPane pL = new VisScrollPane(together);
				pL.setScrollingDisabled(true, false);
				pL.setFadeScrollBars(false);
				pL.setScrollbarsOnTop(true);
				content.add(pL).grow();

				return content;
			}
		});

		setHeight(Gdx.graphics.getHeight() / 2);
		setWidth(Gdx.graphics.getWidth() / 4 * 3);
		pack();

		buildIt();

	}

	public static String getWelcomeMsg() {
		return Gdx.files.internal("readme.txt").readString();
	}

}
