/**
 * 
 */
package de.qossire.yaams.game.museum.rank;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.qossire.yaams.game.museum.rank.RankMgmt.ERank;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;

/**
 * @author sven
 *
 */
public class RankSafety extends BaseRank {

	/**
	 * @param typ
	 * @param title
	 */
	public RankSafety() {
		super(ERank.SAFETY, "Safety");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getIcon()
	 */
	@Override
	public Drawable getIcon() {
		return YIcons.getIconD(YIType.SAFETY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#getDesc()
	 */
	@Override
	public String getDesc() {
		return "The security indicates how safe the items are presented or whether they can simply be stolen. " + notImplemented;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.museum.rang.BaseRang#checkLevel()
	 */
	@Override
	public int checkLevel() {
		// TODO
		return 0;

	}

}
