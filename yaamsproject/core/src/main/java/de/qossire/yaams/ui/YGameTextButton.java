/**
 * 
 */
package de.qossire.yaams.ui;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * @author sven
 *
 */
public abstract class YGameTextButton extends VisLabel {

	/**
	 * 
	 */
	public YGameTextButton(String title, String text, int key) {
		super(text);
		new Tooltip.Builder(title + " (" + Input.Keys.toString(key) + ")").target(this).build();

		// add key
		MapScreen.get().getInputs().keyBindings(key, new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				pressActionIntern();

			}
		});

		addListener(new InputListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#enter(com.badlogic.gdx.scenes.
			 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
			 */
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				addAction(Actions.color(Color.CYAN, 1f));
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#exit(com.badlogic.gdx.scenes.
			 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
			 */
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				addAction(Actions.color(Color.WHITE, 1f));
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#touchDown(com.badlogic.gdx.
			 * scenes.scene2d.InputEvent, float, float, int, int)
			 */
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				pressActionIntern();
				return false;
			}
		});
	}

	/**
	 * This Action will called, if something pressing, with try an catch
	 */
	protected void pressActionIntern() {
		try {
			pressAction();
		} catch (Exception e) {
			YConfig.error(e, false);
		}
	}

	/**
	 * This Action will called, if something pressing
	 */
	public abstract void pressAction();

}
