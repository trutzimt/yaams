package de.qossire.yaams.game.quest.req;

import java.util.LinkedList;

import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.screens.game.MapScreen;

/**
 * Count how much building properties fields exist with min val
 * 
 * @author sven
 *
 */
public class QuestReqTile extends BaseQuestReqMinCount {

	private int tile;
	private String desc;

	/**
	 * @param count
	 * @param tile
	 * @param typ
	 */
	public QuestReqTile(int count, int tile, String desc) {
		super(count);
		this.tile = tile;
		this.desc = desc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#getDesc()
	 */
	@Override
	public String getDesc() {
		return getBaseDesc() + desc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.quest.req.IQuestRequirementCount#getActCount()
	 */
	@Override
	public int getActCount() {
		LinkedList<YPoint> p = MapScreen.get().getData().getAllBuildTilePoints(tile);
		return p == null ? 0 : p.size();
	}

}