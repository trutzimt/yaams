package de.qossire.yaams.base;

import com.badlogic.gdx.scenes.scene2d.Stage;

public interface IScreen {

	/**
	 * @return the stage
	 */
	public Stage getStage();

	/**
	 * @return the yaams
	 */
	public Yaams getYaams();
}
