/**
 * 
 */
package de.qossire.yaams.saveload;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.ui.YChangeListener;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YSplitTab;

/**
 * @author sven
 *
 */
public class LoadTab extends YSplitTab {

	private VisTextButton delete, load;
	private IScreen screen;

	public LoadTab(IScreen screen) {
		super("Load Game", "At the moment you have no save games.");

		this.screen = screen;
		resetElements();

		// add button
		load = new VisTextButton("Please select a save game");
		load.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				doubleClickElement(null);

			}
		});
		buttonBar.addActor(load);

		delete = new VisTextButton("Delete it");
		delete.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				for (int i = 0; i < elements.size(); i++) {
					if (elements.get(i).getUserObject() == active) {
						SaveLoadManagement.delete(((YSaveInfo) active).getFile().nameWithoutExtension());
						elements.remove(i);
						break;
					}
				}
				reset();
				YSounds.click();
				rebuild();

			}
		});
		buttonBar.addActor(delete);

		// add warning
		VisImageTextButton v = new VisImageTextButton("Warning", YIcons.getIconD(YIType.WARNING));
		v.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				Dialogs.showOKDialog(LoadTab.this.screen.getStage(), "Warning!",
						"Support for saving is very rudimentary and prone to error.\n Only general information, the map and artworks are stored.");

			}
		});
		buttonBar.addActor(v);

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		resetElements();
		super.rebuild();
	}

	public void resetElements() {
		elements.clear();
		// add all elements
		Json j = new Json();
		// add saves
		for (FileHandle f : SaveLoadManagement.getSaves()) {
			YSaveInfo q = j.fromJson(YSaveInfo.class, f.read());
			if (q.isHidden())
				continue;

			q.setFile(f);
			// TODO better icon
			addElement(q.getTitle(screen), YIcons.getIconD(YIType.WARNING), q);

		}
	}

	/**
	 * Reset the status
	 */
	@Override
	protected void reset() {
		super.reset();
		delete.setDisabled(true);
		load.setText("Please select a save game");
	}

	/**
	 * Place the selected artwork
	 */
	@Override
	protected void doubleClickElement(Button btn) {

		if (SaveLoadManagement.load(((YSaveInfo) active).getFile(), screen.getYaams(), screen.getStage()))
			YSounds.click();
		else
			YSounds.buzzer();

	}

	@Override
	protected void clickElement(Button btn) {
		load.setText("Load " + ((YSaveInfo) btn.getUserObject()).getTitle(screen));
		delete.setDisabled(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(Button btn) {
		return ((YSaveInfo) btn.getUserObject()).getInfoPanel(screen);
	}
}