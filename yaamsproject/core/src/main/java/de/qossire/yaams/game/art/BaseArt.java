/**
 * 
 */
package de.qossire.yaams.game.art;

import java.util.LinkedList;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.widget.VisTable;

import de.qossire.yaams.base.IMgmtElement;
import de.qossire.yaams.code.YPoint;
import de.qossire.yaams.game.art.ArtworkMgmt.ArtTyp;
import de.qossire.yaams.game.build.BuildManagement.MapProp;
import de.qossire.yaams.game.build.requirement.IRequirement;
import de.qossire.yaams.generator.NamesGenerator;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.TextHelper;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YNotification;
import de.qossire.yaams.ui.YProgressBar;
import de.qossire.yaams.ui.YRandomField;
import de.qossire.yaams.ui.YTable;

/**
 * @author sven
 *
 */
public abstract class BaseArt implements IMgmtElement {

	public enum ArtStatus {
		DEPOT, NEW, DISPLAYED, WORKSHOP
	}

	protected ArtTyp typ;
	protected String name;
	protected int price, priceBase, priceDecorMulti, priceConditionMulti;
	protected int condition;
	protected int decor, decorBase;
	protected int age;
	protected int explore;
	protected int tid; // tileset id
	protected String epochid;
	protected int uid;
	protected YPoint pos;
	protected String artist;
	protected ArtStatus status;
	protected transient ArtTimeAction timeAction;

	protected static final String nE = "not explored";

	/**
	 * 
	 */
	public BaseArt(ArtTyp typ) {
		this.typ = typ;
		this.uid = -1;
		ArtEpoch epoch = MapScreen.get().getPlayer().getArtwork().getRandomEpoch();
		this.epochid = epoch.getID();

		explore = NamesGenerator.getIntBetween(0, 100);
		condition = NamesGenerator.getIntBetween(1, 100);
		age = NamesGenerator.getIntBetween(epoch.getMinAge(), epoch.getMaxAge());
		name = NamesGenerator.getArtWorkName();
		artist = NamesGenerator.getName(NamesGenerator.r.nextBoolean());

		decorBase = NamesGenerator.getIntBetween(1, 5);
		priceBase = NamesGenerator.getIntBetween(50, 2000);
		priceDecorMulti = NamesGenerator.getIntBetween(5, 15);
		priceConditionMulti = NamesGenerator.getIntBetween(10, 50);
		reCalcDecorPrice();

	}

	/**
	 * Recalc the prices
	 */
	public void reCalcDecorPrice() {
		decor = decorBase + condition / 10;
		decor = (decor * explore) / 100;
		decor = decor == 0 ? 1 : decor;
		price = priceBase + decor * priceDecorMulti + condition * priceConditionMulti;
		price = (price * explore) / 100;
		price = price <= 100 ? 100 : price;

	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	public VisTable getInfoPanel() {

		YTable table = new YTable();
		if (explore < 30)
			table.addL("Name", getName());
		else
			table.addL("Name", new YRandomField(getName()) {

				@Override
				protected void saveText(String text) {
					setName(text);

				}

				@Override
				protected String getRndText() {
					return NamesGenerator.getArtWorkName();
				}
			});
		table.addL("Artist", explore < 50 ? nE : artist);
		table.addL("Typ", explore < 5 ? nE : typ.toString().toLowerCase());
		table.addL("Epoch", explore < 20 ? nE : MapScreen.get().getPlayer().getArtwork().getArtEpoch(epochid).getTitle());
		table.addL("Age", explore < 25 ? nE : TextHelper.getYearString(age));
		table.addL("Decor", explore < 10 ? nE : decor + "");
		table.addL("Price", TextHelper.getMoneyString(price));
		if (explore > 25)
			table.addL("Condition", new YProgressBar(condition, 100, condition + "%"));
		else
			table.addL("Condition", nE);

		table.addL("Explored", new YProgressBar(explore, 100, explore + "%"));
		if (timeAction != null)
			table.addL(timeAction.getTitle(), timeAction.getProgress());

		return table;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#checkField(int,
	 * int, de.qossire.yaams.screens.map.MapScreen)
	 */
	public boolean checkField(int x, int y) {
		for (IRequirement req : MapScreen.get().getPlayer().getArtwork().getReq(typ)) {
			if (!req.checkField(x, y, MapScreen.get(), this))
				return false;
		}
		return true;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return explore < 30 ? nE : name;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @return the id
	 */
	public int getTId() {
		return tid;
	}

	/**
	 * @param tid
	 *            the id to set
	 */
	public void setTId(int tid) {
		this.tid = tid;
	}

	/**
	 * @return the typ
	 */
	public ArtTyp getTyp() {
		return typ;
	}

	/**
	 * @return the id
	 */
	public int getUId() {
		return uid;
	}

	/**
	 * @return the pos
	 */
	public YPoint getPos() {
		return pos;
	}

	/**
	 * @param pos
	 *            the pos to set
	 */
	public void setPos(YPoint pos) {
		this.pos = pos;
	}

	/**
	 * @param pos
	 *            the pos to set
	 */
	public void setPos(int x, int y) {
		this.pos = new YPoint(x, y);
	}

	/**
	 * @return the decor
	 */
	public int getDecor() {
		return decor;
	}

	/**
	 * Get the icon
	 * 
	 * @return
	 */
	public TextureRegionDrawable getIcon(boolean scale) {
		return YIcons.getArtIcon(tid, scale);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IMgmtElement#removeAt(int, int)
	 */
	@Override
	public void buildAt(int x, int y) {
		MapScreen map = MapScreen.get();
		// add Properties
		map.getData().addPropsRekursive(MapProp.DECOR, x, y, decor, false);

		// change lists
		setStatus(ArtStatus.DISPLAYED);

		// set it
		map.getData().addProps(MapProp.ARTID, x, y, uid);
		map.getData().setArtTile(tid, x, y);
		setPos(x, y);

		afterLoad();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.saveload.ISave#afterLoad()
	 */
	public void afterLoad() {
		// set viewpoints
		MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX() - 1, pos.getY(), this);
		MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX(), pos.getY() - 1, this);
		MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX() + 1, pos.getY(), this);
		MapScreen.get().getPlayer().getArtwork().addVisibleArtAt(pos.getX(), pos.getY() + 1, this);
	}

	/**
	 * @param condition
	 *            the condition to set
	 */
	public void addCondition(int condition) {
		this.condition += condition;
		reCalcDecorPrice();
		// to old?
		if (this.condition <= 0) {
			MapScreen.get().getPlayer().getArtwork().removeArt(this);
			// inform user
			MapScreen.get().getMapgui()
					.addNotification(new YNotification(name, "Based on the poor condition is your art " + name + " destroyed", new Image(getIcon(true))));
		}
	}

	/**
	 * @return the status
	 */
	public ArtStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(ArtStatus status) {
		this.status = status;
	}

	/**
	 * @return the epochid
	 */
	public String getEpochid() {
		return epochid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.game.build.requirement.IRequirement#checkField(int,
	 * int, de.qossire.yaams.screens.map.MapScreen)
	 */
	public String getErrorMessage(int x, int y, MapScreen map) {
		for (IRequirement req : MapScreen.get().getPlayer().getArtwork().getReq(typ)) {
			if (!req.checkField(x, y, map, this))
				return req.getDesc(x, y, map, this);
		}
		return "You can build " + getName() + " here.";
	}

	/**
	 * @return the condition
	 */
	public int getCondition() {
		return condition;
	}

	/**
	 * @param condition
	 *            the condition to set
	 */
	public void setCondition(int condition) {
		this.condition = condition;
	}

	/**
	 * @return the timeAction
	 */
	public ArtTimeAction getTimeAction() {
		return timeAction;
	}

	/**
	 * @param timeAction
	 *            the timeAction to set
	 */
	public void setTimeAction(ArtTimeAction timeAction) {
		this.timeAction = timeAction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IMgmtElement#removeAt(int, int)
	 */
	@Override
	public void removeAt(int x, int y) {
		MapScreen map = MapScreen.get();

		// remove Properties
		map.getData().addPropsRekursive(MapProp.DECOR, pos.getX(), pos.getY(), -decor, false);

		// set it
		map.getData().removeArtTile(pos.getX(), pos.getY());
		setPos(-1, -1);

		// set viewpoints
		map.getPlayer().getArtwork().removeVisibleArtAt(pos.getX() - 1, pos.getY(), this);
		map.getPlayer().getArtwork().removeVisibleArtAt(pos.getX(), pos.getY() - 1, this);
		map.getPlayer().getArtwork().removeVisibleArtAt(pos.getX() + 1, pos.getY(), this);
		map.getPlayer().getArtwork().removeVisibleArtAt(pos.getX(), pos.getY() + 1, this);
	}

	/**
	 * Add the visiting points to the user list
	 * 
	 * @param points
	 */
	public void addCustomerGoalPoint(LinkedList<YPoint> points) {

		// add points
		points.add(pos.addYandClone(-1));
		points.add(pos.addYandClone(1));
		points.add(pos.addXandClone(-1));
		points.add(pos.addXandClone(1));
	}

	/**
	 * @return the explore
	 */
	public int getExplore() {
		return explore;
	}

	/**
	 * @param explore
	 *            the explore to set
	 */
	public void setExplore(int explore) {
		this.explore = explore;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		if (this.uid != -1)
			throw new IllegalArgumentException("Can not modifizier uid to " + uid);
		this.uid = uid;
	}

}
