package de.qossire.yaams.screens.base;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.ButtonBar;
import com.kotcrab.vis.ui.widget.ButtonBar.ButtonType;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextArea;
import com.kotcrab.vis.ui.widget.VisTextButton;

import java.io.PrintWriter;
import java.io.StringWriter;

import de.qossire.yaams.base.YConfig;
import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.screens.game.MapScreen;

public class ErrorScreen implements Screen {

	protected Throwable e;
	protected Stage stage;
	protected Screen oldScreen;

	/**
	 * @param e
	 */
	public ErrorScreen(Throwable e, Screen oldScreen) {
		super();
		this.e = e;
		this.oldScreen = oldScreen;
		e.printStackTrace();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {

		stage = new Stage();

		Object version = "??", scene = "??", osv = "??", os = "??", java = "??";
		try {
			version = YConfig.VERSION;
			scene = oldScreen == null ? scene : oldScreen.getClass().getSimpleName();
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get Scene name", e1);
		}

		try {
			if (Gdx.app.getType() == ApplicationType.Desktop) {
				osv = System.getProperties().getProperty("os.version", "" + Gdx.app.getVersion());
				os = System.getProperties().getProperty("os.name", Gdx.app.getType().toString());
			} else {
				osv = Gdx.app.getVersion();
				os = Gdx.app.getType();
			}
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get OS", e1);
		}

		try {
			java = System.getProperties().getProperty("java.version", "?");
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get java", e1);
		}

		// generate string
		StringWriter sw = new StringWriter();
		sw.write("Version:" + version + ", Scene:" + scene + ", OS:" + os + " " + osv + ", Java:" + java + "\n");
		e.printStackTrace(new PrintWriter(sw));

		// Reset Gui
		if (VisUI.isLoaded()) {
			VisUI.dispose();
		}
		VisUI.load();

		// Generate Gui
		VisTable t = new VisTable();
		t.setFillParent(true);
		t.add(new VisLabel("yaaMs crashed. To correct the error, please send a message with")).row();
		t.add(new VisLabel("this screen to https://yaams.de/feedback/ Thank you.")).row();

		// build scrollpane
		VisScrollPane v = new VisScrollPane(new VisTextArea(sw.toString()));
		v.setScrollingDisabled(true, false);
		v.setFadeScrollBars(false);
		v.setFlickScroll(false);
		t.add(v).grow().row();

		ButtonBar buttonBar = new ButtonBar();

		VisTextButton b = new VisTextButton("Exit Game");
		b.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.exit();
			}
		});
		buttonBar.setButton(ButtonType.LEFT, b);

		b = new VisTextButton("Send Issue");
		b.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.net.openURI("https://yaams.de/feedback/");
			}
		});
		buttonBar.setButton(ButtonType.RIGHT, b);

		t.add(buttonBar.createTable());

		stage.addActor(t);
		Gdx.input.setInputProcessor(stage);

		//stop everything
        try{
            if (MapScreen.get()!=null)
            MapScreen.get().stop();
            YStatic.music.stop();
        } catch (Throwable x){

        }

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.9f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().setScreenSize(width, height);
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {}

}
