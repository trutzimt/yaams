/**
 *
 */
package de.qossire.yaams.ui.actions;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.qossire.yaams.base.IScreen;
import de.qossire.yaams.base.Yaams;
import de.qossire.yaams.music.YSounds;
import de.qossire.yaams.screens.menu.CreditsWindow;
import de.qossire.yaams.ui.YChangeListener;

/**
 * @author sven
 *
 */
public class AboutAction extends QDefaultAction {

	public AboutAction() {
		super("About yaaMs");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.ui.actions.QDefaultAction#getAction()
	 */
	@Override
	public YChangeListener getAction(final Yaams yaams) {
		return new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				YSounds.click();
				Stage stage = ((IScreen) yaams.getScreen()).getStage();
				stage.addActor(new CreditsWindow(yaams));

			}
		};
	}

}