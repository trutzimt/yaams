package de.qossire.yaams.music;

import com.badlogic.gdx.audio.Sound;

import de.qossire.yaams.base.YSettings;
import de.qossire.yaams.base.YStatic;

public class YSounds {

	/**
	 * Return a sound
	 *
	 * @param name
	 * @return
	 */
	private static Sound get(String name) {
		return YStatic.systemAssets.get("system/sound/" + name + ".mp3", Sound.class);
	}

	/**
	 * Play the click sound
	 */
	public static void play(String name, float vol) {
		if (YSettings.getSoundVolume() > 0) {
			get(name).play(YSettings.getSoundVolume() * vol);
		}
	}

	/**
	 * Play the click sound
	 */
	public static void play(String name) {
		play(name, 1);
	}

	/**
	 * Play the click sound
	 */
	public static void click() {
		play("Cursor");
	}

	/**
	 * Play the click sound
	 */
	public static void camera() {
		play("72714__jankoehl__shutter-photo", 1.25f);
	}

	/**
	 * Play the click sound
	 */
	public static void speechClosing() {
		play("speechClosing", 1.5f);
	}

	/**
	 * Play the click sound
	 */
	public static void book() {
		play("Book2");
	}

	/**
	 * Play the click sound
	 */
	public static void random() {
		play("010-System10");
	}

	/**
	 * Play the click sound
	 */
	public static void buzzer() {
		play("004-System04");
	}

	/**
	 * Play the click sound
	 */
	public static void cancel() {
		play("003-System03");
	}

	/**
	 * Play the click sound
	 */
	public static void buy() {
		play("006-System06");
	}

	/**
	 * Play the click sound
	 */
	public static void nextDay() {
		play("Item");

	}

}
