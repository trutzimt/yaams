package de.qossire.yaams.game.persons;

import de.qossire.yaams.game.persons.customer.CustomerTab;
import de.qossire.yaams.ui.YIcons;
import de.qossire.yaams.ui.YIcons.YIType;
import de.qossire.yaams.ui.YTabWindow;

public class PersonsWindow extends YTabWindow {

	/**
	 * 
	 * @param map
	 */
	public PersonsWindow() {
		super("Persons");

		tabbedPane.add(new CustomerTab());

		buildIt();

		addTitleIcon(YIcons.getIconI(YIType.PERSONS));
	}

}
