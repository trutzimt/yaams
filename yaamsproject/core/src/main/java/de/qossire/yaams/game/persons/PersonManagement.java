/**
 * 
 */
package de.qossire.yaams.game.persons;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import de.qossire.yaams.base.YStatic;
import de.qossire.yaams.game.persons.customer.Customer;
import de.qossire.yaams.game.persons.needs.BaseNeed;
import de.qossire.yaams.game.persons.needs.NeedDrink;
import de.qossire.yaams.game.persons.needs.NeedFood;
import de.qossire.yaams.game.persons.needs.NeedJoy;
import de.qossire.yaams.game.persons.needs.NeedLeave;
import de.qossire.yaams.game.persons.needs.NeedTicket;
import de.qossire.yaams.game.persons.needs.NeedToilet;
import de.qossire.yaams.level.ScenarioSettings.ScenConf;
import de.qossire.yaams.screens.game.MapScreen;
import de.qossire.yaams.ui.YAnimationActor;

/**
 * @author sven
 *
 */
public class PersonManagement {

	public enum PersonNeeds {
		TICKET, JOY, LEAVE, TOILET, DRINK, FOOD;
	}

	public enum PersonDir {
		SOUTH, WEST, EAST, NORTH;
	}

	public class PersonAnimationNeeds {
		public static final int ANGER = 0, DOTDOT = 1, DOUBLENOTE = 2, DROPLET = 3, EXCLAMATION = 4, HEART = 5, LIGHTBULB = 6, QUESTION = 7, SINGLENOTE = 8,
				SLEEPING = 9, SUN = 10;
	}

	private HashMap<PersonNeeds, BaseNeed> needs;

	private Animation<TextureRegion>[] personAnimationNeeds;
	private HashMap<String, Animation<TextureRegion>[]> characters;

	/** 
	 * 
	 */
	public PersonManagement() {
		needs = new HashMap<>();
		needs.put(PersonNeeds.JOY, new NeedJoy());
		if (MapScreen.get().getSettings().isActive(ScenConf.MONEY))
			needs.put(PersonNeeds.TICKET, new NeedTicket());
		needs.put(PersonNeeds.LEAVE, new NeedLeave());
		if (MapScreen.get().getSettings().isActive(ScenConf.TOILET))
			needs.put(PersonNeeds.TOILET, new NeedToilet());
		if (MapScreen.get().getSettings().isActive(ScenConf.DRINK))
			needs.put(PersonNeeds.DRINK, new NeedDrink());
		if (MapScreen.get().getSettings().isActive(ScenConf.FOOD))
			needs.put(PersonNeeds.FOOD, new NeedFood());

		createPersonNeeds();

		characters = new HashMap<>();

	}

	/**
	 * Return the direction to show for this person
	 * 
	 * @param file
	 * @param dir
	 * @return
	 */
	public Animation<TextureRegion> getChar(String file, PersonDir dir) {
		// not exist?
		if (!characters.containsKey(file)) {
			// Load the sprite sheet as a Texture
			Texture ani = YStatic.gameAssets.get("system/character/" + file + ".png", Texture.class);

			// Use the split utility method to create a 2D array of TextureRegions. This is
			// possible because this sprite sheet contains frames of equal size and they are
			// all aligned.
			TextureRegion[][] tmp = TextureRegion.split(ani, ani.getWidth() / 3, ani.getHeight() / 4);
			@SuppressWarnings("unchecked")
			Animation<TextureRegion>[] personAnimationNeeds = new Animation[4];

			for (int j = 0; j < 4; j++) {
				// convert
				TextureRegion[] tmp2 = new TextureRegion[4];
				tmp2[0] = tmp[j][0];
				tmp2[1] = tmp[j][1];
				tmp2[2] = tmp[j][2];
				tmp2[3] = tmp[j][1];

				// Initialize the Animation with the frame interval and array of frames
				personAnimationNeeds[j] = new Animation<TextureRegion>(0.4f, tmp2); // TODO work with time speed
			}

			characters.put(file, personAnimationNeeds);
		}

		return characters.get(file)[dir.ordinal()];
	}

	@SuppressWarnings("unchecked")
	private void createPersonNeeds() {
		// Load the sprite sheet as a Texture

		// Use the split utility method to create a 2D array of TextureRegions. This is
		// possible because this sprite sheet contains frames of equal size and they are
		// all aligned.

		personAnimationNeeds = new Animation[11];

		for (int j = 0; j <= 10; j++) {
			Texture ani = YStatic.gameAssets.get("system/emotion/" + j + ".png", Texture.class);
			TextureRegion[][] tmp = TextureRegion.split(ani, ani.getWidth() / 4, ani.getHeight() / 6);
			// create one line
			TextureRegion[] tmp2 = new TextureRegion[24];
			for (int i = 0; i < 24; i++) {
				// YLog.log(i, i / 4, tmp.length);
				tmp2[i] = tmp[i / 4][i % 4];
			}

			// Initialize the Animation with the frame interval and array of frames
			personAnimationNeeds[j] = new Animation<TextureRegion>(0.1f, tmp2); // TODO work with time speed
		}
	}

	/**
	 * Return the need class
	 * 
	 * @param need
	 * @return
	 */
	public BaseNeed getNeed(PersonNeeds need) {
		return needs.get(need);
	}

	/**
	 * Show for this customer the need
	 * 
	 * @param c
	 * @param need
	 */
	public void showNeed(Customer c, int need) {
		// check need
		if (need < 0 || need >= 11)
			new IllegalArgumentException("Unknown animation need " + need);

		YAnimationActor animation = new YAnimationActor(personAnimationNeeds[need]);
		animation.setPosition(c.getGameX() * 32, c.getGameY() * 32 + 24); // TODO find better way
		animation.addAction(Actions.sequence(Actions.fadeIn(2.4f / MapScreen.get().getClock().getTimeSpeedSafe()), Actions.removeActor()));

		MapScreen.get().getMap().getStage().addActor(animation);
	}

	/**
	 * Show for this customer the need
	 * 
	 * @param c
	 * @param need
	 */
	public String getNeedsText(PersonNeeds p, int value) {
		String mess;
		if (value > 600) {
			mess = "totally ";
		} else if (value > 300) {
			mess = "very ";
		} else if (value > 0) {
			mess = "";
		} else if (value > -300) {
			mess = "a little ";
		} else if (value > -600) {
			mess = "not ";
		} else {
			mess = "totally not ";
		}

		switch (p) {
		case JOY:
			return mess + "happy";
		case LEAVE:
			return value == 1000 ? "is leaving" : "will " + mess + "leaving";
		case TICKET:
			return value == 1000 ? "has a ticket" : "will buy one";
		case TOILET:
			return "need's  " + mess + "a toilet";
		case DRINK:
			return mess + "thirsty";
		case FOOD:
			return mess + "hungry";
		}

		return "?";
	}
}
